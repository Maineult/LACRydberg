# -*- coding: utf-8 -*-
"""
Created on Tue Nov 08 18:09:17 2016

@author: lac
"""

from PyQt4 import QtCore as C
from PyQt4 import QtGui as G
import sys
import CameraLib as cam
import numpy as np
from time import sleep



Cam=cam.Camera()

def window():
    for i in range(10):
        sleep(1)
        Img=Cam.ReadExtTrigImage()
        height,width=Img.shape
        bytesPerLine=width
        qImg = G.QImage(Img.data, width, height, bytesPerLine, G.QImage.Format_Indexed8)
        if i==0:
            app = G.QApplication(sys.argv)
            win = G.QWidget()
            l1 = G.QLabel()
            l1.setPixmap(G.QPixmap(qImg))
        	
            vbox = G.QVBoxLayout()
            vbox.addWidget(l1)
            win.setLayout(vbox)
            win.setWindowTitle("QPixmap Demo")
            win.show()
            app.exec_()
        else:
            print 'ok'
            l1.setPixmap(G.QPixmap(qImg))
            
   
window()   