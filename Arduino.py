# -*- coding: utf-8 -*-
"""
Created on Fri Dec 04 16:49:02 2015

@author: wmaineult
"""

import numpy as np
from pyfirmata import util
import pyfirmata as A
from ArduinoConstants import *
import time
from ExperimentController import InstrError
from ExperimentConstants import *

'''
To talk to the Spellman Alims, we use an Arduino with the Firmata 2.5 plus sketch downloaded on
the board. Arduino talks using serial protocol to the Spellman, Here on hardware Serial 1
(should work on Leonardo/Micro/Due/Mega boards at least). Maybe one has to check on the Firmata.c or .h files
that the Hardware Serial protocol is correctly defined in there.
01/02/2016 Fix : Need to remove the system reset callback from Firmata 2.5 protocol to have serial communication
work... Has to look for registers to see what's the problem if needed but I submitted the bug
Message from computer to Spellman are sent using the Sysex protocol. (2 *7bits to make a complete bit)
Has to check this works correcty
On the board side, the Sysex message are decoded and sent to the board. The board is supposed to listen to 
the HW1 port thanks to the ReadMessage() function, which will relay replys from Arduino to
the computer, which are intecepted using the cmd handler SERIAL Message.
'''
"""
 * Has to modify some files of the standard arduino library to have 
 * PWM working on all ports with arbitrary resolution.
 * All the modified files of the library are inside the directory of this sketch
 * and the modifications have to be implemented in the current arduino due library.
 *  - Clock is now the max clock frequency (84 MHz) (modified in
 * the wiring_analog.c of the Arduino Due library ) 
 *  - added constant definition in firmata.h : 
 * #define SET_RESOLUTION          0xE1  // set the resoltuion on the PWM pins
 * 
 * added function to firmata ino sketch
 * void setPinResolutionCallback(byte pin, int value)
{
    uint32_t chan = g_APinDescription[pin].ulPWMChannel;
    if (pinEnabled[pin]) {
      PWMC_SetPeriod(PWM_INTERFACE, chan, value);
      pinResolution[pin] = value;
      pmc_mck_set_prescaler(2);
    }else{
      Firmata.sendString("Enable requeested pin first")
    }  
}// end setPinResolution
 * 
 * and remplaced the pwm part of the analogwritecallback fucntion
 * 
      case PIN_MODE_PWM:
        if (IS_PIN_PWM(pin)){
          uint32_t chan = g_APinDescription[pin].ulPWMChannel;
          if (value<pinResolution[pin])
            PWMC_SetDutyCycle(PWM_INTERFACE, chan, value);
        }// end if
        pinState[pin] = value;
        break;
    }//End PIN_MODE PWM
 *       
 *       and added the         pinResolution[TOTAL_PIN] = 
 *       initialized to 255 at he same time as
 *       the PWM of the pins;
 *       
 *       Comment the deprecated #PWM definition from the firmata.h header (and all deprecated sysex function headers
 */
"""

"""
For the pyfirmata protocol, please consider updating the due description as shown below in boards.py
    'arduino_due': {
        'digital': tuple(x for x in range(54)),
        'analog': tuple(x for x in range(12)),
        'pwm': tuple(x for x in range(2, 14)+range(34,41)),
        'use_ports': True,
        'disabled': (0, 1)  # Rx, Tx, Crystal
    }
    
    And change the ANALOG_MESSAGE constant : 0xE0 to 0xA0 (Firmata.h and pyfirmata)
    
    Do not use the clock scaler factor from the pwm : it freezes out the communication with the arduino
    
"""
class ArduinoCustom(A.ArduinoDue):
    def __init__(self,port='COM3',Simulate=False):
        self.Simulate = Simulate
        if Simulate:
            return
        super(ArduinoCustom, self).__init__(port,baudrate=57600,timeout=2)
        NewLog(None,self.firmware,2)
        NewLog(None,self.firmata_version,2)
        self.add_cmd_handler(SERIAL_MESSAGE,self._Handle_SpellmanResponse)
        self.add_cmd_handler(A.STRING_DATA,self._Handle_FirmataString)
        self.ConfigureSerial()
        self.SpellmanResponse = ''
        while self.bytes_available():
            self.iterate()
            
    def __test__(self):
        '''
        To do  : when correct protocol communication is implemented, ask 
        Spellmans their current applied voltage
        '''
        if self.Simulate:
            return True
        return self.sp.isOpen()        
    
    def ConfigureSerial(self):
        message = self.Bit_Or(SERIAL_CONFIG,HW_SERIAL1)
        message += util.to_two_bytes(9600) + bytearray([0])
        self.send_sysex(SERIAL_MESSAGE,message)  
        self.digital[19]._mode = A.INPUT
        time.sleep(0.1)
        while self.bytes_available():
            self.iterate()  
    
  
    
    def _Handle_SpellmanResponse(self,*data):
        data = list(data)
#        print 'Handled SpellmanResponse',data
        if len(data)>2:
            if data[1]==2 : 
                self.SpellmanResponse=''
                data.pop(1)
            if data[1]==10 : 
                print self.SpellmanResponse
            for i in data[1:-1] :
                if len(data)>2:
                    self.SpellmanResponse+=chr(i)

    def Bit_Or(self,a,b):
        return bytearray([a+b])
        
    def _to_seven_bits(self,data):
        #Should use firmata.util functions. Kept here for the time
        rep = bytearray(2)        
        if type(data)==type(1):
            try : 
                rep = A.util.to_two_bytes(data)
            except :
                raise ValueError('NonImplemented')      
        if type(data)==type(''):
            if len(data)>1:
                try : 
                    rep =A.util.str_to_two_byte_iter(data)
                except : 
                    raise ValueError('NonImplemented')       
            else : 
                
                rep[0]=ord(data)%128
                rep[1]=ord(data)/128
        return rep
                
    def _Handle_FirmataString(self,*message):
        #Use for debug purposes mainly
#        result = A.util.two_byte_iter_to_str(message)
        result = ''
        for i in np.arange((len(message)+1)/2):
            try :
                result+= chr(2**7*(message[2*i+1])+message[2*i])
            except :
                result += '?' + message[2*i]
        
        self.LeMessage = result
        print result
                
                
    def WriteMessage(self,message):
        m = (self.Bit_Or(SERIAL_WRITE,HW_SERIAL1))
        if type(message) == type('') :
            #for car in message :
                #m = m + self._to_seven_bits(car)    
            m = m+A.util.str_to_two_byte_iter(message)
            self.send_sysex(SERIAL_MESSAGE,m)
    
    def Query(self,message, timeout=1):
        self.WriteMessage(message)
        self.ReadMessage()
        t0 =time.time()
        used = 0
        while time.time()-t0<timeout:        
            while self.bytes_available():
                self.iterate()
                used = 1
        if used:
            self.StopReading()
            return
        self.StopReading()
        
    def ReadMessage(self):
        message = (self.Bit_Or(SERIAL_READ,HW_SERIAL1)
                + self._to_seven_bits(0x00))
        self.send_sysex(SERIAL_MESSAGE,message)
        
    def StopReading(self):
        message = (self.Bit_Or(SERIAL_READ,HW_SERIAL1)
                + self._to_seven_bits(0x01))
        self.send_sysex(SERIAL_MESSAGE,message)

    def _PrintFirmataStream(self):
        while self.bytes_available():
            self.iterate()


    def Serial1Close(self):
        message = (self.Bit_Or(SERIAL_READ,HW_SERIAL1)
                + self._to_seven_bits(SERIAL_CLOSE))
        self.WriteMessage(message) 
        self.digital[19].mode = A.OUTPUT
        
    def close(self):
        message = (chr(HISPELLMAN)+chr(ALL)+
                'EN0')
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(BYSPELLMAN))
        self.WriteMessage(message) 
        self.exit()
        


class HVAlim():
#    def __init__(self, **kwargs):
#        if kwargs.has_key('board'):
#            if type(kwargs['board'])==type(''):
#                raise TypeError('kwargs[board] = {} is type {}, not {}'.format(kwargs['board'].__name__,
#                                type(kwargs['board']),type(ArduinoCustom)))
#        else :
#            raise ValueError('\'board\' not in {} kwargs keys'.format(kwargs.keys()))
#        self.board = kwargs['board']

    def __init__(self,board,Simulate=False):
        self.Simulate = Simulate
        if Simulate:
            return
        self.board = board

    def __test__(self):
        '''
        To do  : when correct protocol communication is implemented, ask 
        Spellmans their current applied voltage
        '''
        if self.Simulate : 
            return True
        return self.board.sp.isOpen() and not self.board.digital[19].mode

    def WriteMCP(self,Volt):
        if self.Simulate:
            return
        V=Volt
        message = (chr(HISPELLMAN)+chr(MCP)+ chr(MCPTYPE)
                    +'V1='+str(int(V))+'.'+str(int(10*(V-int(V)))))
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(BYSPELLMAN))   
        self.board.Query(message)   
                 
        
    def WriteField(self,Volt):
        if self.Simulate:
            return        
        V=Volt
        message = (chr(HISPELLMAN)+chr(ION)+
                chr(IONTYPE)+'V1='+str(int(V))+'.'+str(10*(V-int(V))))
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(BYSPELLMAN))
        self.board.Query(message)
        
    def WritePhosphorus(self,Volt):
        if self.Simulate:
            return
        V=Volt
        message = (chr(HISPELLMAN)+chr(PHOS)+
                chr(PHOSTYPE)+'V1='+str(int(V))+'.'+str(int(10*(V-int(V)))))
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(BYSPELLMAN))         
        self.board.Query(message) 
        
    def CalcCheckSum(self,com):
        chk=sum(map(ord, com))
        a = (2**16-chk)%256
        b = chr((a&127)|64)
        return b
        
    def WriteOn(self):
        if self.Simulate:
            return
        message = (chr(HISPELLMAN)+chr(ALL)+
                'EN1')
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(BYSPELLMAN))
        self.board.WriteMessage(message) 

    def WriteOff(self):
        message = (chr(HISPELLMAN)+chr(ALL)+
                'EN0')
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(BYSPELLMAN))
        self.board.WriteMessage(message) 

    def _SetAdress(self,i):    
        message = (chr(HISPELLMAN)+chr(PHOS)+
                'ID='+chr(i))
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(BYSPELLMAN))
        self.board.Query(message)  
        
        
    def close(self):
        pass

            
class PWMPort(A.Pin):
    def __init__(self, board, pin_number,Simulate=False) :
        self.Simulate = Simulate
        if self.Simulate : 
            self.pin_number = pin_number
            return
        if not board.digital[pin_number].PWM_CAPABLE : 
            raise InstrError('Arduino pin number {0}'.format(pin_number),'understand how he can do some PWM')
        if board.digital[pin_number].mode is not A.OUTPUT : 
            raise A.PinAlreadyTakenError('PWM port {} already take'.format(pin_number))
        super(PWMPort, self).__init__(board,pin_number,type=A.DIGITAL, port=None)
        self.PWM_CAPABLE=True
        self.resolution = 255;
        self.mode = A.PWM;
        
    def __test__(self):
        if self.Simulate : 
            return True
        if self.board.firmware == u'FirmataPlusModified.ino':
            return self.board.sp.isOpen()    
        return False
        
    def WriteResolution(self,value):
        '''
            Set the resolution for the pins (between 4 et 14 bits)
        '''
        if self.Simulate:
            return
        if int(value) < 16 : 
            raise InstrError('User','set a non-laughable analog resolution. Go play lego' )
        elif int(value) > 2**14-1:
            raise ValueError('For the moment, Firmata Analogwrite protocol does not '+
                'Not support more than 14 bits numbers. Consider rewriting Analogwirte with an extra bit')
        else :
            self.resolution = int(value)
            msg = bytearray([SET_RESOLUTION,self.pin_number, value % 128, value >> 7])
            self.board.sp.write(msg)
    
    def write(self, value):
        """
        Output a voltage from the PWM pin
            
        """
        self.value = value      
        if self.mode is A.OUTPUT : 
            if self.port :
                self.port.write()
            else : 
                msg = bytearray([A.DIGITAL_MESSAGE, self.pin_number, value])
                self.board.sp.write(msg)
        elif self.mode is A.PWM : 
            value = int(round(value * self.resolution))   
            msg = bytearray([A.ANALOG_MESSAGE + self.pin_number, value % 128, value >> 7])
            self.board.sp.write(msg)
    
    def close(self):
        self.mode = 1

class DacPin(A.Pin):
    def __init__(self, board, pin_number):
        self.board=board
        self.board.dac[pin_number].mode=A.DAC
        self.pin_number=pin_number
    def __test__(self):
        return self.board.sp.isOpen()        
        
    def Write(self,Value):
        if Value> 0. and Value <1.: 
            self.board.dac[self.pin_number].write(Value)
        else:
            print 'Error: DacPin Value out of range'
            


        
        
class ForsterAlim(PWMPort):
    def __init__(self,board,pin_number=8,Simulate=False):
        super(ForsterAlim, self).__init__(board,pin_number,Simulate)
    
    def WriteVoltField(self,Volt):
        '''
        Writes analog voltage to Alim with preset resolution of 8/13 bits resolution (this module alone/the Experiment framework)
        The latter can be changed through the WriteResolution method
        '''               
        if self.Simulate:
            return
        self.write(Volt*1./VoltMaxForster)

class BertanAlim(DacPin):
    def __init__(self, board, pin_number=0):
        super(BertanAlim, self).__init__(board,pin_number)
    
    def WriteVoltage(self,Voltage):
        if Voltage> 0. and Voltage < 5000:
            value=8.760e-5*Voltage+0.01
            self.Write(value)
        else:
            print 'Error: Voltage value out of range'
            
                
                    
        
class MoglabsPiezo(DacPin):
    def __init__(self,board,pin_number=1):
        super(MoglabsPiezo, self).__init__(board,pin_number)
        self.WriteVoltage(2.)
        
    def WriteVoltage(self,Voltage):
        value=Voltage/3.30
        self.Write(value)
        

if __name__ == "__main__":
    if not gh() =='lac57-95':
        a = ArduinoCustom()
        v = ForsterAlim(a)
    elif gh()=='lac57-95':   
        a=ArduinoCustom('COM7')
    
    p=MoglabsPiezo(a)
    p.WriteVoltage(1.6)

        
    
        