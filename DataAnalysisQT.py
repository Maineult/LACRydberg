# coding: utf8
'''
author = Wilfried
'''

from __future__ import unicode_literals
import numpy, sys, os, csv

import numpy as np

from PyQt4.uic import loadUiType

import matplotlib.ticker as ticker
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)
from ExtendedGUIClasses import ExtendedQLabel, MyTableModel
from xml.etree import ElementTree

from lmfit import minimize, Parameters, Parameter, report_fit   
from PyQt4 import QtCore, QtGui

import MiscFunctions
import UserDefFunctions as udf
import UserDefFunctions_FitInitMathods as udf_ini
import Dict2xml

Ui_MainWindow, QMainWindow = loadUiType('DataAnalysisGUI.ui')
Ui_FitDialog, QDialog = loadUiType('FitDialog.ui')
Ui_NumberDialog, QDialog = loadUiType('getDoubleValue.ui')
Ui_ForsterDialog, QDialog = loadUiType('ForsterDialog.ui')

class DataAnalysis(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(DataAnalysis, self).__init__(parent)
        self.setupUi(self)
        self.actionClose.setShortcut("Ctrl+Q")
        self.actionOpen.setShortcut("Ctrl+N")
        self.actionLoad.setShortcut("Ctrl+O")
        self.actionSave.setShortcut("Ctrl+S")
        self.tabWidget.setCurrentIndex(0)
# Add the graph window        
        self.fig1 = Figure()
        self.canvas = FigureCanvas(self.fig1)
        self.GraphLayout.addWidget(self.canvas)
# Add the declaration for extended Widget classes
        self.DebugLabel = ExtendedQLabel(self.DebugLabelBase)    
        self.DebugLabel.setFixedHeight(self.DebugLabel.parentWidget().height())
        self.DebugLabel.setFixedWidth(self.DebugLabel.parentWidget().width())
        
        self.UpdateCustom = ExtendedQLabel(self.UpdateCustomBase)    
        self.UpdateCustom.setFixedHeight(self.UpdateCustom.parentWidget().height())
        self.UpdateCustom.setFixedWidth(self.UpdateCustom.parentWidget().width())
        self.UpdateCustom.setText(self.UpdateCustom.parentWidget().text())
        
        self.ResDict = {}
        self.temp=''
        self.connectActions()
        

    def connectActions(self):
        '''
        ### Defines the actions triggered when doing something on the GUI 
        '''
        self.actionClose.triggered.connect(QtGui.qApp.quit)
        self.connect(self.fitButton, QtCore.SIGNAL('clicked()'),self.FitRawDataCurves)
        self.connect(self.fitAllButton, QtCore.SIGNAL('clicked()'),self.FitAllCurves)        
        self.actionOpen.triggered.connect(self.ChooseDirectory)
        self.actionLoad.triggered.connect(self.LoadFromXml)
        self.actionSave.triggered.connect(self.Save2Xml)        
        
        self.connect(self.NumberBox, QtCore.SIGNAL("valueChanged(int)"), self.RefreshDrawing)
        self.connect(self.ref0Name, QtCore.SIGNAL('editingFinished()'),self.NewRef)
        self.connect(self.ref1Name, QtCore.SIGNAL('editingFinished()'),self.NewRef)
        self.connect(self.ref2Name, QtCore.SIGNAL('editingFinished()'),self.NewRef)        
        self.connect(self.ref3Name, QtCore.SIGNAL('editingFinished()'),self.NewRef)        
        self.connect(self.ref4Name, QtCore.SIGNAL('editingFinished()'),self.NewRef)        
        self.connect(self.ref0Box,QtCore.SIGNAL('activated(int)'),self.UpdateRef)
        self.connect(self.ref1Box,QtCore.SIGNAL('activated(int)'),self.UpdateRef)
        self.connect(self.ref2Box,QtCore.SIGNAL('activated(int)'),self.UpdateRef)
        self.connect(self.ref3Box,QtCore.SIGNAL('activated(int)'),self.UpdateRef)
        self.connect(self.ref4Box,QtCore.SIGNAL('activated(int)'),self.UpdateRef)
        self.connect(self.ResetAllButton, QtCore.SIGNAL('clicked()'),self.ResetAll)     
        self.connect(self.ResetandFitButton, QtCore.SIGNAL('clicked()'),self.ResetFilter)
        self.connect(self.FilterNoFitButton, QtCore.SIGNAL('clicked()'),self.FilterNoFit)
        self.connect(self.DensitySlider,QtCore.SIGNAL("valueChanged(int)"),self.PrintSlider)
        self.connect(self.ToleranceSlider,QtCore.SIGNAL("valueChanged(int)"),self.PrintSlider)
        self.connect(self.Sigma0Slider,QtCore.SIGNAL("valueChanged(int)"),self.PrintSlider)
        self.connect(self.DebugLabel,QtCore.SIGNAL('clicked()'),self.PrintDebug)
        self.connect(self.tabWidget,QtCore.SIGNAL('currentChanged(int)'),self.TabChanged)        
        self.connect(self.ProceedButton, QtCore.SIGNAL('clicked()'),self.Proceed)     
        self.connect(self.XAxisBox,QtCore.SIGNAL('activated(int)'),self.ShowResult)
        self.connect(self.YAxisBox,QtCore.SIGNAL('activated(int)'),self.ShowResult)
        self.connect(self.UpdateCustom,QtCore.SIGNAL('clicked()'),self.ShowResult)  
        self.connect(self.ShowTreeButton,QtCore.SIGNAL('clicked()'),self.ShowFullTree)  
        self.connect(self.DrawBox,QtCore.SIGNAL('activated(int)'),self.ShowExtraGraph)
        self.connect(self.DrawBox0,QtCore.SIGNAL('activated(int)'),self.ShowExtraGraph)
        self.connect(self.DrawBox1,QtCore.SIGNAL('activated(int)'),self.ShowExtraGraph)
        
    def PrintSlider(self,value):
        self.SliderLabelHint.setText(unicode(value))
       #self.DebugLabel.setGeometry(self.DebugLabel.parentWidget().geometry())

    def TabChanged(self,TabIndex):
        if TabIndex == 2 :
            self.UpdateDebugTab()
        if TabIndex ==1:
            self.Proceed()
            self.ShowExtraGraph()
            
    def addItems(self, parent, DictElem):
        for key in sorted(DictElem.keys()):
            child = DictElem[key]
            item = QtGui.QStandardItem(key)
            parent.appendRow(item)           
            if type(child) is type({}) or type(child) is type(self.ResDict):
                self.addItems(item,child)
            elif  type(child) is type([]) or type(child) is type(np.array([])) :
                if type(child[0]) is type([]) or type(child[0]) is type(np.array([])) :
#                    dim = QtGui.QStandardItem(ndim(child))
#                    child.appendRow[dim]
#                    for i in np.arange(child.shape[0]) :
#                        self.addItems(dim,child[i])
                    for i in np.arange(len(child)) :
                        self.addItems(item,{unicode(i):child[i]})
                elif type(child[0]) is type({}) or type(child[0]) is type(self.ResDict):
                    for i in np.arange(len(child)) :
                    ##if type(child[i]) is type({}) or type(child[i]) is type(self.ResDict):
                        self.addItems(item,child[i])
                else:
                    for i in np.arange(len(child)) :
                        value = QtGui.QStandardItem(unicode(child[i]))
                        item.appendRow(value)
            elif type(child) is type(np.ma.array([])):
                if child.ndim>1 :     
                    for i in np.arange(child.shape[0]):
                        self.addItems(item,{unicode(i):child[i]})
                elif child.ndim ==1:
                    for i in np.arange(child.shape[0]):
                        self.addItems(item,{unicode(i):child[i]})
                else :
                    value = QtGui.QStandardItem(unicode(child.value)+unicode(child.mask))
                    item.appendRow(value)
            
            elif type(child) is np.ma.core.MaskedConstant:
                value = QtGui.QStandardItem('Masked')
                item.appendRow(value)
            else :
                value = QtGui.QStandardItem(unicode(child))
                item.appendRow(value)
            
    
    def UpdateDebugTab(self) :
        Tableau = self.TablePrintBox.currentText()
        tablemodel = MyTableModel(eval(Tableau), self)
        tableview = QtGui.QTableView()
        tableview.setModel(tablemodel)
        self.LayoutTableView.addWidget(tableview)
        self.setLayout(self.LayoutTableView)
        
    def ShowFullTree(self) :
        temp = {'ResDict':self.ResDict}
        model =  QtGui.QStandardItemModel()
        self.addItems(model, temp)        
        self.ResDictView.setModel(model)
    
    def PrintDebug(self):
        print 'PrintDebug'
        if not len(self.DebugLabel.text()):
            texto=''
            for sub in self.ResDict['subdirs'] :
                #sub=self.ResDict['subdirs'][key]                
                dirname = os.path.split(sub)[-1]           
                texto += (dirname+unicode(self.ResDict[dirname]['FitRes']['FitError'])
                        +unicode(self.ResDict[dirname]['FitRes']['A'])+'\n')
            self.DebugLabel.setText(texto)
        else :
            self.DebugLabel.setText('') 
            
            
    def Proceed(self):
        'Averages datas, analyses crosstalks, and print results'
        print 'Proceed'
        try :
            print self.ResDict['maths']['DataFiltered'].shape,'(xx)'
        except :
            self.CalcAverage()
        temp0=0;
        for name in self.ResDict['Refs'].keys() :
            i= self.ResDict['DirOrder'].index(name)       
            try :
                dat = np.array([self.ResDict['maths']['DataFiltered'][i,:-3]])
                dat = dat/dat.sum()
                temp0=np.append(temp0,dat,axis=0)
                print name, dat.sum()
                print 'ratio', dat/temp0[0]
            except :
                temp0 = np.array([self.ResDict['maths']['DataFiltered'][i,:-3]])
                temp0 = temp0/temp0.sum()
                print name, temp0
        (nref, ngates) = temp0.shape
        #print 'nRefs = ', nref, ' ngates = ', ngates
        if not self.IonBox.value()-1 :      
            temp0 = np.delete(temp0,1,axis=1)
        PInv = np.linalg.pinv(temp0)

        for i in np.arange(nref) :
            print i, ' : ' , np.dot(temp0[i,:],PInv)
        
        #transforms the gates values to states values, correcting crosstalks 
        #data : Efield,
        for name in self.ResDict['DirOrder'] :
            i= self.ResDict['DirOrder'].index(name)
            if not name in self.ResDict['Refs'] :
                temp3 = self.ResDict['maths']['DataFiltered'][i,:-3] 
                if not self.IonBox.value()-1 :      
                    temp3 = np.delete(temp3,self.IonBox.value()
                        -int(self.IonBox.value()>self.BkgBox.value() and self.BkgBox.value()>-1))
                try : 
                    a = self.ResDict['maths']['DataFiltered'][i,-3:-1]
                    b =  self.ResDict['maths']['DataFiltered'][i,self.IonBox.value()]
                    flat = np.append(a,b) 
                    temp2 = np.insert(np.array([np.dot(temp3,PInv)]),0,flat)
                except :
                    a = self.ResDict['maths']['DataFiltered'][i,-3:-1]
                    temp2 = np.insert(np.array([np.dot(temp3,PInv)]),0,a)
                try :
                    temp4 = np.append(temp4,[temp2],axis=0)
                except :
                    temp4= np.array([temp2])
        self.ResDict['maths']['Result'] = temp4
        self.ShowResult()
            
        #extras = int(bool(self.IonBox.value()+1))+int(bool(self.IonBox.value()+1))
        
            
            
    def CalcAverage(self):
        'Takes the filtered data, and builds 2D array of filtered data'

        self.ResDict['DirOrder']=[]
        ncols =0
        for dir0 in self.ResDict['subdirs']:
            dirname = os.path.split(dir0)[-1]
            if not ncols :
                ncols = self.ResDict[dirname]['data'].shape[0]
            try :
                self.ResDict['DirOrder'].append(dirname)
                temp0=self.ResDict[dirname]['data'][1:,:].mean(axis=0)
                temp1=self.ResDict[dirname]['data'][1:,:].std(axis=0)
                try :
                    moy = np.append(moy,[temp0],axis=0)
                    dev = np.append(dev,[temp1],axis=0)                        
                except :
                    moy = np.array([temp0])        
                    dev = np.array([temp1]) 
                    #print 'moy', moy.shape
            except :
                QtGui.QMessageBox.information(None,"Error","Didn't load all data."+
                "Use Fit and Filter All function on Load Data tab"+
                "before analysing data ")
                return
        #print 'moy : ', moy
        print 'Taille des datas = ', moy.shape
        if not self.BkgBox.value()+1 :
         #Substracts background 
            ll = np.arange(moy.shape()[0])
            try:
                ll.remove(self.BkgBox.value())
            except ValueError:
                pass
            for i in ll :
                moy[i,:] = moy[i,:]-moy[self.BkgBox.value()]*(
                    eval('self.W'+unicode(i)+'Box').value()
                    /eval('self.W'+unicode(self.BkgBox.value())+'Box')).value()
        try :
            self.ResDict['maths'].keys()
        except :
            self.ResDict['maths']={}
        if not self.BkgBox.value()+1 : 
            print 'Remove Bkg columns'
            self.ResDict['maths']['DataFiltered']=np.delete(moy,self.BkgBox.value(),axis=1)
            self.ResDict['maths']['ErrorBar']=np.delete(moy,self.BkgBox.value(),axis=1)
        else :
            print 'No Remove'
            self.ResDict['maths']['DataFiltered']=moy
            self.ResDict['maths']['ErrorBar']=dev
        
    def LoadFromXml(self):
        '''
        Loads previously saved self.ResDict dictionnary from a xml file
        '''
        try : 
            dir0 = os.path.split(self.ResDict['subdirs'][0])[0]
        except : 
            dir0 = os.getcwd()
        
        dire = QtGui.QFileDialog.getOpenFileName(self, 'Open Processed Data', 
                dir0,"Xml Files (*.xml)")
        strtmp = unicode(dire)
        tmp=Dict2xml.ConvertXmlToDict(strtmp)
 
        try :
            self.ResDict = tmp['ResDict']        
            #print 'cles :',self.ResDict.keys()
        except : 
            QtGui.QMessageBox.information(None,"Error","invadid xml data file")
            return        
        j=-1
        for dir0 in self.ResDict['subdirs'] :
            dirname = os.path.split(dir0)[-1]
            if (dirname in self.ResDict.keys() and
                   'DataRaw' in self.ResDict[dirname].keys() ) :
                if j<=0:
                    j=self.ResDict.keys().index(dirname)
                dataraw=self.ResDict[dirname]['DataRaw']
                mask = np.array(self.ResDict[dirname]['mask']).astype(bool)
                self.ResDict[dirname]['data']=np.ma.masked_array(dataraw,mask=mask)
                del self.ResDict[dirname]['DataRaw']
                del self.ResDict[dirname]['mask']

        self.DensitySlider.setRange(self.ResDict['DensityMin'],self.ResDict['DensityMax'])   
        self.DensitySlider.setValue(self.ResDict['DensitySet'])
        self.DensitySlider.setSingleStep(0.005*(-self.ResDict['DensityMin']+self.ResDict['DensityMax']))
        self.DensitySlider.setPageStep(0.05*(-self.ResDict['DensityMin']+self.ResDict['DensityMax']))
        if 'SliderPosition' in self.ResDict :
            self.ToleranceSlider.setValue(self.ResDict['SliderPosition']['ToleranceSlider'])
            self.ToleranceSlider.setValue(self.ResDict['SliderPosition']['Sigma0Slider'])      

        
        if j>=0:        
            self.NumberBox.setValue(j)        
        
        try : 
            self.NewRef(True)
        except : 
            True
        try :     
            del self.ResDict['Sigma0Position']
        except :
            True
            
        
        
    def Save2Xml(self):
        '''
        Saves the content of self.ResDict dictionnary into a xml file
        '''
        
        dir0 =os.path.split( os.path.dirname(self.ResDict['subdirs'][0]))[0]
        try : 
            dirname = os.path.split(self.ResDict['subdirs'][0])[-1]            
            type(self.ResDict[dirname]['FitRes'])                      
        except : 
            QtGui.QMessageBox.information(None,"Error","No data processed")
            return        
        for dir0 in self.ResDict['subdirs'] :
            dirname = os.path.split(dir0)[-1]
            if (dirname in self.ResDict.keys() and
                   'data' in self.ResDict[dirname].keys() ) :
                self.ResDict[dirname]['DataRaw']=(self.ResDict[dirname]
                        ['data'].data)
                tmp=(self.ResDict[dirname]
                        ['data'].mask)
                self.ResDict[dirname]['mask']=tmp.astype(int)
                print self.ResDict[dirname]['mask']
                del self.ResDict[dirname]['data']
        self.ResDict['SliderPosition']={}
        self.ResDict['SliderPosition']['DensitySlider'] = self.DensitySlider.value()
        self.ResDict['SliderPosition']['ToleranceSlider'] = self.ToleranceSlider.value()
        self.ResDict['SliderPosition']['Sigma0Slider'] = self.Sigma0Slider.value()
        
        temp={'ResDict':self.ResDict}
        root = Dict2xml.ConvertDictToXml(temp)
        tree = ElementTree.ElementTree(root)
#        for elem in root.iter():
#            print(elem.text)            
#            elem.set('type', type(elem.text))
#            print(elem.attrib)            
        FileName = QtGui.QFileDialog.getSaveFileName(self, caption ='Save Processed Data', 
                directory = os.path.split(dir0)[0],filter="Xml Files (*.xml)")
        tree.write(FileName)
        
    def FitRawDataCurves(self,**kwargs):
        '''
        Method to load Data, fit Guaussian lineshape,
        and filter the data according to numbers extracted from the fit
        if global analysis button is not checked
        '''
        print 'FitRawDataCurves'
        i = self.NumberBox.value()
        dirname = os.path.split(self.ResDict['subdirs'][i])[-1]
        try :
            type(self.ResDict[dirname]['data'])
        except :
            self.LoadData() 
        
        if len(self.ResDict[dirname]['data'])==0:
            QtGui.QMessageBox.information(None,"Error","Incorrect data folder")
            return 
        try : 
            type(self.ResDict[dirname]['FitRes'])
        except:
            self.ResDict[dirname]['FitRes']={}   

        #res,params  = MiscFunctions.Fit1Courbe(data[:,0],data[:,-2],'Gauss')
        if 'initialGuess' in kwargs.keys(): 
            res,params  = MiscFunctions.Fit1Courbe(self.ResDict[dirname]['data'][:,0],
                    self.ResDict[dirname]['data'][:,-2],'Gauss',initialGuess = kwargs['initialGuess'])
        else:
            res,params  = MiscFunctions.Fit1Courbe(self.ResDict[dirname]['data'][:,0],
                    self.ResDict[dirname]['data'][:,-2],'Gauss')
                                
        temp = self.ResDict[dirname]['data'][:,0]
        if max([np.abs(params['x0']), np.abs(params['sigma0'])])>max(temp)-min(temp):
            self.ResDict[dirname]['FitRes']['FitError']=True
        else :
            self.ResDict[dirname]['FitRes']['FitError']=False

        try:
            for key in params:        
                self.ResDict[dirname]['FitRes'][key] = params[key].value
            if self.ResDict[dirname]['data'].shape[0]==9:
                self.ResDict[dirname]['RawField'] = self.ResDict[dirname]['data'][:,-1].mean()  
            if not self.ConstDens.isChecked() :
                print 'Mode Indep pour i = ', self.NumberBox.value()            
                td = self.DensitySlider.value()
                told = self.ToleranceSlider.value()
                tf = params['x0'].value
                tolf = abs(params['sigma0'].value)*self.Sigma0Slider.value()/100  
                self.FilterOne(td,told,tf,tolf)
        
        except:
            if res[0] == 'Fun':
                 QtGui.QMessageBox.information(None,"Error","Function not defined (or ckeck spell)")
                 return
            if res[0] == 'Ini':
                QtGui.QMessageBox.information(None,"Error","Give initialization parameters (or program auto initialization method in UserDefFunction_ini)")
                return
            print 'bizarre'
            return

    def FilterNoFit(self):
        ## Filtre les donnees sans fiter. Si un fit correct a été effectué avant, filtre les données en utilisant les 
    ## paramètres du fit. Sinon, filtre en fonction des curseurs de l'interface graphique
        print 'FilterNoFit'
        [xmin,xmax] = self.ax1f1.get_xlim()
        [ymin,ymax] = self.ax1f1.get_ylim()
        td = self.DensitySlider.value()
        i = self.NumberBox.value()
        dirname = os.path.split(self.ResDict['subdirs'][i])[-1] 
        try : 
            if not self.ResDict[dirname]['FitRes']['FitError']:
                tf = self.ResDict[dirname]['FitRes']['x0']
                tolf =  self.ResDict[dirname]['FitRes']['sigma0'] *self.Sigma0Slider.value()/100
                told =  self.ResDict[dirname]['FitRes']['A']*self.ToleranceSlider.value()/100
            else :
                tf = (xmin+xmax)*1./2
                tolf =  1.*(xmax-xmin)*self.Sigma0Slider.value()/100      
                told =  1.*(ymax-ymin)*self.ToleranceSlider.value()/100
        except :
                tf = (xmin+xmax)*1./2
                tolf =  1.*(xmax-xmin)*self.Sigma0Slider.value()/100  
                told =  1.*(ymax-ymin)*self.ToleranceSlider.value()/100

### A priori les lignes suivantes ne servent à rien   
#        if not 'FitRes' in self.ResDict[dirname].keys() :
#            x=self.ResDict[dirname]['data'][:,0]
#            y=self.ResDict[dirname]['data'][:,-2]
#            self.ResDict[dirname]['FitRes'] = udf_ini.Gauss_init(x,y)
        self.FilterOne(td,told,tf,tolf)

 
    def FilterOne(self,td,told,tf,tolf,dirname = ''):
        print 'FilterOne, temp = ', self.temp 
        # Filters for density td +/- told ans
        # and Ti:sa freq in tf+/- told
        if not len(dirname) :         
            i = self.NumberBox.value()
            dirname = os.path.split(self.ResDict['subdirs'][i])[-1]
        if ( not self.ResDict[dirname]['FitRes']['FitError']    
                or (self.ConstDens.isChecked() and self.temp=='FitAll')): 
            td = td + self.ResDict[dirname]['FitRes']['offset']  
            print 'FilterParam = [',td,told,tf,tolf,']'
            test = MiscFunctions.FilterData(self.ResDict[dirname]['data'],density=td,tol=told)
            test2 = MiscFunctions.FilterData(self.ResDict[dirname]['data'],f0=tf,tol=tolf)
        else :
            test = ''
            test2 = ''
        if test=='error' or test2=='error':
            QtGui.QMessageBox.information(None,"Error","Wrong Parameters passed to MiscFunctions.FilterData")
            return
        if test=='wrong data' or test2 == 'wrong data':
            QtGui.QMessageBox.information(None,"Error","Wrong Data : not a 2D array")
            return
        if not self.temp=='FitAll':
            self.RefreshDrawing(i)       
             
        
    def FitAllCurves(self):     
        ### Filter curves, should be offset independant if correctly implemented in the FilterOne program
        subdir = self.ResDict['subdirs']
        moy=0.
        nb = 0.
        self.temp = 'FitAll'
        for i in np.arange(len(self.ResDict['subdirs'])):
            self.NumberBox.setValue(i)
            dirname = os.path.split(subdir[i])[-1]
            mask = np.zeros(self.ResDict[dirname]['data'].shape,dtype=bool) 
            try : 
                self.ResDict[dirname]['data'].mask= mask
            except :
                self.ResDict[dirname]['data'] = np.ma.array(self.ResDict[dirname]['data'],mask)
            self.FitRawDataCurves()
            if not self.ResDict[dirname]['FitRes']['FitError']:
                print 'A(',i,') = ',self.ResDict[dirname]['FitRes']['A'], ' nb ',nb
                moy = moy+self.ResDict[dirname]['FitRes']['A']
                nb = nb+1
        if self.ConstDens.isChecked():
            moy = moy*1./nb
            self.DensitySlider.setValue(int(np.round(moy)))
            print 'Densité visée =', moy
            for i in  np.arange(len(self.ResDict['subdirs'])):
                dirname = os.path.split(subdir[i])[-1]
                told = self.ToleranceSlider.value()
                tf = self.ResDict[dirname]['FitRes']['x0']
                tolf = abs(self.ResDict[dirname]['FitRes']['sigma0'])*self.Sigma0Slider.value()/100  
                print 'Mode correle pour i = ', self.NumberBox.value()                
                self.FilterOne(moy,told,tf,tolf,dirname)
        self.temp = ''
        print 'i de la fin du Fit All',i
        texto=''
        for sub in self.ResDict['subdirs'] :
            dirname = os.path.split(sub)[-1]           
            texto += (dirname+unicode(self.ResDict[dirname]['FitRes']['FitError'])
                        +unicode(self.ResDict[dirname]['FitRes']['A'])+'\n')
        self.DebugLabel.setText(texto)
        self.NumberBox.setValue(i)
        self.RefreshDrawing(i) 
    
    def LoadData(self):
        '''
        Load Data from file
        '''
        i = self.NumberBox.value()
        subdir = self.ResDict['subdirs']
        dirname = os.path.split(subdir[i])[-1]
        try :
            type(self.ResDict[dirname])
        except :
            self.ResDict[dirname]={}
            
        try:
            type(self.ResDict[dirname]['data'])
        except :
            self.ResDict[dirname]['data']={}
        
        if len(subdir)<i+1:
            QtGui.QMessageBox.information(None,"Error","SubDirectory doesn't exists!")
            self.NumberBox.setValue(len(subdir)-1)
            return
        
        if not os.path.isfile(os.path.join(subdir[i],'scan000001.txt')):
            print 'repertoire ok ?', os.path.isdir(subdir[i])
            print 'contenu ', os.listdir(subdir[i])
            QtGui.QMessageBox.information(None,"Error","SubDirectory doens't contain valid data file")
            self.ResDict[dir]='.'
            return
        data = np.loadtxt(os.path.join(subdir[i],'scan000001.txt'),skiprows =0,delimiter = '\t')
        if data.shape[1] > 9:
            QtGui.QMessageBox.information(None,"Warning","Number of columns of data is strange")
        if data.shape[1] <9 :
            QtGui.QMessageBox.information(None,"Warning","Strange : will use col(1) = freq \n col(end) = field, col(end-1)=ntot")
        if data.shape[1]==9:
            moy = data[:,0].mean()
            data[:,0]=(data[:,0]-moy)/3e-4  #Converts to MHz cm-1
            
            try :
                self.ResDict['DensityMin'] = min(self.ResDict['DensityMin'],min(-data[:,-2]))
                self.ResDict['DensityMax'] = max(self.ResDict['DensityMax'],max(-data[:,-2]))
                self.DensitySlider.setRange(self.ResDict['DensityMin'],self.ResDict['DensityMax'])   
            except :
                self.ResDict['DensityMin']= min(-data[:,-2])
                self.ResDict['DensityMax']= max(-data[:,-2])
                self.ResDict['DensitySet']=(1.*(self.ResDict['DensityMax']-self.ResDict['DensityMin'])*
                        self.DensitySlider.value()/self.DensitySlider.maximum())  
                self.DensitySlider.setRange(self.ResDict['DensityMin'],self.ResDict['DensityMax'])   
                self.DensitySlider.setValue(self.ResDict['DensitySet'])
                self.DensitySlider.setSingleStep(0.005*(self.ResDict['DensityMax']-self.ResDict['DensityMin']))
                self.DensitySlider.setPageStep(0.05*(self.ResDict['DensityMax']-self.ResDict['DensityMin']))
             
            #print max(data[:,0]), min(data[:,0])
        self.ResDict[dirname]['data']=np.ma.array(-data,mask=np.zeros(data.shape,dtype=bool))    
        return       
    
    def ShowExtraGraph(self):
        '''
            Shows extra graphs, to better understand the datas
        '''
        if not self.DrawBox.currentIndex() :
            try : 
                self.DrawBox0.setVisible(0)
                self.DrawBox1.setVisible(0)
                self.DescriptionLabel0.setVisible(0)    
                self.DescriptionLabel1.setVisible(0)
                self.fig3.show(False)
                print 'fini'
                self.canvas3.destroy()
                print 'fini'
                self.axExtra.__delete__()
                print 'fini'
            except:
                pass
            return
       
        try : 
            self.canvas3.get_width_height()
            self.axExtra.clear()
        except :
            self.fig3 = Figure()
            self.canvas3 = FigureCanvas(self.fig3)
            self.ExtraGraphLayout.addWidget(self.canvas3)
            self.axExtra = self.fig3.add_axes([0.25, 0.15, 0.65, 0.75])
            self.fig3.set_facecolor((145./255,145./255,200./255))  
            
            
        ### Print alan Variance of filtered Data    
        if self.DrawBox.currentIndex()==1 :
            self.DrawBox0.setVisible(1)
            self.DescriptionLabel0.setVisible(1)
            self.DescriptionLabel0.setText('Choose set')
            if not self.DrawBox0.count():
                for dirname in self.ResDict['subdirs']:
                    a=os.path.split(dirname)[-1]
                    self.DrawBox0.addItem(a)
                self.DrawBox0.setCurrentIndex(0)
            dirname = self.DrawBox0.currentText()            
            y = MiscFunctions.AlanVar(self.ResDict[dirname]['data'],axis=0)
            try : 
                y.shape
            except :
                raise TypeError('AlanVarAnalysis not possible')
            x = MiscFunctions.xAlan(self.ResDict[dirname]['data'],axis=0)
            for i in np.arange(len(y))  : 
                try :
                    labellist.append('Column'+unicode(i))
                except :
                    labellist = ['Column'+unicode(i)]
            self.axExtra.set_yscale('log')
            self.axExtra.set_xscale('log')

        #### Prints Filtered data
        if self.DrawBox.currentIndex() in [2,3] :
            self.DrawBox0.setVisible(1)
            self.DescriptionLabel0.setVisible(1)
            self.DescriptionLabel0.setText('Choose set')
            if not self.DrawBox0.count():
                for dirname in self.ResDict['subdirs']:
                    a=os.path.split(dirname)[-1]
                    self.DrawBox0.addItem(a)
                self.DrawBox0.setCurrentIndex(0)
            dirname = self.DrawBox0.currentText()
            for i in np.arange(self.ResDict[dirname]['data'].shape[1]-3) :
                try : 
                    t = self.ResDict[dirname]['data'][:,i+1]
                    t=t.compressed()
                    if self.DrawBox.currentIndex()==2 :
                        t=t/np.abs(t.mean())
                    y= np.append(y,[t],axis=0)
                    labellist.append('Column'+unicode(i+1))
                except NameError : 
                    t = self.ResDict[dirname]['data'][:,i+1]
                    t=t.compressed()
                    if self.DrawBox.currentIndex()==2 :
                        y = [np.abs(t/t.mean())]
                    labellist = ['Column'+unicode(i+1)]

    
            
            self.axExtra.set_yscale('linear')
            self.axExtra.set_xscale('linear')
            x = np.arange(y.shape[1])



        colorlist =  sorted(QtGui.QColor.colorNames() , key=len)
        for i in np.arange(len(y)) :
            self.axExtra.plot(x,y[i,:], color=colorlist[i],marker = '+',label = labellist[i])
        self.axExtra.legend(fontsize='x-small',loc = 'upper right').draggable()
        
        #self.axExtra.set_ybound(y.min()/2,y.max()*2)
        self.canvas3.draw()      

            
                    
    def ShowResult(self):
        try : 
            self.axRes.clear()
        except :
            self.fig2 = Figure()
            self.canvas2 = FigureCanvas(self.fig2)
            self.ResultLayout.addWidget(self.canvas2)
            self.axRes = self.fig2.add_axes([0.25, 0.15, 0.65, 0.75])
            self.fig2.set_facecolor((145./255,145./255,200./255))
            self.toolbar2 = NavigationToolbar(self.canvas2, self)  
            self.ResultLayout.addWidget(self.toolbar2)

        if self.YAxisBox.currentIndex()== 0 :
            y = self.ResDict['maths']['Result'][:,2:-2]
            print self.ResDict['maths']['Result']
            print self.ResDict['maths']['Result'][:,1:-1]
            self.axRes.set_ylabel('signal (nV.s)')
        else :
            self.axRes.set_ylabel('Transfert Probability (/%)')
            for i in np.arange(2,len(self.ResDict['maths']['Result'][0])) :
                try :
                    y0 = np.array(self.ResDict['maths']['Result'][:,i]/
                            self.ResDict['maths']['Result'][:,0],ndmin=2).T
                    y = np.append(y,y0,axis=1)
                except :
                    y = np.array(self.ResDict['maths']['Result'][:,i]/
                            self.ResDict['maths']['Result'][:,0],ndmin=2).T
                        
        
        if self.XAxisBox.currentIndex() ==0 :
            x= self.ResDict['maths']['Result'][:,1]
            self.axRes.set_xlabel('Volt Supply Output')
        elif self.XAxisBox.currentIndex() == 1 :            
            ScalePrompt = NumberDialog(called='EField')
            ScalePrompt.exec_()
            x = self.ResDict['maths']['Result'][:,1]*(
                    self.ResDict['maths']['EFieldScale'])
            self.XAxisBox.setItemText(1,'Electric Field *'+unicode(
                    self.ResDict['maths']['EFieldScale']))
            self.axRes.set_xlabel('Approximate Field (V/cm)')
        elif self.XAxisBox.currentIndex() == 2:
            ForsterPrompt = ForsterDialog()
            ForsterPrompt.exec_()
            x = self.ResDict['maths']['FRes'][1,0]+ float(
                self.ResDict['maths']['FRes'][1,1]-self.ResDict['maths']['FRes'][1,0])*(
                self.ResDict['maths']['Result'][:,1]-self.ResDict['maths']['FRes'][0,0])/(
                self.ResDict['maths']['FRes'][0,1]-self.ResDict['maths']['FRes'][0,0])
            self.axRes.set_xlabel('Field (V/cm)')

#        else self.XAxixBox.currentIndex() == 3 :
#            CustomPrompt = CustomDialog()
#            CustumPrompt.exec_()
#            x = self.ResDict['maths']['CustomX']
#            self.axRes.set_xlabel(self.ResDict['maths']['CustomAxisName'])
            
        
        
        colorlist =  sorted(QtGui.QColor.colorNames() , key=len)
        if not int(bool(self.IonBox.value()-1)):      
            labellist = ['Ions']
        else :
            labellist = []
        
        for name in self.ResDict['Refs']  : 
            labellist.append(name)
        
        for i in np.arange(y.shape[1]) :
            self.axRes.scatter(x,y[:,i], color=colorlist[i],marker = '+',label = labellist[i]) 
            
        self.axRes.legend(fontsize='x-small',loc = 'upper left')
        self.canvas2.draw() 
        
    def RefreshDrawing(self,i,Refresh=True):
        #Takes total atom number from 1 data set et prints it on the screen
        
        dirname = os.path.split(self.ResDict['subdirs'][i])[-1]        
        try:
            type(self.ResDict[dirname]['data'])
        except:
            self.LoadData() 
        #i = self.NumberBox.value()
        self.DirName.setText(dirname)
        mask = np.array(self.ResDict[dirname]['data'].mask)
        self.FilterLabel.setText('Valid Data : '+ unicode(np.count_nonzero(~mask[:,0]))+
                        ' / '+ unicode(mask.shape[0]))
        self.ResDict[dirname]['data'].mask=np.zeros(mask.shape,dtype=bool)
        x = self.ResDict[dirname]['data'][:,0]
        y = self.ResDict[dirname]['data'][:,-2]     
        try :
            self.ax1f1.set_position([0.25, 0.15, 0.65, 0.75])   
            #print 'ax1f1 existe'
        except :
            print 'ax1f1 existe pas'
            self.ax1f1 = self.fig1.add_axes([0.25, 0.15, 0.65, 0.75])
            self.fig1.set_facecolor((145./255,145./255,200./255))
        
        if Refresh==True:
            self.ax1f1.clear()
        
        self.ax1f1.scatter(x,y, color='b',marker = '+',label = 'Raw')        
        self.ax1f1.set_ylabel('signal (nV.s)')
        self.ax1f1.set_xlabel('Detuning (MHz)')
        start,end = self.ax1f1.get_xlim()
        self.ax1f1.xaxis.set_ticks(np.linspace(start, end, 4))
        self.ax1f1.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))       

        
        
        try :
            params = self.ResDict[dirname]['FitRes']
            y = udf.Gauss(params,x)          
            self.ax1f1.plot(x,y,'r-',label = 'Fit')
        except:
            self.ax1f1.legend()
            self.canvas.draw() 
            print 'No fit',self.ax1f1.lines
            if ('FitRes' in self.ResDict[dirname] and
                    not self.ResDict[dirname]['FitRes']['FitError']):
                return    

        if (np.count_nonzero(mask)>0) :
            self.ResDict[dirname]['data'].mask=mask  
            x = self.ResDict[dirname]['data'][:,0]
            y = self.ResDict[dirname]['data'][:,-2] 
            self.ax1f1.scatter(x,y, color = 'm',marker = 'x',label = 'Filtered')
            self.ax1f1.legend(fontsize='x-small',loc = 'upper left')
        self.canvas.draw() 
        
    def ResetFilter(self,Fit=True) :
        print 'Reset Filter'                    
        i = self.NumberBox.value()    
        dirname = os.path.split(self.ResDict['subdirs'][i])[-1]
        self.ResDict[dirname]['data'].mask = np.zeros(self.ResDict[dirname]['data'].mask.shape,dtype=bool)
        if Fit :
            start,end = self.ax1f1.get_xlim()
            InitialGuess = {'x0':(start+end)*1./2,'sigma0':(start+end)*1./2}
            start,end = self.ax1f1.get_ylim()
            InitialGuess.update({'A':end,'offset':start})
            self.temp=InitialGuess            
            FitPrompt = FDialog()
            FitPrompt.exec_()
            self.temp = ''
            
    def ResetAll(self) :
        print 'Reset All'
        for dirname in self.ResDict['subdirs']:
            di = os.path.split(dirname)[-1]
            try :            
                self.ResDict[di]['data'].mask = np.zeros(self.ResDict[di]['data'].mask.shape,dtype=bool)
            except :
                print 'dirname :',di
        self.RefreshDrawing(self.NumberBox.value())
            
    def ChooseDirectory(self):
        #Parent directory selection of all data
        try:
            rep = self.ResDict['dir']
        except :
            self.ResDict['dir'] = 'c:\\Users\\lac\\Dropbox\\LAC\\Essai'
#            self.ResDict['dir']='c:\\Users\\M Squared\\Documents\\Essai' 
            rep = self.ResDict['dir']
            
        dire = QtGui.QFileDialog.getExistingDirectory(self, 'Directory of data', 
                rep)
        strtmp = unicode(dire)
        # Faudrait surement changer l'conding ici, mais c'est trop complique        
        #QTextCodec *codec = QTextCodec.codecForName(encode)
        #QString utf8dirname = codec->toUnicode(dirname)
        #self.dirLabel.setText(dirname)
        #print strtmp,type(strtmp) 
        
        
        if len(strtmp)==0:
            QtGui.QMessageBox.information(None,"Error","Choose Data Directory first")
            return
        
        subdirectories = next(os.walk(strtmp))[1]
        
        #print 'subdirs', subdirectories
        subdir =[ os.path.join(strtmp,s) for s in subdirectories]
        #print 'subdirs', subdir[0]
        subdir.sort()        
        self.ResDict['subdirs']=subdir       
        for i in np.arange(5):
            eval('self.ref'+unicode(i)+'Box.clear()')
        i=0 
        while bool(len(eval('self.ref'+unicode(i)+'Name.text()'))) and i<5:
            self.PrintDir(i)
            i=i+1            
        if os.path.exists(strtmp):
            i =self.NumberBox.value()
            self.RefreshDrawing(i,True)        

    def NewRef(self,LoadData=False):
        if not LoadData :
            self.ResDict['Refs']={}
            for j in np.arange(5) :
                if bool(len(eval('self.ref'+unicode(j)+'Name.text()'))) :
                    name = eval('self.ref'+unicode(j)+'Name.text()')
                    dir0 = eval('self.ref'+unicode(j)+'Box.currentText()')
                    self.ResDict['Refs'][name]=dir0
 
        j=0
        for key in self.ResDict['Refs'].keys() :
            eval('self.ref'+unicode(j)+'Name').setText(key)
            if not eval('self.ref'+unicode(j)+'Box.count()'):
                self.PrintDir(j)
                if LoadData :
                    n = eval('self.ref'+unicode(j)+'Box').findText(self.ResDict['Refs'][key]) 
                    eval('self.ref'+unicode(j)+'Box').setCurrentIndex(n)      
            else :                  
#            elif len(self.ResDict['Refs'][key]):
                n = eval('self.ref'+unicode(j)+'Box').findText(self.ResDict['Refs'][key]) 
                eval('self.ref'+unicode(j)+'Box').setCurrentIndex(n)        
            j=j+1
        for i in np.arange(j,5):
            eval('self.ref'+unicode(i)+'Name.clear()')
            eval('self.ref'+unicode(i)+'Box.clear()')
            
        self.UpdateRef()
        #print 'liste des refs = ',self.ResDict['Refs'].keys()      
        #print 'liste des items = ',self.ResDict['Refs'].items()                   


    def UpdateRef(self):
        i=0 
        print 'UpdateRef'
        selected = []
        keys = []
        
        while i<5 and bool(len(eval('self.ref'+unicode(i)+'Name.text()'))):
            keys.append(eval('self.ref'+unicode(i)+'Name.text()'))
            selected.append(eval('self.ref'+unicode(i)+'Box').currentText())  
            self.ResDict['Refs'][keys[i]]=selected[i]
            i=i+1
            
        listsub = self.ResDict['subdirs'][:]
        for k in np.arange(len(listsub)):
            listsub[k] = os.path.split(listsub[k])[-1] 
    
        for j in np.arange(0,i):
            eval('self.ref'+unicode(j)+'Box').clear()
            for dirname in listsub:
                a=os.path.split(dirname)[-1]
                eval('self.ref'+unicode(j)+'Box').addItem(a) 
            mask = np.zeros(i,'Bool')
            mask[j]='True'
            for k in np.ma.array(np.arange(i),mask = mask).compressed():
                n = eval('self.ref'+unicode(j)+'Box').findText(selected[k]) 
                eval('self.ref'+unicode(j)+'Box').removeItem(n)
            n = eval('self.ref'+unicode(j)+'Box').findText(selected[j]) 
            eval('self.ref'+unicode(j)+'Box').setCurrentIndex(n)
            
    def PrintDir(self,i) : 
        # Prints the list of subdirectories in the combo Box
        # The default behaviour is to have the 1sts subdir retaining reference levels
        print 'printdir'
        listsub = self.ResDict['subdirs'][:]
        for k in np.arange(len(listsub)):
            listsub[k] = os.path.split(listsub[k])[-1]
        mask = np.zeros(5,'Bool')
        mask[i]='True'
        try:
            for j in np.ma.array(np.arange(5),mask = mask).compressed():
                strtmp = eval('self.ref'+unicode(j)+'Box.currentText()')
                if len(strtmp):
                    listsub.remove(strtmp)
        except :
#            for j in np.ma.array(np.arange(5),mask = mask).compressed():
#                temp = eval('self.ref'+unicode(j)+'Box').currentText()                
#                print 'entree',temp,'type', type(temp),'len(strtmp)',len(temp)
#                if len(temp):
#                    print listsub[0:4]
#                    listsub.remove(temp)
            if len(listsub)<i:
                QtGui.QMessageBox.information(None,"Error","Check directory of data, looks,strange")
        
        for dirname in listsub:
            a=os.path.split(dirname)[-1]
            eval('self.ref'+unicode(i)+'Box').addItem(a)
        #eval('self.ref'+unicode(i)+'Box').addItem(None)  
        #self.UpdateRef()    
            
    def main(self):
        self.show()

class NumberDialog(QDialog,Ui_NumberDialog) :
    def __init__(self,parent=None,called = 'EField'):
        super(NumberDialog,self).__init__(parent)
        self.setupUi(self)
        self.called = called        
        if called == 'EField' :
            self.Label.setText('Enter Scaling Factor')
            self.setWindowTitle('Set E Field Scaling Factor')
            try : 
                self.Valeur.setText(unicode(DataAnalysis.ResDict['maths']['EFieldScale']))
            except :
                pass
        self.connectActions()
        
    def connectActions(self):
        self.connect(self.OK, QtCore.SIGNAL('clicked()'),self.QuitHappy)    
     
    def QuitHappy(self):
        temp = self.Valeur.text()
        if self.called == 'EField' :
            try :
                if self.InverseBox.isChecked() : 
                    DataAnalysis.ResDict['maths']['EFieldScale'] = 1./float(temp)
                else :
                    DataAnalysis.ResDict['maths']['EFieldScale'] = float(temp)
                self.close()
            except ValueError :
                QtGui.QMessageBox.information(None,"Error","Please enter a string convertible to float type")
        else : 
            self.close()
            
class ForsterDialog(QDialog,Ui_ForsterDialog) :
    def __init__(self,parent=None,called = 'EField'):
        super(ForsterDialog,self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle('Field recorded / field predicted correspondance')
        try : 
            self.V0.setText(unicode(DataAnalysis.ResDict['maths']['FRes'][0,0]))
            self.V1.setText(unicode(DataAnalysis.ResDict['maths']['FRes'][0,1]))
            self.V2.setText(unicode(DataAnalysis.ResDict['maths']['FRes'][1,0]))
            self.V3.setText(unicode(DataAnalysis.ResDict['maths']['FRes'][1,1]))
        except :
            pass
        self.connectActions()
        
    def connectActions(self):
        self.connect(self.OK, QtCore.SIGNAL('clicked()'),self.QuitHappy)    
     
    def QuitHappy(self):
        try :
            DataAnalysis.ResDict['maths']['FRes'] = np.array(
            [[float(self.V0.text()),float(self.V1.text())],
              [float(self.V2.text()),float(self.V3.text())]])
            self.close()
        except ValueError :
            QtGui.QMessageBox.information(None,"Error","Please enter a string convertible to float type")

class FDialog(QDialog,Ui_FitDialog):
    def __init__(self, parent=None):
        super(FDialog, self).__init__(parent)
        self.setupUi(self)
        self.setIniValues(DataAnalysis.temp)
        self.connectActions()
        
    def setIniValues(self,params):
        self.A.setText(unicode(params['A']))
        self.offset.setText(unicode(params['offset']))
        self.sigma0.setText(unicode(params['sigma0']))
        self.x0.setText(unicode(params['x0']))
        
    def connectActions(self):
        self.connect(self.Fit, QtCore.SIGNAL('clicked()'),self.FitWithGuess)
        self.connect(self.Done, QtCore.SIGNAL('clicked()'),self.QuitHappy)
        
    def FitWithGuess(self):
        i = DataAnalysis.NumberBox.value()    
        dirname = os.path.split(DataAnalysis.ResDict['subdirs'][i])[-1]
        DataAnalysis.ResDict[dirname]['data'].mask = np.zeros(
                            DataAnalysis.ResDict[dirname]['data'].shape,dtype=bool)
        params = ({'A':float(self.A.text()),'offset':float(self.offset.text()),
                'sigma0':float(self.sigma0.text()),'x0':float(self.x0.text())})
        DataAnalysis.FitRawDataCurves(initialGuess=params)
       
        if not DataAnalysis.ResDict[dirname]['FitRes']['FitError'] :
            self.A.setText(unicode(DataAnalysis.ResDict[dirname]['FitRes']['A']))
            self.offset.setText(unicode(DataAnalysis.ResDict[dirname]['FitRes']['offset']))
            self.sigma0.setText(unicode(DataAnalysis.ResDict[dirname]['FitRes']['sigma0']))
            self.x0.setText(unicode(DataAnalysis.ResDict[dirname]['FitRes']['x0']))
        
    def QuitHappy(self):
        i = DataAnalysis.NumberBox.value()    
        dirname = os.path.split(DataAnalysis.ResDict['subdirs'][i])[-1]
        if float(self.A.text())==0:
            print 'Content'
            DataAnalysis.ResDict[dirname]['FitRes']['FitError']=True
        else:
            print 'PasContent'
            DataAnalysis.ResDict[dirname]['FitRes']['FitError']=False  
        self.close()


if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    DataAnalysis = DataAnalysis()
    DataAnalysis.main()
    sys.exit(app.exec_())
    