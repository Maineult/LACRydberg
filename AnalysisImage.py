# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 22:02:27 2016

@author: henri
"""

import matplotlib.pyplot as plt
import numpy as np
import pickle
import ExperimentModel
import os
import datetime
import sys
import DataAnalysisYb as da
from UserDefFunctions import *
from scipy import optimize



#
#def ChooseBackGround(Img,Bckgs):
#    for i in Bckgs:
        




##### INPUTs ######


        
DataRep ="C:\\Users\\lac\\Documents\\RydbergYtterbiumData\\2017\\04 - 13 (Thursday)"       

Bfilename='AbsImaging_Back_10db_1ms_15us_.ExpResults'
Mfilename='AbsImaging_MOT_10db_1ms_15us_11A.ExpResults'
os.chdir(DataRep)
Bfilee=open(Bfilename,'r')
Bresults=pickle.load(Bfilee)
Mfilee=open(Mfilename,'r')
Mresults=pickle.load(Mfilee)
Back=np.zeros((768, 1024),dtype=float)
MOT=np.zeros((768, 1024),dtype=float)

for i in range(len(Bresults.Resultats["[['RydbergStop', 0.3]]"])):
    Back=Back+Bresults.Resultats["[['RydbergStop', 0.3]]"][i]['']
    MOT=MOT+Mresults.Resultats["[['RydbergStop', 0.3]]"][i]['']
#
Mot=MOT/Back    
#print np.mean(MOT)
#plt.imshow(Mot) 
#plt.figure()
#plt.plot(range(1024),Mot[400])
    
#data=t,I
#data=da.DataRestrict(data,4,11) 
#test=[]
#plt.plot(data[0],data[1],'o')  
#da.FitExp(data[1],data[0],1,1e7,1.6e7,plot=True)
#sig0=(3*(399e-9)**2)/ 2*3.1415

def AtomDensity(TrPer,Size):
    return -np.log(TrPer)/sig0*Size
    
def AtomNumber(TrPer,Size):
    return -Size**2*np.log(TrPer)/sig0   
    
def SaveData(Data,name):
    np.save(name,Data)
  
        
              
os.chdir('C:\Users\lac\Desktop')  
 
#Mot=np.load('mot.npy')
params = -0.05,1,400,376,100,100    
data =Mot
print data.shape
plt.matshow(data)
params = da.Fit2DGaussian(data,params)
fit = da.gaussian(*params)

plt.contour(fit(*np.indices(data.shape)), cmap=plt.cm.copper)
    
plt.figure()
Xin, Yin = np.mgrid[0:768, 0:1024]
data = da.gaussian(*params)(Xin, Yin)
print params
plt.matshow(data)
plt.figure()   
plt.plot(range(1024),Mot[400]) 
SaveData(data,'Mot.npy')