# -*- coding: utf-8 -*-
"""
Created on Wed Sep 09 12:02:31 2015

@author: lac
"""


from MatisseConstants import *
import visa
import time


class Matisse:
    def __init__(self,Simulate = False):
        self.Simulate = Simulate
        if Simulate:
            return
        self.rm = visa.ResourceManager()
        #self.M = self.rm.open_resource(VisaNameEth)
        self.M = self.rm.open_resource(VisaName)
        self.ReadRefCal()
        self.messageprinted = 10
        self.testnum = 0

    def __test__(self):
        if self.Simulate:
            return True
        try : 
            ID = self.M.query('*IDN?')
        except :
            ID = 'Nothing'
        return MatisseName in ID
            
    def Relaunch(self):
        self.close()   
        self.rm = visa.ResourceManager()
        self.M = self.rm.open_resource(VisaName)
        
    def close(self):
        self.M.close()
        self.rm.close()
        
    def ReadRefCal(self):
        self.RefCal = THzperVolt
        print 'Check for actual calibration of the Matisse.'
        print 'Can output some really weard numbers.'
        print 'Printed here to check : Should read like -0.1' 
        try :
            float(self.M.query('SCAN:REFCAL?')[14:])*1e-6  
        except :
            print 'strange'
        return self.RefCal
        
    def ReadScanStatus(self):
        test = self.M.query('SCAN:STATUS? ' )
        if test.find(':SCAN:STA:') : 
            return 'UNKN'
        self.testnum +=1
        return test[11:]
        
    def WriteCancelScan(self):
        self.M.query('SCAN:STATUS STOP')  
        
    def ReadisLocked(self):
        rep = self.M.query('FASTPIEZO:LOCK?')
        if 'TRUE' in rep : 
            return True
        else:
            return False
        
    def WritePZT(self,target,mode,current):
        '''
            Moves the PZT that scans the TiSa Cavity to position target
            if current is not 0, by default it will scan to the desired position
            if current is given, it will scan the piezo if |current-target|>5MHz
            if mode = slow, it will slowly scan the frequency (for instance to perform
            a Rydberg spectroscopy (10MHz/sec)=SlowScan)
            if mode = fast, will go as fast as possible to the target freq (100 MHz/sec)=FAstScan
            if 375<target<400, will assume target and current in THz
            if not, will have to be in PZT units (between 0 and 0.7)
        '''
        if not hasattr(self,'startscan'):
            self.startscan = 0
        test = self.ReadisLocked()
        if not test:
            print unlocked
            return 'Unlocked'
        test =  self.ReadScanStatus()
        if 'RUN' in test: 
            return
        from time import sleep
       
        if 375<target<400:
            if not 375<current<400 : 
                if self.messageprinted :
                    print ('No current frequency given ! PZT change aborted')
                    self.messageprinted -= 1
                return
            b=self.ReadPZT()
            a = (target-current)/self.RefCal+b
        else : 
            a= target
            if current : 
                b=current
            else :
                b=self.ReadPZT()
        scan = 1
        if not (a>SlowPiezo['Min'] and a<SlowPiezo['Max'] ):   
            if self.messageprinted :
                print('MatisseError : Slow Piezo Value Out of Bounds, Or Error reading')
                self.messageprinted -=1
            return 'OutOfBound'
        
      #  print 'curr',self.ReadPZT(),"SCAN:NOW {:.6f}".format(a),'Verif',target,current 
        if current and abs((b-a)*self.RefCal)<5e-6 :
            scan = 0
        if scan : 
            if a-b>0 : 
                self.M.query('SCAN:MODE 6')
                self.M.query('SCAN:LLM {:.7f}'.format(b))
                self.M.query('SCAN:ULM {:.7f}'.format(a))
                if mode == 'fast':
                    self.M.query('SCAN:RSPD {:.7f}'.format(FastScan))
                else : 
                    self.M.query('SCAN:RSPD {:.7f}'.format(SlowScan))
                    
            else :
                self.M.query('SCAN:MODE 7')
                self.M.query('SCAN:LLM {:.7f}'.format(a))
                self.M.query('SCAN:ULM {:.7f}'.format(b))
                if mode == 'fast':
                    self.M.query('SCAN:FSPD {:.7f}'.format(FastScan))
                else : 
                    self.M.query('SCAN:FSPD {:.7f}'.format(SlowScan))
            self.M.query('SCAN:STATUS RUN')  
            self.startscan = 1
        else :
            if a>b : 
                cmd= "SCAN:NOW {:.6f}".format(b+1e-6)
            else : 
                cmd= "SCAN:NOW {:.6f}".format(b-1e-6)
            self.M.query(cmd)
        



    
    def ReadPZT(self):
        temp = self.M.query('REFCELL:NOW?')
        if temp.find(':REFCELL:NOW:'):
            if self.messageprinted :
                self.messageprinted -=1
            return -1
        return float(temp[14:])
                  
if __name__=='__main__':
    toto=Matisse()   
    from time import sleep
    a = []
    for i in range(100) : 
        a.append(toto.ReadPZT())
        
        
        
        
        
