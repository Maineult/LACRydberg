# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 22:02:27 2016

@author: henri
"""

import matplotlib.pyplot as plt
import numpy as np
import pickle
import ExperimentModel
import os

def Sortbyx(Data):
    Sortx=sorted(Data[0])
    for j in range(len(Data)) :
        if j>0:
            CorrespSig=[]
            for i,x in enumerate(Sortx):
                CorrespSig.insert(i,Data[j][Data[0].index(x)])
            Data[j]=CorrespSig
    Data[0]=Sortx    
    return Data    
        
##### INPUTs ######
filename='MW_64-1S0_Rabi2.ExpResults'

###### MAIN #######

os.chdir("C:\\Users\\lac\\Documents\\RydbergYtterbiumData\\2016\\10 - 07 (Friday)")
name=filename.split('.')[0]
InitialState=name.split('_')[1]
ResultsFile=open(filename,'r')
Results=pickle.load(ResultsFile)
tau=[]
Signal=[]
StdDevSignal=[]

for i,keyParam in enumerate(Results.Resultats.iterkeys()):  
    Data=Results.Resultats[keyParam]
    AverageNumber=len(Data)
    a=keyParam.split(',')[1].split(']')[0]
    tau.insert(i,float(a)*1e6)
    S=[]
    for j in range(AverageNumber):
        S.insert(j,(Data[j]['Custom2']/Data[j]['Custom1']))
    Signal.insert(i,np.mean(S))
    StdDevSignal.insert(j,np.std(S))    

    

Data=[tau,Signal,StdDevSignal]
Data=Sortbyx(Data)

#plt.plot(Data[0],Data[1])

plt.errorbar(Data[0],Data[1],yerr=Data[2],linestyle='None',marker='o')
plt.plot()
#plt.plot(Data[0],Data[1],'b')




def SaveExpModel():
    pass    

def SaveData():
    os.chdir("C:\\Users\\lac\\Documents\\GoogleDriveYtterbium\\Donnees\\MicroWaveSpectroscopy")
    os.chdir('./From '+InitialState)
    filename=name
    np.savetxt(filename,Data)
#    fil=open(filename,'w')
#    pickle.dump(DicoData,fil)
    