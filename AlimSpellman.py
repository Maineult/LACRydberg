# -*- coding: utf-8 -*-
"""
Created on Fri Dec 04 16:49:02 2015

@author: wmaineult
"""
"""
 * Has to modify some files of the standard arduino library to have 
 * PWM working on all ports with arbitrary resolution.
 * All the modified files of the library are inside the directory of this sketch
 * and the modifications have to be implemented in the current arduino due library.
 *  - Clock is now the max clock frequency (84 MHz) (modified in
 * the wiring_analog.c of the Arduino Due library ) 
 *  - added constant definition in firmata.h : 
 * #define SET_RESOLUTION          0xE1  // set the resoltuion on the PWM pins
 * 
 * added function to firmata ino sketch
 * void setPinResolutionCallback(byte pin, int value)
{
    uint32_t chan = g_APinDescription[pin].ulPWMChannel;
    if (pinEnabled[pin]) {
      PWMC_SetPeriod(PWM_INTERFACE, chan, value);
      pinResolution[pin] = value;
      pmc_mck_set_prescaler(2);
    }else{
      Firmata.sendString("Enable requeested pin first")
    }  
}// end setPinResolution
 * 
 * and remplaced the pwm part of the analogwritecallback fucntion
 * 
      case PIN_MODE_PWM:
        if (IS_PIN_PWM(pin)){
          uint32_t chan = g_APinDescription[pin].ulPWMChannel;
          if (value<pinResolution[pin])
            PWMC_SetDutyCycle(PWM_INTERFACE, chan, value);
        }// end if
        pinState[pin] = value;
        break;
    }//End PIN_MODE PWM
 *       
 *       and added the         pinResolution[TOTAL_PIN] = 
 *       initialized to 255 at he same time as
 *       the PWM of the pins;
 *       
 *       Comment the deprecated #PWM definition from the firmata.h header (and all deprecated sysex function headers
 */
"""

"""
For the pyfirmata protocol, please consider updating the due description as shown below in boards.py
    'arduino_due': {
        'digital': tuple(x for x in range(54)),
        'analog': tuple(x for x in range(12)),
        'pwm': tuple(x for x in range(2, 14)+range(34,41)),
        'use_ports': True,
        'disabled': (0, 1)  # Rx, Tx, Crystal
    }
"""


import numpy as np
import CesiumConstants as Cs
from pyfirmata import util
import pyfirmata as A
from ArduinoConstants import *
import time

'''
To talk to the Spellman Alims, we use an Arduino with the Firmata 2.5 plus sketch downloaded on
the board. Arduino talks using serial protocol to the Spellman, Here on hardware Serial 1
(should work on Leonardo/Micro/Due/Mega boards at least). Maybe one has to check on the Firmata.c or .h files
that the Hardware Serial protocol is correctly defined in there.
01/02/2016 Fix : Need to remove the system reset callback from Firmata 2.5 protocol to have serial communication
work... Has to look for registers to see what's the problem if needed but I submitted the bug
Message from computer to Spellman are sent using the Sysex protocol. (2 *7bits to make a complete bit)
Has to check this works correcty
On the board side, the Sysex message are decoded and sent to the board. The board is supposed to listen to 
the HW1 port thanks to the ReadMessage() function, which will relay replys from Arduino to
the computer, which are intecepted using the cmd handler SERIAL Message.
'''


class FirmataHT(A.ArduinoDue):
    def __init__(self,port='COM3'):
        super(FirmataHT, self).__init__(port,baudrate=57600,timeout=2)
        print self.firmware
        print self.firmata_version
        self.add_cmd_handler(SERIAL_MESSAGE,self._Handle_SpellmanResponse)
        self.add_cmd_handler(A.STRING_DATA,self._Handle_FirmataString)

        self.ConfigureSerial()
        time.sleep(0.1)
        self.SpellmanResponse = ''
        while self.bytes_available():
            self.iterate()

    def __test__(self):
        raise TypeError('Not Implemented')

    def ConfigureSerial(self):
        message = self.Bit_Or(SERIAL_CONFIG,HW_SERIAL1)
        message += util.to_two_bytes(9600) + bytearray([0])
        self.send_sysex(SERIAL_MESSAGE,message)    
        time.sleep(0.1)
        while self.bytes_available():
            self.iterate()      
    
        
    def SetOn(self):
        message = (chr(int(HISPELLMAN,0))+chr(int(ALL,0))+
                'EN1')
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(int(BYSPELLMAN,0)))
        self.WriteMessage(message) 

    def SetOff(self):
        message = (chr(int(HISPELLMAN,0))+chr(int(ALL,0))+
                'EN0')
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(int(BYSPELLMAN,0)))
        self.WriteMessage(message) 
    
    def _SetAdress(self,i):    
        message = (chr(int(HISPELLMAN,0))+chr(int(PHOS,0))+
                'ID='+chr(i))
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(int(BYSPELLMAN,0)))
        self.Query(message)    
    
    def _Handle_SpellmanResponse(self,*data):
        data = list(data)
        if len(data)>2:
            if data[1]==2 : 
                self.SpellmanResponse=''
                data.pop(1)
            if data[1]==10 : 
                print self.SpellmanResponse
            for i in data[1:-1] :
                if len(data)>2:
                    self.SpellmanResponse+=chr(i)
            
#        message = ''
#        if data[0] != HISPELLMAN:
#            raise TypeError
#        if data[1] != '9':
#            print 'Vraiment Bizarre, adresse fausse'
#        if data[-1] != BYSPELLMAN : 
#            print 'La aussi Bizarre'
#        message = data[2:-1]
#        print 'Le Message',message,'Argh'
#        message = message[1:-1]
#        if not self.CalcCheckSum(message[:-1])==message[-1]:
#            print 'strange'

    def SetVoltsMCP(self,V):
        message = (chr(int(HISPELLMAN,0))+chr(int(MCP,0))+
                chr(int(MCPTYPE,0))+'V1='+str(int(V))+'.'+str(int(10*(V-int(V)))))
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(int(BYSPELLMAN,0)))   
        self.Query(message)   
                        
        
    def SetVoltsIon(self,V):
        message = (chr(int(HISPELLMAN,0))+chr(int(ION,0))+
                chr(int(IONTYPE,0))+'V1='+str(int(V))+'.'+str(int(10*(V-int(V)))))
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(int(BYSPELLMAN,0)))
        self.Query(message)
        
    def SetVoltsPHOS(self,V):
        message = (chr(int(HISPELLMAN,0))+chr(int(PHOS,0))+
                chr(int(PHOSTYPE,0))+'V1='+str(int(V))+'.'+str(int(10*(V-int(V)))))
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(int(BYSPELLMAN,0)))         
        self.Query(message)        

    

    def Bit_Or(self,a,b):
        return bytearray([int(a,0)+int(b,0)])
        
    def _to_seven_bits(self,data):
        #Should use firmata.util functions. Kept here for the time
        rep = bytearray(2)        
        if type(data)==type(1):
            try : 
                rep = A.util.to_two_bytes(data)
            except :
                raise ValueError('NonImplemented')      
        if type(data)==type(''):
            if len(data)>1:
                try : 
                    rep =A.util.str_to_two_byte_iter(data)
                except : 
                    raise ValueError('NonImplemented')       
            else : 
                
                rep[0]=ord(data)%128
                rep[1]=ord(data)/128
        return rep
                
    def _Handle_FirmataString(self,*message):
        #Use for debug purposes mainly
#        result = A.util.two_byte_iter_to_str(message)
        result = ''
        for i in np.arange((len(message)+1)/2):
            try :
                result+= chr(2**7*(message[2*i+1])+message[2*i])
            except :
                result += '?' + str(message[2*i]) + str(message[2*i+1])
        self.LeMessage = result
        print result
                
                
    def WriteMessage(self,message):
        m = (self.Bit_Or(SERIAL_WRITE,HW_SERIAL1))
        if type(message) == type('') :
            #for car in message :
                #m = m + self._to_seven_bits(car)    
            m = m+A.util.str_to_two_byte_iter(message)
            self.send_sysex(SERIAL_MESSAGE,m)
    
    def Query(self,message, timeout=1):
        self.WriteMessage(message)
        self.ReadMessage()
        t0 =time.time()
        used = 0
        while time.time()-t0<timeout:        
            while self.bytes_available():
                self.iterate()
                used = 1
        if used:
            self.StopReading()
            return
        self.StopReading()
        
    def ReadMessage(self):
        message = (self.Bit_Or(SERIAL_READ,HW_SERIAL1)
                + self._to_seven_bits(0x00))
        self.send_sysex(SERIAL_MESSAGE,message)
        
    def StopReading(self):
        message = (self.Bit_Or(SERIAL_READ,HW_SERIAL1)
                + self._to_seven_bits(0x01))
        self.send_sysex(SERIAL_MESSAGE,message)

    def _PrintFirmataStream(self):
        while self.bytes_available():
            self.iterate()

    def CalcCheckSum(self,com):
        chk=sum(map(ord, com))
        a = (2**16-chk)%256
        b = chr((a&127)|64)
        return b
        
    def Serial1Close(self):
        message = (self.Bit_Or(SERIAL_READ,HW_SERIAL1)
                + self._to_seven_bits(SERIAL_CLOSE))
        
    def close(self):
        message = (chr(int(HISPELLMAN,0))+chr(int(ALL,0))+
                'EN0')
        message= (message+self.CalcCheckSum(message[1:])+
                        chr(int(BYSPELLMAN,0)))
        self.WriteMessage(message) 
        self.exit()

  
if __name__ == "__main__":
    a = FirmataHT()
#    a._PrintFirmataStream()
#    a.SetVoltsMCP(20.0)

        
    
        