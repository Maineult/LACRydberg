# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 22:02:27 2016

@author: henri
"""

import matplotlib.pyplot as plt
import numpy as np
import pickle
import ExperimentModel
import os
os.chdir("./YbSpectroData")
import DataAnalysisYb as an
from UserDefFunctions import *




##### INPUTs ######


        
DataRep ="C:\\Users\\lac\\Documents\\RydbergYtterbiumData\\2016\\10 - 31 (Monday)"       

filename='MW_61-1S0_BigScan1.ExpResults'

Analysis=False

FitForm='DampedSin'
A=100
w=1e6
Tau=2e-5
offset=0.3


AnalysedDataRep="C:\Users\lac\Documents\GoogleDriveYtterbium\Donnees\MicroWaveSpectroscopy"



##### FUNKtions
  

def SaveData(Name=filename):
    os.chdir(AnalysedDataRep)
    File=open(Name,'w')
    cPickle.dump(Results,File)
    File.close()
    
def LandeFactor(J,L,S):
    return float(1+ (J*(J+1)-L*(L+1)+S*(S+1))/(2*J*(J+1)))
    
        

###### MAIN #######

os.chdir(DataRep)
name=filename.split('.')[0]
InitialState=name.split('_')[1]
ResultsFile=open(filename,'r')
Results=pickle.load(ResultsFile)
tau=[]
Signal=[]
StdDevSignal=[]

for i,keyParam in enumerate(Results.Resultats.iterkeys()):  
    Data=Results.Resultats[keyParam]
    AverageNumber=len(Data)
    a=keyParam.split(',')[1].split(']')[0]
    tau.insert(i,float(a))
    S=[]
    for j in range(AverageNumber):
        S.insert(j,(Data[j]['Custom2']/Data[j]['Custom1']))
    Signal.insert(i,np.mean(S))
    StdDevSignal.insert(j,np.std(S))    

    

Data=[tau,Signal,StdDevSignal]
Data=an.SortByX(Data)

#plt.plot(Data[0],Data[1])

plt.errorbar(Data[0],Data[1],yerr=Data[2],linestyle='None',marker='o')
plt.plot()
#plt.plot(Data[0],Data[1],'b')

if Analysis:
    if FitForm=='DampedSin':
#        if t1:
#            Data=an.DataRestrict(Data,t1,t2)
        fit=an.FitDampedSin(Data[1],Data[0],w=w,A=A,tau=Tau,offset=offset)
        plt.cla()
        plt.plot(Data[0],Data[1],'-o',color='slateblue',alpha=0.5,label='Experimental')
        plt.plot()
        XX=np.linspace(np.min(Data[0]),np.max(Data[0]),1000)
        R=fit.best_values
        plt.plot(XX,an.DampedSin(XX,R['w'],R['A'],R['tau'],R['offset']),color='mediumblue',label='Fit',linewidth=2)  
        R['Model']=FitForm
        setattr(Results,'AnalysedResults',R)

locs,labels = plt.xticks()
plt.xticks(locs, map(lambda x: "%10g" % x, locs))
plt.xlabel('Pulse Duration (s)')
plt.ylabel('Population Transfer')
ax = plt.gca()
ax.format_coord = lambda x,y: '%10f, %10f' % (x,y)
plt.legend()
plt.grid(b=True)
    
