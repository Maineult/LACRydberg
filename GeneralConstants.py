# -*- coding: utf-8 -*-

#GeneralConstants
import matplotlib
from matplotlib import pyplot as plt

pm = 1e-12
nm = 1e-9
um = 1e-6
mm = 1e-3
cm = 1e-2
m = 1
km = 1e3

ps = 1e-12
ns = 1e-9
us = 1e-6
ms = 1e-3
s  = 1

Hz  = 1
kHz = 1e3
MHz = 1e6
GHz = 1e9
THz = 1e12
PHz = 1e15

dBm = 1

V  = 1
kV = 1e3

Vcm = 1

c = 299792458 * m/s
from numpy import pi

Unit = {u'Hz':Hz,
        u'kHz':kHz,
        u'MHz':MHz,
        u'GHz':GHz,
        u'THz':THz,
        u'PHz':PHz,
        u's':s,
        u'ms':ms,
        u'µs':us,
        u'ns':ns,
        u'ps':ps}


def Diode(a):
    if type(a)==type(1):
        return (a+abs(a))/2
    else :
        return (a+abs(a))*1./2
        
def Zero(a):
    return 0
    
def NewList(a):
    return []

def FindIndex(nestlist,elem):  

    try : 
        return nestlist.index(elem)   
    except ValueError : 
        pass
    for l in nestlist :
        
        if type(l)== type([]):
            try : 
                FindIndex(l,elem)      
                return nestlist.index(l)
            except ValueError as e:
                pass
    raise ValueError('Found no elem in any nestlist matching {}'.format(elem))
    


            
            


ListColors = "bgrcmk"   
Results = {}    

ListSymbols = [symbol for symbol in matplotlib.lines.lineMarkers.keys()]
ListSymbols.remove(0)
ListSymbols.remove(1)
ListSymbols.remove(2)
ListSymbols.remove(3)
ListSymbols.remove('|')
ListSymbols.remove('')
ListSymbols.remove('None')
ListSymbols.remove('_')
ListSymbols.remove(' ')
ListSymbols.remove(None)



def mklist(string,typ='float'):
    try :     
        if string[0]=='[' and string[-1]==']':
            return [ReType[typ](e) for e in string[1:-1].split(',') ]
        else : 
            raise(Exception('{} cannot be formatted in list'.format(string)))
    except Exception as e : 
        NewLog(e)
        
def mktuple(string,typ='float'):
    try :     
        if string[0]=='(' and string[-1]==')':
            return tuple([ReType[typ](e) for e in string[1:-1].split(',') ])
        else : 
            raise(Exception('{} cannot be formatted in list'.format(string)))
    except Exception as e : 
        NewLog(e)
        
def mkdict(string,typ= 'float'):
    try :     
        if string[0]=='{' and string[-1]=='}':
            kw = {}
            tmp = [e.split(':') for e in string[1:-1].split(',')]
            [kw.__setitem__(e,ReType[typ](f)) for e,f in tmp]
            return kw
        else : 
            raise(Exception('{} cannot be formatted in list'.format(string)))
    except Exception as e : 
        NewLog(e)    

import types as t
ReType = ({t.BooleanType:bool,
           t.IntType:int,
           t.DictType:mkdict,
           t.StringType:str,
           t.UnicodeType:unicode,
           t.FloatType:float,
           t.ListType:mklist,
           t.TupleType:mktuple,
           t.LongType:long,
           'bool':bool,
           'int':int,
           'dict':mkdict,
           'str':str,
           'string':str,
           'unicode':unicode,
           'float':float,
           'list':mklist,
           'tuple':mktuple,
           'long':long})
           
           
ExperimentLog = {}

def NewLog(RaisedException = None,CustomMessage = '',Verbose = 0):
    import sys, traceback, datetime
    msg = {}
    if not (RaisedException is None) :
        msg['Error'] = RaisedException.__repr__()
        tb = sys.exc_info()[-1]
        for stk,n in zip(traceback.extract_tb(tb),range(10)):
            msg['traceback'+str(n)] = stk
    if len(CustomMessage):
        msg['Msg'] = CustomMessage
    msg['Verbose'] = Verbose
    ExperimentLog[datetime.datetime.now()] = msg
    
    
DVerbose = ({'Errors':0,
            u'Errors and Warnings':1,
            'Comments':2,
            'All':3
            })
            
            
def importOrReload(module_name, *names):
    import sys

    if module_name in sys.modules:
        reload(sys.modules[module_name])
    else:
        __import__(module_name, fromlist=names)

    for name in names:
        globals()[name] = getattr(sys.modules[module_name], name)

