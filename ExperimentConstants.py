# -*- coding: utf-8 -*-
"""
Created on Fri Feb 19 15:56:25 2016
Experiment Constants
@author: Wilfried
"""
import os
import numpy as np
from GeneralConstants import *

#For Computer recognition and setting of good parameters

ComputerDict = ({'pc-dimitris':'Cesium',
                 'brique':'Cesium',
                 'lac57-95':'Ytterbium'})


# Directory of for saving experimental data                 
DataDir = ({'pc-dimitris':u'C:\\Documents and Settings\\Wilfried\\Mes Documents\\ExperimentResults',
            'brique':u'/home/wilfried/PythonScripts/ExpResults',
            'lac57-95':u'C:\\Users\\lac\\Documents\\RydbergYtterbiumData'
            })
ExpParamDir =   ({'pc-dimitris':u'C:\\Documents and Settings\\Wilfried\\Mes Documents\\ExperimentResults',
            'brique':u'/home/wilfried/PythonScripts/ExpResults',
            'lac57-95':u'C:\\Users\\lac\\Documents\\RydbergYtterbiumData\\SequenceParams'          
            })
            
# COM port of the ArduinoDue with the FirmataPlusModified sktech
ArduinoComPort = ({'pc-dimitris':{'port':'COM3'},
                  'brique':{'port':'tty/dev0'},
                  'lac57-95':{'port':'COM7'}
                  })
                  
                  
                  
                  

from socket import gethostname as gh

#If lambdameter is on a different computer, the communication parameter are stored here
LambdameterComputer = 'admin-TOSH' # or 'lac57-95'
InternalComPort = 16661
BUFFER_SIZE = 1024
LambdameterServer ='admin-TOSH' # or 'lac57-95'
LambdameterSwitch = [None,None,'Cesium','Ytterbium',None,None,None,None]


#For each instrument : Name, Name of the module, Name of the class instance, kwargs for instanciation
InstrumentDict =  ({'SRSDelayGen':('SRSDelayGen','SRSDelayGen',{}),
                    'Matisse':('MatisseLib','Matisse',{}),
                    'Solstis':('SolstisLib','Solstis',{}),
                    'Arduino':('Arduino','ArduinoCustom',ArduinoComPort[gh()]),
                    'Ion':('Arduino','HVAlim','getArduinoInstance'),
                    'ForsterAlim':('Arduino','ForsterAlim','getArduinoInstance'),
                    'Scope':('ScopeFunc','Scope',{'manip':ComputerDict[gh()],'Channel':0}),
                    'Lambdameter':('WaveMeterLib','WMeter','FindWmChannel'),
                    'RemoteLambda':('ServerLib','LambdaServer','GetSelf'),
                    'BNCDelayGen':('BNCDelayGen','DelayGen', {}),
                    'FieldSynth':('ArbFieldSynth','ArbFieldSynth',{}),
                    'MicroWaveGen':('MicroWaveGenLib','MicroWaveGen',{}),
                    'InternalMethods':('InternalMethods','IntMethods',{}),
                    'DacBoard':('DaskAdlink','DACCard',{}),
                    'CameraThor':('ThorCameraLib','ThorCamera',{}),
                    'Camera':('PointGreyLib','PointGreyCamera',{}),
                    'ElectricFields':('DaskAdlink','ElectricFields','getDacInstance'),
                    'LambdameterIon':('WaveMeterLib','WMeter',{'Channel':1,'UseLocal':1,'SecondDevice':1})
                   }) 
                   
                   
                   
                   
### Init parameters for the experiment automatically passed by  the controller when initialized 
InitParams={'ElectricFields':{'EWField':{'V_EW':1.0},'NSField':{'V_NS':-2.9},
                              'UField':{'V_U':0.40},'IonizationVoltage':{'V_Ioniz':4000}
                              ,'MCPVoltage':{'V_MCP':-1450}
                              },
            'CameraThor':{'Exposure':{'ExposureTime':1.},'Gain':{'Gain':55}}                 
            }
        
      

### Initial TimeTable to be passed

ListChannelsBNC=['6p','Ryd','Shutter','IonLaser','StarkPulse','IonField','CamTrig','MOTLaserShut']

InitTimeTable={'Ytterbium':({'Times':[50*us,5*us,1*us,20*us,5*us,1*us,15*us,0.1*us],
                    'TimeTags':['StarkStart','RydbergStart','RydbergStop','Ta mere','MicStart','Micstop','FieldStart','FieldStop','ExperimentEnd'],
                    'BNCDelayGen6p':[0,1,0,0,0,0,0,0],
                    'BNCDelayGenRyd':[0,1,0,0,0,0,0,0],
                    'BNCDelayGenShutter':[0,0,0,0,0,0,0,0],
                    'BNCDelayGenIonLaser':[0,0,0,0,0,0,0,0],
                    'BNCDelayGenStarkPulse':[1,1,1,0,0,0,0,0],
                    'BNCDelayGenIonField':[0,0,0,0,0,0,1,0],
                    'BNCDelayGenCamTrig':[0,0,0,0,0,0,0,0],
                    'BNCDelayGenMOTLaserShut':[0,0,0,0,0,0,0,0]
                   }),

# 
#InitTimeTable={'Ytterbium':({'Times':[300000*us,20*us,100*us,5*us,0.1*us,0.1*us],
#                    'TimeTags':['RydbergStart','RydbergStop','MicStart','Micstop','FieldStart','FieldStop','ExperimentEnd'],
#                    'BNCDelayGen6p':[0,0,0,1,0,0],
#                    'BNCDelayGenRyd':[0,0,0,0,0,0],
#                    'BNCDelayGenShutter':[1,1,1,1,1,0],
#                    'BNCDelayGenIonLaser':[0,0,0,0,0,0],
#                    'BNCDelayGenFieldSynth':[0,0,0,0,0,0],
#                    'BNCDelayGenIonField':[0,0,0,0,0,0],
#                    'BNCDelayGenCamTrig':[0,1,0,0,0,0],
#                    'BNCDelayGenMOTLaserShut':[0,0,0,0,0,0]
#                   }),
                 'Cesium':  ({'Times':[60*ms,10*us,1*us,36*us,1000*us],
                    'TimeTags':['ExperimentStart','RydbergStart','RydbergStop','IonStart','IonStop','ExperimentEnd'],
                    'SRSDelayGens7':[0,1,0,0,0],
                    'SRSDelayGenRydberg':[0,1,0,0,0],
                    'SRSDelayGenMicrowave':[0,0,1,0,0],
                    'SRSDelayGenIon':[0,0,0,1,0],
                   })  }





 
LaserList = ['Matisse','QCW','Solstis']
                 

#For communication with instruments
NotUsed=0
WaitToConnect = 1
InitOK = 2
WaitToStop=3
IsNotInit = 0
ConnectDic =  {0:'NotUsed',1:'WaitToConnect',2:'InitOK'}


On = 1
Off = 0

Stop = 0
Running = 1
Suspended = 2
Starting = 3
Suspend = 2
Automatic = 4
NotApplicable = -1


#Values of SRS amplitudes#
s7DefaultValue = 4
RydbergDefaultValue = 5
MicrowaveDefaultValue = 3.3
IonisationDefaultValue = 5

#MicrowaveMoedes
Pulsed = 0

#

DelayAOM7sOn = 3e-7   ## Not Calibrated
DelayAOM7sOff= 5e-7   ## Not Calibrated
DelayMWOn = 120e-9    ## Not Calibrated
DelayMWOff = 140e-9   ## Not Calibrated
DelayAOMnpOn = 1e-8   ## Not Calibrated
DelayAOMnpOff = 1e-8  ## Not Calibrated


"""
SRS TimeTable : Dictionnary with tuples containing
keys : ChannelName,  values[0] ChannelNumber for Visa Commands
values[1], Channel delay (i.e. AOM delays) for perfect synchronisation
"""
TimingsSRS = {'ExperimentStart':(0,0),
              's7Start':(2,-DelayAOM7sOn),
              's7Stop':(3,DelayAOM7sOff),
              'RydbergStart':(4,-DelayAOMnpOn),
              'RydbergStop':(5,DelayAOMnpOff),
              'MicrowaveStart':(6,-DelayMWOn),
              'MicrowaveStop':(7,DelayMWOff),
              'IonStart':(8,0),
              'IonStop':(9,0)}
            
              
CameraDelay=-690e-6          
FieldSynthDelay=-10e-6
ExperimentStartDelay=100e-6
              
              
              
TimingsBNC= {'ExperimentStart':(0,ExperimentStartDelay),
              '6pStart':(1,0),
              '6pStop':(1,0),
              'RydStart':(2,0),
              'RydStop':(2,0),
              'ShutterStart':(3,0),
              'ShutterStop':(3,0),
              'IonLaserStart':(4,0),
              'IonLaserStop':(4,0),           
              'StarkPulseStart':(5,0),
              'StarkPulseStop':(5,0),
              'IonFieldStart':(6,0),
              'IonFieldStop':(6,0),
              'CamTrigStart':(7,CameraDelay),
              'CamTrigStop':(7,0),
              'MOTLaserShutStart':(8,0),
              'MOTLaserShutStop':(8,0)}


AdressIonization = 1   #5kV positive Spellman MPS generator
AdressMCP = 2           #2kV negative Spellman MPS generator
AdressPhosphore = 3    #2kV positive Spellman MPS generator

AreaOfOneIon = ([[2000,680e-12],
                [1900,310e-12],
                [1800,128e-12],
                [1700,56e-12],
                [1600,8e-12],
                [1500,2.5e-12],
                ])
                
# Calibration from 08/02/2013 Cahier Manip 1
# Seems total voltage across the MCP is the only parameter. first number.
# Is the signal measured on the phosphorous screen like now ?
# Second number is the signal of a single ion.

MCPQuEff = 0.5
# Calibrated Before.

#
VoltMaxForster = 60     # Not calibrated Maybe one can used a electric field calibration on the experiment
ForsterDefaultResolution = 2**13




def LoadSpectroFile(States):
    os.chdir("C:/Users/lac/Documents/Python Scripts/ExperimentControl/YbSpectroData")
    Data=np.loadtxt(States,delimiter=' ')    
    return Data


E1P1=751.526533     ### From Kleinert et al.
AOM=0.000250      ### May change if needed

def RydbergLevelsYb(n,Serie,initial='1P1'):
    try:    
        RydSerie=LoadSpectroFile(Serie+'Serie.txt')    
    except IOError:
        print 'IOERROR, CANT OPEN THE DATA FILE, DONT KNOW THIS Rydberg Serie  !'
        return
        
    LevelIndex=np.where(RydSerie==n)[0]
    LevelEnergy=RydSerie[LevelIndex,1]
    if initial=='1P1':
        try:
            FreqSecPhot=(LevelEnergy[0]-E1P1+AOM)/2
        except:
            print 'ERROR DONT KNOW THIS STATE!'
            return
    return round(FreqSecPhot,6)
            
    

def RydbergLevelsCesium(n,L,J2,initial='7s'):
    """
    Rydberg energy level as a funciton of n, L = L, J2 = 2*J
    """
    L2=2*L
    RCs = 109736.8627339 /cm *c
    EionCs = 31406.4677325 /cm *c
   # E7s = 18535.4257 /cm*c
    
    SixSF4SixPF5 = 351.721961e12 
    SixPF5SeptPF4 = 203956122914205.44
    SixSSixF4 = 4.021776399375e9
    DeltaLock = 15e6
    E7s = +SixSSixF4 + SixSF4SixPF5 + SixPF5SeptPF4
    E6p = SixSF4SixPF5 + SixSSixF4 + DeltaLock 
    
    d0lj = ({'0p':4.0493532 ,
            '1':3.5915871 ,
            '1p': 3.5590676,
            '2':  ValueError('notImplemented'),
            '2p': 2.4663144
            })  #principal quantum defect
            
    d2lj = ({'0p':0.2391 ,
            '1':0.36273 ,
            '1p': 0.37469,
            '2':  ValueError('notImplemented'),
            '2p': 0.01381

            }) #second order quantum defect
    
    if J2==L2+1:
        state = str(L2/2)+'p'
    elif J2==L2-1 : 
        state = str(L2/2)
    else :
        raise ValueError("In cesium, J = L+-1/2, you gave here 2L = {} and 2J = {}".format(L2,J2))
    
    dstar = d0lj[state] + d2lj[state]/(n-d0lj[state])**2
    if initial == '7s' : 
        return round((EionCs - RCs/(n-dstar)**2 -E7s)*1e-12,6)
    elif initial == '6p' :
        return round((EionCs - RCs/(n-dstar)**2 -E6p)*1e-12,6)
    
#if name==__main__: