# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 22:02:27 2016

@author: henri
"""

import matplotlib.pyplot as plt
import numpy as np
import cPickle
import ExperimentModel
import os
os.chdir("C:\Users\lac\Documents\Python Scripts\ExperimentControl\YbSpectroData")
import DataAnalysisYb as an
from UserDefFunctions import *



##### INPUTs ######



DataRep ="C:\\Users\\lac\\Documents\\RydbergYtterbiumData\\2017\\06 - 17 (Saturday)"       

filename='50-G_AutoIoScanPiPolar10us.ExpResults'
 
Analysis=1  
Smoothing=0

FitForm='Lorentzian'
X1=67.8207
X2=67.82127

X3=67.62177


AnalysedDataRep="C:\Users\lac\Desktop"


plt.plot()


##### FUNKtions

def TraceAutoIoDepN():
    plt.figure()
    nlist=[50,55,60,65,70,75,80]
    GammaList=[15.8,12,9.5,6.8,5.3,3.9,3.05]
    plt.errorbar(nlist,GammaList,yerr=0.6,marker='o',ls='',alpha=0.7,color='darkblue')
 #   plt.plot([50,55,60,65,70,80],Func([50,55,60,65,70,80]))    
    plt.plot()            
    fit=an.FitHypCube(GammaList,nlist,a=1,b=1)
    
    plt.plot(nlist,fit.best_fit,'b')
    
    
def AverageTraces(Data):
    TracesList=[]
    for i in range(len(Data)):
        print type(Data)
        TracesList.insert(i,np.array(Data[i]['Data0'][1]))
    AvTraces=np.average(np.array(TracesList))
    
    return AvTraces   
    
def Sortbyx(Data):
    Sortx=sorted(Data[0])
    for j in range(len(Data)) :
        if j>0:
            CorrespSig=[]
            for i,x in enumerate(Sortx):
                CorrespSig.insert(i,Data[j][Data[0].index(x)])
            Data[j]=CorrespSig
    Data[0]=Sortx    
    return Data     


    
def LandeFactor(J,L,S):
    J=float(J)
    L=float(L)
    S=float(S)
    return float(1+ (J*(J+1)-L*(L+1)+S*(S+1))/(2*J*(J+1)))
    
        

###### MAIN #######

os.chdir(DataRep)
name=filename.split('.')[0]
InitialState=name.split('_')[1]
ResultsFile=open(filename,'r')
Results=cPickle.load(ResultsFile)
Freq=[]
S=[]
Sryd=[]
RydSignal=[]
for i,keyParam in enumerate(Results.Resultats.iterkeys()):  
    Data=Results.Resultats[keyParam]
    AverageNumber=len(Data)
    for j in range(AverageNumber):
     #   print Data[j]['Custom2'],Data[j]['Custom1']
        S.insert(i+j,Data[j]['Custom2']/Data[j]['Custom1'])
        Sryd.insert(i+j,Data[j]['Custom1'])
        Freq.insert(i+j,(Data[j]['Frequency']-811.29129)*1e3) ## Résonance +250
print len(Freq)
Data=[Freq,S,Sryd]
#Data=an.SortByX(Data)
#plt.errorbar(Data[0],Data[1],yerr=Data[2],linestyle='None',marker='o')


plt.plot(Data[0],Data[1],'o')
Legend=[]
f = 0.5
M = (64+39)*2     #the frequency interval is f GHz,in the mesurement,the frequency maximum is about 64GHz,minimum is about -39
signal_sum = [0 for m in range(M)]
signal = [0 for m in range(M)]
freq_sum = [0 for m in range(M)]
freq = [0 for m in range(M)]
k = [0 for m in range(M)]
if Smoothing:
    for j in range(1600):
        for m in range(M):
            if Data[0][j]>=-39+f*m and Data[0][j]<=-39+f*(m+1):
                signal_sum[m] = signal_sum[m]+Data[1][j]
                freq_sum[m] = freq_sum[m]+Data[0][j]
                k[m] = k[m]+1
    for m in range (M):
        if k[m] == 0:
            signal[m] = 0
            freq[m] = 0
        else:
            signal[m] = signal_sum[m]/k[m]
            freq[m] = freq_sum[m]/k[m] 
    plt.plot(freq,signal,'o')        




if Analysis:
    if FitForm=='Lorentzian':
        fit=an.FitLorentzian(Data[1],Data[0],amplitude=1,xo=1, fwhm=4, offset=0,plot=False)
        plt.cla()
        plt.plot(Data[0],Data[1],'o',markersize=3,color='slateblue',alpha=0.8,label='Experimental')
        plt.plot()
        XX=np.linspace(np.min(Data[0]),np.max(Data[0]),1000)
        R=fit.best_values
        plt.plot(XX,an.Lorentzian(XX,R['amplitude'],R['xo'],R['fwhm'],R['offset']),color='mediumblue',label='Fit',linewidth=2)  
        plt.legend()
        setattr(Results,'AnalysedResults',R)
    
    
    
    if FitForm=='DoubleGaussian':
        
        fit=an.FitDoubleGaussian(Data[1],Data[0],A1=0.15,A2=0.25,X1=X1,X2=X2,sigma=5e-3, offset=0,plot=False)
        plt.cla()
        plt.plot(Data[0],Data[1],'-o',color='slateblue',alpha=0.8,label='Experimental')
        plt.plot()
        XX=np.linspace(np.min(Data[0]),np.max(Data[0]),1000)
        R=fit.best_values
        plt.plot(XX,an.DoubleGaussian(XX,R['A1'],R['A2'],R['X1'],R['X2'],R['sigma'],R['offset']),color='mediumblue',label='Fit',linewidth=2)  
        R['Model']=FitForm
        print 'Splitting =', R['X2']-R['X1'],'GHz'
        plt.legend()
        setattr(Results,'AnalysedResults',R)
        
    if FitForm=='TripleGaussian':
        fit=an.FitTripleGaussian(Data[1],Data[0],A1=0.25,A2=0.15,A3=0.15,X1=X1,X2=X2,X3=X3,sigma=5e-4, offset=0,plot=False)
        plt.cla()
        plt.plot(Data[0],Data[1],'-o',color='darkred',alpha=0.5,label='Experimental')
        plt.plot()
        XX=np.linspace(np.min(Data[0]),np.max(Data[0]),1000)
        R=fit.best_values
        plt.plot(XX,an.TripleGaussian(XX,R['A1'],R['A2'],R['A3'],R['X1'],R['X2'],R['X3'],R['sigma'],R['offset']),color='darkred',label='Fit',linewidth=2)  
        R['Model']=FitForm
        print 'Splitting =', R['X3']-R['X1'],'GHz'
        plt.legend()
        setattr(Results,'AnalysedResults',R)

locs,labels = plt.xticks()
#•plt.xticks(locs, map(lambda x: "%10g" % x, locs))
plt.xlabel('Frequency (GHz)')
plt.ylabel('AutoIonization Ratio')
ax = plt.gca()
ax.format_coord = lambda x,y: '%10f, %10f' % (x,y)

plt.grid()

def SaveData():
    os.chdir(AnalysedDataRep)
    np.savetxt(name+'.ExpData',Data)

    
 