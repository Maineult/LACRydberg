# -*- coding: utf-8 -*-
"""
Created on Thu Nov 03 15:12:41 2016

@author: lac
"""

from PyQt4 import QtCore as C


class IntMethods(C.QObject):
     NewTargetFreq= C.pyqtSignal(float)
     def __init__(self):
         super(IntMethods,self).__init__()

     def __test__(self):
         return 1
         
     def WriteTargetFreq(self,Freq):
         self.NewTargetFreq.emit(Freq)
         
     def WriteWaitforTrig(self,TrigNumber):
         pass