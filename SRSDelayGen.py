# -*- coding: utf-8 -*-
"""
Created on Wed Dec  9 18:48:00 2015

@author: wilfried
"""
import ExperimentConstants as Cs
import vxi11
import numpy as np
from ExperimentController import InstrError
import sys


class SRSDelayGen():
    def __init__(self,Simulate = False):
        self.Simulate = Simulate
        if Simulate:
            return
        self.instr = vxi11.Instrument("129.175.56.72")
        #self.storedparams()
        #self.WriteRydberg()   
        
    def __test__(self):
        if self.Simulate : 
            return True
        test= self.instr.ask('*IDN?')
        return (test == u'Stanford Research Systems,DG645,s/n002484,ver1.14.10E')        
        
        
    def Relaunch(self):
        self.instr.close()
        from time import sleep
        sleep(.1)
        self.instr= vxi11.Instrument("129.175.56.72")
    
    def WriteRydberg(self,**kwargs):
        if len(kwargs):
            try :
                Excitation = kwargs['Excitation']
                Delay = kwargs['Delay']
            except KeyError, e:
                raise InstrError('WriteRydberg','Unsderstand what {} means'.format(e))
        else : 
            Excitation=2e-6
            Delay = 2e-6
        TimeTable = ([
        0, # 7s On  (if 0, starts DelayAOM7sOn before the beginning of ns pulse)
        0, # 7S Pulse Length (if 0 , pulse stop DelayAOM7sOff after np pulse length)
        0, # nP On   (10 µs after T0 (channel 0) are automatically added)
        Excitation,# nP Pulse Length, takes into accont AOMnS On/Off
        0, # MW On (after end of np Pulse - DelayMWOn )
            # if np.nan, starts DelayMWOn before Excitation pulse
        0, # MW Pulse Length (if 0, stops DelayMWOff before ionisation)
        Delay,# Ionisation Start delay (after end of np Pulse)
        36.1e-6# Ionisation Pulse Stop
        ])
        Amplitudes = ([
            [4.0,0.0,1],  #     Output AB Level, Offset, Polarity   
            [5.0,0.0,1],  #     Output CD Level, Offset, Polarity  
            [3.3,0.0,1],  #     Output EF Level, Offset, Polarity
            [5.0,0.0,1]  #      Output FG Level, Offset, Polarity
        ])
        self.WriteTimings(TimeTable,Amplitudes)

        
    def WriteAlign7s(self):
        TimeTable = ([
        0, # 7s On  (if 0, starts DelayAOM7sOn before the beginning of ns pulse)
        float(5e-2), # 7S Pulse Length (if 0 , pulse stop DelayAOM7sOff after np pulse length)
        0, # nP On   (10 µs after T0 (channel 0) are automatically added)
        1e-8,# nP Pulse Length, takes into accont AOMnS On/Off
        0, # MW On (after end of np Pulse - DelayMWOn )
            # if np.nan, starts DelayMWOn before Excitation pulse
        0, # MW Pulse Length (if 0, stops DelayMWOff before ionisation)
        1e-6,# Ionisation Start delay (after end of np Pulse)
        36.1e-6# Ionisation Pulse Stop
        ])
        Amplitudes = ([
            [4.0,0.0,1],  #     Output AB Level, Offset, Polarity   
            [5.0,0.0,1],  #     Output CD Level, Offset, Polarity  
            [3.3,0.0,1],  #     Output EF Level, Offset, Polarity
            [5.0,0.0,1]  #      Output FG Level, Offset, Polarity
        ])
        self.WriteTimings(TimeTable,Amplitudes)


    def WriteChannel(self,Channel,Command) : 
        cmd = 'DLAY {},{},{:.3g}'.format(Channel+2,Command[0],Command[1])
        print cmd, Channel+2, Command[0], Command[1]
        self.instr.write(cmd)
#        from datetime import datetime
#        print 'l92', datetime.now()

        

    def WritePulse(self,out,on,off,**Tim):
        """
            Writes a SRS channel timings 
            Syntax = ref0 : Channel Name (i.e : 7s, Rydberg, Microwave, Ion)
            on = TimeName in the sequence
            off TimeName in the sequence
            if on and off are composed of ChannelName + [Start/Stop]
            (i.e. ChannelNames refers to SRS TimeTable Cs.TimingsSTS.keys())
            optional Times Dict is not requiered.
            else it will raise an Value Error : Not in timetable
        """

        Times={}
        [Times.__setitem__(key[0],it[0]) for it,key in Tim['Times']]
        
        dt0 = 0
        dt1 = 0        
        if out+'Start' not in Cs.TimingsSRS.keys():
            raise ValueError(out+'Start is not in SRS Timetable')    
        ref0 = Cs.TimingsSRS[out+'Start'][0]
        if on not in Cs.TimingsSRS.keys():
            try:            
                dt0 =  Times[on]
            except KeyError:
                raise ValueError(on + ' is not in Timetable')
            t0 =Cs.TimingsSRS['ExperimentStart'][0]
        else : 
            t0 =Cs.TimingsSRS[on][0]
            dt0 = -Cs.TimingsSRS[on][1]
        if t0 == ref0 :
            t0 =Cs.TimingsSRS['ExperimentStart'][0]
            dt0 +=  Times[on]
        
        if off not in Cs.TimingsSRS.keys():
            try:             
                dt1 = Times[off]            
            except KeyError:
                raise ValueError(off + ' is not in Timetable')
            t1 =Cs.TimingsSRS['ExperimentStart'][0] 
        else : 
            t1 =Cs.TimingsSRS[off][0]
            dt1 = Cs.TimingsSRS[off][1]
        if t1==ref0+1:
            dt1 = Times[off]  +dt1
            t1 =Cs.TimingsSRS['ExperimentStart'][0] 
            
        cmd = 'DLAY '+unicode(ref0)+','+unicode(t0)+','+unicode(dt0)
        self.instr.write(cmd)
        cmd = 'DLAY '+unicode(ref0+1)+','+unicode(t1)+','+unicode(dt1)
        self.instr.write(cmd)
        
    def WriteTimings(self,TimeTable,Amplitudes):
        # Timings Settings
        self.instr.write('DLAY 4,0,'+unicode(1e-5-Cs.DelayAOMnpOn+TimeTable[2]))
        self.instr.write('DLAY 5,4,'+unicode(-Cs.DelayAOMnpOff+TimeTable[3]))
        self.instr.write('DLAY 2,4,'+unicode(-Cs.DelayAOM7sOn +TimeTable[0]))
        if TimeTable[1] :
            self.instr.write('DLAY 3,2,'+unicode(Cs.DelayAOM7sOff +TimeTable[1]))
        else :
            self.instr.write('DLAY 3,5,'+unicode(Cs.DelayAOM7sOff))
        if TimeTable[4]==np.nan:
            self.instr.write('DLAY 6,4,'+unicode(-Cs.DelayMWOn))
        else : 
            self.instr.write('DLAY 6,5,'+unicode(-Cs.DelayMWOn+TimeTable[4]))
        if TimeTable[5] :
            self.instr.write('DLAY 7,6,'+unicode(-Cs.DelayMWOff +TimeTable[5]))
        else :
            self.instr.write('DLAY 7,8,'+unicode(-Cs.DelayMWOff))
        self.instr.write('DLAY 8,5,'+unicode(TimeTable[6]))
        self.instr.write('DLAY 9,8,'+unicode(TimeTable[7]))
        
        # Amplitude Settings (Level, Offset, Polarity)
        self.instr.write('LAMP 1,'+unicode(Amplitudes[0][0]))
        self.instr.write('LAMP 2,'+unicode(Amplitudes[1][0]))
        self.instr.write('LAMP 3,'+unicode(Amplitudes[2][0]))
        self.instr.write('LAMP 4,'+unicode(Amplitudes[3][0]))

        self.instr.write('LOFF 1,'+unicode(Amplitudes[0][1]))
        self.instr.write('LOFF 2,'+unicode(Amplitudes[1][1]))
        self.instr.write('LOFF 3,'+unicode(Amplitudes[2][1]))
        self.instr.write('LOFF 4,'+unicode(Amplitudes[3][1]))
        
        self.instr.write('LPOL 1,'+unicode(Amplitudes[0][2]))
        self.instr.write('LPOL 2,'+unicode(Amplitudes[1][2]))
        self.instr.write('LPOL 3,'+unicode(Amplitudes[2][2]))
        self.instr.write('LPOL 4,'+unicode(Amplitudes[3][2]))

                
    def storedparams(self):
        try : 
            self.instr.write("ADVT 1")   #advanced trigerring on
            self.instr.write('HOLD 0')  #Trigger HoldOff
            self.instr.write('INHB 1')
            self.instr.write('PRES 0,1')  #prescale factors
            self.instr.write('PRES 1,1')
            self.instr.write('PRES 2,1')
            self.instr.write('PRES 3,1')
            self.instr.write('PRES 4,1')
            self.instr.write('SSHD 1e-9')  # SS = steps of various quantities
            self.instr.write('SSPH 1,1')   
            self.instr.write('SSPH 2,1')
            self.instr.write('SSPH 3,1')
            self.instr.write('SSPH 4,1')
            self.instr.write('SSPS 0,1')
            self.instr.write('SSPS 1,1')
            self.instr.write('SSPS 2,1')
            self.instr.write('SSPS 3,1')
            self.instr.write('SSPS 4,1')
            self.instr.write('SSTL +1e-2')
            self.instr.write('TLVL +1.4')   #external trig level
            self.instr.write('TSRC 2')  #external trigger failing edge
            self.instr.write('LAMP 0,5')   # Level Channel 0
            self.instr.write('LOFF 0,0')   # Offset Channel 0
            self.instr.write('LPOL 0,1')   # Polarity Channel 0
            self.instr.write('SSDL 2,1e-7')  #Step delays for different channels
            self.instr.write('SSDL 3,1e-7')
            self.instr.write('SSDL 4,1e-6')
            self.instr.write('SSDL 5,5e-8')
            self.instr.write('SSDL 6,5e-8')
            self.instr.write('SSDL 7,5e-8')
            self.instr.write('SSDL 8,2e-7')
            self.instr.write('SSDL 9,1e-6')
            self.instr.write('SSLA 1,0.5')
            self.instr.write('SSLA 2,1')
            self.instr.write('SSLA 3,0.1')
            self.instr.write('SSLA 4,1')
            self.instr.write('SSLO 1,0.5')
            self.instr.write('SSLO 2,0.5')
            self.instr.write('SSLO 3,0.5')
            self.instr.write('SSLO 4,0.5')

        except :
            error = sys.exc_info()[0]
            print type(error), error
            print 'Sleeping to check the error'
            from time import sleep
            sleep(3)
            
    def close(self):
        if self.instr.ask('UNLK?'):
            self.instr.close()
        else :
            raise InstrError('SRSGen','release lock')
            
    def ReadAllChannels(self):
        res = []        
        for i in range(8):
            ref, dlay = self.instr.ask('DLAY ? '+str(i+2)).split(',')
            res.append((int(ref),float(dlay)))
        return res
        
        
    def __del__(self):
        try :
            self.instr.close()
        except :
            pass
            
if __name__=="__main__":
    a = SRSDelayGen()

        
