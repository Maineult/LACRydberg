# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 11:14:27 2017
"""
'''
Victor Barua for UBC QDG Lab
Summer 2013
Controller for Point Grey Flea2 Camera (Model FL2G-13S2M-C). Instances of the
PointGreyController class are linked to physical cameras. Methods to set camera
parameters and execute its functions are available through its instance.
Notes
- The Region of Interest must be set when the controller is created.


Henri Lehec
Modified for Ytterbium Rydberg Expiment (L.A.C.) 

'''

from PointGreyTypes import *
from ctypes import *
from struct import pack, unpack
from math import ceil
import time
from numpy import binary_repr
from scipy import misc
import numpy as np
import array as ar
FCDriver = CDLL('FlyCapture2_C')
import win32event

# FlyCapture2 C Documentation:
# http://www.ptgrey.com/support/downloads/documents/flycapture/Doxygen/html/index.html

# ----- Conversion Functions ----- #

def hexifier(fl):
	'''
	Converts a floating point number into a number corresponding to its
	hexadecimal/binary representation. Used to convert settings values for input
	into the camera registers.
	'''
	# Taken from:
	# http://stackoverflow.com/questions/1922771/python-obtain-manipulate-as-integers-bit-patterns-of-floats
	s = pack('>f', fl)
	''.join('%2.2x' % ord(c) for c in s)
	i = unpack('>l', s)[0]
	return i
 
def intifier(h):
	'''
	Converts a hexademical value returned by the camera registers to a int
	point number.
	'''
	h = h & 0x7ffff #Correct up to 18 bits
	return h

	
def floatifier(h):
	'''
	Converts a hexademical value returned by the camera registers to a floating
	point number.
	'''
	neg = h & 0x80000000
	h = h & 0x7fffffff
	s = pack('>l', h)
	''.join('%2.2x' % ord(c) for c in s)
	i = unpack('>f', s)[0]
	if neg:
		i = -i
	return i
	
def f2cImToRawData(fcIm):
    print 'beforarr',time.clock()
    for i in range(len(fcIm)):
        SepfcIm=ar.array('B', fcIm[i].pData)
        print 'befreshape',time.clock()
    return SepfcIm

# ----- Region of Interest ----- #

class ROIError(Exception):
	"""For errors in setting the Region of Interest"""
	def __init__(self, value, issueStr):
		self.msg = issueStr + str(value)
		
	def __str__(self):
		return repr(self.msg)

class ROI(object):
	"""Defines the region of interest of the camera."""
	
	def __init__(self):
		self.posLeft = 0
		self.posTop = 0
		self.width = 1288
		self.height = 964
	
	def __str__(self):
		return 'Left: %d, Top: %d, Width: %d, Height: %d' % (self.posLeft, self.posTop, self.width, self.height)
	
	def checkValues(self):
		"""Verifies validity of ROI Values for Flea2 Camera."""
		posTop = self.posTop	#Pixels from left to start ROI.
		posLeft = self.posLeft	#Pixels from top to start ROI.
		width = self.width		#Width (in pixels) of the ROI.
		height = self.height	#Height (in pixels) of the ROI.
		if not(0 <= posTop < 964):
			raise ROIError(posTop, 'Top must be between 0 and 964. Currently ') 
		if not(0 <= posLeft < 1288):
			raise ROIError(posLeft, 'Left must be between 0 and 1288. Currently ')
		if not(0 <= width <= 1288):
			raise ROIError(width, 'Width must be between 0 and 1288. Currently ') 
		if not(0 <= height <= 964):
			raise ROIError(posTop, 'Height must be between 0 and 964. Currently ') 
		if not(0 < posTop + height <= 964):
			raise ROIError(posTop + height, 'Top + height must be <= 964. Currently ')
		if not(0< posLeft + width <= 1288):
			raise 0 < ROIError(posLeft + width, 'Left + width must be <=1288. Currently ')
	
	def setROI(self, posLeft, posTop, width, height):
		"""Sets the ROI parameters explicitly."""
		self.posLeft = posLeft - posLeft % 2	#Pixels from left to start ROI.
		self.posTop = posTop - posTop % 2  #Pixels from top to start ROI.
		self.width = int(ceil(width / 8.) * 8) 	#Width (in pixels) of the ROI.
		self.height = height + height % 2		#Height (in pixels) of the ROI.
		self.checkValues()
	
	def setROICenter(self, center, width, height):
		"""
		Sets the ROI parameters based on where the image center should be along
		with the width and height. center is a tuple of the form (x, y).
		"""
		x = center[0]
		y = center[1]
		self.width = int(ceil(width / 8.) * 8) 	#Width (in pixels) of the ROI.
		self.height = height + height % 2		#Height (in pixels) of the ROI.
		hWidth = self.width/2
		hHeight = self.height/2
		l = x - hWidth
		t = y - hHeight
		self.posLeft = l - l % 2
		self.posTop = t - t % 2
		self.checkValues()	

# ----- Timestamp ----- #

class Timestamp(object):
	"""Used to store and decode image timestamps."""

	secondsPerCount = 1./8000.
	countsPerOffset = 1./3072.
	
	def __init__(self, s, c, o):
		self.seconds = s
		self.count = c
		self.offset = o
	
	def decodeTime(self):
		"""Converts timestamp to time in seconds based on FlyCapture Documentation."""
		return self.seconds + (self.count+self.offset*self.countsPerOffset)*self.secondsPerCount
		
# ----- Point Grey Controller ----- #

def handleError(errorCode):
	'''
	Wrapper for FlyCapture2 API function calls which handles the error
	codes it returns.
	'''
	if errorCode == 19:
		print "!!!!! Image buffer retrieval timed out. The most likely cause is that there was not enough time between triggers."
	if errorCode:
		raise flyCaptureError(errorCode)
		
class propertyError(Exception):
	"""For errors when setting camera property values"""
	def __init__(self, name, value, min, max, units):
		msg = '%s %f %s is outside of range %f to %f.' % (name, value, units, min, max)
		
	def __str__(self):
		return repr(self.msg)		

class flyCaptureError(Exception):
	'''For errors returned from FlyCapture2 API calls'''
	def __init__(self, errorCode):
		self.errorCode = errorCode
		self.msg = fc2ErrorCodeStrings[self.errorCode]
		
	def __str__(self):
		return repr(self.msg)
	
class PointGreyCamera(object):
	
	def __init__(self):
		self.boostFramerate =False
		
		context = fc2Context()
		handleError(FCDriver.fc2CreateContext(byref(context)))		
		self.context = context
		guid = fc2PGRGuid()
		handleError(FCDriver.fc2GetCameraFromIndex(context, 0, byref(guid)))
		self.guid = guid
		handleError(FCDriver.fc2Connect(context, byref(guid)))
		
		# Re-initialize and re-power camera.
#		self.setRegister(fc2Register['Initialize'], 0x80000000)
#		self.setRegister(fc2Register['Power'], 0x80000000)
		
		# Set camera configuration and enable embedded image timestamps
		#self.enableTimestamps()
		
		# Set camera region of interest.
 		#self.setImageSettings()
		
#		# Disables unused camera settings.
		self.setRegister(fc2Register['AutoExposure'], 0x40000000)
		self.setRegister(fc2Register['Sharpness'], 0x40000000) 
		self.setRegister(fc2Register['Gamma'], 0x40000000) 
		self.setRegister(fc2Register['Pan'], 0x40000000)
		self.setRegister(fc2Register['Tilt'], 0x40000000)
##
##		# Initialize Gain and Shutter settings.
		self.setRegister(fc2Register['Gain'], 0x42000000)
		self.setRegister(fc2Register['Shutter'], 0x42000000)
#		self.setRegister(0x604,0xE0000000)
#		self.setRegister(0x600,0xA0000000)
#		self.setRegister(0x608,0xE0000000)
#          self.config=self.getConfig()
#          self.config.grabMode=1
#          self.
		self.ImageSettings=self.getImageSettings()
		self.ImageSettings.pixelFormat=c_int(0x00100000)
		self.ImageSettings.mode=7
		if self.validateImageSettings():
			self.setImageSettings()
		else:
			print 'ERROR IMAGE SETTINGS'
		self.enableHardwareTrigger()
  		#self.trig_event_signal = win32event.CreateEvent(None, False, False, '')
          
	def __test__(self):
         self.ImageSettings=self.getImageSettings()
         if self.ImageSettings.mode==7:
             return True
         else:
             return False    
         
 
     
	def StartCapture(self):
		'''Readies camera to capture images when triggered.
           '''
		#self.setConfig(self.numOfImages)
		context = self.context
		self.TrigEvent=0    
		handleError(FCDriver.fc2StartCapture(context))
  
	def StartCaptureCb(self):
		'''
          Readies camera to capture images when triggered.
          '''
		#self.setConfig(self.numOfImages)
		context = self.context
		self.TrigEvent=0
		CBF=CallBFunc(self.NewTrigEvent)
		handleError(FCDriver.fc2StartCaptureCallback(context,CBF,byref(c_void_p(None))))
  
	def __del__(self):
		context = self.context
		handleError(FCDriver.fc2DestroyContext(context))
	
	def StopCapture(self):
		'''Stop collecting images'''
		context = self.context
#		self.processData()
		handleError(FCDriver.fc2StopCapture(context))
		#handleError(FCDriver.fc2DestroyContext(context))
	
	def setDataBuffers(self, numOfImages):
		'''Sets up the data buffers to which the camera will save data. '''
		self.timestamps = [float()]*numOfImages
		self.rawImageData = [self.initializeImage() for _ in range(numOfImages)]
		#self.conImageData = [self.initializeImage() for _ in range(numOfImages)]
		
# ----- Save Functions
	
	def saveRAWImages(self, fNameFormat = 'rawImage_%03d.raw'):
		'''Saves the images collected in raw format.'''
		images = self.conImageData
		for i in range(len(images)):
			im = images[i]
			fname = fNameFormat % i
			fname = c_char_p(fname)
			handleError(FCDriver.fc2SaveImage(byref(im), fname, fc2ImageFileFormat['RAW']))	
		
	def savePNGImages(self, fNameFormat = 'image_%03d.png'):
		'''Save the images collected in png format.'''
		images = self.conImageData
		for i in range(len(images)):
			im = images[i]
			fname = fNameFormat % i
			fname = c_char_p(fname)
			handleError(FCDriver.fc2SaveImage(byref(im), fname, fc2ImageFileFormat['PNG']))	
		
	def saveLog(self, fpath = 'log.txt'):
		"""Creates and saves a log file for the image collection run"""
		timestamps = self.timestamps
		outfile = open(fpath, 'w')
		outfile.write('Collection Date and Time: %s\n' % (time.strftime("%y%m%d_%H%M%S")))
		outfile.write('Exposure Time: %.3f ms, Gain: %d dB\n' % (self.expTime_ms, self.gain))
		if self.roi:
			outfile.write(str(self.roi))
		else:
			outfile.write('Full Region Utilized (1280x960)')
		outfile.write('\n\nImage #: Time Since First Image (ms) - Processing Status\n')
		for i in range(len(timestamps)): 
			outfile.write('Image %d: {%.14f}\n' % (i, timestamps[i]))
		outfile.close()	

# ----- Settings Functions
	
	def getExposureTime(self):
		'''Get camera exposure time in ms.'''
		t = self.getRegister(0x918)
		t = 1000. * floatifier(t)
		return t
	
	def setExposureTime(self, ms):
		'''Set camera exposure time. Input is in ms.'''
		min = 1000 * floatifier(self.getRegister(0x910))
		max = 1000 * floatifier(self.getRegister(0x914))
		if min < ms < max:
			s = float(ms / 1000.)
			s = hexifier(s)
			self.setRegister(0x918, s)
		else:
			raise propertyError('Exposure time', ms, min, max, 'ms')
		
	def getGain(self):
		'''Get camera gain level in db.'''
		g = self.getRegister(0x928)
		g = floatifier(g)
		return g
		
	def setGain(self, db):
		'''Set camera gain leve in db.'''
		min = floatifier(self.getRegister(0x920))
		max = floatifier(self.getRegister(0x924))
		if min < db < max:
			db = float(db)
			db = hexifier(db)
			self.setRegister(0x928, db)
		else:
			raise propertyError('Gain', db, min, max, 'db')

# ----- Image Handling Functions
	
	def initializeImage(self):
		'''Initializes an fc2Image object for use with camera.'''
		img = fc2Image()
		handleError(FCDriver.fc2CreateImage(byref(img)))
		return img
	
	def retrieveImages(self):
		'''Retrieves all of the images collected by the camera.'''
		context = self.context
		rawDat = self.rawImageData
		for i in range(len(rawDat)):
			im = rawDat[i]  
			handleError(FCDriver.fc2RetrieveBuffer(context, byref(im)))    
		
	def convertImages(self):
		'''
		Converts all the images collected by the camera based on the BGR
		pixel format.
		'''
		rawDat = self.rawImageData
		conDat = self.conImageData
		for i in range(len(rawDat)):
			handleError(FCDriver.fc2ConvertImageTo(fc2PixelFormat['BGR'], byref(rawDat[i]), byref(conDat[i])))

	def setImageSettings(self):
		'''Used to set custom image sizes.'''
		context = self.context
		percentSpeed = c_float(20)
		handleError(FCDriver.fc2SetFormat7Configuration(context, byref(self.ImageSettings) ,percentSpeed))
	
	def getImageSettings(self):
         context=self.context
         Settings=fc2Format7ImageSettings()
         size=c_int(0)
         perc=c_float(0)
         handleError(FCDriver.fc2GetFormat7Configuration(context, byref(Settings),byref(size),byref(perc))) 
         return Settings

	def validateImageSettings(self):
         	context = self.context
		boulle = c_bool(0)
		size=c_int(0)    
		handleError(FCDriver.fc2ValidateFormat7Settings(context, byref(self.ImageSettings) ,byref(boulle),byref(size)))
		return boulle.value
				
# ----- Triggering Functions

	def enableSoftwareTrigger(self):
		'''Enable software triggering of camera.'''
		context = self.context
		triggerMode = fc2TriggerMode()
		triggerMode.onOff = True
		triggerMode.mode = 0;
		triggerMode.parameter = 0;
		triggerMode.source = 7;
		handleError(FCDriver.fc2SetTriggerMode(context, byref(triggerMode)))
		self.TrigMode='Software'
	
	def fireSoftwareTrigger(self):
		'''
		Triggers the camera when software triggering is enabled. Will wait
		until trigger is available before triggering.
		'''
		context = self.context
		while self.getRegister(0x62C):
			pass
		handleError(FCDriver.fc2FireSoftwareTrigger(context))		
	
	def enableHardwareTrigger(self):
		'''Enable hardware triggering of camera.'''
		context = self.context
		triggerMode = fc2TriggerMode()
		triggerMode.onOff = True
		triggerMode.mode = 0;
		triggerMode.parameter = 0;
		triggerMode.source = 0;
		handleError(FCDriver.fc2SetTriggerMode(context, byref(triggerMode)))
		self.TrigMode='External'
		while (self.getRegister(0x62C) & 0x001):
			pass		

# ------- EVENT HANDLING
	def EnableRegisterEvent(self):
         context = self.context
         EventOpt=fc2EventOptions()
         CMPFUNC=CFUNCTYPE(c_int)
         cfunc=CMPFUNC(self.TrigEvent)
         EventOpt.EventCallbackFcn=CMPFUNC(cfunc)
         EventOpt.EventName=c_char_p("EventExposureEnd")
         handleError(FCDriver.fc2RegisterEvent(context, byref(EventOpt)))
         
  

	def WaitForTrigEvent(self, timeout):
         ret = win32event.WaitForSingleObject(self.trig_event_signal, timeout)
         if ret == win32event.WAIT_TIMEOUT:
            print 'TimeOutError'
            return False
         else:
             self.TrigEvent=self.TrigEvent+1
             return True

	def NewTrigEvent(self,arg1):
         self.TrigEvent= self.TrigEvent + 1
         print 'ExtTrigEvent' 

	def WaitForBufferEvent(self,n):
         context=self.context
         image=self.rawImageData[0]
         n=c_int(n)
         handleError(FCDriver.fc2WaitForBufferEvent(context,byref(image),n))
         
         
# ----- Timestamp Functions
	
	def enableTimestamps(self):
		'''Turns embedded image timestamps on.'''
		self.setRegister(0x12F8, 0x00000001)
	
	def parseTimestamp(self, im):
		'''Parses the timestamp encoded in an image.'''
		def binarify(n, d):
			'''
			Outputs a string representing the binary representation of n with d digits,
			adding padding zeros on the left.
			'''
			s = binary_repr(n)
			while len(s) < d:
				s = '0' + s
			return s
		
		data = im.pData
		byteString = data[0:4]
		byteArray = map(ord, byteString)
		acc = ''
		for b in byteArray:
			acc += binarify(b, 8)
		try:
			secondCount = int(acc[0:7], 2)
			cycleCount = int(acc[7:20], 2)
			cycleOffset = int(acc[20:], 2)
		except ValueError:
			print "Timestamp failure occurred in PointGreyController."
			return -1
		timestamp = Timestamp(secondCount, cycleCount, cycleOffset)
		return timestamp.decodeTime()
			
	def extractTimestamps(self):
		'''
		Extracts all of the timestamps from the images collected and stores
		them in the timestamps array.
		'''
		timestamps = self.timestamps
		for i in range(len(timestamps)):
			im = self.rawImageData[i]
			ts = self.parseTimestamp(im)
			timestamps[i] = ts
		t0 = timestamps[0]
		for i in range(len(timestamps)):
			timestamps[i] = (timestamps[i] - t0) * 1000
	
# ----- Register Manipulation Functions	

	def getRegister(self, addr):
		'''
		Gets the value of the camera register at the given address.'''
		context = self.context
		val = c_ulong()
		handleError(FCDriver.fc2ReadRegister(context, addr, byref(val)))
		return val.value
	
	def setRegister(self, addr, val):
		'''
		Sets the value of the camera register at the given address to the
		given value.
		'''
		context = self.context
		val = c_uint(val)
		handleError(FCDriver.fc2WriteRegister(context, addr, val))
		
# ----- Leftover Functions

	def processData(self):
		'''
		Process the data retrieved by the camera into a form that
		is easily saveable.
		''' 
		self.retrieveImages()
		#self.convertImages()
		self.extractTimestamps()

	def setConfig(self):
		'''Sets the camera configuration.'''
		context = self.context
		handleError(FCDriver.fc2SetConfiguration(context, byref(self.config)))

	def clearBuffer(self):
		'''
		Clears out image buffer. Used to ensure that images from previous
		runs are removed from camera. DOES NOT WORK I Replace the cleaning by 
          a START/STOP capture
                      
		''' 
		context = self.context
		im = self.initializeImage()
		while True:           
			e	= FCDriver.fc2RetrieveBuffer(context, byref(im))
			if e == 18:	# Timeout
				
				return 'Time out'
			elif e != 0:	# Other error
				handleError(e)
				
	def getConfig(self):
         context = self.context
         config = fc2Config()
         handleError(FCDriver.fc2GetConfiguration(context, byref(config)))
         return config
         
#    	def setImageSize(self):
#         context = self.context
#         rows=c_uint(1536)
#         cols=c_uint(2048)
#         stride=c_uint(2048)
#         Format=c_uint(self.ImageSettings.pixelFormat)
#         BayerFormat=c_uint(0)         
#         handleError(FCDriver.fc2GetConfiguration(,))
#         return config
#        
    
	def getImages(self,image):
		ImageTime=time.ctime()
		ImageName=unicode(ImageTime)
		handleError(FCDriver.fc2SaveImage(byref(image),ImageName, fc2ImageFileFormat['PGM']))
		return ImageTime
    
	def getImageData(self):
         images=self.rawImageData
         for i in range(len(images)):
			print 'i',i
			ch=c_char_p('r')
			im = images[i]
			handleError(FCDriver.fc2GetImageData(byref(im), byref(ch)))
         return ch
         
	def DataStringDecode(self,DataSt):
         print 'st',time.clock()          
         DataSt=DataSt.encode('hex').encode()
         print 'befVect°',time.clock()
         if self.ImageSettings.pixelFormat==c_int(0x80000000).value: ##Case 8 bits
             VecDataSt=[DataSt[i:i+2] for i in range(0,len(DataSt),2)]
         elif self.ImageSettings.pixelFormat==c_int(0x00100000).value: 
             VecDataSt=[DataSt[i:i+3] for i in range(0,len(DataSt),3)] ##Case 12 bits
         else:
             'Err'
             VecDataSt=None
         print 'afVect°',time.clock()
         rows=self.rawImageData[0].rows
         cols=self.rawImageData[0].cols
         return VecDataSt
          
     
         
   ################################   
   ### HIGH LEVEL FUNCTIONS #######
   # For Rydberg Yb Experiment ####    
   ################################      
         
	def WriteExposure(self,ExposureTime):
         self.setExposureTime(ExposureTime)


	def WriteGain(self,Gain):
         self.setGain(Gain)

	def ReadSingleImageCapture(self):
         self.setDataBuffers(1)
         self.StartCapture()
         self.WaitForBufferEvent(0)
         self.StopCapture()
         return self.getImages(self.rawImageData[0])

	def ReadDoubleImageCapture(self):
         self.setDataBuffers(1)
         self.StartCapture()
         self.WaitForBufferEvent(0)
         t1=time.clock()
         img=self.rawImageData[0]
         self.setDataBuffers(1)
         self.WaitForBufferEvent(0)
         t2=time.clock()
         if t2-t1 > 105e-3:
             self.setDataBuffers(1)
             self.WaitForBufferEvent(0)
             print 'Failed Trig'
             return None,None
         self.StopCapture()
         refimg=self.rawImageData[0]
         return self.getImages(img)
         
#
#    def grabImageToBuffer(self):
#        c = self.context
#        dll.fc2RetrieveBuffer(c, byref(self.imgRaw))
#        # Convert image to 8-bit grayscale.
#        imgConv = Fc2Image()
#        dll.fc2CreateImage(byref(imgConv))
#        dll.fc2ConvertImageTo(0x80000000, byref(self.imgRaw), byref(imgConv))
#        # When the DLL assignes to imgRaw.pData, it becomes a long in python.
#        # Recast to a pointer to a byte array.
#        src = imgConv
#        p = ctypes.cast(src.pData, ctypes.POINTER(ctypes.c_ubyte))
#        data = np.fromiter(p, np.uint8, src.cols * src.rows)
#        self.lastImage = data.reshape((src.rows, src.cols))
                
            









  
if __name__ == '__main__':
	# Usage Example
    
 	import matplotlib.pyplot as plt  
 	PGC = PointGreyCamera()
 	PGC.WriteExposure(1)
   	PGC.WriteGain(20)
 	img=PGC.ReadSingleImageCapture()
 	print time.clock()
 	plt.imshow(img)  
     
  
  
  
  

  
     

    
     
                    