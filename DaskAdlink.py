# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 16:41:12 2016

@author: lac
"""

import ctypes as c
from DacAdlinkConstants import *
from ctypes.wintypes import WORD, INT, UINT, ULONG, DOUBLE,HWND,BOOL,HANDLE,FLOAT
import os



ChannelCompDic={'V_NW':0,'V_NE':1,'V_SW':2,'V_SE':3,'V_U':4}
ChannelDic={'V_NW':0,'V_NE':1,'V_SW':2,'V_SE':3,'V_U':4,'Ioniz':5,'MCP':6,'PiezoMoglabs':7}
lib=c.WinDLL('PCI-Dask')

class DACCard():
    def __init__(self):
        i=0
        NotInit=True    
        while NotInit: 
            try: 
                lib.Register_Card(c.c_int16(PCI_6208V),c.c_int16(i))
                NotInit=False
            except:
                NotInit=True
                i=i+1
            self.Card_Number = i
    
    def __test__(self):
        try :
            OnSenTape1=c.c_int32(0)
            OnSenTape2=c.c_int32(0)
            lib.GetBaseAddr(c.c_int16(self.Card_Number),c.byref(OnSenTape2),c.byref(OnSenTape1))
            Test=True
        except:
            Test=False
        return Test    
        

    def writevoltage(self,Channel,Voltage):
       lib.AO_VWriteChannel(c.c_int16(self.Card_Number),c.c_int16(Channel),c.c_double(Voltage))
       
    def Relaunch(self):
        i=0
        NotInit=True    
        while NotInit: 
            try: 
                lib.Register_Card(c.c_int16(PCI_6208V),c.c_int16(i))
                NotInit=False
            except:
                NotInit=True
                i=i+1
            self.Card_Number = i
            
    def close(self):
        lib.Release_Card(c.c_int16(self.Card_Number))
                
    
    
    
class ElectricFields(DACCard):
    def __init__(self,Card):
        self.DACCard=Card
        self.V_EW=0
        self.V_NS=0
        self.V_U=0
        
    
    def __test__(self):
        '''
        Ok a bit Dangerous
        '''
        return True
        
    def Relaunch():
        self.DACCard=Card
        self.V_EW=0
        self.V_NS=0
        self.V_U=0
        
    def close():
        pass
        
        
    def writecompensationfield(self,V_EW,V_NS,V_U):
        '''
        To write compensation field: Convention is V_NS > 0 ==> Field towards South 
                                                   V_EW>0 ==> Field towards West
        V_NS is really the voltage applied on the men field applied on the north (NOT V_NS/2) 
        and -V_NS is the voltage on the south                                            
        '''
        self.V_EW=V_EW
        self.V_NS=V_NS
        self.V_U=V_U
        self.V_NW,self.V_NE,self.V_SW,self.V_SE,self.V_U=self.calculatevoltages(V_EW,V_NS,V_U)
        for Channel in ChannelCompDic.keys():
            V=getattr(self,Channel)
            ChannelNum=ChannelCompDic[Channel]
            self.DACCard.writevoltage(ChannelNum,V)
    
    def calculatevoltages(self,V_EW,V_NS,V_U):
        if V_EW==0:
            V_NW=-V_NS/2
            V_NE=-V_NS/2
            V_SW=V_NS/2
            V_SE=V_NS/2
            return V_NW,V_NE,V_SW,V_SE,V_U
        elif V_NS==0:
            V_NW=V_EW/2
            V_NE=-V_EW/2
            V_SW=V_EW/2
            V_SE=-V_EW/2
            return V_NW,V_NE,V_SW,V_SE,V_U
        else:
            V_NW=(-V_NS+V_EW)/2
            V_SE=-(-V_NS+V_EW)/2
            V_SW=(V_NS+V_EW)/2
            V_NE=-(V_NS+V_EW)/2
            return V_NW,V_NE,V_SW,V_SE,V_U
            

        
    def WriteMCPVoltage(self,V_MCP):
        if V_MCP < 1800:
            Vdac=float(V_MCP)/168  #### 168 corresponds to the Voltage divider to be changed if needed 
            self.DACCard.writevoltage(ChannelDic['MCP'],Vdac)
        else:
            print 'VOLTAGE TOO HIGH NOT APPLIED'
            

    def WriteIonizationVoltage(self,V_Ioniz):
        if V_Ioniz < 4500:
            VDac=float(V_Ioniz)/2000+60./2000   #### the  60./2000 is just a correction to apply the real  
            self.DACCard.writevoltage(ChannelDic['Ioniz'],VDac)
        else:
            print 'VOLTAGE TOO HIGH NOT APPLIED'
        


    def WriteEWField(self,V_EW):
        self.writecompensationfield(V_EW,self.V_NS,self.V_U)
        
    
    def WriteNSField(self,V_NS):
        self.writecompensationfield(self.V_EW,V_NS,self.V_U)
        
    
    def WriteUField(self,V_U):
        self.writecompensationfield(self.V_EW,self.V_NS,V_U)
        

class PiezoMoglabs(DACCard):
    def __init__(self,Card):
        self.DacCard=Card
        
    def WriteVoltage(self,Voltage):
        self.DacCard.writevoltage(ChannelDic['PiezoMoglabs'],Voltage)
        
    def WriteScanTest(self):
        from time import sleep
        j=0
        while j<5:
            for i in range(100):
                sleep(0.02)
                self.WriteVoltage(0.5*i/100)
            j=j+1
        


    


    
    
    
 


