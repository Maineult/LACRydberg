# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 17:02:20 2016

@author: lac
"""

PCI_6208V =    1
PCI_6208A= 2
PCI_6308V= 3
PCI_6308A= 4
PCI_7200=  5
PCI_7230=  6
PCI_7233=  7
PCI_7234=  8
PCI_7248=  9
PCI_7249=  10
PCI_7250=  11
PCI_7252=  12
PCI_7296=  13
PCI_7300A_RevA= 14
PCI_7300A_RevB =15
PCI_7432=  16
PCI_7433=  17
PCI_7434=  18
PCI_8554=  19
PCI_9111DG=20
PCI_9111HR=21
PCI_9112=  22
PCI_9113=  23
PCI_9114DG=24
PCI_9114HG=25
PCI_9118DG=26
PCI_9118HG=27
PCI_9118HR=28
PCI_9810=  29
PCI_9812=  30
PCI_7396=  31
PCI_9116=  32
PCI_7256=  33
PCI_7258=  34
PCI_7260=  35
PCI_7452=  36
PCI_7442=  37
PCI_7443=  38
PCI_7444=  39
PCI_9221=  40
PCI_9524=  41
PCI_6202=  42
PCI_9222=  43
PCI_9223=  44
PCI_7433C= 45
PCI_7434C= 46
PCI_922A=  47
PCI_7350=  48
PCI_7360=  49
PCI_7300A_RevC =50

MAX_CARD=  32


#/*
# * Error Number
# */
NoError=0
ErrorUnknownCardType=  -1
ErrorInvalidCardNumber=-2
ErrorTooManyCardRegistered =-3
ErrorCardNotRegistered=-4
ErrorFuncNotSupport=   -5
ErrorInvalidIoChannel= -6
ErrorInvalidAdRange=   -7
ErrorContIoNotAllowed= -8
ErrorDiffRangeNotSupport=   -9
ErrorLastChannelNotZero  =  -10
ErrorChannelNotDescending  =-11
ErrorChannelNotAscending =  -12
ErrorOpenDriverFailed= -13
ErrorOpenEventFailed=  -14
ErrorTransferCountTooLarge= -15
ErrorNotDoubleBufferMode  = -16
ErrorInvalidSampleRate=-17
ErrorInvalidCounterMode =   -18
ErrorInvalidCounter=   -19
ErrorInvalidCounterState =  -20
ErrorInvalidBinBcdParam =   -21
ErrorBadCardType= -22
ErrorInvalidDaRefVoltage=   -23
ErrorAdTimeOut    =         -24
ErrorNoAsyncAI    =         -25
ErrorNoAsyncAO    =         -26
ErrorNoAsyncDI    =         -27
ErrorNoAsyncDO    =         -28
ErrorNotInputPort   =       -29
ErrorNotOutputPort   =      -30
ErrorInvalidDioPort  =      -31
ErrorInvalidDioLine  =      -32
ErrorContIoActive =         -33
ErrorDblBufModeNotAllowed = -34
ErrorConfigFailed      =    -35
ErrorInvalidPortDirection  =-36
ErrorBeginThreadError  =    -37
ErrorInvalidPortWidth   =   -38
ErrorInvalidCtrSource  =    -39
ErrorOpenFile    =          -40
ErrorAllocateMemory   =     -41
ErrorDaVoltageOutOfRange =  -42
ErrorDaExtRefNotAllowed  =  -43
ErrorDIODataWidthError  =   -44
ErrorTaskCodeError     =    -45
ErrortriggercountError  =   -46
ErrorInvalidTriggerMode  =  -47
ErrorInvalidTriggerType  =  -48
ErrorInvalidCounterValue =  -50
ErrorInvalidEventHandle  =  -60
ErrorNoMessageAvailable  =  -61
ErrorEventMessgaeNotAdded = -62
ErrorCalibrationTimeOut  =  -63
ErrorUndefinedParameter =   -64
ErrorInvalidBufferID    =   -65
ErrorInvalidSampledClock =  -66
ErrorInvalidOperationMode = -67
#/*Error number for driver API*/
ErrorConfigIoctl     =      -201
ErrorAsyncSetIoctl     =    -202
ErrorDBSetIoctl       =     -203
ErrorDBHalfReadyIoctl  =    -204
ErrorContOPIoctl      =     -205
ErrorContStatusIoctl  =     -206
ErrorPIOIoctl         =     -207
ErrorDIntSetIoctl      =    -208
ErrorWaitEvtIoctl      =    -209
ErrorOpenEvtIoctl       =   -210
ErrorCOSIntSetIoctl    =    -211
ErrorMemMapIoctl       =    -212
ErrorMemUMapSetIoctl   =    -213
ErrorCTRIoctl         =     -214
ErrorGetResIoctl       =    -215
ErrorCalIoctl          =    -216
ErrorPMIntSetIoctl     =    -217



#/*
# * AD Range
# */
AD_B_10_V    = 1
AD_B_5_V      =2
AD_B_2_5_V   = 3
AD_B_1_25_V   =4
AD_B_0_625_V  =5
AD_B_0_3125_V =6
AD_B_0_5_V    =7
AD_B_0_05_V   =8
AD_B_0_005_V  =9
AD_B_1_V      =10
AD_B_0_1_V    =11
AD_B_0_01_V   =12
AD_B_0_001_V  =13
AD_U_20_V     =14
AD_U_10_V     =15
AD_U_5_V      =16
AD_U_2_5_V    =17
AD_U_1_25_V   =18
AD_U_1_V      =19
AD_U_0_1_V    =20
AD_U_0_01_V   =21
AD_U_0_001_V  =22
AD_B_2_V      =23
AD_B_0_25_V   =24
AD_B_0_2_V    =25
AD_U_4_V      =26
AD_U_2_V      =27
AD_U_0_5_V    =28
AD_U_0_4_V    =29


#/*Synchronous Mode*/
SYNCH_OP  =1
ASYNCH_OP =2

#/*AO Terminate Mode*/
DA_TerminateImmediate =0

#/*DIO Port Direction*/
INPUT_PORT  =1
OUTPUT_PORT =2
#/*DIO Line Direction*/
INPUT_LINE  =1
OUTPUT_LINE =2

#/*Clock Mode*/
TRIG_SOFTWARE   =      0
TRIG_INT_PACER   =     1
TRIG_EXT_STROBE   =    2
TRIG_HANDSHAKE     =   3
TRIG_CLK_10MHZ      =  4
TRIG_CLK_20MHZ   =     5
TRIG_DO_CLK_TIMER_ACK= 6
TRIG_DO_CLK_10M_ACK  = 7
TRIG_DO_CLK_20M_ACK  = 8

#/*Virtual Sampling Rate for using external clock as the clock source*/
CLKSRC_EXT_SampRate= 10000

#/*Register by slot*/
RegBySlot= 0x8000


#/*DIO & AFI Voltage Level*/
VoltLevel_3R3= 0
VoltLevel_2R5 =1
VoltLevel_1R8= 2


#/*---------------------------------------------*/
#/* Constants for PCI-6208A/PCI-6308A/PCI-6308V */
#/*---------------------------------------------*/
#/*Output Mode*/
P6208_CURRENT_0_20MA= 0
P6208_CURRENT_4_20MA =3
P6208_CURRENT_5_25MA =1
P6308_CURRENT_0_20MA =0
P6308_CURRENT_4_20MA =3
P6308_CURRENT_5_25MA =1
#/*AO Setting*/
P6308V_AO_CH0_3=      0
P6308V_AO_CH4_7 =     1
P6308V_AO_UNIPOLAR=   0
P6308V_AO_BIPOLAR  =  1


