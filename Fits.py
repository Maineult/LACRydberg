# -*- coding: utf-8 -*-
"""
Created on Tue May 03 15:24:17 2016

@author: Wilfried
"""

import numpy as np
from lmfit import models as m
from math import pi

def fitLorentz(x,y,**kwargs):
    #kwargs can be directly passed to lmfil minimize function
    x= np.array(x)
    y= np.array(y)
    Lorentz = m.LorentzianModel()
    background  = m.ConstantModel()
    model = Lorentz + background
    pars0 = background.make_params(c = y.mean())
    pars = pars0 + Lorentz.guess(y,x=x)
    out = model.fit(y, pars, x=x,**kwargs)
    for x0 in np.linspace(x.min(),x.max(),20):
        pars = pars0+Lorentz.make_params(sigma = x.ptp()*1./10,center = x0,amplitude = -y.ptp()*x.ptp()*1./10*pi)
        out1 = model.fit(y, pars, x=x,**kwargs)
        if out.chisqr>out1.chisqr:
            out = out1
        pars = pars0+Lorentz.make_params(sigma = x.ptp()*1./10,center = x0,amplitude = -y.ptp()*x.ptp()*1./10*pi)
        out1 = model.fit(y, pars, x=x,**kwargs)
        if out.chisqr>out1.chisqr:
            out = out1
    return out
    

    
if __name__=='__main__' : 
    from matplotlib import pyplot as plt
 #   x = np.linspace(-10,10,20) 
 #   y = 1/(1+x**2)+ 0.5*np.random.normal(0, 0.2, len(x))
    x=np.array(toto[0])
    y=np.array(toto[1])
 #   out,redraw = fitLorentz(x,y)
    out = fitLorentz(x,y)
    plt.plot(x,y,'b+')
    plt.plot(x,out.best_fit,'r*')    
    xes = np.linspace(x.min(),x.max(),200)
    yes = out.eval(x=xes)
    plt.plot(xes,yes)
#    plt.plot(redraw(0),redraw(1),'g-')    
#    
    


