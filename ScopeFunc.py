# -*- coding: utf-8 -*-
"""
Scope Additional Functions
"""


import numpy as np
import ivi
import warnings
import time
from ExperimentConstants import ComputerDict,gh,NewLog
import sys
import os

#Modifs to ivi to make the thing work easily : 
# Replace lecroy subdir of anaconda/Lib/site-packages/ivi with YtterbiumPython/lecroy dir.
class Scope(ivi.lecroy.lecroyWR610ZI) : 
    def __init__(self, manip,Channel,Simulate=False) :
        self.Simulate = Simulate
        if Simulate:
            return
        manip=ComputerDict[gh()]
        if manip is 'Ytterbium' : 
            self.mso =ivi.lecroy.lecroyWR610ZI("TCPIP::129.175.56.49::INSTR")
        if manip is 'Cesium' :
            self.mso = ivi.lecroy.lecroyWR104XIA("TCPIP::129.175.56.11::INSTR")
        self.traceLength = 50000
        self.Channel = Channel
            
    def __test__(self):
        if self.Simulate:
            return True
        manip=ComputerDict[gh()]
        if manip == 'Cesium':
            return u'LECROY,WR104MXI-A,LCRY0617N51437,7.8.0' == self.mso._ask('*IDN?')
        elif manip =='Ytterbium':
            return 'LECROY,WR610ZI,LCRY2807N59417,7.4.0' == self.mso._ask('*IDN?')
        else:
            raise TypeError('NotImplemented  copy name of Ytterbium scope in there')


    def Relaunch(self):
        self.mso.close()
        from ivi import lecroy
        manip=ComputerDict[gh()]
        if manip is 'Ytterbium' : 
            Scope.mso =lecroy.lecroyWR610ZI("TCPIP::129.175.56.49::INSTR")
        if manip is 'Cesium' :
            Scope.mso = lecroy.lecroyWR104XIA("TCPIP::129.175.56.11::INSTR")

    def ReadTest(self):
        return self.__test__()        
            
    def WriteTrigger(self,mode):
        '''
        TriggerModes in ['auto', 'norm', 'single', 'stop']
        '''
        if self.Simulate:
            return 'norm'
        self.mso.trigger.mode = mode
        
    def ReadTrigger(self):
        '''
        TriggerModes in ['auto', 'norm', 'single', 'stop']
        '''
        if self.Simulate:
            return 'norm'
        return self.mso.trigger.mode
    
    def ReadChannel(self):
        if self.Simulate:
            return 0
        return self.Channel
  
    def ReadCustom(self, Number):
        Number=int(Number)
        if self.Simulate:
            return 0.4
        cmd = 'PAVA? CUST{!s}'.format(str(Number))
        resp = self.mso._ask(cmd).split(',')
        try : 
            if resp[0]==str(Number) :
                res = float(resp[1])
                return res
            else :
                raise TypeError('cannot extract param value from{}'.format(resp))
        except Exception as e:
            NewLog(e)
                
    
    def ReadParam(self,Channel,Param):
        '''
            Reads by default the PAram from channel i (python style)
            and returns it
        '''
        if self.Simulate:
            return 0.1
        cmd = 'C{!s}:PAVA? {!s}'.format(str(Channel+1),Param)
        resp = self.mso._ask(cmd).split(',')
        try : 
            if resp[0]==Param :
                res = float(resp[1])
                return res
            else :
                raise TypeError('cannot extract param value from{}'.format(resp))
        except Exception as e:
            NewLog(e)
        
    def ReadNewTrig(self):
        if self.Simulate:
            from time import time
            t1 = time()
            if hasattr(self,'t0') and (t1-self.t0)<0.1:
                return False
            self.t0=t1
            return True
        return self.mso._ask('INR?')
            
    def ReadData(self,Channel):
        ''' Channel number is codes python style (ie ch 0,1,2,3 instead of 1,2,3,4)'''
        if self.Simulate:
            temp = [(i,np.exp(-(i-4000)**2/1000)) for i in range(10000)]
#        try : 
        if True : 
            temp = self.mso.channels[Channel].measurement.fetch_waveform()


#        except Exception as e:
#            NewLog(e)
#            return (range(self.traceLength),map(Zero,range(self.traceLength)))

        self.traceLength = len(temp)
        x=np.zeros([self.traceLength,1])
        y=np.zeros([self.traceLength,1])
        for i,data in enumerate(temp) :
            (x[i,0],y[i,0])= data   
        #y=self.CheckTrace(y)
        return x,y
    
    def ReadData0(self):
        return self.ReadData(0)
                    
    def close(self) :
        self.mso.close()
    
    def ReadVoltScale(self,Channel):
        cmd = 'C{!s}:VDIV?'.format(str(Channel+1))
        return float(self.mso._ask(cmd))
        
    def WriteVoltScale(self,Channel,Scale):
        '''
        Works for any Scale, but do not use variable gain is probably better
        (1,2,5,10,20 ...)
        '''        
        if int(Scale):
            suffix = str(int(Scale))+'V'
        else : 
            suffix = str(int(1000*Scale))+'mV'
        cmd = 'C{!s}:VDIV {!s}'.format(str(Channel+1),suffix)
#        self.mso._write(cmd)
        
def testres(test):
    sigmas = {}    
    title = ['y>sigma','y<-sigma','y>2*sigma','y<-2sigma','y>3*sigma','y<-3*sigma','y>4*sigma','y<-4*sigma']
    
    for i, name in enumerate(title) :
        temp = np.array([a[i] for a in test])
        sigmas[name] = [temp.mean(),temp.std(),temp.ptp()]
    for name in title[::2]:
        sigmas[name+'Norm']=[]
        for i,elem in enumerate(sigmas[name]):           
            sigmas[name+'Norm'].append(elem/(sigmas['y>sigma'][0]))
        
    for name in title[1::2]:
        sigmas[name+'Norm']=[]
        for i,elem in enumerate(sigmas[name]):
            sigmas[name+'Norm'].append(elem/(sigmas['y<-sigma'][0]))
    return sigmas
        
def IsDetectableSignal(y0):
    '''
        Set with small signal : shoud detect an excess of points out of gaussian distribution
        Set with large signal : subset statistics would be significantly different.
    '''
    yres = [y0.mean(),y0.std(),y0.ptp()]
    n = len(y0)
    p1 =  len(np.extract((y0-yres[0])>yres[1] ,y))*1./n
    pm1 = len(np.extract((y0-yres[0])<-yres[1] ,y))*1./n
    p2 = len(np.extract((y0-yres[0])>2*yres[1] ,y))*1./n
    pm2 = len(np.extract((y0-yres[0])<-2*yres[1] ,y))*1./n
    p3 = len(np.extract((y0-yres[0])>3*yres[1] ,y))*1./n
    pm3 = len(np.extract((y0-yres[0])<-3*yres[1] ,y))*1./n
    p4 = len(np.extract((y0-yres[0])>4*yres[1] ,y))*1./n
    pm4 = len(np.extract((y0-yres[0])<-4*yres[1] ,y))*1./n        
    
    return [p1,pm1,p2,pm2,p3,pm3,p4,pm4]  


    

if __name__=="__main__":
    if gh() =='lac57-95':
        a=Scope('Cesium',0,False)
        nTrig = 0
        
        
    if not gh() =='lac57-95':
        a=Scope('Cesium',0,False)

        x,y = a.ReadData(0)
        tot = 100
        table = np.zeros((tot,y.shape[0]))
        from time import time
        t0=time()
        for i in range(2):
            x,y = a.ReadData(3)
            time.sleep(4)            #temp.append(IsDetectableSignal(y))
        print time()-t0
    elif gh()=='lac57-95':
        import pickle
        Arr=[]
        a=Scope('Ytterbium',0,False)
        for i in range(5):
            time.sleep(1)
            x,y=a.ReadData(1)
            Arr.insert(i,y)
   #     filee=open('DispCarac8.75','wb')
    #    pickle.dump(dic,filee)
     #   filee.close()
        y=np.average(Arr,0)    
        import matplotlib.pyplot as plt
        Data=np.array([x,y])
        plt.plot(1e6*x,y)
        plt.grid('on')
        l=np.linspace(-25.7,50,1000)
        yy=[]
        i=0
        for xx in l:
            yy.insert(i,50*np.exp(-(xx+25.7)/2))
            i=i+1
       # plt.plot(l,yy)    
            
        np.savetxt('SwitchingVoltageBehlkeRC2us.txt',Data)

#            
#            
#    listscales = [0.02,0.05,0.1,0.2,0.5,1]
#    CalibScope = {}
#    for scale in listscales : 
#        a.WriteVoltScale(0,scale)
#        temp = []
#        for i in range(100):
#            x,y = a.ReadData(0)
#            temp.append(IsDetectableSignal(y))
#        CalibScope[str(scale)+'V'] = testres(temp)
#    
#
#            
#
#    
#            
#    
#    