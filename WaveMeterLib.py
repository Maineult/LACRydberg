# -*- coding: utf-8 -*-
"""
Created on Wed Sep 09 12:02:31 2015

@author: lac
"""
from ExperimentConstants import *
if gh()=='brique' : 
    from ctypes import cdll as windll
    from ctypes import c_long, c_double,byref,c_int
else : 
    from ctypes import windll, c_long, c_double,byref,c_int
import ctypes
from wlmConstants import *
from time import clock
import socket

from time import sleep,localtime





# Comment

class WLMError(Exception):
    def __init__(self, message):
        super(WLMError,self).__init__(message)

class WMeter:
    def __init__(self,Channel,UseLocal=0,SecondDevice=0,Simulate=False):
        self.Channel = Channel
        self.Simulate = Simulate 
        name = gh()
        self.UseLocal=0
        if name=='lac57-95' and SecondDevice:            
            self.UseLocal = 1
        if Simulate :
            return
        if self.UseLocal : 
            self.dll = windll.wlmData
            res = self.dll.ControlWLMEx(cCtrlWLMHide+cCtrlWLMStartSilent+cCtrlWLMWait, 0, 0, 5000, 1)
           # res = self.dll.ControlWLMEx(cCtrlWLMHide+cCtrlWLMStartSilent+cCtrlWLMWait, 0, 0)
            msg = ''    

            if res>=flErrUnknownError:
                msg+= 'Unknown Error'
                res-=flErrUnknownError
            if res>=flErrTemperatureError:
                msg+= 'Temperature Error - unable to report correct measurements'
                res-=flTemperatureError
            if res>=flErrUnknownSN:
                msg+= 'Unknown Serial Number'
                res-=flErrUnknownSN
            if res>=flErrWrongSN:
                msg+= 'Wrong Serial Number'
                res-=flErrWrongSN
            if res>=flErrUnknownDeviceError:
                msg+= 'Unknown Device Error'
                res-=flErrUnknownDeviceError
            if res>=flErrUSBError:
                msg+= 'USB Error'
                res-=flErrUSBError     
            if res>=flErrDriverError:
                msg+= 'Driver Error'
                res-=flErrDriverError
            if res>=flErrDeviceNotFound:
                NewLog(None,'Device Not Found',1)
                res-=flErrDeviceNotFound   
                self.UseLocal = 0
                ip = socket.gethostbyname(LambdameterServer )
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                
                self.socket.connect((ip,InternalComPort))            
                
                NewLog(None,'High Finesse Server reached successfully',2)
            else : 
                if res>=flServerStarted:
                    NewLog(None,'High Finesse Wavelength Meter started successfully',2)
                    res-=flServerStarted
                else:
                    msg += 'timeout - unable to start server'
        
        else : 
            ip = socket.gethostbyname(LambdameterServer)
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            
            self.socket.connect((ip,InternalComPort))
        self.WriteMeasurement(True)
        self.WriteContinuous()
        self.WriteShowApp(False) 
#        self.ReadCycleTime()

    def ReadLocal(self):
        if self.Simulate:
            self.UseLocal = 1
            return True
        return bool(self.UseLocal)

    def __test__(self):
        if self.Simulate:
            return True
        from time import sleep
        sleep(10)
        try : 
            a=self.ReadTemperature()
            if not 23<a<30:
                print "La température du lambdamètre est bizarre"
            return True
        except :
            return False
        
    def Relaunch(self):
        if self.UseLocal:
            self.dll.ControlWLMEx(cCtrlWLMHide+cCtrlWLMStartSilent+cCtrlWLMWait, 0, 0, 5000, 1)
        else : 
            self.close()
            ip = socket.gethostbyname(LambdameterServer)
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            
        
    def close(self):
        if self.UseLocal :
            self.WriteMeasurement(False)
            delattr(self,'dll')
        else :
            self.socket.close()
            delattr(self,'socket')
            

    def WriteShowApp(self,op):
        if self.Simulate:
            return
        if op : 
            opcode = cCtrlWLMShow
        else : 
            opcode = cCtrlWLMHide
        if self.UseLocal : 
            self.dll.ControlWLM(cCtrlWLMShow,0,0)
        else :
            msg = 'WriteShowApp('+self.EncodeStream({'op':op})+')'
            self.WriteRemote(msg)

        
    def WriteMeasurement(self,op):
        if op : 
            opcode = cCtrlStartMeasurement
        else : 
            opcode = cCtrlStopAll
        if self.UseLocal:
            self.dll.Operation(opcode)
        else : 
            msg = 'WriteMeasurement('+self.EncodeStream({'op':op})+')'
            self.WriteRemote(msg)
            
    def WriteRemote(self,msg):
        self.socket.send(msg)
        rep = self.DecodeStream(self.socket.recv(BUFFER_SIZE))
        if rep =='Done':
            return
        else :
            print msg+ 'did not work'
    
    def QueryRemote(self,msg):
        self.socket.send(msg)
        try: 
            res= self.DecodeStream(self.socket.recv(BUFFER_SIZE))
        except : 
            print msg + ' did not work'
            res = 0
        return res

        
        
    def WriteContinuous(self):
        if self.UseLocal:        
            self.dll.SetPulseMode(0)
        else : 
            msg = 'WriteContinuous()'
            self.WriteRemote(msg)
        
    def ReadTemperature(self):
        if self.UseLocal: 
            self.dll.GetTemperature.restype = c_double
            d=c_double(0.)
            return self.dll.GetTemperature(c_double(0))
        else : 
            msg = 'ReadTemperature()'
            return self.QueryRemote(msg)

        
    def ReadWavelength(self, Channel):
        #Channel or ccd can be none ?
        if self.Simulate : 
            return 375.355355
        if self.UseLocal:        
            self.dll.GetWavelengthNum.restype = c_double
            self.dll.TriggerMeasurement(cCtrlMeasurementContinue)
            if Channel :         
                Ch=c_long(Channel)        
                return self.dll.GetWavelengthNum(Ch, c_double(0))
            else : 
                return self.dll.GetWavelength(c_double(0))
        else : 
            msg = 'ReadWavelength('+self.EncodeStream({'Channel':Channel})+')'
            return self.QueryRemote(msg)
    
    def ReadCycleTime(self):
        if self.Simulate:
            return 1
        try :
            dt = localtime().tm_min-self.lasttime
        except AttributeError : 
            dt = 5
        if (not dt) and not (dt)%5 : 
            return self.CycleTime
        if self.UseLocal :
            mode = self.dll.GetSwitcherMode(0)
            if mode : 
                res = 0                
                Use = c_long(0)
                Show = c_long(0)
                for port in range(8):                
                    test = self.dll.GetSwitcherSignalStates(port,byref(Use),byref(Show))
                    if (not test) and Use.value : 
                        res += self.ReadExposure(port)
                self.CycleTime = res/1000.                                             
            else :
                self.CycleTime = self.ReadExposure(None)/1000.
            return self.CycleTime
        else : 
            msg = 'ReadCycleTime()'
            self.CycleTime = self.QueryRemote(msg)
            return self.CycleTime

    def ReadFrequency(self, Channel):
        #Channel or ccd can be none ?
        if self.Simulate:
            return 382.110890
        if self.UseLocal:        
            self.dll.GetFrequencyNum.restype = c_double
            self.dll.GetFrequency.restype = c_double
            self.dll.TriggerMeasurement(cCtrlMeasurementContinue)
            if Channel :         
                Ch=c_long(Channel)        
                res = self.dll.GetFrequencyNum(Ch, c_double(0))
            else : 
                res = self.dll.GetFrequency(c_double(0))   
            if res >0 : 
                return res
            self.ReadCycleTime()
            if res == ErrNoValue : 
                sleep(self.CycleTime)
                res = self.ReadFrequency(Channel)
                return res
            if res==ErrLowSignal :
                te = self.ReadExposure(Channel)
                if te<20 : 
                    self.WriteExposure(Channel,te+1)
                    sleep(self.CycleTime)
                else :
                    return res
            if res == ErrBigSignal :
                te = self.ReadExposure(Channel)
                self.WriteExposure(Channel,te-1)
                sleep(self.CycleTime)     
            res= self.ReadFrequency(Channel)
            return res
        else : 
            msg = 'ReadFrequency('+self.EncodeStream({'Channel':Channel})+')'
            return self.QueryRemote(msg) 
            
    def getCalWl(self):
        return self.dll.GetCalWavelength(c_int(0), c_double(0))
                
        
    def ReadExposure(self, Channel):  
        #Channel or ccd can be none ?
        if self.UseLocal : 
            ccd=c_long(Ccd)
            if Channel:
                Ch=c_long(Channel)
                return self.dll.GetExposureNum(Ch,ccd,c_long(0))        
            else : 
                return self.dll.GetExposure(c_long(0))   
        else : 
            msg = 'ReadExposure('+self.EncodeStream({'Channel':Channel})+')'
            return self.QueryRemote(msg)
                        
    
    def WriteExposure(self,Channel,expo) : 
        #Channel or ccd can be none ?  
        if self.UseLocal : 
            exp = c_long(expo)
            ccd=c_long(Ccd)
            if Channel:
                Ch=c_long(Channel)
                return self.dll.SetExposureNum(Ch,ccd,exp)        
            else : 
                return self.dll.SetExposure(c_long(0))   
        else : 
            msg = 'WriteExposure('+self.EncodeStream({'Channel':Channel,'expo':expo})+')'
            return self.QueryRemote(msg)

    def DecodeStream(self,foo):
        if foo[0]=='K':
            return int(foo[1:])
        if foo[0]=='W':
            return float(foo[1:])
        if foo[0]=='Z':
            return foo[1:]
        if foo[0]=='Y':
            return bool(int(foo[1:]))
        try : 
            toto = {}
            lists = foo.split(',')
            for l in lists :
                [key, value] = l.split('=')
                toto[self.DecodeStream(key)] = self.DecodeStream(value)
            return toto
        except Exception as e:
            print e
            raise TypeError('Decoding of Flux does not support {} type'.format(type(foo)))
        
        
    def EncodeStream(self,foo):
        if type(foo) == type(1):
            return 'K'+str(foo)
        if type(foo) == type(1.):
            return 'W'+str(foo)
        if type(foo) == type(''):
            return 'Z'+foo
        if type(foo) == type(True):
            return 'Y'+str(int(foo))
        if type(foo) == type({}) : 
            lists = [self.EncodeStream(key)+'='+self.EncodeStream(value) for key,value in foo.iteritems() ]
            return 'Q'+','.join(lists)
        raise TypeError('Encoding  of Flux does not support {} type'.format(type(foo)))
    
if __name__=="__main__":

    a = WMeter(1,SecondDevice=1)

