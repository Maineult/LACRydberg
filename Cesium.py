# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 11:45:16 2016

@author: wilfried
"""

import numpy as np
import CesiumConstants as Cs
import lmfit as Fit
import matplotlib.pyplot as plt

class Cesium():
    def __init__(**kwargs):
        #super(,Cesium)
        1+1
        self.MCPVolt=1500
        self.MCPGain = self.CalcMCPGain()
        
        
        
        
    def VsToRydbergs(self,Vs):
        
        if type(Vs)==type(np.array([])) or type(Vs*1.0)==type(1.0):
            NbIons = Vs*self.MCPGain/Cs.MCPQuEff
        else : 
            raise TypeError
        return NbIons            
        
        
    def CalcMPCGain(self):
        