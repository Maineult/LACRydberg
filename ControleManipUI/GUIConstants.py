# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 13:34:36 2016

@author: Wilfried
"""

from ExperimentConstants import *
from PyQt4 import QtGui as G

yellow = 'background-color: rgb(255, 255, 178);\n'
red = 'background-color: rgb(255, 166, 124);\n'
green = 'background-color: rgb(142, 211, 148);\n'
gold = 'background-color: rgb(255, 220, 93);  \n'
grey = 'background-color: rgb(244, 244, 244);\n'
yellowgrey = 'background-color: rgb(200, 195, 140);\n'
greygrey = 'background-color: rgb(200, 200, 200);\n'
goldgrey= 'background-color: rgb(211, 193, 112);\n'

def getrgb(prop) : 
    try : 
        bef,aft = prop.split('rgb(')
        num,aft = aft.split(')')
        cr,cg,cb = num.split(',')
        return '{},{},{}'.format(cr,cg,cb)        
    except Exception as e : 
        NewLog(e,'Invalid Individual line style specification',1)

blacktop = 'border-top-color: rgb(0,0,0);\n'
blackright = 'border-right-color: rgb(0,0,0);\n'
blackleft = 'border-left-color: rgb(0,0,0);\n'
blackbottom = 'border-bottom-color: rgb(0,0,0);\n'
border = 'border-color: rgb(0,0,0);\n'

Stop = 0
Running = 1
Suspended = 2
Starting = 3
NotApplicable = -1

def SetStyle(obj,prop):
    if not type(prop) == str:
        return
    if not issubclass(obj.__class__,G.QWidget):
        return
    if not prop[-2:]== ';\n':
        if prop[-1]==';':
            prop+='\n'
        prop+=';\n'
    name = obj.property('styleSheet')
    if not type(name)==str:
        name = ''
    pname,pvalue =  prop.split(':')
    pos = name.find(pname)
    if pos+1 : 
        cmd0,cmd = name.split(pname)
        pos = cmd.find(';\n')
        cmd1 = cmd[pos+2:]
        name = cmd0+prop+cmd1
    else :
        name += prop
    obj.setProperty('styleSheet',name)


    