# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 12:02:40 2016

@author: Wilfried
"""

from PyQt4 import QtGui as G
from PyQt4 import QtCore as C

MAXSIZE = 16777215
dico = {0:'NotUsed',1:'WaitToConnect',2:'InitOK'}

from GUIConstants import *

class GUIStyle():    
       
    def Gold(self):
        SetStyle(self,gold)
           
    def Green(self):
        SetStyle(self,green)
        
    def Red(self):
        SetStyle(self,red)
        
    def Yellow(self):
        SetStyle(self,yellow)    
        
    def Grey(self):
        SetStyle(self,grey)      
        
    def GreyGrey(self):
        SetStyle(self,greygrey)  
        
    def YellowGrey(self):
        SetStyle(self,yellowgrey)  
    
    def GoldGrey(self):
        SetStyle(self,goldgrey)  

class InstrLayout(G.QLayout):  
    def __init__(self):
        super(InstrLayout,self).__init__()
        self.setContentsMargins(6,6,6,6)
        self.setSpacing(12)
    
    
    
class InstrHLayout(G.QHBoxLayout,InstrLayout):
    def __init__(self):
        super(InstrHLayout,self).__init__()
        
class InstrVLayout(G.QVBoxLayout,InstrLayout):
    def __init__(self):
        super(InstrVLayout,self).__init__()
        
class InstrGridLayout(G.QGridLayout,InstrLayout):
    def __init__(self,qparent):
        super(InstrGridLayout,self).__init__(qparent)

class ClickAndColor(G.QLabel,GUIStyle):
    def __init__(self,msg,QParent=None):
        super(ClickAndColor,self).__init__(' ',QParent)
        t = C.QSize(30,30)
        self.setMaximumSize(t)
        t = C.QSize(20,20)
        self.setMinimumSize(t)
        self.setAlignment(C.Qt.AlignCenter)
        self.Status(msg)
        self.setSizePolicy(G.QSizePolicy.Minimum,G.QSizePolicy.Minimum) 

    def Status(self,msg):
        if type(msg)==type(1):
            func = getattr(self,dico[msg])
            self._status = dico[msg]
        else : 
            func = getattr(self,msg)
            self._status = msg
        func()
        
    def mousePressEvent(self, event):
        NewLog(None,'clicked on '+self.objectName(),2)
        self.emit(C.SIGNAL('clicked()'))

    def setText(self,msg):
        pass
        
    def NotUsed(self):
        self.Red()
        
    def WaitToConnect(self):
        self.Gold()    

    def InitOK(self):
        self.Green()    
        
class InstrOptions(G.QLabel,GUIStyle):
    clicked = C.pyqtSignal()  
    ''' msg : text to show, Parent : obvious '''
    def __init__(self,msg,QParent) : 
        super(InstrOptions,self).__init__(msg,QParent)
        self.Yellow()
        t = C.QSize(16777215,30)
        self.setMaximumSize(t)
        t = C.QSize(0,20)
        self.setMinimumSize(t)
        self.setAlignment(C.Qt.AlignCenter)
        self.setSizePolicy(G.QSizePolicy.Minimum,G.QSizePolicy.Minimum)
        
    
    def mousePressEvent(self, event):
        self.clicked.emit()
        
class ClickTimes(InstrOptions):
    
    def On(self): 
        self.state = True
        self.Red()
        
    def Off(self): 
        self.Yellow()
        self.state = False
        
class InstrLabel(InstrOptions):
    pass



    
        
class InstrEdit(G.QLineEdit,GUIStyle):
    editingFinished = C.pyqtSignal()
    createNewEvent= C.pyqtSignal()
    
    def __init__(self,msg,QParent) :
        super(InstrEdit,self).__init__(msg,QParent)
        self.Yellow()
        self.activated = 1
        t = C.QSize(16777215,30)
        self.setMaximumSize(t)
        t = C.QSize(0,20)
        self.setMinimumSize(t)
        self.setAlignment(C.Qt.AlignCenter)
        self.setContextMenuPolicy(C.Qt.CustomContextMenu)
        self.oldtext = msg
        self.installEventFilter(self)
        self.setSizePolicy(G.QSizePolicy.Minimum,G.QSizePolicy.Minimum) 
        
        
    def eventFilter(self, obj, event):
        if event.type() == C.QEvent.KeyPress and event.key() ==C.Qt.Key_Return :        
            self.nativeParentWidget().setFocus()      
        return False
        
    def Activate(self,b):
        if b : 
            self.Yellow()
            self.activated =1
        else :
            self.YellowGrey()
            self.activated = 0
            
    def IsActivated(self):
        if self.activated==1:
            return True
        else:
            return False  
            
    def mouseDoubleClickEvent(self,QMouseEvent):
        if self.activated:
            self.createNewEvent.emit()
            G.QLineEdit.mouseDoubleClickEvent(self,QMouseEvent)
                        
            
    def focusOutEvent(self,FEvent):
        if not self.oldtext == self.text():
            self.oldtext = self.text()[:]
            self.editingFinished.emit()
        G.QLineEdit.focusOutEvent(self,FEvent)
        
    def focusInEvent(self,FEvent):
        if self.activated:
            self.oldtext = self.text()
            G.QLineEdit.focusInEvent(self,FEvent)
        else : 
            pass
        
class InstrBox(G.QComboBox,GUIStyle):
    def __init__(self,l,i=0,QParent=None):
        super(InstrBox,self).__init__(QParent)
#        self.addItem('')
        self.addItems(l)
        self.activated==0
        self.currentIndexChanged.connect(self.SetColor)    
        t = C.QSize(30,20)
        self.setMinimumSize(t)
        t = C.QSize(150,30)
        self.setMaximumSize(t)
        self.setCurrentIndex(i)
        self.setSizePolicy(G.QSizePolicy.Expanding,G.QSizePolicy.Expanding) 
        
    def SetColor(self,i):
        if i>0 : 
            self.Yellow()
            self.activated=1
        else : 
            self.YellowGrey()
            self.activated=0
            
    def IsActivated(self):
        if self.activated==1:
            return True
        else:
            return False    
            
#    def clear(self):
#        G.QComboBox.clear(self)
#        self.addItem('')
        
class InstrSpinBox(G.QSpinBox,GUIStyle):
    def __init__(self,num=0,mini=None,maxi=None,QParent=None):
        super(InstrSpinBox,self).__init__(QParent)
        if mini:
            self.setMinimum(mini)
        if maxi:
            self.setMaximum(maxi)
        self.Yellow()
        t = C.QSize(30,20)
        self.setMinimumSize(t)
        t = C.QSize(150,30)
        self.setMaximumSize(t)
        self.setSizePolicy(G.QSizePolicy.Expanding,G.QSizePolicy.Expanding) 
        self.setValue(num)  
                
        
class InstrFloatBox(G.QDoubleSpinBox,GUIStyle):
    def __init__(self,num=0,step=None,mini=None,maxi=None,QParent=None):
        super(InstrFloatBox,self).__init__(QParent)
        self.setValue(num)  
        if mini:
            self.setMinimum(mini)
        if maxi:
            self.setMaximum(maxi)
        if step:
            self.setSingleStep(step)
        self.Yellow()
        t = C.QSize(30,20)
        self.setMinimumSize(t)
        t = C.QSize(150,30)
        self.setMaximumSize(t)
        self.setSizePolicy(G.QSizePolicy.Expanding,G.QSizePolicy.Expanding) 
        
        
class InstrButton(G.QPushButton,GUIStyle):
    def __init__(self,msg,QParent) : 
        super(InstrButton,self).__init__(msg,QParent)
        self.Red()
        t = C.QSize(40,30)
        self.setMaximumSize(t)
        t = C.QSize(0,20)
        self.setMinimumSize(t)
        self.setSizePolicy(G.QSizePolicy.Minimum,G.QSizePolicy.Minimum) 
        
class ThreadButton(G.QPushButton,GUIStyle):
    ThreadSignal = C.pyqtSignal(int)  
    Locked = C.pyqtSignal()
    
    def __init__(self,msg,val,QParent) : 
        super(ThreadButton,self).__init__(msg,QParent)
        self.Red()
        t = C.QSize(100,30)
        self.setMaximumSize(t)
        t = C.QSize(0,20)
        self.setMinimumSize(t)
        self.val = val
        self.SetState(self.val)
        self.connect(self,C.SIGNAL('clicked()'),self.ChangeState)
        self.setSizePolicy(G.QSizePolicy.Minimum,G.QSizePolicy.Minimum) 
        
        
    def ChangeState(self,value = 0):
        print 'Changing State', value
        if self.sender() is self and not self.val == NotApplicable : 
            if not value : 
                self.ThreadSignal.emit(Starting)
            else : 
                self.ThreadSignal.emit(Stop)
        else :
            if value ==Suspend :
                self.SetState(Suspended)
                
    def ChangeText(self,text):
        self.setText(text)
        
    def SetState(self,val):
        self.val= val
        #print 'SetState', val, self.text()
        if val == Running: 
            self.Green()
            self.Locked.emit()
        elif val == Suspended:
            self.Red()
        elif val == Stop : 
            self.Yellow()
        elif val == NotApplicable :
            self.GreyGrey()    
            self.Locked.emit()
        
        

class TimeValidator(G.QValidator):
    def validate(self,val,i):
        def testnum(s):
            try : 
                return type(float(s))==type(2.) 
            except : 
                return False
        if len(val)<2 :
            return (G.QValidator.Intermediate,val,i)
        try : 
            if not 's' in val[-2:] :
                if testnum(val) :
                    return (G.QValidator.Intermediate,val,i)
                else : 
                    return (G.QValidator.Invalid,val,i)
        except Exception as e : 
            return (G.QValidator.Invalid,val,i)    
        if val[-2] ==u'µ':
            digit = val[:-2]
        elif val[-2] == u'm':
            digit = val[:-2]
        elif val[-2] == u'n':
            digit = val[:-2]
        elif testnum(val[-2]) :
            digit = val[:-1]
        else :
            return (G.QValidator.Invalid,val,i)
        if testnum(digit):
            self.parent().setText(val)
            
            self.value = float(digit)*Unit[val[len(digit):]]
            return (G.QValidator.Acceptable,val,i)            
        else : 
            return (G.QValidator.Invalid,val,i)
        
    def fixup(self,s):
        try :
            return unicode(float(s))+u'µs'
        except : 
            pass





class TimingStruct(G.QWidget,GUIStyle):
    UpdateTime = C.pyqtSignal(str,str,list)  
    rightclicked = C.pyqtSignal()
#    TimingInstrStatus = C.pyqtSignal()   
    UpdateTimings = C.pyqtSignal(list)
      
    def __init__(self,ExpModel,QParent):
        '''
        liston is the places where digital signal should be on, in widget pixel size
        should drad
        
        '''
        from time import time
        self.lastclick = time()
        super(TimingStruct,self).__init__(QParent)
        self.TimeTable = ExpModel.TimeTable
        self.ExpModel = ExpModel
        self.TimingFrame = QParent.TimingFrame
        self.TimingLayout = QParent.TimingLayout
        self.MW = QParent
        self.RefreshTimes()
        u = InstrLabel('',self.TimingFrame)
        u.setFixedHeight(2)
        u.Red()
        w =self.TimingLayout.columnCount()
        self.TimingLayout.addWidget(u,1,0,1,w)
        self.TimingLayout.setColumnMinimumWidth(0,80)
        self.RefreshChannels()        
        self.MW.TimingInstrStatus.connect(self.Refresh)
        
    def Refresh(self):
        self.TimeTable = self.ExpModel.TimeTable       
        self.RefreshTimes()
        self.RefreshChannels()
        
            
    def BuildNewEvent(self):
        u = self.sender()
        i=u.xPosition
        listtimes = [self.TimingLayout.itemAtPosition(0,j).widget().text() for j in range(1,self.TimingLayout.columnCount())]       
        timeint = [float(timeint[:-2])*1e-6 for timeint in listtimes if timeint[-2:] == u'µs']
        listnewtimes = timeint[:i-1] + [timeint[i-1]/2]+[timeint[i-1]/2]+ timeint[i:]      
        self.TimeTable['Times']=listnewtimes
        for Instr in self.ExpModel.TimingInstr : 
            ListChan = getattr(self.ExpModel,Instr)['Channels']
        for Chan in ListChan:
            Chan=Instr+Chan
            self.TimeTable[Chan].insert(i,self.TimeTable[Chan][i-1])
        self.TimeTable['TimeTags'].insert(i,'NewTimeTag')
       
        self.RefreshTimes()
        self.RefreshChannels()        

    def RemoveEvent(self,i):
        print 'there'
        listtimes = [self.TimingLayout.itemAtPosition(0,j).widget().text() for j in range(1,self.TimingLayout.columnCount())]
        timeint = [float(timeint[:-2])*1e-6 for timeint in listtimes if timeint[-2:] == u'µs']
        listnewtimes = timeint[:i]+ timeint[i+1:]      
        self.TimeTable['Times']=listnewtimes
        for Instr in self.ExpModel.TimingInstr : 
            ListChan = getattr(self.ExpModel,Instr)['Channels']
        for Chan in ListChan:
            Chan=Instr+Chan
            noreturn=self.TimeTable[Chan].pop(i)
        noreturn=self.TimeTable['TimeTags'].pop(i)
        self.RefreshTimes()
        self.RefreshChannels()
        self.RefreshTimes()
        print  self.TimeTable
        
    def RefreshTimes(self):
        t0 = 0
        for i,time in enumerate(self.TimeTable['Times']) :             
            try :
                it = self.TimingLayout.itemAtPosition(0,i+1)
                u = it.widget()
                u.xPosition = i+1
                u.setText(u'{:.3f}µs'.format(1e6*time))
                #print 'here'
            except :
                u = InstrEdit(u'{:.3f}µs'.format(1e6*time),self.TimingFrame)
                u.setMaximumWidth(80)
                u.setValidator(TimeValidator(u))
                u.Yellow()
                u.xPosition = i+1
                self.TimingLayout.addWidget(u,0,i+1)
                #print 'there'
                u.editingFinished.connect(self.NewTimes)
                u.createNewEvent.connect(self.BuildNewEvent)
            setattr(u,'TimeName',self.TimeTable['TimeTags'][i+1])

            #setattr(u,'oldtext',u'{:.3f}µs'.format(time) )
            
        self.toto= [self.TimingLayout.itemAtPosition(0,j) for j in range(1,self.TimingLayout.columnCount())]
     
          
          
        for j in range(i+2,self.TimingLayout.columnCount()):
            try : 
                it = self.TimingLayout.itemAtPosition(0,j)
                wid = it.widget()
                self.TimingLayout.removeItem(it)
                wid.hide()
                wid.destroy()
            except Exception as e: 
                NewLog(e,' Cannot delete time widget' , 1)
                 
    
    def RefreshChannels(self):
         
        nr = 0 
        w = self.TimingLayout.columnCount()
        for Instr in self.ExpModel.TimingInstr : 
            ListChan = getattr(self.ExpModel,Instr)['Channels']
            for Chan in ListChan : 
                try : 
                    self.TimingLayout.itemAtPosition(2*nr+2,0).Channel
                except AttributeError : 
                    n = InstrLabel(Chan,self.TimingFrame)
                    n.setMaximumWidth(80)
                    setattr(n,'Instrument',Instr)
                    setattr(n,'Channel',Chan)
                    self.TimingLayout.addWidget(n,2*(nr+1),0)
                if self.ExpModel._InstrList[Instr] == InitOK : 
                    n.Yellow()
                    setattr(n,'Status',InitOK)
                else :
                    n.YellowGrey()
                    setattr(n,'Status',IsNotInit)
                
                
                line = self.TimeTable[Instr+Chan]
                #print nr, line, n.Status, self.ExpModel._InstrList[Instr]
                for i in range(1,w):
                    try : 
                        m=self.TimingLayout.itemAtPosition(2*nr+2,i).widget()
                    except AttributeError : 
                        m = ClickTimes('',self)
                        self.TimingLayout.addWidget(m,2*nr+2,i)
                    if line[i-1] :
                        m.On()
                        if n.Status : 
                            m.Gold()
                            m.clicked.connect(self.TimingUpdate)
                        else:
                            m.GoldGrey()
                            try : 
                                m.clicked.disconnect()
                            except :
                                pass
                    else :
                        m.Off()
                        if n.Status : 
                            m.Grey()
                            m.clicked.connect(self.TimingUpdate)
                        else:
                            m.GreyGrey()
                            try : 
                                m.clicked.disconnect()
                            except :
                                pass
                try :
                    nr +=1
                    self.TimingLayout.itemAtPosition(2*nr+1,0).widget()
                except AttributeError: 
                    u = InstrLabel('',self.TimingFrame)
                    u.setFixedHeight(2)
                    u.Red()
                    nr +=1
                    self.TimingLayout.addWidget(u,2*nr+1,0,1,w)
                   
                    
    def TimingUpdate(self):
        from time import time
        tmp = time()
        if tmp-self.lastclick<0.3 : 
            return
        else : 
            self.lastclick = tmp
        obj = self.sender()
        n = self.TimingLayout.indexOf(obj)
        (row,col,spx,spy) = self.TimingLayout.getItemPosition(n)     
        InstrLabel = self.TimingLayout.itemAtPosition(row,0).widget()
        liststate = [self.TimingLayout.itemAtPosition(row,j).widget().state for j in range(1,self.TimingLayout.columnCount())]
        liststate[col-1] = not liststate[col-1]
        liststate = [int(e) for e in liststate]
        self.UpdateTime.emit(InstrLabel.Instrument,InstrLabel.Channel,liststate)  
         
        
    def NewTimes(self):
        from time import time
        tmp = time()
        if tmp-self.lastclick<0.2 : 
            return
        else : 
            self.lastclick = tmp
        self.nativeParentWidget().setFocus()
        listtimes = [self.TimingLayout.itemAtPosition(0,j).widget().text() for j in range(1,self.TimingLayout.columnCount())]
        try : 
            timeint = [float(timeint[:-2])*1e-6 for timeint in listtimes if timeint[-2:] == u'µs']
            if not len(timeint) is len(listtimes):
                raise ValueError('format of times in the GUI = {:.3f}µs')
            for i,times in enumerate(timeint):
                if times == 0.:
                    print 'here'
                    self.RemoveEvent(i)
            print 'timeint', timeint        
            self.UpdateTimings.emit(timeint)
        except Exception as e : 
            NewLog(e,'',1)

                
class InstrDict(G.QTreeWidget,GUIStyle):
    def __init__(self,dic,QParent):
        super(InstrDict,self).__init__(QParent)
        self.Yellow()
        t = C.QSize(300,16777215)
        self.setMaximumSize(t)
        t = C.QSize(0,20)
        self.setMinimumSize(t)
        self.showDic(dic)
        self.header().hide()
        
    def fill_item(self,item, value):
        if type(value) is dict:
            for key, val in sorted(value.iteritems()):
                child = G.QTreeWidgetItem()
                child.setText(0, unicode(key))
                item.addChild(child)
                self.fill_item(child, val)
        elif type(value) is list:
            for val in value:
                child = G.QTreeWidgetItem()
                item.addChild(child)
                if type(val) is dict:      
                    child.setText(0, '[dict]')
                    self.fill_item(child, val)
                elif type(val) is list:
                    child.setText(0, '[list]')
                    self.fill_item(child, val)
                else:
                    child.setText(0, item.data(0,0))
                    child.setText(1,unicode(val))       
                    child.setExpanded(False)
        else:
            child = G.QTreeWidgetItem()
            child.setText(0, item.data(0,0))
            child.setText(1,unicode(value))
            item.addChild(child)

    def showDic(self, value):
        self.clear()
        self.setColumnCount(2)
        self.fill_item(self.invisibleRootItem(), value)
        self.collapseAll()
        
        
        
    