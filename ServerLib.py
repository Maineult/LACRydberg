# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 16:18:35 2016

@author: Wilfried
"""

from ExperimentController import *
from GeneralConstants import *
from threading import Thread
import socket
from time import sleep



class LambdaServer(Thread):
    def __init__(self,controller):
        
        self.controller = controller
        super(LambdaServer,self).__init__()
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind((socket.gethostname(), InternalComPort))
        self.server.listen(3)
        self.running = 1
        self.start()
        
    def __test__(self):
        print 'test Lambdaserver'
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try :
            s.connect((socket.gethostbyname('lac57-95'),InternalComPort)) 
            return True
        except Exception as e: 
            print("something's wrong with the server. Exception is %s" % ( e))
            return False
        finally:
            s.close()
        
    def run(self):
        while self.running : 
            self.conn, self.addr = self.server.accept()
            print 'accepted connection from ',self.addr
            ct = Connection(self.conn,self.controller)
            ct.setDaemon(self)
            ct.start()
            sleep(0.005)
            
class Connection(Thread):
    def __init__(self,conn,controller):
        super(Connection,self).__init__()
        self.conn = conn
        self.controller = controller
        
    def run(self):
        from time import sleep
        while True : 
            msg = self.conn.recv(BUFFER_SIZE)
            if len(msg):
                rep = self.TreatMsg(msg)
                rep = self.EncodeStream(rep)
                self.conn.send(rep)
            sleep(0.001)            
            
    def TreatMsg(self,msg):
        # Parses the message received from the communication in a way that can be used by
        # Write to Instrument and the ReadInstrument method of the controoleur
        if not msg.find('Write'): 
            msg = msg[5:] 
            func = getattr(self.controller,'WriteToInstrument')
        elif not msg.find('Read'):
            msg = msg[4:]
            func = getattr(self.controller,'ReadInstrument')
        else :
            return
            #raise InstrError('LambdaServer','receive a valid Lambdameter command')
        pos = msg.find('(')
        command = msg[:pos]
        res = msg[pos+1:-1]
 
        if len(res):
            kw = self.DecodeStream(res)
            temp = func('Lambdameter',command,**kw)
        else : 
            temp = func('Lambdameter',command)
        return temp
            

        
    def DecodeStream(self,foo):
        if foo[0]=='K':
            return int(foo[1:])
        if foo[0]=='W':
            return float(foo[1:])
        if foo[0]=='Z':
            return foo[1:]
        if foo[0]=='Y':
            return bool(int(foo[1:]))
        if type(foo)== type(None):
            return {}
        try : 
            toto = {}
            lists = foo[1:].split(',')
            for l in lists :
                [key, value] = l.split('=')
                toto[self.DecodeStream(key)] = self.DecodeStream(value)
            return toto
        except Exception as e:
            print e
            raise TypeError('Decoding of Flux does not support {} type'.format(type(foo)))
        
        
    def EncodeStream(self,foo):
        if type(foo) == type(1):
            return 'K'+str(foo)
        if type(foo) == type(1.):
            return 'W'+str(foo)
        if type(foo) == type(''):
            return 'Z'+foo
        if type(foo) == type(True):
            return 'Y'+str(int(foo))
        if type(foo) == type({}) : 
            lists = [self.EncodeStream(key)+'='+self.EncodeStream(value) for key,value in foo.iteritems() ]
            return 'Q'+','.join(lists)
        raise TypeError('Encoding  of Flux does not support {} type'.format(type(foo)))
    
if __name__=='__main__':
    controlleur = ExpController