# -*- coding: utf-8 -*-
"""
Created on Fri Apr 29 10:27:29 2016

@author: HLehec
"""
from scipy import optimize
import YbConstants as C
import numpy as np
from lmfit import Model
from UserDefFunctions import *
from matplotlib import pyplot as plt
from PyQt4 import QtGui
import os
from termcolor import colored

def LoadYbSpectroDic():
    YbSpectroDic={}
    YbSpectroDic['Freq_1S0']=np.loadtxt('SingletS.txt')
    YbSpectroDic['Freq_1D2']=np.loadtxt('SingletD.txt')
    YbSpectroDic['Freq_3D2']=np.loadtxt('TripletD.txt')
    YbSpectroDic['QuantDef_1S0']=np.zeros((np.size(YbSpectroDic['Freq_1S0'][:,0]),2))
    for n in np.arange(np.size(YbSpectroDic['Freq_1S0'][:,0])):
        YbSpectroDic['QuantDef_1S0'][n][0]=YbSpectroDic['Freq_1S0'][0,0]+n
        YbSpectroDic['QuantDef_1S0'][n][1]=YbSpectroDic['QuantDef_1S0'][n][0]-np.sqrt(C.RyYb/(C.I6s-YbSpectroDic['Freq_1S0'][n,1]))
    YbSpectroDic['QuantDef_1D2']=np.zeros((np.size(YbSpectroDic['Freq_1D2'][:,0]),2))
    for n in np.arange(np.size(YbSpectroDic['Freq_1D2'][:,0])):
        YbSpectroDic['QuantDef_1D2'][n][0]=YbSpectroDic['Freq_1D2'][0,0]+n
        YbSpectroDic['QuantDef_1D2'][n][1]=YbSpectroDic['QuantDef_1D2'][n][0]-np.sqrt(C.RyYb/(C.I6s-YbSpectroDic['Freq_1D2'][n,1]))
    YbSpectroDic['QuantDef_3D2']=np.zeros((np.size(YbSpectroDic['Freq_3D2'][:,0]),2))    
    for n in np.arange(np.size(YbSpectroDic['Freq_3D2'][:,0])):
        YbSpectroDic['QuantDef_3D2'][n][0]=YbSpectroDic['Freq_3D2'][0,0]+n
        YbSpectroDic['QuantDef_3D2'][n][1]=YbSpectroDic['QuantDef_3D2'][n][0]-np.sqrt(C.RyYb/(C.I6s-YbSpectroDic['Freq_3D2'][n,1]))
    return YbSpectroDic

def OpenData(Type='npz'):
    plt.subplots(1,1) 
    dir0=os.getcwd()
    dire =QtGui.QFileDialog.getOpenFileName(None, 'Open Data', dir0,"AllFiles (*)")
    strtmp=unicode(dire)
    if Type=='npz':
        data= np.load(strtmp)
        
    elif Type==',':
        data =np.loadtxt(strtmp, delimiter=',')
        data=data.transpose()
    else: 
        data =np.loadtxt(strtmp)
        data=data.transpose()
    if np.shape(data)[0]==2:
        plt.plot(data[0],data[1],'.')
    elif np.shape(data)[0]==3:
        plt.plot(data[0],data[1],'.')
        plt.plot(data[0],data[2],'.')
    elif np.shape(data)[0]==4:
        plt.plot(data[0],data[1],'.')
        plt.plot(data[0],data[2],'.')
        plt.plot(data[0],data[3],'.')
    else :
        print colored('Dimension Error, 4 columns Maximum','red')
    ax = plt.gca()
    ax.format_coord = lambda x,y: '%10f, %10f' % (x,y)    
    plt.show(block=False)       
    return data    

def DataRestrict(Data,xin,xfin):
    i=0
    Ndarray=np.zeros(1)
    if type(Data)==type(Ndarray):
        Data=Data.tolist()
    while i < len(Data[0]):            
        if Data[0][i]<=xin or Data[0][i]>=xfin:
            for j in range(len(Data)):    
                Data[j].pop(i)
        else:
            i=i+1    
    return Data    
       
    
def SortByX(Data):
    Sortx=sorted(Data[0])
    for j in range(len(Data)) :
        if j>0:
            CorrespSig=[]
            for i,x in enumerate(Sortx):
                CorrespSig.insert(i,Data[j][Data[0].index(x)])
            Data[j]=CorrespSig
    Data[0]=Sortx    
    return Data     
      #########      CONVERSION   #######


def WlToFreq(Data,WlUnit='nm',FreqUnit='THz'):
    if type(Data)=='tuple':
        Data=np.array(Data)
    if WlUnit=='nm' and FreqUnit=='THz':
        return 1e-3*C.c/Data 
    elif WlUnit=='nm' and FreqUnit=='GHz':
        return C.c/Data
    elif WlUnit=='nm' and FreqUnit=='MHz': 
        return 1e3*C.c/Data
    else :
        print colored('Unit Error','red')   
                    
def FreqToWl(Data,FreqUnit='THz',WlUnit='nm'):
    if type(Data)=='tuple':
        Data=np.array(Data)
    if WlUnit=='nm' and FreqUnit=='THz':
        return 1e-3*C.c/Data
    elif WlUnit=='nm' and FreqUnit=='GHz':
        return C.c/Data
    elif WlUnit=='nm' and FreqUnit=='MHz': 
        return 1e3*C.c/Data    
    else :
        print colored('Unit Error','red')
            
def FreqToWn(Data,FreqUnit='THz',WnUnit='cm-1'):
    if type(Data)=='tuple':
        Data=np.array(Data)
    if WnUnit=='cm-1' and FreqUnit=='THz': 
        return 1e11*Data/C.c
    if WnUnit=='cm-1' and FreqUnit=='GHz': 
        return 1e8*Data/C.c
    if WnUnit=='cm-1' and FreqUnit=='MHz': 
        return 1e5*Data/C.c
    else :
        print colored('Unit Error','red')
         
def WnToFreq(Data,WnUnit='cm-1',FreqUnit='THz'):
    if type(Data)=='tuple':
        Data=np.array(Data)
    if WnUnit=='cm-1' and FreqUnit=='THz': 
        return 1e-10*Data*C.c
    if WnUnit=='cm-1' and FreqUnit=='GHz': 
        return 1e-7*Data*C.c
    if WnUnit=='cm-1' and FreqUnit=='MHz': 
        return 1e-4*Data*C.c            
    else :
        print colored('Unit Error','red')
        
                    
                    
        ######    FITTING   #####                    
                    
def FitGaussian(y,x,amplitude=1, xo=1, sigma=1, offset=1,plot=False):
    model=Model(Gaussian)
    result =  model.fit(y,x=x,amplitude=amplitude,xo=xo,sigma=sigma,offset=offset)
    print '  Chi2=  '+unicode(result.chisqr)
    print '  Parameters  =  '+unicode(result.values)
    if plot:
        amplitude=result.values.get('amplitude')
        xo=result.values.get('xo')
        sigma=result.values.get('sigma')
        offset=result.values.get('offset')
        xx=np.arange(np.min(x),np.max(x),(np.max(x)-np.min(x))/1000)
        yy=Gaussian(xx,amplitude=amplitude,xo=xo,sigma=sigma,offset=offset)
        plt.clf()
        plt.plot(x,y,'.')
        plt.plot(xx,yy,'r')
        plt.title('Gaussian Fit:'+unicode(result.values))
        plt.draw()
    return result
    
def FitLorentzian(y,x, amplitude=1,xo=1, fwhm=1, offset=0,plot=False):
    model=Model(Lorentzian)
    result =  model.fit(y,x=x,amplitude=amplitude,xo=xo,fwhm=fwhm,offset=offset)
    print '  Chi2=  '+unicode(result.chisqr)
    print '  Parameters  =  '+unicode(result.values)
    if plot:
        amplitude=result.values.get('amplitude')
        xo=result.values.get('xo')
        fwhm=result.values.get('fwhm')
        offset=result.values.get('offset')
        xx=np.arange(np.min(x),np.max(x),(np.max(x)-np.min(x))/1000)
        yy=Lorentzian(xx,amplitude=amplitude,xo=xo,fwhm=fwhm,offset=offset)
        plt.clf()
        plt.plot(x,y,'o''b')
        plt.plot(xx,yy,'b')
        plt.title('Lorentzian Fit (line)'+unicode(result.values))
        plt.draw()
    return result   
        
def FitLinear(y,x,a=1,b=1,plot=False):
    model=Model(Linear)
    result =  model.fit(y,x=x,a=a,b=a)
    print '  Chi2=  '+unicode(result.chisqr)
    print '  Parameters  =  '+unicode(result.values)
    if plot:
        a=result.values.get('a')
        b=result.values.get('b')
        xx=np.arange(np.min(x),np.max(x),(np.max(x)-np.min(x))/1000)
        yy=Linear(x,a=a,b=b)
        plt.clf()
        plt.plot(x,y,'.')
        plt.plot(xx,yy,'r')
        plt.title('Linear Fit (line)'+unicode(result.values))
        plt.draw()
    return result      
    
def FitRydLaw(y,x,I,d=1,plot=False):
    model=Model(RydLaw)
    result =  model.fit(y,n=x,I=I,d=d)
    print '  Chi2=  '+unicode(result.chisqr)
    print '  Parameters  =  '+unicode(result.values)    
    if plot:
        plt.clf()
        plt.plot(x,y,'o')
        plt.plot(x,result.best_fit)
        plt.show(block=False)
    return result          

def FitDoubleSinc(y,x,X1,X2,A1,A2,k,plot=False):
    model=Model(DoubleSinc)
    result =  model.fit(y,x=x,X1=X1,X2=X2,A1=A1,A2=A2,k=k)
    print '  Chi2=  '+unicode(result.chisqr)
    print '  Parameters  =  '+unicode(result.values)
    if plot:
        plt.plot(x,result.best_fit)
    return result
    
def FitDoubleGaussian(y,x,A1,A2,X1,X2, sigma, offset=0,plot=False)  :
    model=Model(DoubleGaussian)
    result =  model.fit(y,x=x,A1=A1,A2=A2,X1=X1,X2=X2,sigma=sigma,offset=offset)
    print '  Chi2=  '+unicode(result.chisqr)
    print '  Parameters  =  '+unicode(result.values)
    if plot:
        plt.plot(x,result.best_fit)
    return result    
    
def FitTripleGaussian(y,x,A1,A2,A3,X1,X2,X3,sigma,offset=0,plot=False)  :
    model=Model(TripleGaussian)
    result =  model.fit(y,x=x,A1=A1,A2=A2,A3=A3,X1=X1,X2=X2,X3=X3,sigma=sigma,offset=offset)
    print '  Chi2=  '+unicode(result.chisqr)
    print '  Parameters  =  '+unicode(result.values)
    if plot:
        plt.plot(x,result.best_fit)
    return result        
    
def FitDampedSin(y,x,w,A,tau,offset,plot=None):
    model=Model(DampedSin)
    result =  model.fit(y,x=x,w=w,A=A,tau=tau,offset=offset)
    print '  Chi2=  '+unicode(result.chisqr)
    print '  Parameters  =  '+unicode(result.values)
    if plot:
        plt.plot(x,result.best_fit)
    return result
    
def FitExp(y,x,tau,A,offset,plot=None):
    model=Model(Exp)
    result =  model.fit(y,x=x,tau=tau,A=A,offset=offset)
    print '  Chi2=  '+unicode(result.chisqr)
    print '  Parameters  =  '+unicode(result.values)
    if plot:
        plt.plot(x,result.best_fit)
    return result
    
def FitHypCube(y,x,a,b,plot=None):
    model=Model(HypCube)
    result =  model.fit(y,x=x,a=a,b=b)
    print '  Chi2=  '+unicode(result.chisqr)
    print '  Parameters  =  '+unicode(result.values)
    if plot:
        plt.plot(x,result.best_fit)
    return result

def gaussian(height,offset, center_x, center_y, width_x, width_y):
    """Returns a gaussian function with the given parameters"""
    width_x = float(width_x)
    width_y = float(width_y)
    return lambda x,y: height*np.exp(
                -(((center_x-x)/width_x)**2+((center_y-y)/width_y)**2)/2)+offset  
                

def Fit2DGaussian(data,params):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    errorfunction = lambda p: np.ravel(gaussian(*p)(*np.indices(data.shape)) -
                                 data)
    p, success = optimize.leastsq(errorfunction, params)
    return p           
    
        
 