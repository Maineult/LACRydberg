# -*- coding: utf-8 -*-
import numpy as np

def Gauss(params,x):

    '''Gauss Function '''
    a = params['A']
    x0 = params['x0']
    sigma0 = params['sigma0']
    offset = params['offset']
    b = -(x-x0)**2
    return a * np.exp(-(x-x0)**2/(2*sigma0**2) ) + offset
    
    
def GaussRes(params, x, data):
    """ model gauss func, subtract data"""
    try: 
        A = params['A'].value
        x0 = params['x0'].value
        sigma0 = params['sigma0'].value
        offset = params['offset'].value
    except : 
        A = params['A']
        x0 = params['x0']
        sigma0 = params['sigma0']
        offset = params['offset'] 
    model = A * np.exp(-(x-x0)**2/(2*sigma0**2) ) + offset
    return (model - data)**2
#    
#def Linear(params,x):
# 
#    ''' Linear with weight y=ax+b'''
#    a = params['a'].value
#    b = params['b'].value   
#    
#    return a*x +b
    
def LinearRes(params,x,data,dataErr=None):
    ''' Linear with weight data=ax+b, with data = data +/-dataErr'''  
    if not (data.shape == dataErr.shape):
        raise TypeError('Should give ndarray of the same shape')   
    if not (data.shape == x.shape):
        raise TypeError('Should give ndarray of the same shape')
    model = Linear(params,x)   
    if dataErr==None :        
        return (model -data)**2
    else : 
        return (model -data)**2/dataErr**2
    
def Parabola(params,x):

    ''' Parabola y=a(x-x0)**2+y0'''
    try : 
        a = params['a'].value
        x0 = params['x0'].value
        y0 = params['y0'].value
    except :
        a = params['a']
        x0 = params['x0']
        y0 = params['y0']
        
    return a*(x-x0)**2+y0
    
def ParabolaRes(params,x,data,dataErr=None):
    ''' Parabola y=a(x-x0)**2+y0'''
    a = params['a'].value
    x0 = params['x0'].value
    y0 = params['y0'].value
    model = Parabola(params,x)   
    if dataErr==None :        
        return (model -data)**2
    else : 
        return (model -data)**2/dataErr**2
        
def Gaussian(x, amplitude, xo, sigma, offset): 
    return offset + amplitude*np.exp( - ((x-xo)**2)/sigma**2)
    
def Lorentzian(x, amplitude,xo, fwhm, offset):
    return offset + amplitude*2/(np.pi*fwhm) * 1/(1+(2*(x-xo)/fwhm)**2)
    
def Linear(x,a,b):
    return a*x+b  
    
def RydLaw(n,I,d):  
    return I - 3289.831297/(n-d)**2    ##### Rydberg constant corrected for Yb
    
def DoubleSinc(x,X1,X2,A1,A2,k):
    return A1*np.sinc(k*(x-X1))+A2*np.sinc(k*(x-X2))
    
def DoubleGaussian(x,A1,A2,X1,X2,sigma,offset):
    return offset + A1*np.exp( - ((x-X1)**2)/sigma**2)+A2*np.exp(-((x-X2)**2)/sigma**2)
    
def TripleGaussian(x,A1,A2,A3,X1,X2,X3,sigma,offset):
    return offset + A1*np.exp( - ((x-X1)**2)/sigma**2)+A2*np.exp(-((x-X2)**2)/sigma**2)+A3*np.exp(-((x-X3)**2)/sigma**2)
    
def DampedSin(x,w,A,tau,offset):
    return A*np.exp(-x/tau)*np.sin(w*x)+offset
    
def Exp(x,tau,A,offset):
    return A*np.exp(-x/tau)+offset
    
def HypCube(x,a,b):
    return b+a/x**3    
    
def Gaussian2D((x,y), amplitude, xo, yo, sigma_x, sigma_y, offset):
    return offset+amplitude*np.exp(-x**2/sigma_x**2)*np.exp(-y**2/sigma_y**2)