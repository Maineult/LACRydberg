# -*- coding: utf-8 -*-


from math import pi
import numpy as np

c=299792458
hbar=1.054571726e-34
e=1.602176565e-19
eo=8.854e-12
mp=1.6e-27
me=9.109e-31
ao=0.53e-10
RyYb=3289.831297 ##### in THz (in cm-1 : 109736.96)

## Blue laser
myb=174*mp
lab=398.911e-9
wb=2*pi*c/lab
kb=2*pi/lab
GamB=28e6*2*pi
IsatB = 60  ## mW/cm2
DipoleB=np.sqrt(3*pi*hbar*eo*(c**3)*GamB/(wb**3))
## Green Laser

lag=555.802e-9
Gamg=180e3*2*pi
kg=2*pi/lag

## Rydberg Laser
GamR=1e4
DipoleRy = np.sqrt(3*pi*hbar*eo*(c**3)*GamR/((2*pi*c/(394e-9))**3))

IsatR=(hbar**2*GamR**2*eo*c/(4*(DipoleRy)**2))/10 ## mW/cm2


DipoleB=np.sqrt(3*pi*hbar*eo*(c**3)*GamB/(wb**3))


I6s=1512.245210



