# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 18:49:58 2016
Experiment view of a MVC
@author: wilfried
"""

'''
Make sure that Qt4, PyQt4 are installed on the computer
'''

'''
Structure of the program : 
ExpMainView controls all the GUI printing and the interaction with other part of the program
It reads the value of ExpModel, containing all the data of the exeperiment to have
a correct display of the values
Il controls and lauch the Experimental Sequence where are located all the function allowing 
a real time control of the experiment, and the expController, which is used 
to communicate with instruments, and store the results in the ExpModel part 
'''

'Locking Laser activates the experiment possibilities'

import sys
from PyQt4.uic import loadUiType
from PyQt4 import QtCore as C
from PyQt4 import QtGui as G
from ExperimentalSequence import *
from ControleManipUI import GUIWidgets as GW


from os import path,walk

temp =[item for item in next(walk(path.curdir))[1] if 'UI' in item]
try : 
    rel=path.join(path.curdir,temp[0])
except : 
    raise ValueError('No Ui subdirectory files found')    

temp = [item for item in next(walk(rel))[2] if 'ExperimentView' in item]

Ui_MainWindow, QMainWindow = loadUiType(path.join(rel,temp[0]))

temp = [item for item in next(walk(rel))[2] if 'UpdateDict' in item]
Ui_UpdateDict, QUpdateDictDialog = loadUiType(path.join(rel,temp[0]))

temp = [item for item in next(walk(rel))[2] if 'SeqDict' in item]
Ui_SeqDict, QSeqDialog = loadUiType(path.join(rel,temp[0]))

temp = [item for item in next(walk(rel))[2] if 'NewDict' in item]
Ui_NewDictEntry, QNewDictDialog = loadUiType(path.join(rel,temp[0]))

temp = [item for item in next(walk(rel))[2] if 'ChooseRydberg' in item]
Ui_ChooseRydberg, QChooseRydberg = loadUiType(path.join(rel,temp[0]))

temp = [item for item in next(walk(rel))[2] if 'Ask2Value' in item]
Ui_2Value, Q2Value = loadUiType(path.join(rel,temp[0]))

temp = [item for item in next(walk(rel))[2] if 'Running' in item]
Ui_Running, Qtoto = loadUiType(path.join(rel,temp[0]))

temp = [item for item in next(walk(rel))[2] if 'PlotCamera' in item]
Ui_PlotCamera, QSeqDialog = loadUiType(path.join(rel,temp[0]))


#%%

class FrequencyValidator(G.QValidator):
    def validate(self,val,i):
        def testnum(s):
            try : 
                return type(float(s))==type(2.) 
            except : 
                return False
        if len(val)<2 :
            return (G.QValidator.Intermediate,val,i)
        try : 
            if not 'Hz' in val[-2:] :
                if testnum(val) :
                    return (G.QValidator.Intermediate,val,i)
                else : 
                    return (G.QValidator.Invalid,val,i)
        except Exception as e : 
            return (G.QValidator.Invalid,val,i)    
        if val[-3] in [u'M',u'G',u'T',u' ']:
            digit = val[:-3]
        elif testnum(val[-3]) :
            digit = val[:-2]
        else :
            return (G.QValidator.Invalid,val,i)
        if testnum(digit):
            self.parent().setText(val)
            return (G.QValidator.Acceptable,val,i)            
        else : 
            return (G.QValidator.Invalid,val,i)
        
    def fixup(self,s):
        try :
            if type(s)==type(2):
                return unicode(int(s))+u'MHz'
            elif type(s)==type(2.):
                return unicode(int(s))+u'GHz'
        except : 
            pass
        
class AskFreqScan(G.QDialog,Ui_2Value):
    def __init__(self,ini0,ini1, parent=None):
        super(AskFreqScan, self).__init__(parent)
        self.setupUi(self)
        self.Label0.setText('Scan center Freq')
        self.Label1.setText('Scan Range')
        self.Value0.setText('{:.6f} THz'.format(ini0))
        self.Value0.setValidator(FrequencyValidator(self.Value0))
        self.Value1.setText('{} MHz'.format(int(ini1*1e6)))
        self.Value1.setValidator(FrequencyValidator(self.Value1))
        self.connect(self.buttonBox,C.SIGNAL('clicked()'),self.test)
    
    def test(self):
        self.accept()
    def getValues(self):
        fcenter = self.Value0.text()
        if fcenter[-3] in [u'M',u'G',u'T',u' ']:
            f0 = float(fcenter[:-3])*Unit[fcenter[-3:]]
        else : 
            f0 = float(fcenter[:-2])
        df = self.Value1.text()
        if df[-3] in [u'M',u'G',u'T',u' ']:
            df0 = float(df[:-3])*Unit[df[-3:]]
        else : 
            df0 = float(df[:-2])
        return f0/1e12,df0/1e12

#%%        

class NewDictEntry(G.QDialog,Ui_NewDictEntry):
    def __init__(self,kwitem, parent=None):
        super(NewDictEntry, self).__init__(parent)
        self.setupUi(self)
        self.fd = GW.InstrOptions('kwname',self)
        
        if not kwitem is None : 
            self.kwName = GW.InstrOptions(kwitem,self)
        else :
            self.kwName =  GW.InstrEdit(self)
        self.formLayout.insertRow(0,self.fd,self.kwName)
        self.connect(self.buttonBox,C.SIGNAL('clicked()'),self.test)
    
    def test(self):
        try : 
            self.kwvalue = ReType[self.DataType.currentText()](self.KwValue.text())
            if len(self.kwName.text()): 
                self.accept()
        except Exception as e :
            NewLog(e)

    def getValues(self):
        return self.kwName.text(),self.kwvalue

#%%
class MatchPrototype(G.QValidator):    
    # fproto variable must be assigned just after object creation, before any call to validate
    fproto = ''
    varyval = 0
    valtyp = C.pyqtSignal(str)
    '''
    fproto sould be of the type var1:(),var2:()
    function validate removes any space, look for var1, var2... in val that users enters
    and then trys to assess a python type to the object from ReType in General Constants modules
    
    '''
    
    def validate(self,val,i):
        
        def testsnum(s):
            try : 
                return type(float(s))==type(2.) 
            except : 
                return False
#        print self.fproto,val, i
        if (not len(self.fproto)) and not(len(val)):
            self.params = {}
#            print 'accpeted'
            return (G.QValidator.Acceptable,val,i) 
        def lena(val):
            if type(val)==type(''):
                return min(1,len(val))
            else:
                return 1
        


        elems = self.fproto.split('()')
        
        elems = [unicode(elem) for elem in elems if len(elem)]
        n = len(elems)
        mem = val
        val = val.replace(' ','')
        args = [] 
        params = {}
        for elem in elems:
            elem.replace(',','')
            
            j = val.find(elem)
    #        print 'elem is ', [e for e in elem], 'val is', [v for v in val]
            if not j+1 :
                print 'invalid'
                return (G.QValidator.Invalid,mem,i)
            else : 
                stp = val.split(elem)
                if len(stp)<2 : 
                    return (G.QValidator.Invalid,mem,i)
                j = stp[1].find(',')
                if not j+1 : 
                    value = stp[1]
                    j=0
                   # stp[1] = stp[1]+' '
                else : 
                    value = stp[1][:j]
#            print 'l210',value, stp, stp[1:j-1], j, 'val'+value[1:j-1]+'val'        
            if not value[0]=='(' and value[-1]==')':
                print 'invalid'
                return (G.QValidator.Invalid,mem,i)
            if not elem[-1] == ':':
                print elem
            if testsnum(value[1:j-1]):
                args.append(float(value[1:j-1]))
                params[elem[:-1]]=float(value[1:j-1])
            else : 
                args.append(value[1:j-1])
                params[elem[:-1]]=value[1:j-1]
            val = stp[0]+stp[1][j:]
        m=sum([lena(val) for val in params.itervalues()])
        self.params = params
#        print 'l225   ', params
        if m == n : 
            test = 0
            for t in ReType :
                if t in params.itervalues():
                    self.type = t
                    index = args.index(t)
                    self.vary = [key for key,val in params.iteritems() if val ==t ][0]
                    test+=1
            if test ==self.varyval : 
                self.params = params
                if test:
                    self.valtyp.emit(self.type)
 #               
                if locals().has_key('index'):
                    args.pop(index)
                for a in args:
                    if not len(unicode(a)):
                        return (G.QValidator.Intermediate,mem,i) 
   #             print 'accepted'
                return (G.QValidator.Acceptable,mem,i) 
            elif test>self.varyval:
                print 'invalid'
                NewLog('','One argument can be looped in a single function')
                return (G.QValidator.Invalid,mem,i) 
            else : 
 #               print 'intermedi'
                return (G.QValidator.Intermediate,mem,i) 
        else : 
  #          print 'intermediate'
            return (G.QValidator.Intermediate,mem,i) 
            
#%%
class dirValidator(G.QValidator):
    numval = 0
    currentdir = ''
    def validate(self,val,i):
        from os import walk, path   
        tl = lambda x:path.getmtime(path.join(self.currentdir,x+u'.ExpResults')) 
        cmpe = lambda x,y: cmp(tl(x),tl(y))
        files = ([item[:item.find('.ExpResults')] 
             for item in next(walk(self.currentdir))[2] if 'ExpResults' in item])
        self.files = sorted(files,cmpe)
        if val+'.ExpResults' in self.files:
            return (G.QValidator.Intermediate,val,i)
        elif not len(val):
            return (G.QValidator.Intermediate,val,i)
        else : 
            return (G.QValidator.Acceptable,val,i)
        if not numval : 
            numval = 1
            self.fixup(val)
            
    def fixup(self,val):
        self.validate(val,0)
        if len(self.files):
            last = self.files[-1]
            ll = [(last[i:],last[:i]) for i in range(len(last))]
            for l in ll : 
                try :          
                    return (u'{}{:0'+unicode(len(l[0]))+'d}').format(l[1],int(l[0])+1)
                except : 
                    pass
        else : 
            return '0000'
        
         
#%%
 
class IsList(G.QValidator):    
    '''
    Tests if a valid list is passed to python, with list elems of type typ
    that must be provided before any call to the validate func
    '''
    import numpy as np
    typ = None
    dic = {'np.linspace':np.linspace,'range':range,'np.arange':np.arange}
    def validate(self,val,i):

        if self.typ == None:
            return (G.QValidator.Intermediate,val,i)        
        val = val.replace(' ','')
        mem = val
        func = [elem for elem in self.dic.iterkeys() if not val.find(elem)]
        if len(val)<3:
            return (G.QValidator.Intermediate,mem,i) 
        if not len(func):
            if not val[0]+val[-1]=='[]':
                if len(val)>14 and not val[0]=='[':
                    return (G.QValidator.Invalid,mem,i) 
                return (G.QValidator.Intermediate,mem,i) 
            else : 
                func = list
                val = val[1:-1]
        else :
            val = val.strip(func[0])[1:-1]
            func = self.dic[func[0]]
#        print 'l279',val, func
        try : 
            l = val.split(',')
            val = tuple([ReType[self.typ](v) for v in l])
            if func == list : 
                tab = func(val)
            else : 
                tab = func(*val)
            self.res = [ReType[self.typ](v) for v in tab]
            print 'Accepted', self.res[0]
            return (G.QValidator.Acceptable,mem,i) 
        except Exception as e:
            return (G.QValidator.Intermediate,mem,i) 
            
    def GiveType(self,s):
        self.typ = s
        
        
        

        
            
#%%

class ChooseSequence(G.QDialog,Ui_SeqDict):        
    def __init__(self,parent=None):
        super(ChooseSequence,self).__init__(parent)
        self.setupUi(self)
        self.GUI = parent
        GW.SetStyle(self,GW.greygrey)
        self.ExpModel = self.GUI.ExpModel
        self.ExpControl = self.GUI.ExpControl
        ab = [but for but in self.buttonBox.buttons() if but.text()==u'Apply']
        sb = [but for but in self.buttonBox.buttons() if but.text()==u'Save']
        lb = [but for but in self.buttonBox.buttons() if but.text()==u'Open']
        ab[0].setText(u'Run')
        lb[0].setText(u'Load')
        self.connect(ab[0],C.SIGNAL('clicked()'),self.OK)       
        self.connect(sb[0],C.SIGNAL('clicked()'),self.SaveSeqParams)
        self.connect(lb[0],C.SIGNAL('clicked()'),self.LoadSeqParams)
        GW.SetStyle(self.InstrFrame,GW.grey)
        self.VerticallLayout.insertWidget(0,self.InstrFrame)
        self.InstrLayout = GW.InstrVLayout()
        self.InstrFrame.setLayout(self.InstrLayout)
        self.setMinimumHeight(600)
        if not hasattr(self.GUI.MS,'Experiment'):
            u = ExperimentResults()
            setattr(self.GUI.MS,'Experiment',u)
        self.ExpRes = self.GUI.MS.Experiment
        self.EmptyParam = 0
        self.EmptyRec = 0
        self.DoInit()
    
    def Cancel(self):
        if self.GUI.LastClick():
            self.reject()
        
    def RenewInit(self,SeqParams=None):
        print 'Renew Layout'
        def ClearLayout(layout):
            for i in reversed(range(layout.count())):
                item = layout.itemAt(i)
                if isinstance(item, G.QWidgetItem):
                    item.widget().close()
                else:
                    ClearLayout(item.layout())
                layout.removeItem(item)           
        ClearLayout(self.InstrLayout)  
        self.GUI.ClickOK()
        self.EmptyParam = 0
        self.EmptyRec = 0
        self.DoInit(SeqParams)
        
    def DoInit(self,SeqParams=None):
        if self.GUI.LastClick():
            self.ParamLine={}
            self.RecLine={}
            u =  GW.InstrOptions('Parameters to Vary',self)
            u.Gold()
            self.InstrLayout.addWidget(u,0,C.Qt.AlignCenter)
            u = GW.InstrOptions('Parameters to Record',self)
            u.Gold()
            self.InstrLayout.addWidget(u,0,C.Qt.AlignCenter)
            self.StatusBar = G.QStatusBar(self)
            self.InstrLayout.addWidget(self.StatusBar)
            self.nParam = 0
            self.nRec = 0
            from inspect import ismethod,getmembers          
            self.listmethods ={'':''}
            self.listread = {'':''}
            self.ListInstr = [Instr for Instr,init in self.ExpModel._InstrList.iteritems() if init == InitOK]

            for Instr in self.ListInstr :
                
              
                InstrObj = self.ExpControl._InstrumentInstances[Instr]
                u = getmembers(InstrObj,predicate=ismethod)
                if Instr in self.ExpModel.TimingInstr:
                    v = ['{:0>2d}-{}'.format(i,e) for i,e in enumerate(self.ExpModel.TimeTable['TimeTags']) if not (e in ['ExperimentStart','ExperimentEnd'])]
                    v.insert(0,'')
                    w = [name[4:] for name,o in u if not name.find('Read')]
                    w.insert(0,'')
                else : 
                    v = [name[5:] for name,o in u if not name.find('Write')]
                    v.insert(0,'')
                    w = [name[4:] for name,o in u if not name.find('Read')]
                    w.insert(0,'')
                self.listmethods[Instr] = sorted(v)
                self.listread[Instr]=sorted(w)
                
            '''
                Params varied during the sequence and the measurement recorded (Rec)
                are drawn one after the other in a QHBoxLayout, first Box gives intrument
                second box the command to read(name of the function in the Instr Library),
                the third the arguments passed to the function, and the last
                the list of params to use.
            '''
         #☻   for Param in self.ExpRes.Params.iteritems():
            if SeqParams==None:
                for Param in self.ExpRes.Params:
                    self.addParam(Param)
                    self.nParam +=1
                self.addParam()
                self.nParam+=1
                for Rec in self.ExpRes.Rec : 
                    self.addRec(Rec)
                    self.nRec+=1  
                self.addRec()
                self.nRec+=1
                self.Hint = GW.InstrOptions('',self)
                self.Hint.Gold()
                self.InstrLayout.addWidget(self.Hint)
                u = GW.InstrHLayout()
                self.InstrLayout.addLayout(u)
                v = GW.InstrLabel('Averaging number',self)
                u.addWidget(v)
                self.num = GW.InstrSpinBox(20,0,1000,self)
                u.addWidget(self.num)
                u = GW.InstrHLayout()
                self.InstrLayout.addLayout(u)
                v = GW.InstrLabel('ExperimentName',self)
                u.addWidget(v)
                self.name = GW.InstrEdit('',self)
                u.addWidget(self.name)
                a = dirValidator()
                a.currentdir = self.ExpControl.GetTodayDir()
                self.name.setValidator(a)
                u= GW.InstrOptions('',self)
                u.Gold()
                self.InstrLayout.addWidget(u)
                
            else:
                self.LoadSequence(SeqParams)
            
        
    def addRec(self,Rec=None):
        u = GW.InstrHLayout()
        i=-1
        print 'Rec',Rec
        if Rec : 
            i = sorted(self.listread.keys()).index(Rec[0])          
        v = GW.InstrBox(sorted(self.listread.keys()),i,self)
        v.currentIndexChanged.connect(self.InstrRecChanged)
        setattr(v,'i',self.nRec)
        u.addWidget(v)  
        v = GW.InstrBox([],-1,self)
        v.currentIndexChanged.connect(self.RecChanged)
        setattr(v,'Empty',True)
        setattr(v,'i',self.nRec)
        v.SetColor(0)
        u.addWidget(v)
        self.EmptyRec+=1
        v = GW.InstrEdit('',self)
        u.addWidget(v)
        v.Activate(False)
        v = GW.InstrEdit('',self)
        u.addWidget(v)
        v.hide()
        v.Activate(False)
        
        if not Rec==None:
            u.itemAt(0).widget().setCurrentIndex(sorted(self.listread.keys()).index(Rec[0]))
            self.InstrChanged(1,u.itemAt(0).widget(),u,'Rec')
            u.itemAt(1).widget().setCurrentIndex(self.listread[Rec[0]].index(str(Rec[1])))
            self.Changed(self.nRec,'read',u.itemAt(1).widget(),u,Rec[0])
            u.itemAt(2).widget().setText(Rec[2])
            
        self.InstrLayout.insertLayout(self.nParam+2+self.nRec,u)
        self.RecLine[self.nRec]=u 
        print 'Added Rec', self.EmptyRec        
        
        
    def addParam(self,Param=None):
        u = GW.InstrHLayout()
        print Param
        i=-1
        if Param : 
            i = sorted(self.listmethods.keys()).index(Param[0])
            
        v = GW.InstrBox(sorted(self.listmethods.keys()),i,self)
        v.currentIndexChanged.connect(self.InstrParamChanged)
        setattr(v,'i',self.nParam)
        u.addWidget(v)      
        v = GW.InstrBox([],-1,self)
        v.YellowGrey()
        v.setMinimumWidth(80)
        v.setSizePolicy(G.QSizePolicy.Preferred,G.QSizePolicy.Preferred) 
        setattr(v,'Empty',True)
        setattr(v,'i',self.nParam)
        v.currentIndexChanged.connect(self.ParamChanged)
        u.addWidget(v)
        self.EmptyParam = 1
        v = GW.InstrEdit('',self)
        v.setMinimumWidth(80)
        v.setSizePolicy(G.QSizePolicy.Preferred,G.QSizePolicy.Preferred) 
        u.addWidget(v)
        v.Activate(False)
        v = GW.InstrEdit('',self)
        u.addWidget(v)
        v.Activate(False)
        if not Param==None:
            u.itemAt(0).widget().setCurrentIndex(sorted(self.listmethods.keys()).index(Param[0]))
            print 'methods', self.listmethods[Param[0]]
            print 'param',Param[1]
            self.InstrChanged(1,u.itemAt(0).widget(),u,'Param')
            if Param[0] in self.ExpModel.TimingInstr:
                listmethodstimeinstr=[]
                for i,meth in enumerate(self.listmethods[Param[0]]):
                    try:
                        print 'split', meth.split('-')
                        listmethodstimeinstr.insert(i,meth.split('-')[1])
                    except:
                        listmethodstimeinstr.insert(i,'')
                print listmethodstimeinstr
                u.itemAt(1).widget().setCurrentIndex(listmethodstimeinstr.index(str(Param[1])))
            else:
                u.itemAt(1).widget().setCurrentIndex(self.listmethods[Param[0]].index(str(Param[1])))
            self.Changed(self.nParam,'methods',u.itemAt(1).widget(),u,Param[0])
            u.itemAt(2).widget().setText(Param[2])
            u.itemAt(3).widget().setText(Param[3])
        self.ParamLine[self.nParam]=u
        self.InstrLayout.insertLayout(self.nParam+1,u)
#        if i>0:
#            v.setCurrentIndex(i)
        print 'Added Param', self.EmptyParam
        '''
        # ajouter sur le widget 3 des params à passer au fonction le repère
        # c'est un TimeInstr, et mettre le prototype qui marche avec le validator
        '''      
        
        
    def InstrParamChanged(self,i):
        if not self.GUI.LastClick():
            return    
        InstrBox = self.sender()
        lay = self.ParamLine[InstrBox.i]
        self.InstrChanged(i,InstrBox,lay,'Param')
        
    def InstrRecChanged(self,i):
        if not self.GUI.LastClick():
            return    
        InstrBox = self.sender()
        lay = self.RecLine[InstrBox.i]
        self.InstrChanged(i,InstrBox,lay,'Rec')    
        
        
    def InstrChanged(self,i,InstrBox,lay,t):          
        Instr = InstrBox.currentText()
        w = lay.itemAt(1).widget()
        w.clear()
        w.setCurrentIndex(-1)
        print 'InstrChanged',i
        if not i>0:
            lay.itemAt(0).widget().SetColor(0)
            lay.itemAt(1).widget().SetColor(0)
            lay
            w.currentIndexChanged.disconnect()
            lay.itemAt(2).widget().Activate(-1)
            lay.itemAt(3).widget().Activate(-1)
            return 
        Instr = InstrBox.currentText()
        
        if t=='Param':
            w.addItems(sorted(self.listmethods[Instr]))
            w.currentIndexChanged.connect(self.ParamChanged)
        else :
            w.currentIndexChanged.connect(self.RecChanged)
            w.addItems(sorted(self.listread[Instr]))
        print 'Should be connected'
        u =lay.itemAt(2).widget()
        print 'type u',type(u)
        if Instr in self.ExpModel.TimingInstr:            
            u.isTimeInstr=True
        else:
            u.isTimeInstr=False
            
        
    def ParamChanged(self,i):    
        print 'ParamChanged'
        if not self.GUI.LastClick():
            print 'returned'
            return
        ParamBox = self.sender()
        if not i>0:
            #print self.EmptyParam, 'l482'
            self.EmptyParam +=1
            ParamBox.Empty = True
            return
        else : 
            if ParamBox.Empty:
                self.EmptyParam -=1
                #print self.EmptyParam, 'l489'
                #if not self.EmptyParam :
                if self.ParamLine[self.nParam-1].itemAt(1).widget().IsActivated():
                    print 'Adding Param'
                    self.addParam()
                    self.nParam+=1
        lay = self.ParamLine[ParamBox.i]
        Instr = lay.itemAt(0).widget().currentText()
        self.Changed(i,'methods',ParamBox,lay,Instr)
        
    
    def RecChanged(self,i):
        print 'RecChanged'
        if not self.GUI.LastClick():
            print 'returned'
            return
        RecBox = self.sender()
        if not i>0:
            self.EmptyRec +=1
            print self.EmptyRec, 'l506'
            RecBox.Empty = True
            return
        else : 
            if RecBox.Empty:
                self.EmptyRec -=1
                print self.EmptyRec,' l512'
                if self.RecLine[self.nRec-1].itemAt(1).widget().IsActivated():
                    self.addRec()
                    self.nRec+=1
        lay = self.RecLine[RecBox.i]
        Instr = lay.itemAt(0).widget().currentText()
        try : 
            self.Changed(i,'read',RecBox,lay,Instr)
        except Exception as e : 
            NewLog(e,'Not sure i understand',0)
        #╬lay.itemAt(3).widget().hide()
        
    def Changed(self,i,m,Box,lay,Instr):
        '''
            UserDefined elements written in the line edits must match the
            function prototypes
        '''           
        print 'Changed'
        u = lay.itemAt(2).widget()
        v = lay.itemAt(3).widget()
        y=getattr(self,'list'+m)
        w = y[Instr]
        i = w.index(Box.currentText())
        if m == 'read':
            funcname = 'Read'+y[Instr][i]
        else : 
            funcname = 'Write'+y[Instr][i]
        print funcname, i, Instr
        if not u.isTimeInstr:
            func = getattr(self.ExpControl._InstrumentInstances[Instr],funcname)
        if not i>0 : #user changed to no def
            u.Activate(False)
            v.Activate(False)
        else :
            if u.isTimeInstr:
                vmsg = 'dt:()'
                msg = 'dt:(float)'
            else : 
                from inspect import getargspec
                ArgSpec = getargspec(func)
                print ArgSpec
                if ArgSpec[2]:
                    msg = ',kw : ()'
                args=ArgSpec[0]
                if not ArgSpec[3] is None:
                    for a in ArgSpec[3][::-1]:
                        msg = a+':'+args.pop(-1) +msg
                else : 
                    msg = ''
                for a in ArgSpec[0][::-1]:
                    if not a=='self':
                        msg = a+':(),'+msg
                vmsg = msg
            if len(vmsg) and vmsg[-1] ==',':
                vmsg = vmsg[:-1]    
            if len(msg) and msg[-1] ==',':
                msg = msg[:-1]
            print 'msg written = ', msg
            u.Activate(True)    
            a = MatchPrototype(u)
            setattr(a,'fproto',vmsg)
            if m == 'read':
                setattr(a,'varyval',0)
            else : 
                setattr(a,'varyval',1)
            u.setStatusTip(msg)
            u.setToolTip(msg)
            u.setText(msg)
            u.setValidator(a)
            v.Activate(True)
            b = IsList(v)
            a.valtyp.connect(b.GiveType)
            v.setValidator(b)
            a.validate(msg,0)
            

        
    def OK(self,launch=True):
        '''
        Load Sequence Params for ExpRT from Sequence Window (= Opposite of self.LoadSequence) 
        '''
        print self.listmethods['BNCDelayGen']
        print self.listmethods['BNCDelayGen'][2].split('-')
        
        print 'OK-self.param',self.nParam
        print 'OK-self.rec', self.nRec
        if not self.GUI.LastClick():
            return
        self.resultseq={}
        self.resultrec={}
        for i in range(self.nParam-1):
            lay = self.InstrLayout.itemAt(i+1)
            Instr = lay.itemAt(0).widget().currentText()
            func = lay.itemAt(1).widget().currentText()
            print '3e case acceptable?:', lay.itemAt(2).widget().hasAcceptableInput()==True
            if lay.itemAt(2).widget().hasAcceptableInput():
                predic = lay.itemAt(2).widget().text()
                print predic
                lay.itemAt(2).widget().validator()
                pred2 = lay.itemAt(2).widget().validator().params
                print predic, pred2, type(pred2)
                if lay.itemAt(3).widget().hasAcceptableInput():
                    
                    if hasattr(self.resultseq,u'Vary'+unicode(func)):
                        keys=sorted(self.resultsec.keys,reverse=True)
                        key = [key for key in keys if not key.find('Vary'+unicode(func))+1][0]+'_1'
                    else : 
                        key = u'Vary'+unicode(func)
                    if Instr in self.ExpModel.TimingInstr:
                        func = func[3:]
                    self.resultseq[key] = ({'InstrName':Instr,
                                            'InstrCommand':func,
                                            'Instrkwargs': pred2,
                                            'param' : lay.itemAt(2).widget().validator().vary,
                                            'List':lay.itemAt(3).widget().validator().res})
                else : 
                    print 'List not acceptable'
                    return
            else : 
                print 'params for vary param not acceptable'
                return
        lmp=0        
        for i in range(self.nRec-1):
            print lmp
            lmp+1
            lay = self.InstrLayout.itemAt(i+self.nParam+2)
            print 'typelayOK:',type(lay)
            Instr = lay.itemAt(0).widget().currentText()
            print Instr
            func = lay.itemAt(1).widget().currentText()
            if not lay.itemAt(2).widget().hasAcceptableInput():
                params = lay.itemAt(2).widget().validator().params
                print 'Fixing Patams', params
                try : 
                    for p in params.iterkeys() : 
                        toto = self.ExpControl._InstrumentInstances['Lambdameter']
                        params[p] = getattr(toto,p)
                        lay.itemAt(2).widget().setText(p+':('+unicode(params[p])+')')
                        print 'typewidgetOK:',type(lay.itemAt(2).widget())
                except Exception as e:
                    print 'NotValid'     
                    return
            print lay.itemAt(2).widget().hasAcceptableInput(),lay.itemAt(2).widget().text(),lay.itemAt(2).widget().validator().fproto
                        
            if lay.itemAt(2).widget().hasAcceptableInput():
                 pred2 = lay.itemAt(2).widget().validator().params
                 self.resultrec[u'Rec'+unicode(func)+'_'+unicode(pred2)] = ({'InstrName':Instr,
                        'InstrCommand':func,
                        'Instrkwargs': pred2})
            else :
                print 'params for rec param not acceptable'
                return
            if launch:
                self.accept()

        
    def LoadSequence(self,Dic):
        '''
        Load a Sequence window from a dictionnary which contains Sequence Params from ExpRT (=Opposite of self.OK) 
         '''
        resseq={}
        resrec={}
        for key,val in Dic.iteritems():
            if key.find('Vary')==0:
                resseq[key]=val
            if key.find('Rec')==0:
                resrec[key]=val
        Nparam=len(resseq.keys())
        Nrec=len(resrec.keys())
        NameSave=Dic['Save']
        NAverages=Dic['Averages']
        print 'nparam',Nparam
        print 'nrec',Nrec
        print resrec
        for val in resseq.itervalues():
            NewPar=(val['InstrName'],val['InstrCommand'],str(val['param']+':(float)'),str(val['List']))
            self.addParam(Param=NewPar)
            self.nParam+=1
        self.addParam()
        self.nParam+=1
        for val in resrec.itervalues():
            if val['Instrkwargs']=={}:
                NewRec=(val['InstrName'],val['InstrCommand'],'','')
            else:
                NewRec=(val['InstrName'],val['InstrCommand'],str(val['Instrkwargs'].keys()[0])+':('+str(val['Instrkwargs'].values()[0])+')','')
            self.addRec(Rec=NewRec)
            self.nRec+=1
        self.addRec()
        self.nRec+=1
        self.Hint = GW.InstrOptions('',self)
        self.Hint.Gold()
        self.InstrLayout.addWidget(self.Hint)
        u = GW.InstrHLayout()
        self.InstrLayout.addLayout(u)
        v = GW.InstrLabel('Averaging number',self)
        u.addWidget(v)
        self.num = GW.InstrSpinBox(NAverages,0,1000,self)
        u.addWidget(self.num)
        u = GW.InstrHLayout()
        self.InstrLayout.addLayout(u)
        v = GW.InstrLabel('ExperimentName',self)
        u.addWidget(v)
        try :
            Name=NameSave.split('_')[0]+'_'+NameSave.split('_')[1]+'_NEWNAME'
            self.name = GW.InstrEdit(Name,self)
        except:    
            self.name=GW.InstrEdit('',self)
        u.addWidget(self.name)
        a = dirValidator()
        a.currentdir = self.ExpControl.GetTodayDir()
        self.name.setValidator(a)

    def LoadSeqParams(self):
        '''
         Load from a file and Reload the window with params from the file
         '''
        import cPickle
        dir0=ExpParamDir[gh()]
        Dir =G.QFileDialog.getOpenFileName(None, 'Open Data', dir0,"AllFiles (*)")
        strDir=unicode(Dir)
        File=open(strDir,'r')
        tmp=cPickle.load(File)
        self.RenewInit(tmp)
        
    
    def SaveSeqParams(self):
        
        '''
         Save window params in a file
         '''
        import os
        import cPickle
        self.OK(launch=False)
        print self.name.text()
        tmp = {'Save':self.name.text(),
              'Averages':self.num.value(),
              }
        for key,val in self.resultseq.iteritems() : 
           tmp[key]=val
        for key,val in self.resultrec.iteritems() : 
           tmp[key]=val
        
        dir0=os.getcwd()
        Dir =G.QFileDialog.getExistingDirectory(None, 'Open Data', dir0)
        strDir=unicode(Dir)
        os.chdir(strDir)
        print tmp['Save']
        if tmp['Save']:
            File=open(tmp['Save']+'.ExpSequence','w')
        else:
            File=open('0000.ExpSequence','w')
        cPickle.dump(tmp,File) 
        File.close()
         
   
   

       

           
           


#%%

class ListParam(G.QDialog,Ui_UpdateDict):
    '''
        List all attributes in the Intrument class Instr, and allow to modifies them
    '''      
    def __init__(self, Instr,InstrModel,parent=None):
        super(ListParam, self).__init__(parent)
        self.setupUi(self)
        self.Model = InstrModel
        self.GUI = parent
        self.Instr = Instr
        try :         
            self._Write = getattr(parent.ExpControl,'WriteToInstrument')
            self._Read = getattr(parent.ExpControl,'ReadInstrument')
        except Exception as e : 
            NewLog(e,'Cannot access to controller read write functions')
        from importlib import import_module
        try :
            (NameModule,NameClass,Instrkwargs) = InstrumentDict[Instr]
        except Exception as e: 
            NewLog(e)
            return
        module = import_module(NameModule)
        self.Instrclass = getattr(module,NameClass)
        from inspect import ismethod,getmembers
        self.listmethods = getmembers(self.Instrclass,predicate=ismethod)
        self.ListWrite = [m for m in self.listmethods if 'Write' in m[0]]
        self.DoInit()
        rb = [but for but in self.buttonBox.buttons() if but.text()==u'Reset']
        ab = [but for but in self.buttonBox.buttons() if but.text()==u'Apply']
        self.connect(ab[0],C.SIGNAL('clicked()'),self.OK)            
        self.connect(self.buttonBox,C.SIGNAL('Rejected()'),self.Cancel)
        self.connect(rb[0],C.SIGNAL('clicked()'),self.DoInit)
        self.olddic = {}
        for name,func in self.ListWrite : 
            dic = getattr(self.InstrFrame,'{}kw'.format(name[5:]))
            self.olddic[name[5:]] = dic.copy()            
        
        
    def DoInit(self):
        from inspect import getargspec
        print 'Model = ' , self.Model
        for n,(name,fnc) in enumerate(self.ListWrite):
            wl = GW.InstrOptions(name[5:],self.InstrFrame)
            self.InstrLayout.addWidget(wl,n,0)
            wl.setMaximumSize(80,30)
            setattr(self.InstrFrame,'{}kw'.format(name[5:]),{})
            dic = getattr(self.InstrFrame,'{}kw'.format(name[5:]))
            args,varargs,kw,defaults = getargspec(fnc)            
            [varargs,kw,defaults] = [ [] for e in [varargs,kw,defaults] if type(e)==type(None)]
            args.pop(args.index('self'))
            for i in range(len(args)-len(defaults)):
               defaults.insert(0,None)
            for nam,value in zip(args,defaults) :
                dic[nam]=value
            print 'Name = ',name
            print 'Dico = ', dic
            try :      
                tmp = self.Model[name[5:]]
                if len(args)==1 :
                    dic[nam] = tmp
                else :
                    for key in dic.iterkeys():
                        dic[key] = tmp[key]
            except KeyError as e :
                NewLog(e,'Parameter not found in ExpModel',2) 
            except Exception as e :
                NewLog(e) 
            self.CheckNone(dic)
            dic = getattr(self.InstrFrame,'{}kw'.format(name[5:]))
            NewLog(e,str(dic),2)
            wd = GW.InstrDict(dic,self.InstrFrame)
            setattr(self.InstrFrame,'{}DicWidget'.format(name[5:]),wd)
            self.InstrLayout.addWidget(wd,n,1)
            setattr(self.InstrFrame,'{}Button'.format(name[5:]),GW.InstrButton('Modify',self.InstrFrame))
            wb = getattr(self.InstrFrame,'{}Button'.format(name[5:]))
            test = '{}Button'.format(name[5:])
            wb.setObjectName(test)
            self.InstrLayout.addWidget(wb,n,2)
            self.connect(wb,C.SIGNAL('clicked()'),self.Update)
    
    def CheckNone(self,dic):
        for key,val in dic.iteritems():
            if val == None :
                try : 
                    l = [m for m in self.listmethods if ('Read' in m[0] and key in m[0])]
                    if len(l)==1 :                    
                        res = self._Read(self.Instr,key,**{})
                    elif len(l)==0 : 
                        res = self.Model[key]
                    dic[key] = res
                except KeyError as e:
                    NewLog(e,'Cannot find current or automatic value of {} parameter in {} class'.format(key,self.Instr), 2)
                    if self.Instr == 'Scope':
                        NewLog(None,'Would be better to program something in Scope C', 2)
                        pass
                except Exception as e : 
                    NewLog(e)
                
      
    def Update(self):
        if not self.GUI.LastClick():
            return
        obj = self.sender()
        Name = obj.objectName()
        n = Name.find('Button')
        line = Name[:n]
        dic = getattr(self.InstrFrame,'{}kw'.format(line))
        dlg2 = UpdatingDict(dic,self.Instr,self,'Write{}'.format(line))
        if dlg2.exec_():
            dic = getattr(self.InstrFrame,'{}kw'.format(line))
            for key,val in dic.iteritems() : 
                dic[key]=val
            NewLog(None,'dic ={}'.format(str(dic)),2)
        t=getattr(self.InstrFrame,'{}DicWidget'.format(line))
        t.showDic(dic)
     
    def OK(self):
        if not self.GUI.LastClick()  :
            return
        for name,func in self.ListWrite : 
            dic = getattr(self.InstrFrame,'{}kw'.format(name[5:]))
            if not self.olddic[name[5:]] == dic :
                try: 
                    rep = self._Write(self.Instr,name[5:],**dic)
                    if 'Done'.find(rep):
                        NewLog(None,'self._Write({}{}{}) gave {}'.format(self.Instr,name[5:],str(dic),rep),2)
                except Exception as e :
                    NewLog(e, 'Should be able to modify {} param {}'.format(self.Instr,name[5:]),Verbose = 0)    
        self.accept()           
        
    def Cancel(self):       
        if not self.GUI.LastClick()  :
            return
        self.reject()
            
            
        
#%%
    
 
    
class UpdatingDict(G.QDialog,Ui_UpdateDict):
    '''
        List all attributes in a dictionnary Dict containing parameters passed to
        functions func of Instr, to modify them or to reintialize them by parsing
        the function prototype
    '''
    def __init__(self, Dict,Instr,parent=None,func='__init__'):
        super(UpdatingDict, self).__init__(parent)       
        self.setupUi(self)
        self.func = func
        self.n=[] 
        self.kwold =Dict
        self.Instr = Instr
        self.setWindowModality(C.Qt.ApplicationModal)
        for kwitem,kwvalue in Dict.iteritems() :
            self.n.append(kwitem)
            self.AddLine(kwitem,kwvalue)
        rb = [but for but in self.buttonBox.buttons() if but.text()==u'Reset']
        ab = [but for but in self.buttonBox.buttons() if but.text()==u'Apply']
        self.connect(ab[0],C.SIGNAL('clicked()'),self.OK)            
        self.connect(self.buttonBox,C.SIGNAL('Rejected()'),self.Cancel)
        self.connect(rb[0],C.SIGNAL('clicked()'),self.Reset)

   
    def Reset(self):
        for names in self.n:
            self.RemoveLine(names)
        from importlib import import_module
        try :
            (NameModule,NameClass,Instrkwargs) = InstrumentDict[self.Instr]
        except Exception as e: 
            NewLog(e)
            return
        module = import_module(NameModule)
        Instrclass = getattr(module,NameClass)
        from inspect import getargspec
        fcn = getattr(Instrclass,self.func)
        args,varargs,kw,defaults = getargspec(fcn)            
        [varargs,kw,defaults] = [ [] for e in [varargs,kw,defaults] if type(e)==type(None)]
        args.pop(args.index('self'))
        for i in range(len(args)-len(defaults)):
            defaults.insert(0,None)
        for item,value in zip(args,defaults) : 
            self.n.append(item)
            self.AddLine(item,value)
        if len(kw) :
            self.addButton = GW.InstrButton('Add Parameter',self)
            self.InstrLayout.addWidget(self.addButton, self.n,0)
#            

        
    def AddLine(self,kwitem=None,kwvalue=None):
        n = len(self.n)                    
        if hasattr(self,'addButton'):
            self.InstrLayout.removeWidget(self.addButton)
            self.InstrLayout.addWidget(self.addButton,n+1,0)
            
        if kwvalue == None:
            newWindow = NewDictEntry(kwitem,self)
            retu = newWindow.exec_()
            print retu
            if retu :
                kwitem,kwvalue = newWindow.getValues()
            else : 
                try : 
                    raise Exception('GUI not working properly')
                except Exception as e : 
                    NewLog(e)
                    return
        
        setattr(self.InstrLayout,'{}type'.format(kwitem),type(kwvalue))                  
        setattr(self.InstrLayout,'{}OldValue'.format(kwitem),GW.InstrOptions(kwitem+'/'+str(kwvalue),self.InstrFrame))
        tmp=getattr(self.InstrLayout,'{}OldValue'.format(kwitem))
        self.InstrLayout.addWidget(tmp,n,0)  
        tmp.setObjectName('{}OldValue'.format(kwitem))
        self.setProperty('styleSheet','background-color: rgb(255, 220, 93)')
        setattr(self.InstrLayout,'{}Item'.format(kwitem),GW.InstrOptions(kwitem,self.InstrFrame))
        self.InstrLayout.addWidget(getattr(self.InstrLayout,'{}Item'.format(kwitem)),n,1)
        setattr(self.InstrLayout,'{}NewValue'.format(kwitem),GW.InstrEdit(str(kwvalue),self.InstrFrame))
        tmp=getattr(self.InstrLayout,'{}NewValue'.format(kwitem))
        self.InstrLayout.addWidget(tmp,n,2)  
        setattr(self.InstrLayout,'{}Remove'.format(kwitem),GW.InstrButton('Remove',self.InstrFrame))
        tmp=getattr(self.InstrLayout,'{}Remove'.format(kwitem))
        tmp.setObjectName('{}Remove'.format(kwitem))
        self.InstrLayout.addWidget(tmp,n,3)
        self.connect(tmp,C.SIGNAL('clicked()'),self.RemoveLine)              
        
    
    def RemoveLine(self,line=''):
        if not len(line):
            obj = self.sender()
            Name = obj.objectName()
            n = Name.find('Remove')
            line = Name[:n]      
        for name in ['OldValue','NewValue','Remove','Item']:
            qobj = getattr(self.InstrLayout,'{}{}'.format(line,name))
            qobj.hide()
            self.InstrLayout.removeWidget(qobj)
            if name == 'Remove' : 
                self.disconnect(qobj,C.SIGNAL('clicked()'),self.RemoveLine) 
            delattr(self.InstrLayout,'{}{}'.format(line,name))
        self.n.remove(line)
    
    def Cancel(self):
        self.accepted()
            
    def OK(self):
        for kwitem in self.n : 
            typ = getattr(self.InstrLayout,'{}type'.format(kwitem))
            obj = getattr(self.InstrLayout,'{}NewValue'.format(kwitem))
            self.kwold[kwitem]= ReType[typ](obj.text())
        self.accept()           

#%%


class WhichLevel(G.QDialog,Ui_ChooseRydberg):
    '''
        Choosing the target Rydberg Frequency
    '''
    def __init__(self, f=None,r=None,laser='Matisse',parent=None):
        super(WhichLevel, self).__init__(parent)    
        self.setupUi(self)
        self.setWindowModality(C.Qt.ApplicationModal)
        self.f = f
        self.r = r
        if f : 
            self.FrequencyBox.setValue(f)
        if r : 
            self.RydbergLine.setText(r)
        self.DoAbord.clicked.connect(self.Cancel)
        self.AcceptR.clicked.connect(self.AcceptRydberg)
        self.AcceptF.clicked.connect(self.AcceptFreq)
        self.Laser = laser
        
    def HandleResult(self):
        return self.f,self.r
    
    def AcceptRydberg(self):
        self.r =  self.RydbergLine.text()       
        print self.r
        try :
            self.f =  self.calcFreq()
            self.accept()
        except Exception as e:
            NewLog(e,'Probably a wrong Rydberg level string formatting',1)
    
    def AcceptFreq(self):
        self.f = self.FrequencyBox.value()
        self.r = ''
        self.accept()
        
    def calcFreq(self):
        if self.Laser == 'Solstis':
            n,ser = self.r.split('-')
            Freq=RydbergLevelsYb(int(n),ser)
            
            return Freq
        
        else:
            print 'ok'
            l,r = self.r.split('/')
            dic = {'s':0,'p':1,'d':2,'f':3,'g':4}
            for car in l:
                if car in dic.keys():
                    L = dic[car]
                    c=car
            n,J = l.split(c)
            NewLog(None, 'State = {}{}{}'.format(n,L,J),2)
            if self.Laser == 'Matisse' : 
                ff = RydbergLevelsCesium(int(n),L,int(J),'7s')
            elif self.Laser == 'QCW' : 
                ff =  RydbergLevelsCesium(int(n),L,int(J),'6p')
            NewLog(None,'{} was calculated to be {:.6f}'.format(self.r,ff),2)
            return ff
        

    def Cancel(self):
        self.reject()


#%%

    
class LevelValidator(G.QValidator): 
    def validate(self,val,i):
        def testint(s):
            try : 
                return type(int(s))==type(2) 
            except : 
                return False
        
        try :         
            l,r = val.split('/')
        except Exception:
            return (G.QValidator.Intermediate,val,i)
        level = ['s','p','d','f','g','h','i']
        if len(r) and not r=='2':
            return (G.QValidator.Invalid,val,i)
        if not len(r):
            return (G.QValidator.Intermediate,val,i)
        for car in l:
            if car in level:
                i=level.index(car)
                nl,nr = l.split(level[i])
        if not (int(nr)>0  and int(nr) in [2*i-1,2*i+1]):
            return (G.QValidator.Intermediate,val,i)
        try : 
            if testint(nl) and testint(nr):
                return (G.QValidator.Acceptable,val,i)   
            return (G.QValidator.Invalid,val,i)
        except : 
            return (G.QValidator.Invalid,val,i)


#%%    
class RecRef(G.QDialog,Ui_UpdateDict):
    '''
        List all attributes in a dictionnary Dict containing parameters passed to
        functions func of Instr, to modify them or to reintialize them by parsing
        the function prototype
    '''
    def __init__(self, refname,f0,parent):
        super(RecRef, self).__init__(parent)       
        self.setupUi(self)
        self.ini=(refname,f0)
        self.setMinimumHeight(400)
        self.setWindowModality(C.Qt.ApplicationModal)
        lparams = ['Level Label (i.e. npx/2)', 'Averaging Number', 'Expected Frequency Resonnance (THz)', 'Frequency Span (MHz)']        
        for i,name in enumerate(lparams) : 
            u=GW.InstrLabel(name,self)
            u.setMaximumHeight(50)
            u.setSizePolicy(G.QSizePolicy.Minimum,G.QSizePolicy.Expanding)
            u.Gold()
            self.InstrLayout.addWidget(u,i,0)
        v = GW.InstrEdit(refname,self)
        v.setValidator(LevelValidator())
        self.InstrLayout.addWidget(v,0,1)
        v = GW.InstrSpinBox(200,10,2000,self)
        v.setSingleStep(50)
        self.InstrLayout.addWidget(v,1,1)
        v = GW.InstrSpinBox(200,10,100000,self)
        v.setSingleStep(50)
        self.InstrLayout.addWidget(v,3,1)     
        print f0
        v = GW.InstrFloatBox(f0,25e-6,300,800,self)
        v.setDecimals(6)
        v.setValue(f0)
        v.setSingleStep(25e-6)
        self.InstrLayout.addWidget(v,2,1) 
        rb = [but for but in self.buttonBox.buttons() if but.text()==u'Reset']
        ab = [but for but in self.buttonBox.buttons() if but.text()==u'Apply']
        self.connect(ab[0],C.SIGNAL('clicked()'),self.OK)            
        self.connect(self.buttonBox,C.SIGNAL('Rejected()'),self.Cancel)
        self.connect(rb[0],C.SIGNAL('clicked()'),self.Reset)

    def OK(self):
        v = self.InstrLayout.itemAtPosition(0,1).widget()
        refname = v.text()
        v = self.InstrLayout.itemAtPosition(1,1).widget()
        niter = v.value()
        v = self.InstrLayout.itemAtPosition(2,1).widget()
        f0 = v.value()
        v = self.InstrLayout.itemAtPosition(3,1).widget()
        df = v.value()*1e-6      
        self.results = (refname,niter,f0,df)
        self.accept()          

    def Cancel(self):
        self.reject()

    def Reset(self):
        v = self.InstrLayout.itemAtPosition(0,1).widget()
        v.setText(self.ini[0])
        v = self.InstrLayout.itemAtPosition(1,1).widget()
        v.setValue(200)
        v = self.InstrLayout.itemAtPosition(2,1).widget()
        v.setValue(self.ini[1])
        v = self.InstrLayout.itemAtPosition(3,1).widget()
        v.setValue(200)        
        

#%%
class RunningDlg(G.QDialog,Ui_Running):
    Pausing = C.pyqtSignal()
    HaveFinished = C.pyqtSignal()
    def __init__(self,refname,n,parent=None):
        super(RunningDlg,self).__init__(parent)
        self.setupUi(self)
  #      self.setWindowModality(C.Qt.ApplicationModal)
        self.setWindowModality(C.Qt.NonModal)
        
        self.GUI=parent
        self.ExpModel = self.GUI.ExpModel
        if not self.GUI.NoLockSeq:
            self.GUI.MS.LambdameterRT.NewLambda.connect(self.PrintLambda)
        if hasattr(self.GUI.MS,'ScopeRT'):
            self.GUI.MS.ScopeRT.ScopeTriggered.connect(self.PrintAtNum)
        self.Name.setText('Ref Acquisition : '+refname)
        self.ProgressBar1.setMaximum(0)
        if hasattr(self.GUI.MS,'NewRef'):
            self.GUI.MS.NewRef.Progress.connect(self.ShowRefProgress)
            self.ProgressBar2.hide()
            self.ProgressBar1.setValue(0)
            self.navg=n

        if hasattr(self.GUI.MS,'ExperimentRT') and not self.GUI.MS.ExperimentRT.pause:
            self.HaveFinished.connect(self.GUI.MS.ExperimentRT.CatchHaveFinished)
            self.GUI.MS.ExperimentRT.Progress.connect(self.ShowExpProgress)
            self.ProgressBar1.setValue(0)
            self.ProgressBar2.setValue(0)
            self.npoints=n[0]
            self.navg = n[1]
            self.ProgressBar2.setMaximum(self.npoints)
            self.ProgressBar1.setMaximum(self.navg)
            self.Pausing.connect(self.GUI.MS.ExperimentRT.Pause)
            if not self.GUI.NoLockSeq:
                self.FreqLabel_2.setText('{:.6f}'.format(self.GUI.TargetFreq))
                self.GUI.MS.RydbergRT.EmitStatus.connect(self.ColorFreq)
        self.connect(self.Pause,C.SIGNAL('clicked()'),self.Pausing)
        self.connect(self,C.SIGNAL('rejected()'),self.AbortExperiment)
        
        
    def ShowRefProgress(self,n):
        if n>self.niter:
            self.accept()
            return
        self.ProgressBar1.setValue(n)
        
    def ShowExpProgress(self,n1,n2):
        if n2==self.npoints and n1==self.navg: 
            self.HaveFinished.emit()
            self.accept()
            return
        self.ProgressBar1.setValue(n1)
        self.ProgressBar2.setValue(n2)
        
    def ColorFreq(self,val):
        if val==Running:
            GW.SetStyle(self.FreqLabel,GW.yellow)  
        else : 
            GW.SetStyle(self.FreqLabel,GW.red)

    def FoundLock(self,n0):
        self.ProgressBar1.setMaximum(self.n)
        self.MaxAtomNum.setText('{:.3f}'.format(n0))
        self.FreqLabel_2.setText('{:.6f}'.format(self.MS.TargetFreq))
              
    def PrintLambda(self):
        self.FreqLabel.setText('{:.6f}'.format(self.ExpModel.Lambdameter['Frequency']))

    def PrintAtNum(self):
        if self.ExpModel.manipe == 'Ytterbium':
            self.AtomNumLabel.setText('{:.3f} / (PKPK:) {:.3f}'.format(self.ExpModel.Scope['CUST1'],self.ExpModel.Scope['PKPK0']))
        else : 
            self.AtomNumLabel.setText('{:.3f} / (PKPK:) {:.3f}'.format(self.ExpModel.Scope['CUST2'],self.ExpModel.Scope['PKPK0']))

    def AbortExperiment(self):
        print 'Abort ExperimentRT'
        self.GUI.MS.ExperimentRT.terminate()
        del self.GUI.MS.ExperimentRT
        
        
   

class PlotCamera(G.QDialog,Ui_PlotCamera):
    CameraTriggered=C.pyqtSignal()
    def __init__(self,parent=None):
        super(PlotCamera,self).__init__(parent)
        self.setupUi(self)
        self.GUI = parent
        self.ExpModel = self.GUI.ExpModel
        self.GUI.MS.CameraRT.CameraTriggered.connect(self.PlotNewImage)
        self.Img = self.ExpModel.CameraThor['SingleImageCapture']
        self.pxmap=G.QPixmap(self.ConvertImage(self.Img))
        self.vbox=self.verticalLayout
        self.size=self.pxmap.size()
        self.label = G.QLabel()
        self.label.setPixmap(self.pxmap)
        self.label.setFixedSize(self.size)
        self.vbox.addWidget(self.label)
        self.setSizeGripEnabled(False) 
        
    def PlotNewImage(self):
        self.Img = self.ExpModel.CameraThor['SingleImageCapture']
        self.pxmap=G.QPixmap(self.ConvertImage(self.Img))
        self.label.setPixmap(self.pxmap)    
            
    def ConvertImage(self,Img):
        import numpy as np
        self.height,self.width=Img.shape
        bytesPerLine=self.width
        qImg = G.QImage(Img.data, self.width, self.height, bytesPerLine,G.QImage.Format_Indexed8)
        return qImg
          
    def closeEvent(self,event):
        self.GUI.CameraImagDis()
        

#%%
            
class ExpMainView(QMainWindow, Ui_MainWindow):
    InstrChangedStatus = C.pyqtSignal('QString')
    rightclicked = C.pyqtSignal()
    TimingInstrStatus = C.pyqtSignal()
    UpdateTime = C.pyqtSignal(str,str,list)     
    UpdateTimings = C.pyqtSignal(list)
    NewTargetFreq= C.pyqtSignal(str)
    
    
    def __init__(self, parent=None):
        super(ExpMainView, self).__init__(parent)
        self.setupUi(self)
        self.GlobalLayout = G.QGridLayout(self.ExperimentTab)
        self.GlobalLayout.addWidget(self.Timings,0,0,1,0)
        self.GlobalLayout.addWidget(self.InstrumentStatus,1,0)
        self.GlobalLayout.addWidget(self.NumberFrame,1,1,C.Qt.AlignBottom)
        self.InitManipe()
    
        self.ConnectActions()
        self.NoLockSeq=False
    
    def InitManipe(self):
        self.ExpControl = Controller()
        self.ExpModel = self.ExpControl.ExpModel
        self._InstrList = self.ExpModel._InstrList
        self.ShowInstrList()
        self.ShowTimings()
        self.MS = MainSequence(self.ExpControl,self)
        self.MS.start()
        self.RydbergLevel = GW.ThreadButton('ChooseRydbergLevel',Suspend,self.ActionFrame)
        self.ActionLayout.addWidget(self.RydbergLevel)
        self.RecordRefButton = GW.ThreadButton('Record Reference',NotApplicable,self.ActionFrame)
        self.RunSeqButton = GW.ThreadButton('Run Sequence',NotApplicable,self.ActionFrame)
        self.CameraViewButton = GW.ThreadButton('CameraView',NotApplicable,self.ActionFrame)
        self.LockLaserButton = GW.ThreadButton('Lock Rydberg Laser',NotApplicable,self.ActionFrame)
        self.FindWavelengthButton = GW.ThreadButton('FindRydbergLine',NotApplicable,self.ActionFrame)
        tbs= [tb for tb in self.ActionFrame.children() if isinstance(tb,GW.ThreadButton)]
        for tb in tbs : 
            self.ActionLayout.addWidget(tb)
        
        self.ExpControl.ToggleInstruments('InternalMethods')
     
    def ConnectActions(self) : 
        self.connect(self.Tabs,C.SIGNAL( 'currentChanged(int)'),self.ChangeTab)
        self.connect(self.VerboseBox,C.SIGNAL('currentIndexChanged(QString)'),self.PrintLog)
        self.MS.InstrChangedStatus.connect(self.SetInstrStatus)
        self.RunSeqButton.ThreadSignal.connect(self.RunSeq)
        self.LockLaserButton.ThreadSignal.connect(self.LockLaser)
        self.LockLaserButton.Locked.connect(self.ActivateExp)
        self.RecordRefButton.ThreadSignal.connect(self.RecordRef)
        self.FindWavelengthButton.ThreadSignal.connect(self.FindWavelength)
        self.connect(self.RydbergLevel,C.SIGNAL('clicked()'),self.ChooseRydbergLevel)
        self.actionSave = G.QAction("Save",self)
        self.actionSave.setShortcut("Ctrl+S")
        self.menuFichier.addAction(self.actionSave)     
        self.actionSave.triggered.connect(self.Save)
        self.actionLoad = G.QAction("Load",self)
        self.actionLoad.setShortcut("Ctrl+L")
        self.menuFichier.addAction(self.actionLoad)     
        self.actionLoad.triggered.connect(self.Load)
        self.SimulateBox.stateChanged.connect(self.Simulate)
        self.NoLockSeqBox.stateChanged.connect(self.AllowNoLockSeq)
        self.CameraViewButton.ThreadSignal.connect(self.RunCameraView)
      #  self.connect(self.ReloadButton,C.SIGNAL('clicked()'), self.reloadExpRT)
        self.ExpControl._InstrumentInstances['InternalMethods'].NewTargetFreq.connect(self.ApplyNewTargetFreq)
        from time import time
        self._lastclick = time()
    
#    def reloadExpRT(self):
#        importOrReload('ExperimentalSequence','ExperimentRT')
        
    def RunCameraView(self):
        from time import clock
        if hasattr(self,'CamWindow'):
            delattr(self,'CamWindow')
            
        self.CamWindow=PlotCamera(self)
        self.CamWindow.show()
        
    def CameraImagDis(self):       
        self.MS.CameraRT.CameraTriggered.disconnect()
        
    def AllowNoLockSeq(self):
        if self.NoLockSeq==False:
            self.NoLockSeq=True
            self.ActivateExp()
        else:
            self.NoLockSeq=False
            self.ActivateExp()
            
    def Save(self):
        print 'Save'
        filename = G.QFileDialog.getSaveFileName(self,'Save experimental parameters',
                            self.ExpControl.GetTodayDir(),'ExpParams (*.ExpParams)') 
        if filename : 
            self.ExpControl.SaveExpModel(filename)
    
    def Load(self):
        print 'Load'
        filename = G.QFileDialog.getOpenFileName(self,'Open old experimental parameters',
                            self.ExpControl.GetTodayDir(),'ExpParams (*.ExpParams)') 
        self.ExpControl.LoadExpModel(filename)
 
        
    def LoadLast(self):
        print 'NotImplemented'
    
    def ActivateExp(self):
        if (self.LockLaserButton.val == Running):
            if self.RunSeqButton.val is NotApplicable : 
                
                self.RecordRefButton.SetState(Stop)
                self.RunSeqButton.SetState(Stop)
                self.FindWavelengthButton.SetState(Stop)
                
        elif self.NoLockSeq==True:
                self.RunSeqButton.SetState(Stop)     
        else :
            if not self.RunSeqButton.val is NotApplicable:
                self.RecordRefButton.SetState(NotApplicable)
                self.RunSeqButton.SetState(NotApplicable)
                self.FindWavelengthButton.SetState(NotApplicable)     
     
    def SetLockState(self,val):
        self.LockLaserButton.SetState(val)
        self.ActivateExp()
     
    def Simulate(self,i):
        if i:
            self.ExpModel.Simulate = True
        else :
            self.ExpModel.Simulate = False
    
    def RecordRef(self,newstatus):
        if newstatus == Starting:
             refname = self.RydbergLevel.text()
             f0 = self.ExpModel.Lambdameter['Frequency']
             dlg = RecRef(refname,f0,self)
             if dlg.exec_() : 
                 refname,niter,f0,df = dlg.results
             else :
                 self.RecordRefButton.SetState(Stop)
                 return
             self.MS.NewRef = RecordRef(refname,niter,f0,df,self.MS)
             self.IsRunningDlg = RunningDlg(refname,niter,self)
             self.IsRunningDlg.exec_()
             self.MS.OldRef =self.MS.NewRef
             delattr(self.MS,'NewRef')
        else : 
            pass
    
    def RunSeq(self,newstatus):
        from time import sleep        

        '''
            passes the argument to the ExprimentRT thing, and then shows the exp dialog
        '''
        
        os.chdir(self.MS.ExpControl.GetTodayDir())
        self.tmp = ChooseSequence(self)
        if hasattr(self,'TempSeq'):
            self.tmp.RenewInit(self.TempSeq)
        if self.tmp.exec_():
            resultseq= self.tmp.resultseq
            resultrec= self.tmp.resultrec
            num = self.tmp.num.value()
            name = self.tmp.name.text()
        else : 
            delattr(self,'tmp')
            return
            
        kw = ({'Save':name,
               'Averages':num,
               'NoLockSeq':self.NoLockSeq
               })
                  
        for key,val in resultseq.iteritems() : 
            kw[key]=val
        for key,val in resultrec.iteritems() : 
            kw[key]=val      
        self.TempSeq = kw.copy()
        niter = kw['Averages']
        self.MS.ExperimentRT = ExperimentRT(self.MS,**kw)
        
        npoints = self.MS.ExperimentRT.npoints
        
        self.IsRunningDlg = RunningDlg(name,[npoints,niter],self)
        self.IsRunningDlg.exec_()
        #self.IsRunningDlg.show()
    
    def LockLaser(self,newstatus):
        if not self.LastClick()  :      
            return
        self.MS.LockLaser()
        self.MS.RydbergRT.EmitStatus.connect(self.SetLockState)
        
    
    def FindWavelength(self,newstatus):        
        if self.FindWavelengthButton == NotApplicable : 
            return
        if not self.LastClick()  :
            return
        if newstatus == Stop :
            self.MS.Scan.running = 0
            self.MS.Scan.ScanFinished.disconnect()
            sleep(0.01)
            delattr(self.MS,'Scan')
            self.FindWavelengthButton.SetState(Stop)
            
        else : 
            if newstatus == Automatic : 
                center = self.ExpModel.Lambdameter['Frequency']
                df = 50e-6
            else:
                ui = AskFreqScan(self.ExpModel.Lambdameter['Frequency'],1e-4,self)
                res = ui.exec_()
                if res : 
                    center,df = ui.getValues()
                else :
                    center = self.ExpModel.Lambdameter['Frequency']
                    df = 1e-4

            setattr(self.MS,'ScanRT', ScanResonnance(self.MS,center,df))
            self.FindWavelengthButton.SetState(Running)
            self.MS.ScanRT.ScanFinished.connect(self.MS.FreqScanFinished)
            self.IsRunningDlg = RunningDlg('Scanning',0,self)
            self.MS.ScanRT.ScanFinished.connect(self.IsRunningDlg.accept)
            self.IsRunningDlg.exec_()
            self.MS.OldScan = self.MS.ScanRT
            delattr(self.MS,'ScanRT')
            self.FindWavelengthButton.SetState(Stop)
            
    def ClickOK(self):
        self._lastclick-=1
    
    def LastClick(self):
        from time import time
        tmp = time()
        if tmp-self._lastclick<0.2 : 
            return False
        else : 
            self._lastclick = tmp         
            return True
    
    def ChooseRydbergLevel(self) : 
        obj = self.RydbergLevel
        Lasers = [Instr for Instr in self.ExpControl._InstrumentInstances.iterkeys() if Instr in LaserList] 
        if not len(Lasers)==1 and not 'Lambdameter' in self.ExpControl._InstrumentInstances.keys(): 
            obj.SetState(NotApplicable)
            NewLog(None,'One and Only one Rydbarg Laser can be handled at the moment',1)
            return 
        if hasattr(self,'TargetFreq'):
            l = self.TargetFreq
        else :
            l=''
        if hasattr(self,'TargetRydberg'):
            r = self.TargetRydberg
        else :
            r =''
        t= WhichLevel(l,r,Lasers[0],self) 
        if t.exec_():
            tmp = t.HandleResult()
            self.TargetFreq = float(tmp[0])
            if tmp[1] :
                self.TargetRydberg = tmp[1]
                self.ExpModel.RydbergLevel = tmp[1]
                self.RydbergLevel.ChangeText(self.TargetRydberg)
            self.RydbergLevel.SetState(Stop)
            self.TargetFrequencyLabel.setText('{:.6f} THz'.format(self.TargetFreq))
            obj.SetState(Running)
            self.LockLaserButton.SetState(Stop)
            self.MS.TargetFreq = self.TargetFreq
            
            return
        obj.SetState(Stop)
    
    def ChangeTab(self,n):
        if n==3 : 
            v = self.VerboseBox.currentText() 
            self.PrintLog(v)
        
    def PrintLog(self,v):
        Dico = {}
#        print 'Verbose',v
#        if not v in DVerbose:
#            v= DVerbose[0]
            
        for key,value in ExperimentLog.iteritems(): 
            if DVerbose[v] == (value['Verbose']):
                Dico[key]=value.copy()
            if DVerbose[v] == 1 and value['Verbose'] ==0 :
                Dico[key]=value.copy()
            if DVerbose[v] ==3:
                Dico[key]=value.copy()
            if Dico.has_key(key):
                Dico[key].__delitem__('Verbose')
        self.fill_widget(self.TreeLog, Dico)
        self.TreeLog.collapseAll()
        
            
    def ShowTimings(self):
        self.TimingStruct = GW.TimingStruct(self.ExpModel,self) 
    #    self.rightclicked.connect(self.AddTime)
        self.TimingStruct.UpdateTime.connect(self.ControlTimings)
        self.TimingStruct.UpdateTimings.connect(self.ControlTimings)
        
    def ControlTimings(self,*ListArgs):
        try : 
            res = self.ExpControl.UpdateTimings(*ListArgs)
        except Exception as e:
            NewLog(e)
        finally : 
            self.TimingStruct.Refresh()
                
    def ChannelOptions(self):
        print 'ChannelOptions not implemented'
        NewLog(None,'Channel Options is not implemented',1)
                          
    def ShowInstrList(self):
        n=0 
        for Instr,Status in self._InstrList.iteritems() : 
            try : 
                temp = getattr(self.InstrLayout,'{}Status'.format(Instr))
                temp.Status(Status)
            except AttributeError : 
                setattr(self.InstrLayout,'{}Label'.format(Instr),GW.InstrOptions(Instr,self))
                temp = getattr(self.InstrLayout,'{}Label'.format(Instr))
                temp.setObjectName('{}Label'.format(Instr))
                self.InstrLayout.addWidget(temp,n,0)
                self.connect(temp,C.SIGNAL('clicked()'),self.ShowInstrKwargs)
                setattr(self.InstrLayout,'{}Status'.format(Instr),GW.ClickAndColor(Status,self.InstrFrame))
                tmp =getattr(self.InstrLayout,'{}Status'.format(Instr))
                tmp.setObjectName('{}Status'.format(Instr))
                self.InstrLayout.addWidget(tmp,n,1)
                self.connect(tmp,C.SIGNAL('clicked()'),self.ToggleInstr)
                setattr(self.InstrLayout,'{}Param'.format(Instr),GW.InstrButton('Params',self))
                tmp =getattr(self.InstrLayout,'{}Param'.format(Instr))
                tmp.setObjectName('{}Param'.format(Instr))
                self.InstrLayout.addWidget(tmp,n,2)
                self.connect(tmp,C.SIGNAL('clicked()'),self.ShowInstrParam)
            except Exception as e:
                NewLog(e)
                return
            finally : 
                n += 1
   
    def ShowInstrParam(self):
        '''
        Invokes a Dialog that prints all the Parameters user can set
        for a specific instrument and allows to change them
        '''        
        if not self.LastClick():
            return
        obj = self.sender()
        Name = obj.objectName()
        n = Name.find('Param')
        Instr = Name[:n] 
        if not self._InstrList[Instr]==2 :
            return
        ch = ListParam(Instr,getattr(self.ExpModel,Instr),self)
        if ch.exec_():
            #strange that there is no return ?
            return
                  
    def SetInstrStatus(self,name):    
        if not self.LastClick():
            return
        obj = getattr(self.InstrLayout,name)        
        try : 
            obj.Status(self._InstrList[name[:-6]])
            if obj._status==1:
                return
            if name[:-6] in self.ExpModel.TimingInstr : 
                self.TimingInstrStatus.emit()
            if name[:-6] == 'CameraThor':
                print 'there'
                self.MS.CameraRTStartStop()
                self.CameraViewButton.SetState(Stop)
                
            if name[:-6] == 'Lambdameter':
                if not obj._status ==  InitOK:
                    GW.SetStyle(self.FrequencyLabel,GW.yellow)
                else : 
                    GW.SetStyle(self.FrequencyLabel,GW.yellowgrey)
                self.MS.LambdaRT()                
            if name[:-6] == 'Scope':                
                if not obj._status == InitOK:
                    GW.SetStyle(self.RydbergNumberLabel,GW.gold)
                else : 
                    GW.SetStyle(self.RydbergNumberLabel,GW.yellowgrey)
                self.MS.OsciloRT()
            if name[:-6]  in LaserList and not self._InstrList[name[:-6]]: 
                if hasattr(self.MS,'RydbergRT'):
                    self.MS.RydbergLock = 0
                    self.MS.RydbergRT.running = 0
                    sleep(0.01)
                    delattr(self.MS,'RydbergRT')  
            if name[:-6] in ['Scope','Lambdameter']:
                self.ActivateExp()
            if name[:-6] in self.ExpModel.InstrToSet:
                self.MS.SetInstr(name[:-6])
        except Exception as e:
            NewLog(e)
    
    def PrintLambda(self):
        self.FrequencyLabel.setText('{:.6f}'.format(self.ExpModel.Lambdameter['Frequency']))
        if hasattr(self,'TargetFreq') and int(1e6*(self.TargetFreq-  self.MS.TargetFreq)):      
            self.TargetFrequencyLabel.setText('{:.6f} THz'.format(self.MS.TargetFreq))
            self.TargetFreq = self.MS.TargetFreq        
        
    def PrintAtNum(self):
        if self.ExpModel.manipe == 'Ytterbium':
            self.RydbergNumberLabel.setText('{:.3f} / (PKPK:) {:.3f}'.format(self.ExpModel.Scope['CUST1'],self.ExpModel.Scope['PKPK0']))
        else : 
            self.RydbergNumberLabel.setText('{:.3f} / (PKPK:) {:.3f}'.format(self.ExpModel.Scope['CUST2'],self.ExpModel.Scope['PKPK0']))

    
    def ToggleInstr(self):
        from time import sleep
        if not self.LastClick():
            return      
        obj = self.sender()
        Name = obj.objectName()
        n = Name.find('Status')
        Instr = Name[:n]      
        obj.Status(int(not self._InstrList[Instr]))
        sleep(0.01)        
        self.ExpectedStatus = self._InstrList.copy()
        self.ExpectedStatus[Instr] = int(not self.ExpectedStatus[Instr])
        [self.ExpectedStatus.__setitem__(key, InitOK) for key,value in self.ExpectedStatus.iteritems() if value is WaitToConnect]
        self.ExpControl.ToggleInstruments(Instr)
        
    def ShowInstrKwargs(self):
        obj = self.sender()
        Name = obj.objectName()
        n = Name.find('Label')
        Instr = Name[:n]
        dico = getattr(self.ExpModel,Instr)
        dlg = UpdatingDict(dico['InitKwargs'],Instr,self)
        if dlg.exec_():
            dico['InitKwargs'] = dlg.kwold
             
        
    def fill_item(self,item, value):
        item.setExpanded(True)
        if type(value) is dict:
            for key, val in sorted(value.iteritems()):
                child = G.QTreeWidgetItem()
                child.setText(0, unicode(key))
                item.addChild(child)
                self.fill_item(child, val)
        elif type(value) is list:
            for val in value:
                child = G.QTreeWidgetItem()
                item.addChild(child)
                if type(val) is dict:      
                    child.setText(0, '[dict]')
                    self.fill_item(child, val)
                elif type(val) is list:
                    child.setText(0, '[list]')
                    self.fill_item(child, val)
                else:
                    child.setText(0, unicode(val))              
                    child.setExpanded(False)
        else:
            child = G.QTreeWidgetItem()
            child.setText(0, unicode(value))
            item.addChild(child)

    def fill_widget(self,widget, value):
        widget.clear()
        self.fill_item(widget.invisibleRootItem(), value)
        widget.collapseAll()
        
        
    def ApplyNewTargetFreq(self,Arg):
        self.MS.TargetFreq=Arg
        

#%%
        
def execution():
    app = G.QApplication(sys.argv)
    test = 0
    if test == 0 :
        a = ExpMainView() 
        a.show()
        app.exec_()
        if hasattr(a.MS,'LambdameterRT'):
            a.MS.LambdameterRT.terminate()
        if hasattr(a.MS,'ScopeRT'):
            a.MS.ScopeRT.terminate()  
        if hasattr(a.MS,'CameraRT'):
            a.MS.CameraRT.running=False
            a.MS.msleep(2000)
            a.MS.CameraRT.terminate()
        return a

    elif test ==1 :    
        a = UpdatingDict({'toto':1},'Scope')
        print a.exec_()
    elif test == 2  :
        b = ExpMainView()      
        d = getattr(b.ExpModel,'Scope')
        a = ListParam('Scope',d)
        #a.exec_()
    elif test == 3  :
        b = ExpMainView()      
        d = getattr(b.ExpModel,'Scope')
        a = ListParam('Scope',d)
        a.show()
    elif test == 4 : 
        a = ExpMainView() 
        a.show()
        
    if a.ExpControl._InstrumentInstances.has_key('SRSDelayGen'):
        a.ExpControl._InstrumentInstances['SRSDelayGen'].__del__()
    return a
        
if __name__=="__main__":
    a=execution()

        
    