# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 10:27:21 2016

@author: lac
"""

import CameraLib as c
from matplotlib import pylab as plt
import numpy as np
import time as t

cam=c.Camera()
cam.WriteExposure(15)
cam.WriteGain(100)

raw_input('BackGround')
bckg=cam.ReadSoftTrigImage()
bckg=bckg.astype(np.float64)
t.sleep(0.1)
plt.ion()
plt.show()
i=1
while i<1000:
    t.sleep(0.1)
    Fluo=cam.ReadSoftTrigImage()
    Fluo=Fluo.astype(np.float64)  
    S=np.sum(Fluo-bckg)
    print S
    if S>0:    
        plt.scatter(t.clock(),S)
    plt.draw()
    plt.pause(0.1)
    i=i+1
 
        