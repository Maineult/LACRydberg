# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 22:02:27 2016

@author: henri
"""

import matplotlib.pyplot as plt
import numpy as np
import pickle
import ExperimentModel
import os

def SortbyFreq(Data):
    SortFreq=sorted(Data[0])
    CorrespSig=[]
    for i,freq in enumerate(SortFreq):
        CorrespSig.insert(i,Data[1][Data[0].index(freq)])
    Data=SortFreq,CorrespSig
    return Data    
        
##### INPUTs ######
filename='MW_64-1S0_StarkScan5VLargeAndPrecise.ExpResults'

###### MAIN #######

os.chdir("C:\\Users\\lac\\Documents\\RydbergYtterbiumData\\2016\\10 - 07 (Friday)")
name=filename.split('.')[0]
InitialState=name.split('_')[1]
ResultsFile=open(filename,'r')
Results=pickle.load(ResultsFile)
Freq=[]
Signal=[]
StdDevSignal=[]

for i,keyParam in enumerate(Results.Resultats.iterkeys()):  
    Data=Results.Resultats[keyParam]
    AverageNumber=len(Data)
    a=keyParam.split('{')[1].split('}')[0]
    Freq.insert(i,float(a.split(': ')[1]))
    S=[]
    for j in range(AverageNumber):
        S.insert(j,(Data[j]['Custom2']/Data[j]['Custom1']))
    Signal.insert(i,np.mean(S))
    StdDevSignal.insert(j,np.std(S))    

    

Data=[Freq,Signal]
Data=SortbyFreq(Data)
plt.plot(Data[0],Data[1],'o')
plt.plot(Data[0],Data[1],'b')




def SaveExpModel():
    pass    

def SaveData():
    os.chdir("C:\\Users\\lac\\Documents\\GoogleDriveYtterbium\\Donnees\\MicroWaveSpectroscopy")
    os.chdir('./From '+InitialState)
    filename=name
    np.savetxt(filename,Data)
#    fil=open(filename,'w')
#    pickle.dump(DicoData,fil)
    
                                                                                                                                                                                                                                                                                                                                                                                                
