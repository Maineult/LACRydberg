# -*- coding: utf-8 -*-
"""
Created on Mon Nov 07 17:49:01 2016

@author: lac
"""
'''
Mainly inspired (and heavily copy-pasted) of Nate Bogdanowicz Library. 
Main difference is that it does not handle colors
and it is more suited for fast ext trig capture
'''


import ctypes as c
from ctypes.wintypes import *
from UC480constants import *
from UC480structs import *
import win32event
import numpy as np

from time import sleep,clock
import platform



if platform.architecture()[0].startswith('64'):
    lib = c.WinDLL('uc480_64')
else:
    lib = c.CDLL('uc480')
NULL = c.POINTER(HWND)()    


class BufferInfo(object):
    def __init__(self, ptr, id):
        self.ptr = ptr
        self.id = id
        
def _value_getter(member_str):
    def getter(self):
        return getattr(self, member_str)
    return getter


### LOW LEVEL FUNCTIONS

class Camera():
    def __init__(self):
        num=INT()
        lib.is_GetNumberOfCameras(c.byref(num))
        self.CamID=DWORD(1)
        self.PixelClock=8
        ret=lib.is_InitCamera(c.pointer(self.CamID), NULL)
        self._enable_reports()
        lib.is_SetPixelClock(self.CamID,ULONG(self.PixelClock)) 
        self._width, self._height = INT(), INT()
        self._buffers = []
        self._seq_event = win32event.CreateEvent(None, False, False, '')
        self._frame_event = win32event.CreateEvent(None, True, False, '')
        self._color_depth = INT()
        self._color_mode = INT()
        self._refresh_sizes()
        self._queue_enabled=True
        self.set_external_trigger_mode()
        self._trig_event = win32event.CreateEvent(None, False, False, '')
        self._seq_event = win32event.CreateEvent(None, False, False, '')
        self._frame_event = win32event.CreateEvent(None, False, False, '')  # Don't auto-reset
        lib.is_InitEvent(self.CamID, self._trig_event.handle, IS_SET_EVENT_EXTTRIG)
        lib.is_InitEvent(self.CamID, self._seq_event.handle, IS_SET_EVENT_SEQ)
        lib.is_InitEvent(self.CamID, self._frame_event.handle, IS_SET_EVENT_FRAME)    
        self._allocate_mem_seq(1)
        self._init_colormode()
        self.WriteExposure(15)

        
    def _enable_reports(self):
        Boule=BOOL(True)
        print lib.is_SetErrorReport(self.CamID,c.byref(Boule))
            
        
    def _get_sensor_color_mode(self):
            sInfo = SENSORINFO()
            lib.is_GetSensorInfo(self.CamID, c.byref(sInfo))
            return int(sInfo.nColorMode.encode('hex'), 16)    
    
    def _init_colormode(self):
        sensor_mode =self._get_sensor_color_mode()
        mode_map = {
            IS_COLORMODE_MONOCHROME: (IS_CM_MONO8, 8),
            IS_COLORMODE_BAYER: (IS_CM_RGBA8_PACKED, 32)
        }
        try:
            mode, depth = mode_map[sensor_mode]
        except KeyError:
            raise Exception("Currently unsupported sensor color mode!")
        self._color_mode, self._color_depth = DWORD(mode), DWORD(depth)
        lib.is_SetColorMode(self.CamID, self._color_mode)    
    def _get_AOI(self):
        rect = IS_RECT()
        lib.is_AOI(self.CamID, IS_AOI_IMAGE_GET_AOI, c.byref(rect), c.sizeof(rect))
        return rect.s32X, rect.s32Y, rect.s32Width, rect.s32Height

    
    def _refresh_sizes(self):
        _, _, self._width, self._height = self._get_AOI()
        self._max_width, self._max_height = self._get_max_img_size()  
        
    def _get_max_img_size(self):
        # TODO: Make this more robust
        sInfo = SENSORINFO()
        lib.is_GetSensorInfo(self.CamID, c.byref(sInfo))
        return int(sInfo.nMaxWidth), int(sInfo.nMaxHeight)    
        
    def __test__(self):
        Time=DOUBLE()
        return lib.is_Exposure(self.CamID, IS_EXPOSURE_CMD_GET_EXPOSURE,c.byref(Time), 8)==IS_SUCCESS    
        
    def _free_image_mem_seq(self):
        lib.is_ClearSequence(self.CamID)
        for buf in self._buffers:
            lib.is_FreeImageMem(self.CamID, buf.ptr, buf.id)
        self._buffers = []

    def _allocate_mem_seq(self, num_bufs):
        """
        Create and setup the image memory for live capture
        """
        for i in range(num_bufs):
            p_img_mem = c.POINTER(c_char)()
            memid = INT()
            ret=lib.is_AllocImageMem(self.CamID, self._width, self._height, self._color_depth,
                                 c.pointer(p_img_mem), c.pointer(memid))                     
            lib.is_AddToSequence(self.CamID, p_img_mem, memid)
            self._buffers.append(BufferInfo(p_img_mem, memid))  
        
    def _array_from_buffer(self, buf):
        h = self.height
        arr = np.frombuffer(buf, np.uint8)

        if self._color_mode == IS_CM_RGBA8_PACKED:
            w = self.bytes_per_line/4
            arr = arr.reshape((h, w, 4), order='C')
        elif self._color_mode == IS_CM_BGRA8_PACKED:
            w = self.bytes_per_line/4
            arr = arr.reshape((h, w, 4), order='C')[:, :, 2::-1]
        elif self._color_mode == IS_CM_MONO8:
            w = self.width
            arr = arr.reshape((h, w), order='C')
        else:
            w = self.width
            arr = arr.reshape((h, w), order='C')
        return arr
    
    def _set_queueing(self, enable):
        if enable:
            if not self._queue_enabled:
                lib.is_InitImageQueue(self._hcam, 0)
        else:
            if self._queue_enabled:
                lib.is_ExitImageQueue(self.CamID)
        self._queue_enabled = True
    

    def set_software_trigger_mode(self):
        ret=lib.is_SetExternalTrigger(self.CamID, IS_SET_TRIGGER_SOFTWARE)
        if ret==IS_SUCCESS:
            self.mode='SoftwareTrigger'
            
    def software_trig_capture(self):
        if self.mode=='ExternalTrigger':
            self.set_software_trigger_mode()
        self._free_image_mem_seq()
        self._allocate_mem_seq(1)    
            
        lib.is_EnableEvent(self.CamID, IS_SET_EVENT_FRAME)
        lib.is_FreezeVideo(self.CamID, IS_DONT_WAIT)
        
            
    def set_external_trigger_mode(self):
        ret=lib.is_SetExternalTrigger(self.CamID, IS_SET_TRIGGER_LO_HI)
        self._set_queueing(True)
        if ret==IS_SUCCESS:
            self.mode='ExternalTrigger'
        
    def external_trig_capture(self):
        if self.mode=='SoftwareTrigger':
            self.set_external_trigger_mode()
        self._free_image_mem_seq()
        self._allocate_mem_seq(1)     
        lib.is_EnableEvent(self.CamID, IS_SET_EVENT_FRAME)
        lib.is_FreezeVideo(self.CamID, IS_WAIT)
        

    def read_image(self, timeout= 2000 , copy=True):
        ret = win32event.WaitForSingleObject(self._frame_event , timeout)
        lib.is_DisableEvent(self.CamID, IS_SET_EVENT_FRAME)
  #      print self.deux-self.prems
        if ret == win32event.WAIT_TIMEOUT:
            print 'TimeOutError'
        elif ret != win32event.WAIT_OBJECT_0:
            print ('Failed to grab image')
        mem_ptrs = [buf.ptr for buf in self._buffers]
        arrays = []
        for ptr in mem_ptrs:
            buf_ptr = c.cast(ptr, c.POINTER(c.c_char * (self.width*self.height)))
            array = self._array_from_buffer(buffer(buf_ptr.contents))
            arrays.append(np.copy(array) if copy else array)     
        if len(arrays) == 1:
            return arrays[0]
        else:
            return tuple(arrays) 
            
    
    def __del__(self):
        lib.is_ExitCamera(self.CamID)
    
    height = property(_value_getter('_height'))
    width = property(_value_getter('_width'))
        
        
        
#### HIGH LEVEL FUNCS        
        
        
    def WriteExposure(self,ExposureTime):
        ExposureTime=DOUBLE(ExposureTime)
        lib.is_Exposure(self.CamID,IS_EXPOSURE_CMD_SET_EXPOSURE,c.byref(ExposureTime),8)
        
    def ReadExposure(self):
        Time=DOUBLE()
        lib.is_Exposure(self.CamID, IS_EXPOSURE_CMD_GET_EXPOSURE,c.byref(Time), 8)
        return Time
    
    def WriteGain(self,Gain):
        ret=lib.is_SetHardwareGain(self.CamID,Gain,-1,-1,-1)
        return ret
    
    def ReadExtTrigImage(self):
        self.external_trig_capture()
        return self.read_image()
        
    def ReadSoftTrigImage(self):
        self.software_trig_capture()
        return self.read_image()
    
 
if __name__=='__main__' : 
    cam=Camera()
    for i in range(10):
        cam.ReadExtTrigImage()