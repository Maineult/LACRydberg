# -*- coding: utf-8 -*-
"""
Created on Fri Aug 26 15:32:39 2016

@author: lac
"""
import visa 
import time

class MicroWaveGen():
    def __init__(self,Configuration='x 6'):
        self.rm = visa.ResourceManager()
        self.MWG = self.rm.open_resource(u'GPIB0::19::INSTR')
        time.sleep(0.001)
        self.SetTriggerMode()
        self.MultipFactor=int(Configuration.split(' ')[1])

        
    def __test__(self):
        self.MWG.write('*IDN?')
        time.sleep(0.001)
        test= self.MWG.read()
        return (test == u'HEWLETT-PACKARD,83731B,US37100534,REV  10.02\n')
     
    def SetTriggerMode(self):
        self.MWG.write('TRIG:SEQ:STAR:SOUR: EXT')
     
    def ReadFrequency(self):
        self.MWG.write('FREQ:CW?')
        time.sleep(0.001)
        Freq=float(self.MWG.read())/1e9 
        return Freq*self.MultipFactor  ### In GHz
        
    def WriteFrequency(self,Freq):    ### In GHz
        Freq=Freq/self.MultipFactor
        Noreturn=self.MWG.write('FREQ:CW '+unicode(Freq)+'GHZ')
        
    def ReadPowerLevel(self):
        self.MWG.write('POW:LEV?')
        time.sleep(0.001)
        return float(self.MWG.read()) 
        
    def WritePowerLevel(self,Pow): 
        Noreturn=self.MWG.write('POW:LEV '+unicode(Pow)+'DBM') ## In dBm
        
    def WriteOutput(self,Bool):
        if Bool==True:
            Noreturn=self.MWG.write('OUTP:STAT ON')
        elif Bool==False:
            Noreturn=self.MWG.write('OUTP:STAT OFF')
        
        
        
        
        