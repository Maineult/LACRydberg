# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 14:20:06 2015

@author: lac
"""

import visa, pyvisa, time

##sleep time after every command:
ts =0.05

## PULSE GENERATOR GENERAL PROPERTIES ##

class PulseGen():
    def __init__(self):
        self.rm = visa.ResourceManager()
        self.PG = self.rm.open_resource(u'ASRL3::INSTR')
        self.PG.baud_rate=38400    
        self.PG.write(u':PULSE0:MODE NORM')
        time.sleep(ts)
        
              
    def cycleperiod(self,T):
        self.PG.write(u':PULSE0:PER '+unicode(T))
        time.sleep(ts)
        
    def singleshot(self):
        self.PG.write(u':PULSE0:MODE SING') 
        time.sleep(ts)
        
    def continuousshot(self):
        self.PG.write(u':PULSE0:MODE NORM:') 
        time.sleep(ts)
        
    def start(self):
        self.PG.write(u':PULSE0:STATE ON')
        
    def stop(self):
        self.PG.write(u':PULSE0:STATE OFF')
         
    def close(self):
        self.PG = self.PG.close()
        self.rm = self.rm.close()

## CHANNEL FUNCTIONS ##
        
class Channel(PulseGen):
    def __init__(self,n):
        PulseGen.__init__(self)
        self.n=n
        self.PG.write(u':PULS1:MOD ADJ')
        time.sleep(ts)
        self.PG.write(u':PULS2:MOD ADJ')
        time.sleep(ts)
        self.PG.write(u':PULS3:MOD ADJ')
        time.sleep(ts)
        self.PG.write(u':PULS4:MOD ADJ')
        time.sleep(ts)
        self.PG.write(u':PULS5:MOD ADJ')
        time.sleep(ts)
        self.PG.write(u':PULS6:MOD ADJ')
        time.sleep(ts)
        self.PG.write(u':PULS7:MOD ADJ')
        time.sleep(ts)
        self.PG.write(u':PULS8:MOD ADJ')
        time.sleep(ts)
        
        
    def enable(self):
        self.PG.write(u':PULS'+unicode(self.n)+':STATE ON')
        time.sleep(ts)
        
    def disable(self):
        self.PG.write(u':PULS'+unicode(self.n)+':STATE OFF')
        time.sleep(ts)
   
        ##Polarity: to be filled with NORM (active high) or INV (active low)     
    def polarity(self,polarity):
        self.PG.write(u':PULS'+unicode(self.n)+':POL '+unicode(polarity))
        time.sleep(ts)
        
    def level(self,voltage):
        self.PG.write(u':PULS'+unicode(self.n)+':AMP '+unicode(voltage))
        time.sleep(ts)
        
        ## All times in seconds ##
    def delay(self,delay):
        self.PG.write(u':PULS'+unicode(self.n)+':DELAY '+unicode(delay))
        time.sleep(ts)
        
    def width(self,width):
        self.PG.write(u':PULS'+unicode(self.n)+':WIDT '+unicode(width))
        time.sleep(ts)
        
    
        
        