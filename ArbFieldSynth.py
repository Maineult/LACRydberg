# -*- coding: utf-8 -*-
"""
Created on Mon Aug 08 10:23:27 2016

@author: lac
"""

import visa
from time import sleep
import numpy as np
import serial as s

SleepTime=0.000001
NumPoints=500


class ArbFieldSynth():
    def __init__(self,ComPort=24):
        self.AFS=s.Serial(ComPort)
        sleep(SleepTime)
        
    def WriteEnable(self):             # Start the Synth (synth still wait for external trigger) 
        self.AFS.write("-1\n")
        sleep(SleepTime)
        
    def WriteDisable(self):
        self.AFS.write("-2\n")  
        sleep(SleepTime)
            
    def InitWritingVoltage(self):
        self.AFS.write("-3\n")
        sleep(SleepTime)
        self.AFS.write("1\n")
        sleep(SleepTime)
        self.AFS.write("0\n")
        sleep(SleepTime)
        self.AFS.write(str(NumPoints)+"\n")
        sleep(SleepTime)
        self.AFS.write("1\n")
        sleep(SleepTime)
        self.AFS.write("0\n")
        sleep(SleepTime)
        self.AFS.write(str(NumPoints)+"\n")
        sleep(SleepTime)
        self.AFS.write("0\n")
        sleep(SleepTime)        
        
        
    def WriteArbVoltage(self,Data):    # From Data file  
        pass
    
    def WriteConstVoltage(self,Voltage):
        """
        Write a Constant voltage in Volts
        """
        self.InitWritingVoltage()
        for i in np.arange(NumPoints):
            self.AFS.write(str(Voltage)+"\n")
            sleep(SleepTime)
        self.WriteEnable()    
        
    def WriteSmoothConstVoltage(self,Voltage,NumSmoothPoint):
        self.InitWritingVoltage()
        for i in np.arange(NumSmoothPoint):
            self.AFS.write(str(Voltage*i/NumSmoothPoint)+"\n")
            sleep(SleepTime)
        for i in np.arange(NumPoints-NumSmoothPoint):
            self.AFS.write(str(Voltage)+"\n")
            sleep(SleepTime)
        self.WriteEnable()        
            
            
    def WriteVoltageRamp(self,Vi,Vf):
        self.InitWritingVoltage()
        for i in np.arange(NumPoints):
            self.AFS.write(str(Vi+i*(Vf-Vi)/NumPoints)+"\n")
            sleep(SleepTime)
        self.WriteEnable()     
        
    def WriteStarkSwitching(self,V,Tex,TRamp):
        self.InitWritingVoltage()
        for i in range(int(Tex/100e-9)):
            self.AFS.write(str(V)+"\n")
            sleep(SleepTime)
        for i in range(int(TRamp/100e-9)):
            self.AFS.write(str(V-i*V/int(TRamp/100e-9))+"\n")
            sleep(SleepTime)
        RemainingPoints=NumPoints-Tex/100e-9-TRamp/100e-9                      
        for i in range(int(RemainingPoints)):
            self.AFS.write(str(0)+"\n")
            sleep(SleepTime)    
        self.WriteEnable()             
        
            
    def ReadLoadedVoltage(self):
        r=[]
        for i in np.arange(NumPoints):
            sleep(SleepTime)
            r.insert(i,self.AFS.readline())
        return r
        
    def __test__(self):
        self.WriteConstVoltage(10)
        r=float(self.ReadLoadedVoltage()[1])
        return (int(round(r))==10)

if __name__=='__main__':
    a=ArbFieldSynth(23)