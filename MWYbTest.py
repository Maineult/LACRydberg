# -*- coding: utf-8 -*-
"""
Created on Wed Sep 14 15:25:29 2016

@author: lac
"""

import MicroWaveGenLib as mw
import matplotlib.pyplot as plt
from ScopeFunc import *
import numpy as np
from time import sleep

Freqinit=15
Freqstep=0.001
Freqfin=16
Freq=Freqinit

sc=Scope('Ytterbium',0,False)

MwGen=mw.MicroWaveGen()
Frequence=[]
S1=[]
S2=[]
S=[]
i=0
while Freq< Freqfin:
    MwGen.WriteFrequency(Freq)
    
    Freq=Freq+Freqstep
    
    
    x,y=sc.ReadData(0)
    S1.insert(i,np.sum(y[0:499]))
    S2.insert(i,np.sum(y[500:999]))
    S.insert(i,S1[i]/S2[i])
    Frequence.insert(i,Freq*6)
    sleep(0.5)
    print MwGen.ReadFrequency(),S1[i],S2[i]
    i=i+1