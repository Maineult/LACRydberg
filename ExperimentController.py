# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 18:46:11 2016
Experiment Controlleur of MVC 
@author: wilfried
"""

from socket import gethostname
from ExperimentConstants import *
from ExperimentModel import ExperimentParams,ExperimentResults
from GeneralConstants import *
from PyQt4 import QtCore as C   

class Controller(C.QThread):
    
    def __init__(self):
        super(Controller,self).__init__()
        self.ExpModel = ExperimentParams()
        self._InstrumentInstances = {}
        self.ExpResult = ExperimentResults()
        self._Read = getattr(self,'ReadInstrument')
        self._Write = getattr(self,'WriteToInstrument')  
        self.ExpModel.Simulate=False
        self.start()    
        
    def run(self):
        while True:
            pass
        
    def InitInstruments(self):
        '''        
            Initialized instruments which in self.ExpModel.InstrList are set to WaitToConnect        
        '''
        
        
        from importlib import import_module
        InstrList = ([key for key,value in 
                self.ExpModel._InstrList.iteritems() if value is WaitToConnect])
        if 'Arduino' in InstrList : 
            # If there is another master instrument needed to connect subinstruments
            # don't forget to put them ahead in this list 
            InstrList.remove('Arduino')
            InstrList = ['Arduino']+InstrList
            
        if 'DacBoard' in InstrList :   
            InstrList.remove('DacBoard')
            InstrList = ['DacBoard']+InstrList
            
            
        if 'RemoteLambda' in InstrList :
            if not 'Lambdameter' in InstrList : 
                InstrList = ['Lambdameter']+InstrList
        
        for Instr in InstrList :
            NewLog(None,'Lauching {}'.format(Instr),2)
            try :
                (NameModule,NameClass,Instrkwargs) = InstrumentDict[Instr]
            except Exception as e: 
                NewLog(e)
                '''  
            
                Normally, the eventual arguments that ones needs to pass to the __init__ method of Instr
            is stored in a dictionnary in the ExperimentConstants module inside the InstrumentDict dictionnary
            as Instrkwargs i.e. unnamed parameters that are recognized by this init method and parsed
                If the variable that is passed in the __init__ method for Instr cannot be defined 
            directly in the Experimental constants (for instance if you have to pass class instances)
            you can pass a string name, that will be a function of the controller object, that will return the demanded 
            an Instrkwargs dictionnary containing as dict value the requiered instance

                '''
            if hasattr(self.ExpModel,Instr):
                tmp = getattr(self.ExpModel,Instr)
            else : 
                setattr(self.ExpModel,Instr,{})
                tmp = getattr(self.ExpModel,Instr)
            if tmp.has_key('InitKwargs'):
                Instrkwargs = tmp['InitKwargs']
            else :
                NewLog(None,'Instanciating with None kwargs',1)
                #Instrkwargs = {}

            if (type(Instrkwargs) is not type({})) and len(Instrkwargs) :
                if type(Instrkwargs) is not type('') :
                    NewLog(None,'{} is {} type, neither dict or string'.format(
                            Instrkwargs.__class__,type(Instrkwargs)))
                t = getattr(self,Instrkwargs)
                kw = t(Instr)
                Instrkwargs=kw.copy()
            module = import_module(NameModule)
            backupkw = Instrkwargs.copy()
        #☻    print Instrkwargs
            if Instr not in self._InstrumentInstances : 
                Instrclass = getattr(module,NameClass)
                from inspect import getargspec
                ArgsSpec = getargspec(Instrclass.__init__)
                NewLog(None,'{} kwargs = {}'.format(Instr,ArgsSpec),2)
                if ArgsSpec.varargs is not None : 
                    raise TypeError('Only supports functions with args and **kwargs, no *args\n Consider modyfying {}'.format(InstrCommand))
                try : 
                    nbargs = len(ArgsSpec[0])-1
                except :
                    nbargs= 0
                Instrkwargs['Simulate']=self.ExpModel.Simulate
                try : 
                    nbdefined = len(ArgsSpec[3]) 
                    TestDefined=[Instrkwargs.pop(key) for key in ArgsSpec[0][:-nbdefined] if key in Instrkwargs.keys()]
                except : 
                    nbdefined = 0
                    TestDefined=[Instrkwargs.pop(key) for key in ArgsSpec[0] if key in Instrkwargs.keys()]
     #           print 'InitArgSpecs',ArgsSpec,"\n",'backup', backupkw,'\n Instrkwargs',Instrkwargs
                if len(TestDefined)<nbargs-nbdefined : 
                    raise TypeError('Not Enough arguments : Instrkwargs must contain {}'.format(ArgsSpec[0][:-nbdefined]))
                Options=[Instrkwargs.pop(key) for key in ArgsSpec[0] if key in Instrkwargs.keys()]
                ListArgs = TestDefined+Options
                if Instrkwargs.has_key('Simulate'):
                    Instrkwargs.__delitem__('Simulate')
                if len(Instrkwargs):
                    if ArgsSpec[2] is None:
                        raise TypeError('arguments passed to kwargs : '+
                                '{0} do not match {1} constructor'.format(kwargs.keys[0],InstrCommand))
                    self._InstrumentInstances[Instr] = Instrclass(*ListArgs,**kwargs)
                    
                else : 
                    self._InstrumentInstances[Instr] = Instrclass(*ListArgs)
     #           print 'backupkw',backupkw
                for key,elem in backupkw.copy().iteritems():
                    if not type(elem) in [int,float,str,unicode,list,dict,tuple]:
                        backupkw.__delitem__(key)
                tmp['InitKwargs'] = backupkw     
                InstrumentDict['Instr']=(NameModule,NameClass,backupkw)
                if self._InstrumentInstances[Instr].__test__():
                    NewLog(None, '{} Init  OK'.format(Instr),2)    
                    self.ExpModel._InstrList[Instr] = InitOK
                    if Instr in ['SRSDelayGen','BNCDelayGen']:
                        for i,curr in enumerate(tmp['AllChannels']):
                            if Instr == 'SRSDelayGen':                               
                                self._Write(Instr,'Channel',**{'Channel' : i, 'Command' : curr})
                            elif Instr =='BNCDelayGen':
                                self._Write(Instr,'Channel',**{'Command' : curr})
                        self._Read(Instr,'AllChannels',**{})
                        tim = self.ExpModel.CalcTimes()
                        temp = self.ExpModel.TimeTable
                        self.ExpModel.TimeTable = self.ExpModel.BuildTimingTable(tim)
                        self.UpdateTimings(**temp)    
                    return True
                else : 
                    NewLog(None, '{} Init  Failed'.format(Instr),0) 
                    self.ExpModel._InstrList[Instr] = NotUsed
                    return False  

    def PassInitParams(self,Instr):
        '''
        Pass Init Params after Instr Initialization
        '''
        if InitParams.has_key(Instr):
            for key in InitParams[Instr].iterkeys():
                self.WriteToInstrument(Instr,key,**InitParams[Instr][key])
                print 'Writing initial '+unicode(key)+' on '+unicode(Instr)                
                    
                    
                
    def ToggleInstruments(self,Instr):
        if self.ExpModel._InstrList[Instr] in [NotUsed] : 
            self.ExpModel._InstrList[Instr] = WaitToConnect
            try : 
                test = self.InitInstruments()
                self.PassInitParams(Instr)
                if not test:
                    self.ExpModel._InstrList[Instr] = NotUsed
                    self._InstrumentInstances.pop(Instr)
                    raise InstrError(Instr,'initialized')
            except Exception as e : 
                NewLog(e)
        else : 
            last = self.ExpModel._InstrList[Instr]
            self.ExpModel._InstrList[Instr] = NotUsed
            try : 
                if not Instr in ['ElectricFields']:
                    print Instr
                    self._InstrumentInstances[Instr].close()
            except Exception as e:
                NewLog(None, '{} Did not close properly'.format(Instr),0) 
            finally : 
                self._InstrumentInstances[Instr] = None
                self._InstrumentInstances.__delitem__(Instr)
    
    def RefreshInstruments(self):
        '''
            Calls Exit method of the instrument, and clears the link for closed instr instances
        '''        
        NewLog(None,"RefreshInstruments ",2)
        InstrList = ([key for key,value in 
                self.ExpModel._InstrList.iteritems() if value is InitOK])
        InstrConnected = self._InstrumentInstances.keys()
        if not cmp(InstrList,InstrConnected):
            [InstrConnected.pop(Instr) for Instr in InstrList]
            for Instr in InstrConnected :
                self._InstrumentInstances[Instr].close()
                self._InstrumentInstances.pop(Instr)
            try : 
                if not self.InitInstruments():
                    self.ExpModel._InstrList[Instr] = NotUsed
                    self._InstrumentInstances.pop(Instr)
                    raise InstrError(Instr,'initialized')
            except Exception as e : 
                NewLog(e)
    
    def TestInstr(self,Instr):
        try :
            return self._InstrumentInstances[Instr].__test__()
        except Exception as e : 
            NewLog(e)
            return False
            
    def ReLaunch(self,Instr):
        Instr
        InstrInst = self._InstrumentInstances[Instr] 
        InstrInst.Relaunch()

        
    
    def CheckInstruments(self,InstrList = None):
        '''
            If no argument is given, check that all connection with intruments are still alive
            If an InstrList is given, check if thoses connections are alive AND if not, initializes them
        '''
        NewLog(None,"CheckInstruments, {}".format(InstrList),2)
        if InstrList : 
            try : 
                InstrDict = {}
                InstrDict = ([InstrDict.__setitem__(key,self._InstrumentInstances[key])
                            for key in InstrList])
            except : 
                for Instr in InstrList : 
                    self.ExpModel._InstrList[Instr] = WaitToConnect
                try : 
                    if not self.InitInstruments():
                        self.ExpModel._InstrList[Instr] = NotUsed
                        self._InstrumentInstances.pop(Instr)
                        raise InstrError(Instr,'initialized')
                except Exception as e : 
                    NewLog(e) 
        else :
            InstrDict = self._InstrumentInstances
        for InstrName,InstrInstance in InstrDict.iteritems():
            if InstrInstance.__test__():
                self.ExpModel._InstrList[InstrName] = InitOK
            else :
                try :                
                    raise InstrError(InstrName,'Initialised')
                except Exception as e : 
                    NewLog(e)

                
    def ReadInstrument(self,InstrName,InstrCommand,**kwargs):
        ''' 
        Get an instrument, an InstrumentCommand, and optionnal arguments to be passed to a function
        and excecutes the Instr.Read+'InstrumentCommand'(arg)
        Test also if the function complies with the constructor
        '''   
        test = kwargs.copy()
        if not InstrName in ['Lambdameter','Scope']:
            NewLog(None,'ReadInstrument {},{},{}'.format(InstrName,InstrCommand,kwargs),2)
        if self.ExpModel._InstrList[InstrName] is not InitOK:
            self.CheckInstruments([InstrName])
            if self.ExpModel._InstrList[InstrName] is not InitOK:
                try : 
                    raise InstrError(InstrName,'receive any Initialization order')
                except Exception as e:
                    NewLog(e)
        Instr = self._InstrumentInstances[InstrName]
        from inspect import getargspec        
        
#        if InstrName=='Scope':
#            print InstrCommand, type(InstrCommand)
        if not type(InstrCommand) in [type(u''),type('')]:
            raise TypeError('InstrCommand should be string type')
        InstrFunc = getattr(Instr,'Read'+InstrCommand)
        ArgsSpec = getargspec(InstrFunc)     
        if ArgsSpec.varargs is not None : 
            raise TypeError('Only supports functions with args and **kwargs, no *args\n Consider modyfying {}'.format(InstrCommand))
        try : 
            nbargs = len(ArgsSpec[0])-1
        except :
            nbargs= 0
        try : 
            nbdefined = len(ArgsSpec[3]) 
            TestDefined=[kwargs.pop(key) for key in ArgsSpec[0][:-nbdefined] if key in kwargs.keys()]
        except : 
            nbdefined = 0
            TestDefined=[kwargs.pop(key) for key in ArgsSpec[0] if key in kwargs.keys()]
        if len(TestDefined)<nbargs-nbdefined : 
            #print ArgsSpec
            raise TypeError('Not Enough arguments : kwargs must contain {0}'.format(ArgsSpec[0][:-nbdefined]))
            
        Options=[kwargs.pop(key) for key in ArgsSpec[0] if key in kwargs.keys()]
        ListArgs = TestDefined+Options
        if len(kwargs):
            if ArgsSpec[2] is None:
                raise TypeError('arguments passed to kwargs : {0} do not match {1} constructor'.format(kwargs.keys[0],InstrCommand))
            Read = InstrFunc(*ListArgs,**kwargs)
        else : 
            Read = InstrFunc(*ListArgs)
        try :
            InstrModel = getattr(self.ExpModel,InstrName)
            kwargs = test
            if (not len(kwargs)) and len(ListArgs)==1 :
                InstrModel[InstrCommand+str(ListArgs[0])] = Read
            elif (InstrCommand is 'Param') and ('Channel' in kwargs.keys()):
                i=str(kwargs.pop('Channel'))
                temp = [value for value in kwargs.itervalues()]
                temp.append(i)
                i = ''.join(temp)
                InstrModel[i] = Read
            elif (InstrCommand is 'Custom') :
                i=str(kwargs.pop('Number'))
                temp = 'CUST'+i                
                i = ''.join(temp)
                InstrModel[i] = Read
            else : 
                if InstrName=='Lambdameter' or 'LambdameterIon':
                    try :                     
                        InstrModel[InstrCommand] 
                    except KeyError: 
                        InstrModel[InstrCommand] =0  
                    if Read :
                        InstrModel[InstrCommand] = Read
                else :
                    InstrModel[InstrCommand] = Read
        except AttributeError as e: 
            if 'SRSDelayGen' not in e.args[0]:
                raise

        return Read
    
    def Read2(self,InstrName,InstrCommand,**kwargs):
        ''' 
        Get an instrument, an InstrumentCommand, and optionnal arguments to be passed to a function
        and excecutes the Instr.Read+'InstrumentCommand'(arg)
        Test also if the function complies with the constructor
        '''   
        test = kwargs.copy()
        if not InstrName in ['Lambdameter','Scope']:
            NewLog(None,'ReadInstrument {},{},{}'.format(InstrName,InstrCommand,kwargs),2)
        if self.ExpModel._InstrList[InstrName] is not InitOK:
            self.CheckInstruments([InstrName])
            if self.ExpModel._InstrList[InstrName] is not InitOK:
                try : 
                    raise InstrError(InstrName,'receive any Initialization order')
                except Exception as e:
                    NewLog(e)
        Instr = self._InstrumentInstances[InstrName]
        from inspect import getargspec        
        if not type(InstrCommand) in [type('')]:
            raise TypeError('InstrCommand should be string type')
        InstrFunc = getattr(Instr,'Read'+InstrCommand)
        ArgsSpec = getargspec(InstrFunc)     
        if ArgsSpec.varargs is not None : 
            raise TypeError('Only supports functions with args and **kwargs, no *args\n Consider modyfying {}'.format(InstrCommand))
        try : 
            nbargs = len(ArgsSpec[0])-1
        except :
            nbargs= 0
        try : 
            nbdefined = len(ArgsSpec[3]) 
            TestDefined=[kwargs.pop(key) for key in ArgsSpec[0][:-nbdefined] if key in kwargs.keys()]
        except : 
            nbdefined = 0
            TestDefined=[kwargs.pop(key) for key in ArgsSpec[0] if key in kwargs.keys()]
        if len(TestDefined)<nbargs-nbdefined : 
            #print ArgsSpec
            raise TypeError('Not Enough arguments : kwargs must contain {0}'.format(ArgsSpec[0][:-nbdefined]))
            
        Options=[kwargs.pop(key) for key in ArgsSpec[0] if key in kwargs.keys()]
        ListArgs = TestDefined+Options
        if len(kwargs):
            if ArgsSpec[2] is None:
                raise TypeError('arguments passed to kwargs : {0} do not match {1} constructor'.format(kwargs.keys[0],InstrCommand))
            Read = InstrFunc(*ListArgs,**kwargs)
        else : 
            Read = InstrFunc(*ListArgs)
        try :
            InstrModel = getattr(self.ExpModel,InstrName)
            kwargs = test
            if (not len(kwargs)) and len(ListArgs)==1 :
                InstrModel[InstrCommand+str(ListArgs[0])] = Read
            elif (InstrCommand is 'Param') and ('Channel' in kwargs.keys()):
                i=str(kwargs.pop('Channel'))
                temp = [value for value in kwargs.itervalues()]
                temp.append(i)
                i = ''.join(temp)
                InstrModel[i] = Read
            elif (InstrCommand is 'Custom') :
                i=str(kwargs.pop('Number'))
                temp = 'CUST'+i                
                i = ''.join(temp)
                InstrModel[i] = Read
            else : 
                if InstrName=='Lambdameter' or 'LambdameterIon':
                    try :                     
                        InstrModel[InstrCommand] 
                    except KeyError: 
                        InstrModel[InstrCommand] =0  
                    if Read :
                        InstrModel[InstrCommand] = Read
                else :
                    InstrModel[InstrCommand] = Read
        except AttributeError as e: 
            if 'SRSDelayGen' not in e.args[0]:
                raise       
        return Read
        
        
    def UpdateTimings(self,*ListArgs,**tim0):
#        print 'list',ListArgs
#        print 'kw',tim0
        if len(tim0):
            try : 
                chnames = []               
                for Instr in self.ExpModel.TimingInstr:
                    
                    ins = getattr(self.ExpModel,Instr)
                    [chnames.append(Instr+Chan) for Chan in ins['Channels']]
                test=[tim0.has_key(elem) for elem in chnames+['Times','TimeTags']]
                if not all(test):
                    raise KeyError
                tim=tim0.copy()
            except KeyError: 
                if len(ListArgs):
                    pass
                else : 
                    return False
            except Exception as e:
                NewLog(e)
                return False
        if len(ListArgs):
            try : 
                Instr,Channel,liststates = ListArgs
                if not Instr in self.ExpModel.TimingInstr:
                    raise ValueError('invalid Instrument')
                ins = getattr(self.ExpModel,Instr)['Channels']
                if not Channel in ins:
                    raise ValueError('invalid Channel')
                tim = self.ExpModel.TimeTable.copy()
                tim[Instr+Channel] = liststates
            except ValueError : 
                listtimes = ListArgs[0]
                #print listtimes
                if not len(listtimes)==len(self.ExpModel.TimeTable['Times']):
                    raise ValueError('Invalid array length')
                tim = self.ExpModel.TimeTable.copy()
                tim['Times'] = listtimes     
            except Exception as e:
                NewLog(e,'Invalid Args to function UpdateTiming'.format(ListArgs),0)
                return False
#        print 'NewTimeTable',tim
#        print 'OldTimeTalbe',self.ExpModel.TimeTable
        InstrDicts = [getattr(self.ExpModel,Instr) for Instr in self.ExpModel.TimingInstr ]
        previous = [InstrDict['AllChannels'][:] for InstrDict in  InstrDicts]
        self.ExpModel.BuildTimingCommands(tim)
        self.ExpModel.Times = self.ExpModel.CalcTimes()
        current = [InstrDict['AllChannels'][:] for InstrDict in  InstrDicts]
        try : 
            InstrInstces = [self._InstrumentInstances[Instr] for Instr in self.ExpModel.TimingInstr]
        except : 
            self.ExpModel.TimeTable = tim
            self.ExpModel.Times = self.ExpModel.CalcTimes()
            return True   
        from datetime import datetime
        print 'l369', datetime.now()
        for prevInstr,currInstr,Instr in zip(previous,current,self.ExpModel.TimingInstr):
            for i,(prev,curr) in enumerate(zip(prevInstr,currInstr)):
                print i, prev, curr
                if not prev==curr :
                    try : 
                        if Instr == 'SRSDelayGen':
                            print 'l376', datetime.now()
                            ret = self._Write(Instr,'Channel',**{'Channel' : i, 'Command' : curr})
                            print 'l378', datetime.now()
                        elif Instr == 'BNCDelayGen':
                            print curr
                            ret = self._Write(Instr,'Channel',**{'Command' : curr})
                        self.msleep(1)
                        if not ret == 'Done' :
                            raise ValueError('Controller in pass timings params to Instr')
                        print 'l384', datetime.now()
                    except Exception as e: 
                        self.ReLaunch(Instr)
                        self.ExpModel.BuildTimingCommands(self.ExpModel.TimeTable)
                        NewLog(e)
                        return False                        
        self.ExpModel.TimeTable = tim
        self.ExpModel.Times = self.ExpModel.CalcTimes()
        return True
        

    
    def WriteToInstrument(self,InstrName,InstrCommand,**kwargs):
        ''' 
        Get an instrument, an InstrumentCommand, and optionnal arguments to be passed to a function
        and excecutes the Instr.Write+'InstrumentCommand'(arg)
        Test also if the function complies with the constructor
        '''  
        #if not (InstrName in ['Matisse','Scope']):
        if not (InstrName in ['Matisse']):
            NewLog(None,'WriteToInstrument = {}{}{}'.format(InstrName,InstrCommand,kwargs),2)
        keep = kwargs.copy()
        #print InstrName, self.ExpModel._InstrList[InstrName]
        if not self.ExpModel._InstrList[InstrName] is InitOK:
            self.CheckInstruments([InstrName])
            if self.ExpModel._InstrList[InstrName] is not InitOK:
                raise InstrError(InstrName,'receive any Initialization order')
        Instr = self._InstrumentInstances[InstrName]
        from inspect import getargspec        
        if type(InstrCommand) is not type(''):
            raise TypeError('InstrCommand should be string type')
        InstrFunc = getattr(Instr,'Write'+InstrCommand)
        ArgsSpec = getargspec(InstrFunc)    
      #  print ArgsSpec,kwargs
        if kwargs.has_key('Unknown') and len(ArgsSpec[0])==2 and list(ArgsSpec[1:])==[None]*3 : 
            val=kwargs.pop('Unknown')
            key = [e for e in ArgsSpec[0] if not e =='self'][0]
            kwargs[key]=val
            keep = {key:val}
        #☻print 'Modified kw', kwargs
        if ArgsSpec.varargs is not None : 
            raise TypeError('Only supports functions with args and **kwargs, no *args\n Consider modyfying {}'.format(InstrCommand))
        try : 
            nbargs = len(ArgsSpec[0])-1
        except :
            nbargs= 0
        try : 
            nbdefined = len(ArgsSpec[3]) 
            TestDefined=[kwargs.pop(key) for key in ArgsSpec[0][:-nbdefined] if key in kwargs.keys()]
        except : 
            nbdefined = 0
            TestDefined=[kwargs.pop(key) for key in ArgsSpec[0] if key in kwargs.keys()]
       # print nbargs, TestDefined, kwargs
        if len(TestDefined)<nbargs-nbdefined : 
            raise TypeError('Not Enough arguments : kwargs must contain {0}'.format(ArgsSpec[0][:-nbdefined]))
        Options=[kwargs.pop(key) for key in ArgsSpec[0] if key in kwargs.keys()]
        ListArgs = TestDefined+Options
        if len(kwargs):
            if ArgsSpec[2] is None:
                raise TypeError('arguments passed to kwargs : {0} do not match {1} constructor'.format(kwargs.keys[0],InstrCommand))
            res = InstrFunc(*ListArgs,**kwargs)
        else : 
            res = InstrFunc(*ListArgs)

        try :

            InstrModel = getattr(self.ExpModel,InstrName)
            if res == 'OutOfBound' :
                return res
            if res == 'Unlocked':
                return res
            if res == 'Disconnected' :
                self._InstrumentInstances['InstrName'] = None
                self.ExpModel._InstrList['InstrName'] = WaitToConnect
            if InstrName in self.ExpModel.TimingInstr and InstrCommand == 'Channel' : 
                return 'Done'
            if len(kwargs):
                if InstrName in ['Matisse']:
                    return
                InstrModel[InstrCommand] = keep
            else :
                if len(ListArgs)==1 : 
                    InstrModel[InstrCommand] = ListArgs[0]
                else :
                    InstrModel[InstrCommand] = keep
        except AttributeError as e: 
            NewLog(e)
        return 'Done' #for remote control message acknolegement
    
          
            
    def SaveExpModel(self,filename=None,location=None):        
        '''
            Experiment Parameters are stored using pickle, a python serializer object (writes objects to files and vice versa)
            You can pass location and file to the parameters, but you have de default location defined for each computer
            in the ExperimentConstants part. Files are name usually 000x.ExpParam.
            ExpParam is mandatory for the program to work, but one can add extra info to the file name for easiness
            From default location on the computer a subdirectory tree with Year/date is created. Normally you can also add some more
            details in the folder name and it will still work.
        '''
        from os import path
        if not location : 
            location = self.GetTodayDir()
        if not filename:
            self.MakefileName(location)
        if not len(path.splitdrive(filename)[0]):
            filename = path.join(location,filename+'.ExpParam')
        from cPickle import dump,dumps
        f = open(filename,'w')
        dump(self.ExpModel,f)
        f.close()  
        
    def SaveReference(self,refname,data,location=None):
        if not location : 
            location = self.GetTodayDir()
        oldrefs = [refname for item in next(walk(location))[2] if not item.find(refname)+1]
        fil = refname+str(len(oldref)/2)+'.RefData'
        filename=path.joint(location,fil)
        from cPickle import dump,dumps
        f = open(filename,'w')
        dump(data,f)
        f.close()
        fil = refname+str(len(oldref)/2)+'.RefParam'
        filename=path.joint(location,fil)
        f = open(filename,'w')
        dump(self.ExpModel,f)
        f.close()
        
    def SaveExpResult(self,ExpResults,temp=None):        
        '''
            Experiment Parameters are stored using pickle, a python serializer object (writes objects to files and vice versa)
            You can pass location and file to the parameters, but you have de default location defined for each computer
            in the ExperimentConstants part. Files are name usually 000x.ExpParam.
            ExpParam is mandatory for the program to work, but one can add extra info to the file name for easiness
            From default location on the computer a subdirectory tree with Year/date is created. Normally you can also add some more
            details in the folder name and it will still work.
        '''
        if not temp : 
            filename = ExpResults.fileResults
        try : 
            f = open(filename,'w')
        except :    
            location = self.GetTodayDir()
        #if not filename :
            if temp :
                filename =temp
            else : 
                filename = self.MakefileName(location)
            from os import path
            filename = path.join(location,filename+'.ExpResults')
            f = open(filename,'w')
            print filename
        from cPickle import dump,dumps
        dump(ExpResults,f)
        f.close()      


    def GetTodayDir(self):
        from os import path,walk,makedirs        
        from datetime import datetime
        dateinfo = datetime.today()
        basedir = path.join(DataDir[gethostname()],unicode(dateinfo.year))
        if not path.exists(basedir):
            makedirs(basedir)
        partdirname = dateinfo.strftime('%m - %d (%A)')
        test = [item for item in next(walk(basedir))[1] if partdirname in item]
        if len(test)>1:
            NewLog(None,'Warning : Multiple directory in the today date \n'+str(test),1)
        elif not len(test):
            test = [partdirname]
        location = path.join(basedir,test[0])
        if not path.isdir(location):
            makedirs(location)
        return location

    def MakefileName(self,loc):
        from os import path,walk,makedirs        
        files = ([item[:item.find('.ExpResults')] 
             for item in next(walk(loc))[2] if 'ExpResults' in item])
        if len(files):
            t = files[end]
            last = t.split('.')[0]
            ll = [(last[i:],last[:i]) for i in range(len(last))]
            for l in ll : 
                try :          
                    return (u'{}{:0'+unicode(len(l[0]))+'d}').format(l[1],int(l[0])+1)
                except : 
                    pass
        else : 
            return '0000'
        
    def LoadExpResult(self,filename,location = None):

        filename = path.join(location,filename)
        from cPickle import load
        f = open(filename,'r')
        self.ExpResult = load(f)
        f.close()
        
    def LoadExpModel(self,filename,location = None):
        from os import path
        if not location : 
            location = self.GetTodayDir()
        if not len(path.splitdrive(filename)[0]):
            filename = path.join(location,filename)
            
        oldval = self.ExpModel._InstrList.copy()
        from cPickle import load
        f = open(filename,'r')
        self.ExpModel = load(f)
        f.close()
        for key,val in self.ExpModel._InstrList.iteritems():
            if not oldval[key]==val:
                self.ExpModel[key] = val/2
        self.InitInstruments()
        from inspect import ismethod,getmembers
        for Instr,obj in self._InstrumentInstances.iteritems():
            listmethods = getmembers(obj,predicate=ismethod)
            ListWrite = [(m[0][5:]) for m in listmethods if 'Write' in m[0]]
            for key,val in self.InstrModel[Instr].iteritems(): 
                if key in ListWrite : 
                    self.WriteToInstrument(Instr,key,val)
                
        
    def getArduinoInstance(self,Instr):
        """        
            Check if arduino is already intanciated (=ArduinoCustum object is already defined in self._InstrumentIntances)
            if not, intanciates it and return the instance
        """
        if not Instr in ['Ion','BertanAlim','ForsterAlim']:
            print 'May not work !!! '
            NewLog(None,'Define the dictionnary you need to return for your instrument, default would be just the arduino instance')
        NewLog(None, 'getArduinoInstance{}'.format(Instr),2)
        if self._InstrumentInstances.has_key('Arduino'):
            dico =  {'board':self._InstrumentInstances['Arduino']}
        else :
            self.CheckInstruments( ['Arduino'])
            dico =   {'board':self._InstrumentInstances['Arduino']}
        if Instr is 'ForsterAlim':
            dico['pin_number'] = 8           
        return dico
        
        
    def getDacInstance(self,Instr):
        """        
            Check if DacBoard is already intanciated
            if not, intanciates it and return the instance
        """
        if not Instr in ['ElectricFields']:
            print 'May not work !!! '
            NewLog(None,'Define the dictionnary you need to return for your instrument, default would be just the DacBoard instance')
        NewLog(None, 'getDacInstance{}'.format(Instr),2)
        if self._InstrumentInstances.has_key('DacBoard'):
            dico =  {'Card':self._InstrumentInstances['DacBoard']}
        else :
            self.CheckInstruments( ['Arduino'])
            dico =   {'board':self._InstrumentInstances['Arduino']}
        return dico    
            
    def GetSelf(self,Instr):
        if Instr is 'RemoteLambda':
            return {'controller':self}
            
    def FindWmChannel(self,Instr):
        self.ExpModel.Lambdameter['Channel'] = LambdameterSwitch.index(ComputerDict[gh()])+1
        return {'Channel':LambdameterSwitch.index(ComputerDict[gh()])+1}
        
    
    def BasicStart(self):
        self.ExpModel._InstrList['Matisse'] = 1
        self.ExpModel._InstrList['SRSDelayGen'] = 1
        self.ExpModel._InstrList['Arduino'] = 1
        self.ExpModel._InstrList['Ion']=1
        self.ExpModel._InstrList['ForsterAlim']=1
        self.ExpModel._InstrList['Scope']=1
        self.ExpModel._InstrList['Lambdameter'] = 1
        self.InitInstruments()
#        self.UpdateTimings('Rydberg',('ExperimentStart',10*us,1002*us))
        self.WriteToInstrument('Ion','Phosphorus',**{'Volt':2000.0}) 
        self.WriteToInstrument('Ion','Field',**{'Volt':900.0})   
        self.WriteToInstrument('Ion','MCP',**{'Volt':1450.0})
#        self.WriteToInstrument('ForsterAlim','Volt',**{'Volt':00.0})
#       a._InstrumentInstances['Lambdameter'].WriteShowApp(True)
        
    def TestLock(self):
        self.ExpModel._InstrList['Matisse'] = 1
        self.ExpModel._InstrList['Scope']=1
        self.ExpModel._InstrList['Lambdameter'] = 1
        self.InitInstruments()
        
    def LockSolstis(self):
        self.ExpModel._InstrList['Solstis'] = 1
       # self.ExpModel._InstrList['Scope']=1
        self.ExpModel._InstrList['Lambdameter'] = 1
        #self.ExpModel._InstrList['PulseGen'] = 1
        self.InitInstruments()
        
    def LockMoglabs(self):
        self.ExpModel._InstrList['arduino'] = 1
        self.ExpModel._InstrList['Lambdameter218']=1
        self.InitInstruments()
    
    def BasicStop(self):    
        self.WriteToInstrument('Ion','MCP',**{'Volt':0.0})
        self.WriteToInstrument('Ion','Phosphorus',**{'Volt':0.0}) 
        self.WriteToInstrument('Ion','Field',**{'Volt':0.0}) 
        
    def BasicHV(self):
        self.WriteToInstrument('Ion','MCP',**{'Volt':1600.0})
        self.WriteToInstrument('Ion','Phosphorus',**{'Volt':1600.0}) 
        self.WriteToInstrument('Ion','Field',**{'Volt':2000.0}) 
        
    def BasicHV2(self):
        self.WriteToInstrument('Ion','MCP',**{'Volt':1450.0})
        self.WriteToInstrument('Ion','Phosphorus',**{'Volt':2000.0}) 
        self.WriteToInstrument('Ion','Field',**{'Volt':900.0}) 
        
    def StartRemoteLambdaServer(self):
        self.ExpModel._InstrList['RemoteLambda'] = 1
        self.InitInstruments()
#        self.UpdateTimings('Rydberg',('ExperimentStart',10*us,1002*us))
#        self.WriteToInstrument('Ion','Phosphorus',**{'Volt':1700.0}) 
#        self.WriteToInstrument('Ion','Field',**{'Volt':2000.0})   
#        self.WriteToInstrument('Ion','MCP',**{'Volt':1800.0})
#        self.WriteToInstrument('ForsterAlim','Volt',**{'Volt':00.0})
#       a._InstrumentInstances['Lambdameter'].WriteShowApp(True)

    def StopRemoteLambdaServer(self):
        self.ExpModel._InstrList['RemoteLambda'] = 0
        self.RefreshInstruments()

    



class InstrError(Exception):
    """
    Raises for communication errors with intruments in the experimenr
    Raises also for stupid values passed to instruments
    """
    def __init__(self,Instr,cause):
        self.Instr = str(Instr)
        self.cause = str(cause)
        self.message = self.Instr+ ' did not properly '+ self.cause
        
    def __str__(self):
        return repr(self.message)
        

if __name__=="__main__":

    a = Controller()
#    if gethostname() == 'lac57-95':
#        a.ExpModel._InstrList['Lambdameter'] = 1
#        a.InitInstruments()
#    if ComputerDict[gethostname()] == 'Cesium' :
#        a.ExpModel._InstrList['Scope']=1
#        a.InitInstruments()
#    if a._InstrumentInstances.has_key('Lambdameter'):
#        if a._InstrumentInstances['Lambdameter'].UseLocal:
#            a._InstrumentInstances['Lambdameter'].WriteShowApp(True)
#            a.StartRemoteLambdaServer()
#    a.ExpModel._InstrList['SRSDelayGen'] = 1
#    a.InitInstruments()

    
