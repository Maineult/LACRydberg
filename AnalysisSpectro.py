# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 16:55:09 2017

@author: lac
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 22:02:27 2016

@author: henri
"""

import matplotlib.pyplot as plt
import numpy as np
import cPickle
import ExperimentModel
import os
os.chdir("C:\Users\lac\Documents\Python Scripts\ExperimentControl\YbSpectroData")
import DataAnalysisYb as an
from UserDefFunctions import *



##### INPUTs ######


        
DataRep ="C:\\Users\\lac\\Documents\\RydbergYtterbiumData\\2017\\07 - 07 (Friday)"       
filename='40-Multiplicity_90V.ExpResults'

Analysis=0
FitForm='Lorentzian'
X1=89.625
X2=89.6273


AnalysedDataRep="C:\Users\lac\Desktop"






##### FUNKtions

def Sortbyx(Data):
    Sortx=sorted(Data[0])
    for j in range(len(Data)) :
        if j>0:
            CorrespSig=[]
            for i,x in enumerate(Sortx):
                CorrespSig.insert(i,Data[j][Data[0].index(x)])
            Data[j]=CorrespSig
    Data[0]=Sortx    
    return Data     


    
def LandeFactor(J,L,S):
    return float(1+ (J*(J+1)-L*(L+1)+S*(S+1))/(2*J*(J+1)))
    
        

###### MAIN #######

os.chdir(DataRep)
name=filename.split('.')[0]
InitialState=name.split('_')[1]
ResultsFile=open(filename,'r')
Results=cPickle.load(ResultsFile)
Frequency=[]
Signal=[]
StdDevSignal=[]
RydSignal=[]
for i,keyParam in enumerate(Results.Resultats.iterkeys()):  
    Data=Results.Resultats[keyParam]
    AverageNumber=len(Data)
    Sryd=[]
    Freq=[]
    for j in range(AverageNumber):
        #S.insert(j,(Data[j]['Custom2']/Data[j]['Custom1']))
        Sryd.insert(j,Data[j]['Custom1'])
        Freq.insert(j,Data[j]['Frequency'])
    if np.std(Freq)<1e-6: 
        RydSignal.insert(i,1e9*np.mean(Sryd))    
        Signal.insert(i,np.mean(Sryd))
        StdDevSignal.insert(i,np.std(Sryd))    
        Frequency.insert(i,np.mean(Freq))
    

Data=[Frequency,Signal,StdDevSignal]
Data=an.SortByX(Data)
#plt.errorbar(Data[0],Data[1],yerr=Data[2],linestyle='None',marker='o')
plt.plot(Data[0],Data[1],'-o')
Legend=[]



if Analysis:
    if FitForm=='Lorentzian':
        fit=an.FitLorentzian(Data[1],Data[0], amplitude=-5e-8,xo=Data[0][np.argmin(Data[1])], fwhm=7e-6, offset=-1e-8,plot=False)
        plt.plot(Data[0],Data[1],'-o',color='slateblue',alpha=0.8,label='Experimental')
        plt.plot()
        XX=np.linspace(np.min(Data[0]),np.max(Data[0]),1000)
        R=fit.best_values
        plt.plot(Data[0],fit.best_fit)  
        plt.legend()
        setattr(Results,'AnalysedResults',R)

locs,labels = plt.xticks()
plt.xticks(locs, map(lambda x: "%10g" % x, locs))
plt.xlabel('Frequency 2ndPhoton')
plt.ylabel('Population Transfer')
ax = plt.gca()
ax.format_coord = lambda x,y: '%10f, %10f' % (x,y)

plt.grid()

def SaveData():
    os.chdir(AnalysedDataRep)
    np.savetxt(name+'.ExpData',Data)
#    File.close()