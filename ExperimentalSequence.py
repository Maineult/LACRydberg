"""
Created on Fri Mar 25 15:44:08 2016

@author: Wilfried
"""

from ExperimentController import *
from ExperimentView import *
from ExperimentModel import Traces,ExperimentResults

matplotlib.use('Qt4Agg')
import numpy as np

from PyQt4 import QtCore as C


#%% 
            
class CameraRealTime(C.QThread):
      CameraTriggered=C.pyqtSignal()
      def __init__(self,MS):
          try:
              super(CameraRealTime,self).__init__(QParent)
          except:
              super(CameraRealTime,self).__init__()
          self.MS=MS  
          if self.MS.ExpControl._InstrumentInstances.has_key('CameraThor'):
              print 'CameraThor'
              self.Camera='CameraThor'
          else:
              self.Camera='Camera'
              
          self._Write = getattr(MS.ExpControl,'WriteToInstrument')
          self._Read = getattr(MS.ExpControl,'ReadInstrument')
          self.running=1
          self.start()
        #  print 'begin CameraRT in sequence'
          
      def run(self):
          while self.running:
                  img=self._Read(self.Camera,'SingleImageCapture',**{})
                  self.CameraTriggered.emit()
        
              
class LambdameterRealTime(C.QThread):
    
    '''
        Reads continuouly the frequency of the WMter
        This can probably be done using built in callback function of the wlmdata.dll library
        but you need to read out the manual to do the correct ctypes programming.
        Think it will work very well like this.
    '''
    NewLambda = C.pyqtSignal()    
    
    def __init__(self,MS,**kwargs):
        try : 
            super(LambdameterRealTime,self).__init__(QParent)       
        except : 
            super(LambdameterRealTime,self).__init__()     
        self._Write = getattr(MS.ExpControl,'WriteToInstrument')
        self._Read = getattr(MS.ExpControl,'ReadInstrument')
        self.MS = MS
        self.Lambda = MS.ExpControl.ExpModel.Lambdameter
        
        self.running = 1 
        if 'Channel' in kwargs.keys():
            self.Channel = kwargs['Channel']
        else :
            self.Channel = 0
        self.Lambda['Channel'] = self.Channel
        self.waittime = self._Read('Lambdameter','CycleTime')
        self.time=0
        self.name = 'Lambdameter'
        self.WriteShowApp(False)
        self.start()
        
        
    def WriteShowApp(self,yn):
        if yn:
            self._Write('Lambdameter','ShowApp',**{'op':True})
        else : 
            self._Write('Lambdameter','ShowApp',**{'op':False})
            
    
    def run(self):
        from time import sleep,clock
        self.tick = 0
        while self.running:          
            try : 
                self._Read('Lambdameter','Frequency',**{'Channel':self.Channel})  
                if self.Lambda['Frequency']>0 : 
                    self.NewLambda.emit()      
                    sleep(self.waittime)
            except Exception as e : 
                print 'Exception'
                self.MS.ExpControl.ReLaunch('Lambdameter')
        
        print 'LambdameterRealTime Thread Finished'
      
        
 
#%% 
 
class ScopeRealTime(C.QThread):
    ScopeTriggered = C.pyqtSignal()
    def __init__(self,MS):
        '''
             Iterator for the scope #
             If Results Saved in ExpModel
        '''    
        super(ScopeRealTime, self).__init__()
        self._Write = getattr(MS.ExpControl,'WriteToInstrument')
        self._Read = getattr(MS.ExpControl,'ReadInstrument')
        self.ScopeModel = MS.ExpModel.Scope
        self.MS = MS
        self.name = 'Scope'
        self.running = 1
        self.MS.mode = 'Align'     
        self.ScopeErrors=0
        self.start()
            
    def run(self):           
        from time import sleep,clock
        self._Write('Scope','Trigger',**{'mode':'norm'})
        while self.running : 
           # try : 
            if True:
                if int(self._Read('Scope','NewTrig',**{}))& 1:
                    self.msleep(1)
                    
                    if self.MS.mode == 'Trace':
                        last, lastTrace = self._Read('Scope','Data',**{'Channel':0})
                        self._Read('Scope','Custom',**{'Number':1})
                        self._Read('Scope','Custom',**{'Number':2})
                        self._Read('Scope','Custom',**{'Number':4})
                        try : 
                            lastTrace = Traces.CheckTrace(self.MS.ExperimentRT,lastTrace)
                        except : 
                            pass
#                        if max(abs(lastTrace)):     
#                            if self.MS.ExpModel.manipe == 'Ytterbium':   
#                                self.ScopeModel['CUST1']=(last[1][0]-last[0][0])*np.sum(lastTrace)
#                            else : 
#                                self.ScopeModel['CUST2']=(last[1][0]-last[0][0])*np.sum(lastTrace)
#                            self.ScopeModel['PKPK0']=np.max(lastTrace)-np.min(lastTrace)
                    if self.MS.mode == 'Align':
                        if self.MS.ExpModel.manipe == 'Ytterbium':                        
                            self._Read('Scope','Custom',**{'Number':1})
                        else : 
                            self._Read('Scope','Custom',**{'Number':2})
                        self._Read('Scope','Param',**{'Channel':0,'Param':'PKPK'})    
                    
                    self.ScopeTriggered.emit()
                    self.ScopeErrors = 0
                self.msleep(20)

                
#            except Exception as e : 
#                NewLog(e,'Scope Crashed',1)
#                try : 
#                    bo = self._Read('Scope','Test',**{})
#                    self.ScopeErrors += 1
#                except :
#                    self.MS.ExpControl.ReLaunch('Scope')
#                self.sleep(15)
#                print 'ScopeError',self.ScopeErrors
                
                    
        print 'ScopeRealTime Thread Finished'

#%%        
#class Traces():
#    def TestResults(self):
#        from time import sleep
#        self.densitytolerance = 0.1
#        if not self.Calib : 
#            re = self.ScopeModel['VoltScale0']
#            self.Calib = self._Calib[str(re)+'V']
#        if self.currentDensity : 
#            Signal = self.AnalyseData()
#            self.IntegratedSignal.append(Signal)
#            self.IntegratedSignal.remove(0)
#            test = 0
#            ([test.__add__(1) for e in self.IntegratedSignal if 
#                (e-self.currentDensity)/self.currentDensity < self.Densitytol])
#            if test<6 : 
#                estim = self.EstimateDensity()
#                c = self._tp['Rydberg'][2]   
#                self.Exp.action.append(('Timings','Rydberg','ExperimentStart',10*us,c*estim/self.currentDensity))
#            if (Signal-self.currentDensity)/self.currentDensity < self.Densitytol:
#                return True
#            else:
#                return False
#        if self.avgcurrent<10 : 
#            if not self.Tbaseline : 
#                self.Tbaseline = self.BaseLineDetection()
#            Signal = self.AnalyseData()
#            self.IntegratedSignal.append(Signal)
#            self.IntegratedSignal.remove(0)
#            return True
#        if self.avgcurrent==10 : 
#            self.ExpResults.addResults({'Params':curparams[:],'Model':self.Exp.ExpModel,
#                                                    'Trace':self.ScopeModel['Data0'][:],
#                                                    'Frequency' : self.Lambdameter['Frequency']})
#            estim = self.EstimateDensity()
#            test = 0
#            ([test.__add__(1) for e in self.IntegratedSignal if 
#                (e-estim) < self.Densitytol*estim])
#            if test == 10 : 
#                return False
#            else : 
#                self.avgcurrent = test
#                lasts = self.ExpResults.FindLatest(curparams)
#                self.IntegratedSignal = []
#                t = []
#                for data in lasts : 
#                    test = self.AnalyseData(data[1])
#                    if (test-estim) < self.Densitytol*estim : 
#                        t.append(data)
#                        self.IntegratedSignal.append(test)
#                lasts = t 
#                return False
#                
#                        
#    
#    def EstimateDensity(self):
#        a = self.IntegratedSignal[:]
#        a.sort()
#        res = max([(len([e-f for j,e in enumerate(a) 
#            if (abs(e-f)<2*self.Densitytol*self.currentDensity and i>j)]),i)
#                for i,f in enumerate(a) ])  #finds the index of the sorted list 
#                # where the lasgest number of previous shots hitted the specified tolerance
#                #and returns also the number of such items
#        
#        if max(res)[0] :
#            a = a[max(res)[1]-max(res)[0]:max(res)[1]+1]
#            inte = 0
#            [inte.__add__(e) for e in a]
#            return inte/len(a)
#        else :
#            print ('tolerance is probably to small, or signal fluctuates too much compared',
#                   ' to noise, returning the median of the list')
#            return a[5]
#
#
#    def BaseLineDetection(self,data):
#        x,y = data
#        dt = (x[-1]-x[0])/50.
#        t = x[0]+dt
#        yprim = np.extract(x<t ,y)
#        while not (self.IsDetectableSignal(yprim)):
#            t = t+dt
#            yprim = np.extract(x<t ,y)
#        self.Tbaseline =  t-dt
#            
#    def IsDetectableSignal(self,y0):
#        '''
#            Set with small signal : shoud detect an excess of points out of gaussian distribution
#            Set with large signal : Check if it works also (it should)
#            nsigma parameters describes the senstibity of the algorithm
#        '''
#        
#        nsigmas = 5
#        yres = [y0.mean(),y0.std(),y0.ptp()]
#        n = len(y0)
#        p1 =  len(np.extract((y0-yres[0])>yres[1] ,y))*1./n
#        pm1 = len(np.extract((y0-yres[0])<-yres[1] ,y))*1./n
#        p2 = len(np.extract((y0-yres[0])>2*yres[1] ,y))*1./n
#        pm2 = len(np.extract((y0-yres[0])<-2*yres[1] ,y))*1./n
#        p3 = len(np.extract((y0-yres[0])>3*yres[1] ,y))*1./n
#        pm3 = len(np.extract((y0-yres[0])<-3*yres[1] ,y))*1./n
#        p4 = len(np.extract((y0-yres[0])>4*yres[1] ,y))*1./n
#        pm4 = len(np.extract((y0-yres[0])<-4*yres[1] ,y))*1./n                
#        currentValue = [p1,pm1,p2,pm2,p3,pm3,p4,pm4]
#        titles = ['y>sigma','y<-sigma','y>2*sigma','y<-2sigma','y>3*sigma','y<-3*sigma','y>4*sigma','y<-4*sigma']    
#        maxValues = [self.Calib[elem][0]+max(5*self.Calib[elem][1],self.Calib[elem][2]) for elem in titles]
#        minValues = [self.Calib[elem][0]-max(5*self.Calib[elem][1],self.Calib[elem][2]) for elem in titles] 
#        Test = 0 
#        [Test.__add__(m<c).__add__(c>M) for m,M,c in zip(maxValues,minValues,currentValue)]
#        return Test
#            
#                
#        
#    def CheckTrace(self,a) :        
#        #Removes Nan from the scope         
#        b=np.isnan(a)
#        if np.sum(b):
#            print 'NaN detected in the data, will be replaced by 0.6 V.\n'
#            a = np.invert(b)*np.nan_to_num(a)+ b*0.599999
#        return a
#    
#
#    def AnalyseData(self,**kwargs):
#        """
#        Analyse MCP signals coming from data (just the y of the scope)
#        From 0 to Baseline (in numpoints units) in calculates average and 
#        the actual noise level
#        Then substracts offsets, removes Nan in the traces, and integrates
#        over the points where the signal is non zero        
#        """
#        try : 
#            x,y = kwargs['data']    
#        except : 
#            x,y = self.ScopeModel['Data0']
#        SampleRate = np.diff(x).mean()
#
#        Base = np.extract(self.Tbaseline>x,y)
#        MeanPoints = Base[:].mean()
#        LocalInfiniteSigma = Base.min(axis = 0)-MeanPoints
#        trace = np.extract(self.Tbaseline<x,y)-MeanPoints  #substrats slowy varying offsets.
#        trace = self.CheckTrace(trace)   # treats eventuals NaN
#        Signal = np.extract(np.abs(trace)>2*LocalInfiniteSigma , trace).sum()/SampleRate
#        if not Signal : 
#            self.NeedsAveraging =1
#        return Signal


#%% 
"""
class ExperimentRTOld(C.QThread,Traces):
    '''
Versatile Instance for making a experiment. 
kwargs parameters shall include : 
    *The list of the parameters that should vary
        Key of the dictionnary should be 'Vary'+ParamName
        item of the dictionnary should be a dictionnary, with items {InstrName:xx, InstrCommand:yy, Instrkwargs: **kwgs, param: toto ,List : [values]} 
            to be passed to the ReadInstrument or the WriteToInstrument method
            The instrument kwargs are the one that do not vary during a experimental sequence 
            the varying parameter is passed using the passed kwarg, and its varying values throuhg elements of the List key
            for the moment, only one parameter per intrument can be varied at the same time.
        an exception is when one wants to update Timings (only one for the moment). In this case, the dictionnary should be VaryTimings
        and item of the dictionnary should be a dictionnary, with items {Param:tt,TimingRef:xx, Start:yy,Duration:zz,  List : [values]}
        with tt in {Rydberg,Delay,Ion} (for the moment)
            by default it will change the duration of Param, not its start. to vary the start point, use optional kw VaryStart, with key True
kwargs can include : 
    *Save : FileName to store the results in the today directory
    *Averages:  Number of good oscillo trace to acquire, default = 10
    *TargetNumber : a number of atoms
    
    '''
    def __init__(self,MS,**kwargs):       
        super(VaryParam,self).__init__()
        self.MS = MS
        self.ExpControl = MS.ExpControl
        self._Read = getattr(self.ExpControl,'ReadInstrument')
        self._tp = getattr( MS.ExpModel,'_Durations')
        self.ScopeModel = MS.ExpModel.Scope
        self.LambdaModel = MS.ExpModel.Lambdameter
        self.running = 1
        self.currentDensity = 0
        self.densitytol = 0.1
        self.Tbaseline = 0
        
        
        try : 
            self.ListVary = [key for key in kwargs.iterkeys() if not key.find('Vary') ]
        except KeyError : 
            print 'Invalid keys'
            self.running = 0
        except Exception as e :
            print e
            self.running = 0            
            self._Thread__stop()
            return
        if kwargs.has_key('Save'):
            self.oldmode = MS.mode
            MS.mode = 'Trace'        
            self.file = kwargs.pop('Save')
        else :
            self.file = 'temp'
        from os.path import join 
        try : 
            self.ExpResults = ExperimentalResults(
                join(ExpControl.GetTodayDir() ,self.file+'.ExpResults'),kwargs.pop('Averages'))
        except KeyError : 
            self.ExpResults = ExperimentalResults(
                    join(self.ExpControl.GetTodayDir() ,self.file+'.ExpResults'),10)
        except Exception as e :
            print e
            self.running = 0
            
        if kwargs.has_key('TargetNumber') : 
            print 'tranforsm current number in density according to the mcp calibrations !!!!'
            self.currentDensity = kwargs.pop('TargetNumber')
        if 'VoltField' in self.ListVary :
            print 'tranforsm current number in density according to the mcp calibrations !!!!'
            self.ListVary.insert(0,self.ListVary.pop(self.ListVary.index('VoltField')))

        if 'Timings' in self.ListVary and ListVary['Timings']['Param'] == 'Rydberg':
            self.texcurrent = 0
        else :
            self.texcurrent = self._tp['Rydberg'][2]   
        if 'Timings' in self.ListVary:
            try : 
                r=self.ListVary['Timings']['VaryStart']
            except KeyError:
                self.ListVary['Timings']['VaryStart'] = False      
        self.avgcurrent = self.ExpResults.Average
        self.expcont = 0
        self.Listkwargs = [ kwargs.pop['Vary'+elem]   for elem in self.ListVary ]
        if len(kwargs) :
            print 'some kwargs passed to function are not usefull, be carefull : '
            print kwargs
            from time import sleep
            sleep(5)
        try : 
            self.listdim = [len(elem['List']) for elem in self.Listkwargs]
        except : 
            'kwargs of Vary parameters are not properly formatted'
            self.running = 0            
            return
        self.IntegratedSignal = []     

        fil = 'C:\\Documents and Settings\\Wilfried\\Mes documents\\ExperimentResults\\ScopeNoIons\\NoIon.pickles'
        from os import path        
        from cPickle import load
        if path.exists(fil) : 
            self._Calib = load(fil)
            MS.action.append(['Read','Scope','VoltScale',{'Channel':0}])
            #Exp.action.append(['Read','Scope','VoltScale',**{'Channel':0}])
            self.Calib = 0
        else : 
            print 'Error, no calibration file for Scope. Thread will not work !!!'
            time.sleep(5)
            self.running = 0
        
    def run(self):
        from time import sleep
        try : 
            tempdic = {}
            [tempdic.__setitem__(elem['Param'], elem['List'][:]) for elem in self.Listkwargs]
            while self.running : 
                if self.avgcurrent == self.ExpResults.Averages : 
                    '''
                        Cycle through the list parameters and passes it to main Thread to execute them
                    '''
                    curparams = []
                    for elem in self.Listkwargs : 
                        if elem == 'Timings' : 
                            name = elem['Param']
                            tref = elem['TimingRef']
                            if elem['VaryStart'] : 
                                start = elem['List'].pop(0)
                                stop = elem['Duration']
                            else : 
                                start = elem['Start']
                                stop = elem['List'].pop(0)
                            self.MS.action.append(('Timings',name,tref,start,stop))
                        else : 
                            name = elem['InstrName']
                            cmd = elem['InstrCommand']
                            kw = elem['kwargs']
                            kw[elem['Param']] = elem['List'].pop(0)
                            curparam.append[name,cmd,kw]
                            if not len(elem['List']) : 
                            #    When elem[List] is empty, reinitalyses it. When scan is finished, 
                            #    normally the List element is in the same state as initial, and then it exits
                                elem['List'] = tempdic['Param'][:]
                            self.MS.action.append(('Write',name,cmd,kw))
                    self.expcont +=1
                    curdic = {}
                    [curdic.__setitem__(elem['InstrCommand'], elem['List'][:]) for elem in self.Listkwargs]
                    if curdir == tempdic : 
                        self.running = 0
                    self.avgcurrent = 0
                elif not len(self.MS.action) : 
                    if self.ScopeModel['NewMeasure'] & 0b1000 : 
                        self.ScopeModel['NewMeasure'] | 0b0111
                        if self.TestResults():
                            self.ExpResults.addResults({'Params':curparams[:],'Model':self.Exp.ExpModel,
                                                    'Trace':self.ScopeModel['Data0'][:],
                                                    'Frequency' : self.Lambdameter['Frequency']})
                                                    
                            self.avgcurrent +=1
                        elif self.NeedsAveraging : 
                            self.ExpResults.addResults({'Params':curparams[:],'Model':self.Exp.ExpModel,
                                                    'Trace':self.ScopeModel['Data0'][:],
                                                    'Frequency' : self.Lambdameter['Frequency']*1})
                                                    
                            self.avgcurrent +=1
                            
                else : 
                    sleep(1)
                sleep(0.05)
        except Exception as e :
            print e
            sleep(5)
        finally :           
            sleep(0.05)
            self.MS.mode = self.oldmode 
            self.ExpControl.ExpResults = self.ExpResults
            self.ExpControl.SaveExpResult(self.ExpResults)

"""

#%% 
class ExperimentRT(C.QThread,Traces):
    '''
Versatile Instance for making a experiment. 
kwargs parameters shall include : 
    *The list of the parameters that should vary
        Key of the dictionnary should be 'Vary'+ParamName
        item of the dictionnary should be a dictionnary, with items {InstrName:xx, InstrCommand:yy, Instrkwargs: **kwgs, param: toto ,List : [values]} 
            to be passed to the ReadInstrument or the WriteToInstrument method
            The instrument kwargs are the one that do not vary during a experimental sequence 
            the varying parameter is passed using the passed kwarg, and its varying values throuhg elements of the List key
            for the moment, only one parameter per intrument can be varied at the same time.
        TimingInstr have InstrCommand which is a element of the TimeTags of the TimeTable dictionnary (RydbergStop is a particular TimeTag, which control the length of the excitation laser and is used to lock the density)
kwargs can include : 
    *Save : FileName to store the results in the today directory
    *Averages:  Number of good oscillo trace to acquire, default = 10
    *TargetNumber : a number of atoms
    
    '''
    Progress = C.pyqtSignal(float,float)
    TrigScope = C.pyqtSignal()
    def __init__(self,MS,**kwargs):    
        
        super(ExperimentRT,self).__init__()        
     #   C.Qthread
        self.MS = MS
        self.ExpControl = MS.ExpControl
        self.ExpModel = MS.ExpModel
        self._Read = getattr(self.ExpControl,'ReadInstrument')
        self._Write = getattr(self.ExpControl,'ReadInstrument')
        self.TimeTable = getattr( MS.ExpModel, 'TimeTable')
        self._Timings = getattr(MS.ExpControl,'UpdateTimings')
        self.ScopeModel = MS.ExpModel.Scope
        self.LambdaModel = MS.ExpModel.Lambdameter
        self.running = 1
        self.currentDensity = 0
        self.densitytol = 0.1
        self.Tbaseline = 0
        print kwargs
        self.NoLockSeq=kwargs['NoLockSeq']
        try : 
            self.ListVary = [key for key in kwargs.iterkeys() if not key.find('Vary') ]
        except KeyError : 
            print 'Invalid keys'
            self.running = 0
        except Exception as e :
            print e
            self.running = 0            
            self.terminate()
            return
        if kwargs.has_key('Save'):

            self.file = kwargs.pop('Save')
        else :
            self.file = 'temp'
 
        from os.path import join 
        try : 
            self.ExpResults = ExperimentResults(
                join(self.ExpControl.GetTodayDir() ,self.file+'.ExpResults'),kwargs.pop('Averages'))
        except KeyError : 
            self.ExpResults = ExperimentResults(
                    join(self.ExpControl.GetTodayDir() ,self.file+'.ExpResults'),10)
        except Exception as e :
            print e
            self.running = 0
        setattr(self.ExpResults,'ExpModel',self.ExpModel)
        if kwargs.has_key('TargetNumber') : 
            print 'tranforsm current number in density according to the mcp calibrations !!!!'
            self.currentDensity = kwargs.pop('TargetNumber')
        if 'VoltField' in self.ListVary :
            print 'tranforsm current number in density according to the mcp calibrations !!!!'
            self.ListVary.insert(0,self.ListVary.pop(self.ListVary.index('VoltField')))      
        for key in self.ListVary:
            if kwargs[key]['InstrName'] in self.ExpModel.TimingInstr and kwargs[key]['InstrCommand']=='RydbergStop': 
                self.texcurrent = 0
        if not hasattr(self,'textcurrent'):
            i = self.TimeTable['TimeTags'].index('RydbergStop')
            self.texcurrent = self.TimeTable['Times'][i-1] 
        self.avgcurrent = self.ExpResults.Average
        self.expcont = 0
        self.Listkwargs = [ kwargs.pop(elem)   for elem in self.ListVary ]
        if (not kwargs.has_key('RecData0'))and (not kwargs.has_key('RecFrequency')):
            NewLog(None,'Strange one often records Oscillo Traces and Lambdameter Frequency',2)

        Reckeys = [key for key in kwargs.iterkeys() if not key.find('Rec') ]
        self.ListRec = [kwargs.pop(key) for key in Reckeys]
        ListInstrRec=[Rec['InstrName'] for Rec in self.ListRec]
        print ListInstrRec
#        l = [elem['InstrCommand'] for elem in self.ListRec ]
#        l.append('Time')
#        for elem in l : 
#            self.ExpResults.Resultats[elem]=[]

        
        if len(kwargs) :
            print 'some kwargs passed to function are not usefull, be careful : '
            print kwargs
            self.sleep(1)
        if True :
            self.listdim = [len(elem['List']) for elem in self.Listkwargs]
#        except : 
#            'kwargs of Vary parameters are not properly formatted'
#            self.running = 0            
#            return
        self.IntegratedSignal = []     
#        dims = [len(self.Listkwargs[key]['List']) for key in self.ListVary]
        res = 1
        for n in self.listdim : 
            res*=n
        self.npoints = res
        self.NextTempPoint=int(self.npoints/10.)
        self.n=0
        self.pause = False
        self.IsLocked = 1
        self.nwait = 0
        self.CurrentScanSpan = 50e-6
        self.oneprint = 1   
        IN=0
        while hasattr(self,'TrigInstr')==False or IN<len(self.ExpModel.TrigInstr):            
            if self.ExpModel.TrigInstr[IN] in ListInstrRec:
                self.TrigInstr=self.ExpModel.TrigInstr[IN]
            IN+=1    
        self.NewTrigo = 0
        self.userpause = False
        if self.NoLockSeq:
            self.LaserStable = None
        else:
            self.LaserStable = 0
            self.MS.LambdameterRT.NewLambda.connect(self.HasNewLambda)
            

        self.start()
        print self.TrigInstr
        print 'init ok'

### Loads the MachineLearning parameters to recognize signals here To be implemented
#        fil = 'C:\\Documents and Settings\\Wilfried\\Mes documents\\ExperimentResults\\ScopeNoIons\\NoIon.pickles'
#        from os import path        
#        from cPickle import load
#        if path.exists(fil) : 
#            self._Calib = load(fil)
#            MS.action.append(['Read','Scope','VoltScale',{'Channel':0}])
#            #Exp.action.append(['Read','Scope','VoltScale',**{'Channel':0}])
#            self.Calib = 0
#        else : 
#            print 'Error, no calibration file for Scope. Thread will not work !!!'
#            time.sleep(5)
#            self.running = 0

    def ResonnanceScanned(self):
     #   print 'l(598)',hasattr(self,'freq'),hasattr(self,'signal'),hasattr(self.MS,'ScanRT')
        self.pause = False
        self.CurrentScanSpan = abs(self.MS.TargetFreq - self.oldfreq)+30e-6
        tmp = [self.MS.lastfit.data,self.MS.lastfit.params,self.MS.lastfit.result.redchi]
        self.ExpResults.addTemp(tmp)
        self.MS.OldScan = 'ScanRT'
        delattr(self.MS,'ScanRT')
        
    def HasNewLambda(self):
        if (self.MS.TargetFreq - self.ExpModel.Lambdameter['Frequency'])*1e6<2 : 
            self.LaserStable +=1
        else : 
            self.LaserStable =0

    def NewTrace(self):
        if self.pause : 
            if (not len(self.MS.action)):
                if self.oneprint:
                    self.elems = dir(self.MS)
                    self.elemself = dir(self)
                    self.oneprint =0
                self.NewTrigo = 1              
            self.NewTrig =0
            return
        self.NewTrig =1                
                
    def Pause(self):
        self.userpause = not self.userpause
        
    def CatchHaveFinished(self):
        self.running = 0
      
    def run(self):
        

        from datetime import datetime
        if True:
            self.NewTrig = 0
            if self.TrigInstr =='Scope':
                self.MS.ScopeRT.ScopeTriggered.connect(self.NewTrace)
            elif self.TrigInstr =='CameraThor':
                self.MS.CameraRT.CameraTriggered.connect(self.NewTrace)
            while not self.NewTrig:
                self.msleep(1)
            self.MS.mode = 'Trace'   
            tempdic = {}
            [tempdic.__setitem__(elem['param'], elem['List'][:]) for elem in self.Listkwargs]
            self.tmp = tempdic.copy()
            
            while self.running : 
                if self.avgcurrent == self.ExpResults.Average : 
                    '''
                        Cycle through the list parameters and passes it to main Thread to execute them
                    '''      
                    curparams = []

                    KeepCmdLoop=1
                    CmdModifiesRydberg=0
                    NCmd=0
                    for elem in self.Listkwargs :
                        if KeepCmdLoop :
                            NCmd+=1
                            if not len(elem['List']) :
                                elem['List'] = tempdic[elem['param']][:]
                            elif self.expcont>0:
                                KeepCmdLoop=0
                            
                            if elem['InstrName'] in self.ExpModel.TimingInstr : 
                                cmd = elem['InstrCommand']
                                tim = self.TimeTable.copy()
                                i = self.TimeTable['TimeTags'].index(cmd)
                                timechanged =  elem['List'].pop(0)
                                tim['Times'][i-1]  = timechanged
                                self.MS.action.append(('Timings',tim))
                                curparams.append([self.TimeTable['TimeTags'][i],timechanged])
                      #          print 'l641', datetime.now()
                            else : 
                                name = elem['InstrName']
                                cmd = elem['InstrCommand']
                                kw = elem['Instrkwargs']
                                kw[elem['param']] = elem['List'].pop(0)
                                #print 'elem', elem['List']
                                curparams.append([name,cmd,kw])
                                if cmd == 'WaitforTrig':
                                    raw_input('PressEnter') 
                                else:
                                    self.MS.action.append(('Write',name,cmd,kw))
                            if cmd in self.ExpResults.ModifiesRydbergFreq:
                                CmdModifiesRydberg=1
              #      print 'l649',cmd,self.ExpResults.ModifiesRydbergFreq                   
              #      print 'Nombre de commandes = ', NCmd
                    if CmdModifiesRydberg :

               #         print 'l670',cmd,self.ExpResults.ModifiesRydbergFreq 
                        self.pause = True
                        self.oldfreq = self.MS.TargetFreq
                        print 'Scanning resonnance'
                        self.MS.ScanRT = ScanResonnance(self.MS,self.oldfreq,self.CurrentScanSpan)
                        self.freq = []
                        self.signal = []
                        self.MS.ScanRT.ScanFinished.connect(self.ResonnanceScanned)  
                      #  print 'l679',hasattr(self.MS,'ScanRT'),self.MS,  'ScanRT' in dir(self.MS)
                    
                    self.avgcurrent = 0
                    self.Progress.emit(self.avgcurrent,self.expcont)
                elif not len(self.MS.action) : 
                    if self.NewTrig : 
                        self.NewTrig=0
                        if self.TestResults(self.ExpResults.Average,self.avgcurrent,self.userpause):
                            if self.avgcurrent==0 :
                                self.msleep(300)                            
                            self.Datas = {}
                            #print self.ListRec
                            for elements in self.ListRec : 
                                
                                name = str(elements['InstrName'])
                                cmd = str(elements['InstrCommand'])
                                kw = elements['Instrkwargs']
                                print elements
                                if name == 'Scope':
                                    if cmd=='Data0': 
                                        self.Datas['Data0'] = self.ExpModel.Scope['Data']
                                    elif cmd=='Custom':
                                         N=kw['Number']
                                         self.Datas[cmd+unicode(int(N))] = self.ExpModel.Scope['CUST'+unicode(int(N))]
                                    else:
                                        print 'Command not implemented in the real-time threads of Scope'     
                                elif name == 'Lambdameter' and kw['Channel']== LambdameterSwitch.index(ComputerDict[gh()])+1:
                                    self.Datas['Frequency']=self.ExpModel.Lambdameter['Frequency'] 
                                elif name == 'CameraThor':
                                    self.Datas['']=self.ExpModel.CameraThor['SingleImageCapture']
                                
                                else:
                                    self.Datas[cmd] = self.ExpControl.Read2(name,cmd,**kw)    

                            self.Datas['Time'] = datetime.now()     
                            
                            if self.LaserStable>2 or self.NoLockSeq : 
                                self.ExpResults.addTemp(self.Datas.copy()) 
                                self.avgcurrent +=1
                               # print 'l707','Data recorded'
                            else : 
                                print 'l705','Laser out of lock'
                            self.Progress.emit(self.avgcurrent,self.expcont)  
                            
#                        if (not self.pause) and self.MS.RydbergRT.indic==Suspended : 
#                            self.avgcurrent = 0
#                            self.ExpResults.ClearCurrent()
#                            self.Progress.emit(self.avgcurrent,self.expcont)                             
                            
                        if not len(elem['List']) : 
                        #    When elem[List] is empty, reinitalyses it. When scan is finished, 
                        #    normally the List element is in the same state as initial, and then it exit
                            elem['List'] = tempdic[elem['param']][:]
                        if self.avgcurrent==self.ExpResults.Average:
                            print unicode(int(self.expcont+1)) +'/'+unicode(int(self.npoints))
                            print 'curparams[:] = ',curparams[:]
                            print ''
                            self.ExpResults.NewPoint(curparams[:])
                            if self.npoints > 20 and self.expcont==self.NextTempPoint:  ## Take a Temporary results each 10% of the sequence for long sequences
                                self.ExpControl.SaveExpResult(self.ExpResults,'temp')
                                self.NextTempPoint=self.NextTempPoint+int(self.npoints/10.)
                            self.expcont +=1
                            self.Progress.emit(self.avgcurrent,self.expcont)
                            if self.expcont==self.npoints:
                                self.running=0
     
                else : 
                    self.nwait +=1
          #          print 'l710',hasattr(self.MS,'ScanRT')
                    self.msleep(10)
                self.msleep(10)
        print 'finished !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        if True :        
            self.MS.mode = 'Align'
            self.ExpControl.ExpResults = self.ExpResults
            self.ExpControl.SaveExpResult(self.ExpResults) 


#%%
class RydbergRT(C.QThread):
    EmitStatus = C.pyqtSignal(int)
    def __init__(self,MS,Laser):
        super(RydbergRT,self).__init__()
        self.MS = MS
        self.LambdaModel = MS.ExpModel.Lambdameter
        self.laser=Laser
        if Laser == 'Matisse':
            self.cmd = 'PZT' 
        elif Laser == 'Solstis' : 
            self.cmd = 'ResonatorLock' 
        self._Read = getattr(MS.ExpControl,'ReadInstrument')
        self._Write = getattr(MS.ExpControl,'WriteToInstrument')
        self.speed = 'fast'
        self.running = 1
        self.New = 0
        self.indic = MS.GUI.LockLaserButton
        self.GUI = MS.GUI
#        setattr(self.indic,'Lock',Off)
        #Don't know if it is useful
        self.start()
        
    def NewMeas(self):
        self.New=1
        
    def ColorIndic(self,df):
        if abs(1e6*df)<2 :
            if self.indic.val == Running :
                return
            else : 
                self.EmitStatus.emit(Running)
        else : 
            if self.indic.val == Suspended:                
                return
            else : 
                self.EmitStatus.emit(Suspended)

        
    
    def run(self):
        while self.running : 
            try : 
                if self.MS.RydbergLock and self.New==1 and not self.MS.ExpModel.Simulate: 
                    self.New=0
                    df = self.MS.TargetFreq - self.LambdaModel['Frequency']
                    df = np.round(df,6)
                    if  int(1e6*df) :
                        if not ComputerDict[gh()]=='Ytterbium':
                            res = self._Write(self.laser,self.cmd,**{'target':self.MS.TargetFreq,
                                            'current':self.LambdaModel['Frequency'],'mode':self.speed})
                                          
                        else:
                            res = self._Write(self.laser,self.cmd,**{'target':self.MS.TargetFreq,
                                            'current':self.LambdaModel['Frequency'],'mode':self.speed})
                            self.msleep(100)
                        self.ColorIndic(df)
                        
                        if res in ['OutOfBound','Unlocked'] :
                            self.EmitStatus.emit(Stop)
                            self.MS.RydbergLock=0
                            
                    self.msleep(max(min(int(1000*self.LambdaModel['CycleTime']),2),100))         
                if self.MS.ExpModel.Simulate and not self.indic.val is Running: 
                    self.EmitStatus.emit(Running)
            except Exception as e : 
                NewLog(e,'Laser seem disconnected',1)
                self.MS.ExpControl.ReLaunch(self.laser)
           
#%% 

class ScanResonnance(C.QThread,Traces):
    '''
        Find the Rydbergs resonance
        
    '''
    ScanFinished = C.pyqtSignal()
    def __init__(self,MS,fcenter,df):
        super(ScanResonnance,self).__init__()
        self.MS = MS
        self.ScopeModel = MS.ExpModel.Scope
        self.LambdaModel = MS.ExpModel.Lambdameter
        self.freq = []
        self.signal = []
        if hasattr(self.MS,'mode'):    
            self.oldmode = self.MS.mode
        else:
            self.oldmode = 'Align'
        self.MS.mode = 'Align'
        self.MS.TargetFreq = fcenter-df
        self.running = 1     
        self.oldspeed = MS.RydbergRT.speed
        self.MS.RydbergRT.speed = 'fast'
        self.f = (fcenter-df,fcenter+df)
  #      print self.f,self.lastlambda()
        self.sleep(1)
        self.st = 0
        self.start()
        
    def lastlambda(self):
        return self.LambdaModel['Frequency']
        
    def AddSignal(self):
       # print 'l809'
        if self.st : 
            if self.MS.ExpModel.manipe == 'Ytterbium':
                self.signal.append(self.ScopeModel['CUST1'])
            else : 
                self.signal.append(self.ScopeModel['CUST2'])
            self.freq.append(self.lastlambda())    
        
    def run(self):
        from datetime import datetime
        t0 = datetime.now()
        while self.running : 
            if self.st :
                if self.lastlambda() >= self.f[1]:
                    self.running=0
                    self.st = 0
                    self.MS.suspended = False
                    self.MS.mode = self.oldmode
                self.msleep(1)
                if (datetime.now().second - t0.second)/20:
                    t0 = datetime.now()
                 #   print 'l849', len(self.freq), len(self.signal)
                    self.MS.ScopeRT.ScopeTriggered.connect(self.AddSignal)
                    
                if hasattr(self.MS,'ExperimentRT'):
                    if self.MS.ExperimentRT.NewTrigo == 1:
                        self.MS.ExperimentRT.NewTrigo = 0
                        if self.MS.ExpModel.manipe == 'Ytterbium':
                            self.signal.append(self.ScopeModel['CUST1'])
                        else : 
                            self.signal.append(self.ScopeModel['CUST2'])
                        self.freq.append(self.lastlambda())  
            else :
                if self.lastlambda() <= self.f[0] and (not len(self.MS.action)):
                    self.st = 1
                    self.MS.RydbergRT.speed = 'slow'
                    self.MS.TargetFreq = self.f[1] 
                    self.MS.ScopeRT.ScopeTriggered.connect(self.AddSignal)
                self.msleep(5)
        self.ProcessData()
            
    def ProcessData(self):
        if len(self.freq) >3: 
            from Fits import fitLorentz
            res = fitLorentz(self.freq,self.signal)
            self.MS.lastfit = res
            if (res.best_values['amplitude']> 0 and 
                    np.abs(res.best_values['amplitude'])> 10*np.abs(res.best_values['c'])):
                raise TypeError('Define a correct amplitude best values in the following')
            self.MS.RydbergRT.speed = 'fast'
            self.MS.TargetFreq = res.params['center'].value
            while self.lastlambda() > res.params['center'].value:
                self.msleep(10)
            self.msleep(100)
            while self.lastlambda() < res.params['center'].value: #in case of an overshoot
                self.msleep(10)
            #res.plot_fit()
            self.MS.RydbergRT.speed = self.oldspeed
            print res.best_values
        else : 
            print 'l849',self.freq

        self.ScanFinished.emit()
        
        print 'Scan Finished'
        
        

#%% 
class RecordRef(C.QThread,Traces):
    Progress = C.pyqtSignal(int)
    DetectionOK = C.pyqtSignal()
    
    def __init__(self,refname,npoints,f0,df,MS):
        super(RecordRef,self).__init__()
        self.MS = MS
        self.refname=refname
        self.ScopeModel = MS.ExpModel.Scope
        self.LambdaModel = MS.ExpModel.Lambdameter
        self.MS.ScopeRT.ScopeTriggered.connect(self.NewTrace)
        self.running = 1
        self.ScanRT = ScanResonnance(MS,f0,df)
        self.ScanRT.ScanFinished.connect(self.start)
        self.MS.RydbergRT.EmitStatus.connect(self.LambdaOK)
        self.traces = []
    
    def LambdaOK(self,i):
        self._LambdaOK= i==Running

    def NewTrace(self):
        from time import time
        if self.running and self._lambdaOK:
            tmp = self.ScopeModel[Data0]
            if self.TestResult():
                self.traces.append(tmp)
                self.Progress.emit(len(self.traces))
        
    def run(self):
        while self.running : 
            if len(traces)<npoints:
                msleep(100)
        
        self.MS.ExpControl.SaveReference(self.refname,self.traces)
        self.Progress.emit(npoints+1)


#%%

class MainSequence(C.QThread):
    InstrChangedStatus = C.pyqtSignal('QString')
    
    def __init__(self,Controller,GUI):     
        super(MainSequence,self).__init__()
        self.ExpControl = Controller  
        self.ExpModel = self.ExpControl.ExpModel
        self._Write = getattr(Controller,'WriteToInstrument')
        self._Read = getattr(Controller,'ReadInstrument')
        self._Dur = getattr(Controller,'UpdateTimings')
        self.GUI = GUI
        self.mode = 'Align'
        if GUI == None : 
            Controller.StartRemoteLambdaServer
            Controller.BasicStart()
            self.RealStart()
            self.notini = False
        else : 
            self.notini=True
            self.running = 1  
            self.RydbergLock = 0
            

    def RealStart(self):
        self._Read('Lambdameter','Local',**{})
        if self.ExpModel.manipe == 'Ytterbium' : 
            self.ExpControl.LockSolstis()
            self.LambdameterRT =  LambdameterRealTime(self,**{'Channel':4})
            self.TargetFreq =  379.912851
        elif self.ExpModel.manipe == 'Cesium' : 
            if self.ExpModel.Lambdameter['Local'] : 
                self.LambdameterRT =  LambdameterRealTime(self,**{})
            else : 
                self.LambdameterRT =  LambdameterRealTime(self,**{'Channel':3})
            self.TargetFreq =  RydbergLevelsCesium(35,1,1)-9e-5  
            self.ScopeRT = ScopeRealTime(self)
            #self.ScopeRT.start()
            self.ExpModel.CopyTiming()

        self.RydbergLock = 0        
        self.DensityLock = 0
        self.newTrig = 0

        self.LambdameterRT.start()
        self.sleep(1)
        
        self.suspended = False        

        self.action = []
        print 'REal Start Action '
        self.InstrStatus = self.ExpModel._InstrList.copy()
        # action is a list of tupples, than can nearly be passed to Read and Write method
        # Exception is for some specific method (i.e. scanresonnance ... )
   
    def LockLaser(self):
        if not hasattr(self,'LambdameterRT'):
            self.GUI.LockLaserButton.SetState(NotApplicable)
            return
        Lasers = [Instr for Instr in self.ExpControl._InstrumentInstances.iterkeys() if Instr in LaserList ] 
        if not len(Lasers)== 1 :
            self.GUI.LockLaserButton.SetState(NotApplicable)
            return
        if hasattr(self,'RydbergRT') and self.RydbergRT.isFinished():
            self.LambdameterRT.NewLambda.disconnect()
            self.RydbergRT = RydbergRT(self,Lasers[0])
            self.LambdameterRT.NewLambda.connect(self.RydbergRT.NewMeas)
        if not hasattr(self,'RydbergRT') : 
            self.RydbergRT = RydbergRT(self,Lasers[0])
            self.LambdameterRT.NewLambda.connect(self.RydbergRT.NewMeas)
        if not hasattr(self,'RydbergLock'):
            self.RydbergLock = 0
        if self.RydbergLock:
            print 'releaseLock'
            self.RydbergLock = 0
            
            self.GUI.LockLaserButton.SetState(Stop)
#            if Lasers[0]=='Solstis':
#                delattr(self.ExpModel.Solstis,'ResoPerCent')
        else :

            self.RydbergLock = 1
            
    def FreqScanFinished(self):
        print 'Scan Finished'
        try:
            self.lastfit.plot()
        except :
            pass
        
    def SetInstr(self,Instr):
        Params = getattr(self.ExpModel,Instr)
        if not self.ExpModel._InstrList[Instr]==InitOK:
            print 'NotInitialized'
            return 
        for key,val in Params.iteritems():
            try : 
                if not key is 'InitKwargs':
                    self._Write(Instr,key,**{'Unknown':val})
            except Exception as e : 
                NewLog(e,'Cannot Call {} func with unique param value for Instr {}'.format(key,Instr))
    
    
    def OsciloRT(self):
        '''
            lauches and stops the Scope Acquisition Thread theead        
        '''
        print 'ScopeStatus', 
        if 'Scope' in self.ExpControl._InstrumentInstances.keys():
            if (not hasattr(self,'ScopeRT')) or self.ScopeRT.isFinished():
                setattr(self,'ScopeRT',ScopeRealTime(self))
                self.ScopeRT.ScopeTriggered.connect(self.GUI.PrintAtNum)
        else : 
            if hasattr(self,'ScopeRT'):
                print 'trying to finish'
                self.ScopeRT.running = 0;
                self.ScopeRT.ScopeTriggered.disconnect()
                while not self.ScopeRT.isFinished():
                    self.msleep(100)
                delattr(self,'ScopeRT')
        print 'HasScopeRT',hasattr(self,'ScopeRT')
    
    def LambdaRT(self):
        '''
            lauches and stops the Reading frequency thread        
        '''
        self._Read('Lambdameter','Local',**{})
        if 'Lambdameter' in self.ExpControl._InstrumentInstances.keys():
            if self.ExpModel.Lambdameter.has_key('Channel') :
                b=  self.ExpModel.Lambdameter['Channel']
                self.LambdameterRT =  LambdameterRealTime(self,**{'Channel':b})
            else : 
                self.LambdameterRT =  LambdameterRealTime(self,**{})
            self.LambdameterRT.NewLambda.connect(self.GUI.PrintLambda)
            
            #self.GUI.printLambda
           # if self.RydbergLock : 
        else :
            self.LambdameterRT.running=0
            self.LambdameterRT.NewLambda.disconnect()
            while not self.LambdameterRT.isFinished():
                self.sleep(0.02)
            delattr(self,'LambdameterRT')
            
            
    def CameraRTStartStop(self):
        '''
            lauches and stops the Reading frequency thread        
        '''
        
        if hasattr(self,'CameraRT'):
            self.CameraRT.terminate()
            delattr(self,'CameraRT')
            
        else:
            print 'here'
            if 'CameraThor' in self.ExpControl._InstrumentInstances.keys():
                self.CameraRT=CameraRealTime(self)
            else:
                self.CameraRT.running=0
                delattr(self,'CameraRT')
            
            
            
            
   
    def test(self):
        from ControleManipUI import GUIWidgets as GW
        l0 = [elem for elem in self.GUI.InstrFrame.children() if isinstance(elem,GW.ClickAndColor)]
        l = [[elem.objectName().replace('Status',''),elem] for elem in l0 if 'Status' in elem.objectName()]
        for Instr,elem in l:
            if not elem._status == ConnectDic[self.ExpModel._InstrList[Instr]] : 
                self.InstrChangedStatus.emit(elem.objectName())
        if ComputerDict[gh()]=='Cesium':
            return not (self.ExpModel._InstrList['Lambdameter'] + 
                    self.ExpModel._InstrList['Scope'] +
                    self.ExpModel._InstrList['SRSDelayGen'] 
                    == 6 )
        elif ComputerDict[gh()]=='Ytterbium':
            return not (self.ExpModel._InstrList['Lambdameter'] + 
                    self.ExpModel._InstrList['Scope'] +
                    self.ExpModel._InstrList['BNCDelayGen'] 
                    == 6 )

    def __del__(self):
        self.wait()     
        
    
    def run(self):
        NewLog(None,'StartMainThread',2)
          
        while self.running :
            self.notini = self.test()
            if self.notini :              
                self.msleep(5)
                self.action = []
                           
            else : 
                if (len(self.action)): 
                    Popped = self.action.pop(0)
                    if (not Popped[0].find('Read') )or (not Popped[0].find('Write')) :
                        if type(Popped[1]) in [type(u''),type('')] :                     
                            Instr = str(Popped[1]) 
                        if type(Popped[2]) in [type(u''),type('')] : 
                            Cmd = str(Popped[2])
                        if type(Popped[3])==type({}) : 
                            kwargs = Popped[3]
                        if (not Popped[0].find('Read') ):
                            func = self._Read
                        if (not Popped[0].find('Write') ):
                            func = self._Write
                        try : 
                            func(Instr,Cmd,**kwargs)
                        except Exception as e:
                            print e
                            self.msleep(2)
                    elif not Popped[0].find('Timings') :
                        from datetime import datetime
                        print 'l1080', datetime.now()
                        try : 
                            self._Dur(**(Popped[1]))
                        except Exception as e:
                            print e
                            self.msleep(5)
                    else : 
                        method = getattr(self,Popped[0])
                        tup = tuple(Popped[1:])
                        method(*tup)
                        self.suspended = True
                        self.msleep(1)
                else :
#                    b = True
#                    for key,val in self.GUI.ExpectedStatus.iteritems() : 
#                        if val is InitOK : 
#                            b = b and self.ExpControl.TestInstr(key)    
#                    if not b :
#                        self.suspended = 1
#                        try : 
#                            self.ExpModel._InstrList =  self.GUI.ExpectedStatus.copy()
#                            self.ExpControl.CheckInstruments()     
#                        except Exception as e:
#                            NewLog(e)
#                        if self.ExpModel._InstrList == self.GUI.ExpectedStatus :
#                            self.suspended = 0
#                            self.msleep(1)
#                        else : 
#                            self.msleep(5)
#                    else :
                    if True:
                        self.msleep(1)        

         
        if hasattr(self,'RydbergRT'):
            self.RydbergRT.running=0
        NewLog(None,'MainThreadFinished',2)
        
    def stop(self):
        self.running = 0
        self.RydbergRT.running=0
        
    

        
#%%             
    

    
if __name__=="__main__":
    from PyQt4 import QtGui as G
    app = G.QApplication(sys.argv)
    b = Controller()
    a = MainSequence(b,None)
    
    #a.start()
    InstrumentDict
    
#%% 
#%%     
#            
#import matplotlib
#import matplotlib.pyplot as plt
#import matplotlib.cm as cmap
#import time
#
#class BasicPlot(C.QThread):
#    def __init__(self,Main):
#        super(BasicPlot,self).__init__()
#        self.Fig,[[self.ax1,self.ax2a],[self.ax3,self.ax0]] = plt.subplots(2, 2,figsize=(16,16),dpi=48,facecolor='w')
#        self.ax2b = self.ax2a.twinx()
#        self.ax1.set_title('ScopeTrace')
#        self.ax3.set_title('Lambdameter')
#        self.ax2a.set_title('IonSignal')
#        self.ax0.set_title('SpeciesSignal')
#        self.PltView = 1000
#        plt.show()
#        plt.ion()
#        self.Fig.canvas.draw()    
#        self.running = 1
#        self.plot = 0b0000
#        self.Main = Main
#        self.ScopeModel = Main.ExpControl.ExpModel.Scope
#        self.LambdaModel = Main.ExpControl.ExpModel.Lambdameter
#        self.ndatamax = (1000,)
#        self.tlambda = np.nan*np.ones(self.ndatamax)
#        self.Lambda = np.nan*np.ones(self.ndatamax)
##        self.ttotal = np.nan*np.ones(self.ndatamax)
##        self.TotalMean = np.nan*np.ones(self.ndatamax)
##        self.TotalPeakPeak = np.nan*np.ones(self.ndatamax)
#        self.ttotal = []
#        self.TotalMean = []
#        self.TotalPeakPeak = []
#        self.counts = [0,0,0,0]
#        self.raster = slice(1000,20000,100)
#        self.timeax1=0
#        self.timeax2=0
#        self.timeax3=0
#        self.timeax3a=0
#        self.timeax3b=0
#        self.timeax3c=0
#        self.timebackend=0
#        self.t0 = time.time()
#        self.colora = matplotlib.colors.cnames['firebrick']
#        self.colorb = matplotlib.colors.cnames['dimgrey']
#        
#    def redrawAxis(self,ax,axis):
#        try : 
#            ax.draw_artist(axis)
#        except RuntimeError as e :
#            print e.args
#            print self.ax3.get_ybound()
#            print 'Understood ? '  
#       
#    def run(self):
#        while self.running:
#            updated = 0
#            if self.plot& 0b0100 :
#                t0 = time.clock()                
#                if self.LambdaModel['NewMeasure']& 0b0100 :
#                    self.LambdaModel['NewMeasure'] -= 0b0100
#                    if self.counts[3]>990:
#                        self.tlambda[:495]=self.tlambda[:990:2]
#                        self.tlambda[496:]=self.tlambda[496:]*np.nan
#                        self.Lambda[:495]=self.Lambda[:990:2]
#                        self.Lambda[496:]=self.Lambda[496:]*np.nan  
#                        self.counts[3]=496
#                    self.tlambda[self.counts[3]]=time.time() - self.t0
#                    self.Lambda[self.counts[3]]=self.LambdaModel['Frequency']
#                    self.counts[3]+=1
#                    te = self.LambdaModel['Frequency']
#                    if not  self.ax3.get_ybound()[0]<te:
#                        temp = self.ax3.get_ybound()[1]
#                        self.ax3.set_ylim([te-0.1*(temp-te),temp])
#                        self.redrawAxis(self.ax3,self.ax3.yaxis)
#                    if not te< self.ax3.get_ybound()[1]:
#                        temp = self.ax3.get_ybound()[0]
#                        self.ax3.set_ylim([temp,te+0.1*(te-temp)])
#                        self.redrawAxis(self.ax3,self.ax3.yaxis)
#                    te=self.tlambda[self.counts[3]-1]
#                    if self.ax3.get_xbound()[1]<te: 
#                        self.ax3.set_xlim([self.tlambda[0],self.ax3.get_xbound()[1]+self.PltView])
#                        self.redrawAxis(self.ax3,self.ax3.yaxis) 
#                    t0a = time.clock()
#                    self.timeax3a += t0a-t0
#                    if len(self.ax3.lines) :                        
#                        self.line3.set_xdata(self.tlambda)
#                        self.line3.set_ydata(self.Lambda)
#                        t0b = time.clock()
#                        self.timeax3b += t0b-t0a
#                    else : 
#                        self.line3, = self.ax3.plot(self.tlambda,self.Lambda,marker = '.',color = self.colora)
#                        t0b = time.clock()
#                        self.timeax3b += t0b-t0a
#                        self.ax3.set_xlim(0,self.PltView)
#                        self.ax3.set_ylim([self.Lambda[0]-1e-12,self.Lambda[0]+1e-12])
#                        self.ax3.draw_artist(self.ax3.patch)
#                        self.redrawAxis(self.ax3,self.ax3.xaxis)
#                        self.redrawAxis(self.ax3,self.ax3.yaxis)
#                    self.ax3.draw_artist(self.line3)
#                    t0c = time.clock()
#                    self.timeax3c +=  t0c-t0b 
#                    self.timeax3 += t0c -t0
#                    updated = 1
#            else  : 
#                 
#                if self.LambdaModel['NewMeasure']& 0b0100 :
#                    self.LambdaModel['NewMeasure'] -= 0b0100  
#                    updated = 1
#                    try : 
#                        self.lamddatext.set_text('{:.6f}'.format(self.LambdaModel['Frequency']))
#                        self.lambdatarg.set_text('{:.6f}'.format(self.Main.TargetFreq))
#                    except AttributeError : 
#                        self.lamddatext=self.ax3.annotate('{:.6f}'.format(self.LambdaModel['Frequency']),xy=(0.01,0.45),xycoords='axes fraction')
#                        self.lamddatext.set_fontsize(110)
#                        self.lambdatarg = self.ax3.annotate('{:.6f}'.format(self.Main.TargetFreq),xy=(0.01,0.25),xycoords='axes fraction')
#                        self.lambdatarg.set_fontsize(80)
#                        self.counts[3]+=1
#                    except :
#                        raise
#                    try : 
#                        self.ax3.draw_artist(self.ax3.patch)
#                        self.ax3.draw_artist(self.lamddatext)
#                        self.ax3.draw_artist(self.lambdatarg)    
#                    except Exception as e :
#                        print e
#                        pass
#            
#            if self.plot& 0b0010 :
#                t0 = time.clock()
#                if self.ScopeModel['NewMeasure']& 0b0010 and self.Main.mode is 'Data':
#                    self.ScopeModel['NewMeasure'] -= 0b0010
#                    self.ttotal.append(time.time())
#                    temp=self.ScopeModel['Data1'][self.Main.DensityWindow]
#                    self.TotalMean.append(temp.mean())
#                    self.TotalPeakPeak.append(temp.min()-self.TotalMean[-1])  
#                    if len(self.ttotal)<2 :
#                        self.ax2a.set_ylim([self.TotalMean[0]-1e-12,self.TotalMean[0]+1e-12])
#                        self.ax2b.set_ylim([self.TotalPeakPeak[0]-1e-12,self.TotalPeakPeak[0]+1e-12])
#                    ymin,ymax = self.ax2a.get_ybound()
#                    if (self.TotalMean[-1]> ymax or
#                            self.TotalMean[-1]< ymin):
#                        mimax = [min(self.TotalMean),max(self.TotalMean)]                     
#                        yrange = max((mimax[1]-mimax[0])*0.1,1e-6)
#                        self.ax2a.set_ylim([min(mimax[0]-yrange,ymin),
#                                           max(mimax[1]+yrange,ymax)])
#                        self.ax2a.draw_artist(self.ax2a.yaxis)
#                    ymin,ymax = self.ax2b.get_ybound()
#                    if (self.TotalPeakPeak[-1]> ymax or
#                            self.TotalPeakPeak[-1]< ymin):
#                        mimax = [min(self.TotalPeakPeak),max(self.TotalPeakPeak)]                     
#                        yrange = max((mimax[1]-mimax[0])*0.1,1e-6)
#                        self.ax2b.set_ylim([min(mimax[0]-yrange,ymin),
#                                           max(mimax[1]+yrange,ymax)])
#                        self.ax2b.draw_artist(self.ax2b.yaxis)
#                    if self.ttotal[-1]>self.ax2a.get_xbound()[1] :
#                        self.ax2a.set_xlim(self.ttotal[0],self.ttotal[-1]+self.PltView)
#                        self.ax2a.draw_artist(self.ax2a.xaxis)
#                    try :
#                        if not len(self.ttotal)==len(self.TotalMean) :
#                            print ('Weird, {} is not same length as {}\ntruncating the smaller one'.format('ttotal','TotalMean'))
#                            [self.ttotal,self.TotalMean]=zip(self.ttotal,self.TotalMean)
#                        if not len(self.ttotal)==len(self.TotalPeakPeak) :
#                            print ('Weird, {} is not same length as {}\ntruncating the smaller one'.format('ttotal','TotalMean'))
#                            [self.ttotal,self.TotalPeakPeak]=zip(self.ttotal,self.TotalPeakPeak)                            
#                            [self.ttotal,self.TotalMean]=zip(self.ttotal,self.TotalMean)
#                        self.line2a.set_xdata(self.ttotal) 
#                        self.line2a.set_ydata(self.TotalMean)
#                        self.line2b.set_xdata(self.ttotal) 
#                        self.line2b.set_ydata(self.TotalPeakPeak)
#                    except AttributeError :
#                        self.line2a, = self.ax2a.plot(self.ttotal,self.TotalMean,'k+')
#                        self.line2b, = self.ax2b.plot(self.ttotal,self.TotalPeakPeak,'r+')
#                        self.ax2a.set_xlim(0,self.PltView)
#                        self.ax2a.draw_artist(self.ax2a.xaxis)
#                        self.ax2a.draw_artist(self.ax2a.yaxis)
#                        self.ax2b.draw_artist(self.ax2b.yaxis)    
#                    except RuntimeError,KeyError :
#                        pass
#                    except :
#                        raise    
#                    self.ax2a.draw_artist(self.ax2a.patch)
#                    self.ax2a.draw_artist(self.line2a)
#                    self.ax2b.draw_artist(self.line2b)
#                    self.timeax2 += time.clock()-t0
#                    updated = 1
#            else : 
#                if self.ScopeModel['NewMeasure']& 0b0010 and self.Main.mode is 'Align':
#                    self.ScopeModel['NewMeasure'] -= 0b0010
#                    updated =1
#                    try : 
#                        self.RyAreatext.set_text('{:.2f}'.format(self.ScopeModel['CUST2']*1e9))
#                        self.RyPkPktext.set_text('{:.4f}'.format(self.ScopeModel['PKPK0']))
#                    except AttributeError,KeyError : 
#                        self.RyAreatext=self.ax2a.annotate('{:.2f}'.format(self.ScopeModel['CUST2']*1e9),xy=(0.01,0.45),xycoords='axes fraction')
#                        self.RyAreatext.set_fontsize(110)
#                        self.RyPkPktext = self.ax2a.annotate('{:.4f}'.format(self.ScopeModel['PKPK0']),xy=(0.01,0.25),xycoords='axes fraction')
#                        self.RyPkPktext.set_fontsize(80)
#                        self.counts[3]+=1
#                    except :
#                        raise
#                    try : 
#                        self.ax2a.draw_artist(self.ax2a.patch)
#                        self.ax2a.draw_artist(self.RyAreatext)
#                        self.ax2a.draw_artist(self.RyPkPktext)    
#                    except Exception as e: 
#                        print e
#                        pass       
#                    
#            if self.plot & 0b1000 : 
#                self.plot = self.plot & 0b0111
#                self.ax0.cla()
#                out = self.Main.lastfit
#                y = out.data
#                x = out.userkws['x']
#                self.ax0.plot(x,y,'k+')
#                xes = np.linspace(x.min(),x.max(),200)
#                yes = out.eval(x=xes)
#                self.ax0.plot(xes,yes,'r-')
#                updated =1
#                
#            if self.plot& 0b0001 : 
#                t0 = time.clock()
#                if self.ScopeModel['NewMeasure']& 0b0001 :
#                    self.ScopeModel['NewMeasure'] -= 0b0001
#                    mimax = [min(self.Scope.lastTrace ),max(self.Scope.lastTrace)] 
#                    
#                    if (mimax[1]> self.ax1.get_ybound()[1] or
#                        mimax[0]< self.ax1.get_ybound()[0]):                    
#                        yrange = max((mimax[1]-mimax[0])*0.1,1e-6)
#                        self.ax1.set_ylim([mimax[0]-yrange,mimax[1]+yrange])
#                        self.ax1.draw_artist(self.ax1.yaxis)   
#                    try :    
#                        self.line1.set_xdata(self.Scope.last[self.raster])
#                        self.line1.set_ydata(self.Scope.lastTrace[self.raster])
#                        self.debug1.set_text('{0}'.format(a.Plot.Scope.nTrigs))
#                    except AttributeError : 
#                        self.line1, = self.ax1.plot(self.Scope.last[self.raster],self.Scope.lastTrace[self.raster],'k-')
#                        self.debug1 = self.ax1.annotate('{0}'.format(self.
#                                Scope.nTrigs),xy=(0.1,0.9),xycoords='axes fraction')                        
#                        self.ax1.draw_artist(self.ax1.xaxis)
#                        self.ax1.draw_artist(self.ax1.yaxis)
#                        self.AdjustSpacings()
#                    except : 
#                        raise
#                    self.ax1.draw_artist(self.ax1.patch)
#                    self.ax1.draw_artist(self.line1)
#                    self.ax1.draw_artist(self.debug1)
#                    self.timeax1 += time.clock()-t0
#                    t0 = time.clock()
#                    self.Fig.canvas.update()
#                    self.Fig.canvas.flush_events()
#                    self.timebackend += time.clock()-t0          
#                    updated = 1
#                        
#            if updated :      
#                try: 
#                    self.Fig.canvas.update()
#                    self.Fig.canvas.flush_events()
#                except RuntimeError :
#                    pass
#                    
#            time.sleep(0.05)
#    
#    def AdjustSpacings(self):
#        self.Fig.tight_layout()
#       
#    def ShowProcUsage(self):
#        print 'Tax1 : ',self.timeax1, 'per iteration : ',  self.timeax1/self.Scope.nTrigs ,'nTrigs' ,self.Scope.nTrigs
#        print 'Tax2 : ',self.timeax2, 'per iteration : ',  self.timeax2/len(self.ttotal)  ,'nScreened' ,len(self.ttotal)
#        print 'Tax3 : ',self.timeax3,'per iteration : ',  self.timeax3/self.Scope.nTrigs ,'nScreened' ,np.argmax(self.tlambda)
#        print 'Tax3a : ',self.timeax3a,'per iteration : ',  self.timeax3/len(self.tlambda)  ,'nScreened' ,np.argmax(self.tlambda)
#        print 'Tax3b : ',self.timeax3b,'per iteration : ',  self.timeax3/len(self.tlambda)  ,'nScreened' ,np.argmax(self.tlambda)
#        print 'Tax3c : ',self.timeax3c,'per iteration : ',  self.timeax3/len(self.tlambda)  ,'nScreened' ,np.argmax(self.tlambda)
#        print 'Tagg : ',self.timebackend,'per iteration : ',  self.timebackend/self.Scope.nTrigs ,'nTrigs' ,self.Scope.nTrigs    