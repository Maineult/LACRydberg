VisaName = u'USB0::0x17E7::0x0102::09-51-15::INSTR'
VisaNameEth = u'visa://127.0.0.1/USB0::0x17E7::0x0102::09-51-15::INSTR'
SlowPiezo = {'Min':0.02,'Max':0.65}
FastScan = 800e-6 #THz/second
SlowScan = 50e-6 #THz/second
THzperVolt = -0.11204481792717089
MatisseName = u':IDN: "Matisse TS, S/N:09-51-15, DSP Rev.:01.01, Firmware:1.15, Date:Oct 30 2015" '