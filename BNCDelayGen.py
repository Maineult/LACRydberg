  # -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 14:20:06 2015

@author: lac
"""

import visa, pyvisa, time

##sleep time after every command:
ts =0.05

## PULSE GENERATOR GENERAL PROPERTIES ##

class DelayGen():
    def __init__(self):
        self.rm = visa.ResourceManager()
        self.PG = self.rm.open_resource(u'ASRL13::INSTR')
        self.PG.baud_rate=38400  
        self.PG.ask(u':PULSE0:MODE NORM')
        time.sleep(ts)
        self.PG.write(u':PULSE0:STATE ON')
        time.sleep(ts)
        
    def __test__(self):
        test =None
        while self.PG.bytes_in_buffer:
            self.PG.read()
        self.PG.write('*IDN?')
        time.sleep(ts)
        test= self.PG.read()
        return (test == u'BNC,575-8,35488,2.4.2-2.0.11\r\n')
        
### HIGH LEVEL FUNC:        
        
    def Relaunch(self):
        self.PG.close()
        time.sleep(0.1)
        self.PG = self.rm.open_resource(u'ASRL13::INSTR')
        self.PG.baud_rate=38400  
        
        
    def singleshot(self):
        self.PG.write(u':PULSE0:MODE SING') 
        time.sleep(ts)
        
    def continuousshot(self):
        self.PG.write(u':PULSE0:MODE NORM:') 
        time.sleep(ts)      
              
    def cycleperiod(self,T):
        self.PG.write(u':PULSE0:PER '+unicode(T))
        time.sleep(ts)
        
    def start(self):
        self.PG.write(u':PULSE0:STATE ON')
        
    def stop(self):
        self.PG.write(u':PULSE0:STATE OFF')
         
    def close(self):
        self.PG = self.PG.close()
        self.rm = self.rm.close()
    
    def __del__(self):
        self.close()

        
    def WriteEnable(self, n):
        self.PG.write(u':PULS'+unicode(n)+':STATE ON')
        time.sleep(ts)

    def WriteDisable(self, n):
        self.PG.write(u':PULS'+unicode(n)+':STATE OFF')
        time.sleep(ts)
   
        ##Polarity: to be filled with NORM (active high) or INV (active low)     
    def WritePolarity(self,n, polarity):
        self.PG.write(u':PULS'+unicode(n)+':POL '+unicode(polarity))
        time.sleep(ts)
        
    def WriteLevel(self,n, voltage):
        self.PG.write(u':PULS'+unicode(n)+':AMP '+unicode(voltage))
        time.sleep(ts)
        
        ## All times in seconds ##
    def WriteDelay(self,n,delay):
        self.PG.write(u':PULS'+unicode(n)+':DELAY '+unicode(delay))
        time.sleep(ts)
        
    def WriteWidth(self,n,width):
        self.PG.write(u':PULS'+unicode(n)+':WIDT '+unicode(width))
        time.sleep(ts)

    def ReadDelay(self,n):
        Delay=self.PG.query(u':PULS'+unicode(n)+':DELAY?')
        Delay=float(Delay.replace(',','.'))
        time.sleep(ts)
        return Delay
        
        
    def ReadWidth(self,n):
        Width=self.PG.query(u':PULS'+unicode(n)+':WIDT?')
        Width=float(Width.replace(',','.'))
        time.sleep(ts)
        return Width
        

####  LOW LEVEL FUNC :

    def WriteChannel(self, Command):
        n=Command[0]
        delay =Command[1]  
        width = Command[2] 
        if width==0:
            self.WriteDisable(n)
        else:
            self.WriteEnable(n)
            self.WriteDelay(n,delay)
            self.WriteWidth(n,width)
            if len(Command)==4 : 
                self.WritePolarity(n,Command[3])
        self.stop
        self.start        
        
        def WriteCycleTime(self,CycleTime):
            self.cycleperiod(CycleTime)
        


#    def WriteChannel(self, n, delay, width, polarity='NORM'):
#        self.WriteDelay(n,delay)
#        self.WriteWidth(n,width)
#        self.WritePolarity(n,polarity)
#        self.WriteEnable(n)
#        
    def ReadAllChannels(self):
        Res=[]        
        for i in range(8):
            n=i+1
            e=1
            while e:
                try:    
                    Delay=self.ReadDelay(n)
                    Width=self.ReadWidth(n)
                    Res.insert(i,(n,Delay,Width))
                    e=0
                except:
                    time.sleep(0.01)
                    pass                
        return Res
        
    
if __name__=='__main__' : 
    a=DelayGen()        