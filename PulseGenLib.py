 # -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 14:20:06 2015

@author: lac
"""

import visa, pyvisa, time

##sleep time after every command:
ts =0.05

## PULSE GENERATOR GENERAL PROPERTIES ##

class PulseGen():
    def __init__(self):
        self.rm = visa.ResourceManager()
        self.PG = self.rm.open_resource(u'ASRL7::INSTR')
        self.PG.baud_rate=38400  
        self.PG.ask(u':PULSE0:MODE NORM')
        time.sleep(ts)
        
    def __test__(self):
        while self.PG.bytes_in_buffer:
            self.PG.read()
        test= self.PG.ask('*IDN?')
        return (test == u'BNC,575-8,35488,2.4.2-2.0.11\r\n')    
  
    def singleshot(self):
        self.PG.write(u':PULSE0:MODE SING') 
        time.sleep(ts)
        
    def continuousshot(self):
        self.PG.write(u':PULSE0:MODE NORM:') 
        time.sleep(ts)      
              
    def cycleperiod(self,T):
        self.PG.write(u':PULSE0:PER '+unicode(T))
        time.sleep(ts)
        
    def start(self):
        self.PG.write(u':PULSE0:STATE ON')
        
    def stop(self):
        self.PG.write(u':PULSE0:STATE OFF')
         
    def close(self):
        self.PG = self.PG.close()
        self.rm = self.rm.close()
    
    def __del__(self):
        self.close()
        self.PG = None
        self.rm = None
        
    def WriteEnable(self, n):
        self.PG.write(u':PULS'+unicode(n)+':STATE ON')
        time.sleep(ts)

    def WriteDisable(self, n):
        self.PG.write(u':PULS'+unicode(n)+':STATE OFF')
        time.sleep(ts)
   
        ##Polarity: to be filled with NORM (active high) or INV (active low)     
    def WritePolarity(self,n, polarity):
        self.PG.write(u':PULS'+unicode(n)+':POL '+unicode(polarity))
        time.sleep(ts)
        
    def WriteLevel(self,n, voltage):
        self.PG.write(u':PULS'+unicode(n)+':AMP '+unicode(voltage))
        time.sleep(ts)
        
        ## All times in seconds ##
    def WriteDelay(self,n,delay):
        self.PG.write(u':PULS'+unicode(n)+':DELAY '+unicode(delay))
        time.sleep(ts)
        
    def WriteWidth(self,n,width):
        self.PG.write(u':PULS'+unicode(n)+':WIDT '+unicode(width))
        time.sleep(ts)

    def WriteChanProp(self, n, delay, width, polarity='NORM'):
        self.WriteDelay(n,delay)
        time.sleep(ts)
        self.WriteWidth(n,width)
        time.sleep(ts)
        self.WritePolarity(n,polarity)
        time.sleep(ts)
        self.WriteEnable(n)
        time.sleep(ts)
        
        
    
if __name__=='__main__' : 
    a=PulseGen()        
    test= a.PG.ask('*IDN?')
    print test    