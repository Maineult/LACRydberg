# -*- coding: utf-8 -*-
"""
Created on Thu Dec 17 10:02:20 2015

@author: wmaineult
"""

'''
Implementation of requiered components ofconstants ,
can be removed once pyfirmata works with Firmata 2.5 (and probably higher versions)
'''



SERIAL_MESSAGE = 0x60             #SERIAL_MESSAGE == SERIAL_DATA
SERIAL_CONFIG  = '0x10'             #Message to configure Serial Communication

SERIAL_WRITE   = '0x20'
SERIAL_READ    = '0x30'
SERIAL_REPLY   = '0x40'
SERIAL_CLOSE   = 0x50

#HW_SERIAL0 ='0x00'                 #Hardware Serial port 0 of Duino, normally do not use   
HW_SERIAL1 ='0x01'                  #Serial 1 of Duino  
HW_SERIAL2 ='0x02'                  #Serial 2 of Duino 
HW_SERIAL3 ='0x03'                  #Serial 3 of Duino 
SW_SERIAL0 ='0x08'                  #Hardware Serial port 0 of Duino, normally do not use   
SW_SERIAL1 ='0x09'                  #Serial 1 of Duino  
SW_SERIAL2 ='0x0A'                  #Serial 2 of Duino 
SW_SERIAL3 ='0x0B'                  #Serial 3 of Duino 

SERIAL_READ_CONTINUOUSLY = '0x00'   #Initiate reading on a serial port
SERIAL_STOP_READING = '0x01'        #Stop Reading
#define SERIAL_MODE_MASK            0xF0



HISPELLMAN = '0x02'                 #START CHARACTER OF MESSAGE TO SPELLMAN
ALL = '0x30'
PHOS =  '0x31'                 # Adress of Phosphor Screen HV Alim
MCP =  '0x32'                       # Adress of MCP Screen HV Alim
ION =  '0x33'                # Adress of Ionisation Screen HV Alim
PHOSTYPE = '0x33'
MCPTYPE = '0x33'
IONTYPE = '0x35'
BYSPELLMAN = '0x0A'                 # STOP CHARACTER OF MESSAGE TO SPELLMAN

#Simple Copy of commands

START_SYSEX = 0xF0          # start a MIDI SysEx msg
END_SYSEX = 0xF7            # end a MIDI SysEx msg

 