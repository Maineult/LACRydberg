# -*- coding: utf-8 -*-
"""
Created on Fri Feb 19 11:27:16 2016
Model of MVC of experiment control program.
@author: Wilfried
"""
'''
Stores experimental parameters and provides way to modify them
Thoses parameters will be the one passed to the different instruments to
make the experiment work

To add Instruments : 
Add them in the ExperimentParams._InstrList
Add default package name, inst class name, and Initkwargs in ExperimentConstants variable InstrumentDict 
Add in ExperimentParams a dictionnary named ExperimentParams._InstrList[Instr]
If Timing Instrument, add it in TimingInstument List
If timing Instrument, add a Dict entry in ExperimentParams.Instr named config, where you store the attributes of the differents
edges. Name should be like NameStart or NameStop see in SRSDelayGen the examples.
'''


import socket
from ExperimentConstants import *
from GeneralConstants import *
import numpy as np

class ExperimentParams():
    #general Model of the MVC structure for the setting the experimental parameters
    
    def __init__(self,fileParams=None):
    # if a file is passed as parameter, uses it to initialize the experiment parameters
    # and restore a previous status.
        #self.__name__='ExpParams'        
        Computer = socket.gethostname()
        self.Computer = Computer
        self.ExpHandler = getattr(self,ComputerDict[Computer])
        self.manipe = ComputerDict[Computer]
        self.ExpHandler()
        
        #self.BuildTimingTable()
        for Instr in self._InstrList.iterkeys() :
            try : 
                ( p, m,Initkwargs ) = InstrumentDict[Instr]
                dic =getattr(self,Instr)
                dic['InitKwargs']=Initkwargs
            except Exception as e:
                NewLog(e,'{} Not defined in ExperimentParams class or in ExperimentConstants module'.format(Instr),2)
                

                        
    def __str__(self):
        Message = ComputerDict[socket.gethostname()] + ' Experiment \n'
        Message += '===== Durations ===================\n '
        Message += str(self._Durations) + '\n'
        Message += '======Instruments Used ============\n'
        Message += str(self._InstrList)
        return Message
        
    def __getstate__(self):
        odict = self.__dict__.copy()
        odict.pop('ExpHandler')
        return odict
       
    def __setstate__(self, dict):
        Computer = socket.gethostname()
        exp=  getattr(self,ComputerDict[Computer])
        self.__dict__.update(dict)
        self.ExpHandler = exp        
    
    def Ytterbium(self):
        self._InstrList = ({'Lambdameter':NotUsed,
                          'MicroWaveGen':NotUsed,
                          'Scope':NotUsed,
                          'Solstis':NotUsed,
                          'BNCDelayGen':NotUsed,
                          'FieldSynth':NotUsed,
                          'InternalMethods':NotUsed,
                          'CameraThor':NotUsed,
                          'DacBoard':NotUsed,
                          'ElectricFields':NotUsed,
                          'LambdameterIon':NotUsed
                          })
                        
        self.TimingInstr = ['BNCDelayGen']
    
        self.TrigInstr=['Scope','CameraThor']  ### Ranked by priority order
        
       # self.BertanAlim= ({})
        
        self.DacBoard=({})
        
        self.ElectricFields=({})
        
        self.BNCDelayGen = ({'Config':TimingsBNC,
                             'MultiPulse':False,
                             'Channels' : ListChannelsBNC,
                             'AllChannels':[(1, 2e-6, 1e-6),
                                            (2, 2e-6, 1e-6),
                                            (3, 2e-9, 0),
                                            (4, 2e-9, 0),
                                            (5, 2e-9, 0),
                                            (6, 15e-6, 4e-6),
                                            (7, 2e-9, 0),                                           
                                            (8,2e-9,0.)]
                            })
                         
        self.Rydberg = ({'Start':'RydbergStart',
                         'Stop':'RydbergStop',
                         'Laser':'Solstis',
                              })
                              
        self.Scope=   ({ 'Data0':np.array([])})
                
        
        self.Lambdameter = ({})
        
        self.LambdameterIon = ({})

        
        self.MicroWaveGen = ({ 'Multiplier':6,
                              })        
        
        self.InstrToSet = []        
    
        
        self.Camera=({}) 
        self.CameraThor=({})  
        self.ReverseTimingConfig()
#        self.Times = self.CalcTimes()
#        self.TimeTable = self.BuildTimingTable()
        self.TimeTable = InitTimeTable['Ytterbium'] 
        self.BuildTimingCommands(self.TimeTable)
        self.Times = self.CalcTimes()   
        
        
        
        
    def Cesium(self):
        self._InstrList = ({'SRSDelayGen':NotUsed,
                          'Arduino':NotUsed,
                          'Matisse':NotUsed,
                          'Lambdameter':NotUsed,
                          'QCW':NotUsed,
                          'Microwave':NotUsed,
                          'Scope':NotUsed,
                          'ForsterAlim':NotUsed,
                          'Ion':NotUsed,
                          'RemoteLambda':NotUsed
                          })
                
        '''                
            Sets also the SortPriority of the different TimingInstuments to set
            up the different times. The delays gens shoud stay firsts in the list, probably.
            A timing Instrument must have a Channel key, describing all the channels
            a All Channel, listing the commands passed to individual channels using the appropriate syntax for the subprogram
            See SRSDelyaGen for example.
        '''
        self.TimingInstr = ['SRSDelayGen']
        
        self.Matisse =   ({'DFreqDPZT':-30,
                          'DPZTMaxScan':200*1./5e-3})
        
        self.QCW =       ({     })
        
        self.Arduino =  ({})
        
        self.Microwave = ({
                           'Frequency': 50*GHz,
                           'Power':10*dBm,
                           })
                           
                 
                           
        self.Ion = ({   'Field':0.9*kV,
                            'Phosphorus':2.0*kV,
                            'MCP':1.5*kV,
                            })
         
        self.SRSDelayGen = ({'Config':TimingsSRS,
                             'MultiPulse':False,
                             'Channels' : ['s7','Rydberg','Microwave','Ion'],
                             'AllChannels':[(4, -2.9e-07),
                                            (5, 4.9e-07),
                                            (0, 9.99e-06),
                                            (4, 5.2e-07),
                                            (5, -3.5e-08),
                                            (8, 2.5e-08),
                                            (5, 9.9e-07),
                                            (8, 3.6e-05)]
                             })
                             
        
                   
        self.ForsterAlim = ({ 
                        'VoltField' : 0*V,
                        'Resolution':2**13
                        })
                                
#        self.LockLaser = ({'Status':Off,
#                           'SetLambda':785*nm,
#                           'ReactTime':1*s,
#                           'ReactOn' : 'Rydberg',
#                           'ReactOnParam' : 'FineTuningPiezo'                               
#                            })
                             
        self.Scope=   ({ 'Data0':np.array([])})
        
        self.RemoteLambda = ({})
        
        self.Lambdameter = ({})
        
        self.InstrToSet = ['ForsterAlim','Ion','Microwave']
        
        self.ReverseTimingConfig()
#        self.Times = self.CalcTimes()
#        self.TimeTable = self.BuildTimingTable()
        self.TimeTable = InitTimeTable['Cesium']
        self.BuildTimingCommands(self.TimeTable)
        self.Times = self.CalcTimes()
        

    def ReverseTimingConfig(self):
        for Instr in self.TimingInstr : 
            In = getattr(self,Instr)
            ChannelDict= {}    
            for key,val in In['Config'].iteritems():
                ChannelDict[val[0]]= (key,val[1])    
            In['Num2Chan'] = ChannelDict
            if self.TimingInstr == ['SRSDelayGen']:
                for Chan in self.SRSDelayGen['Channels']:
                    self.SRSDelayGen[Chan] = [None,None]
            elif self.TimingInstr == ['BNCDelayGen']:
                for Chan in self.BNCDelayGen['Channels']:
                    self.BNCDelayGen[Chan] = [None,None]   
            
            
    def CalcTimes(self) : 
        '''        
            takes as input TimingInstr.AllChannels (command listand Timing        
        
        '''
        def sort2(tup):            
            return tup[1]
        for Instr in self.TimingInstr : 
            
            if Instr == 'BNCDelayGen':
                dic=self.BNCDelayGen
                ttimes = [('ExperimentStart',0)]
                config = []

                #Converts channel timesteps into channel name of the step
                for i in np.arange(np.size(dic['Channels'])):
                    name=dic['Channels'][i]+'Start'
                    ref='ExperimentStart'
                    time=dic['AllChannels'][i][1]
                    ttimes.append(tuple([name,time]))
                    name=dic['Channels'][i]+'Stop'
                    ref='ExperimentStart'
                    time=dic['AllChannels'][i][1]+dic['AllChannels'][i][2]
                    ttimes.append(tuple([name,time]))
                ttimes.append(tuple(['ExperimentStop',0.1]))
       
            if Instr == 'SRSDelayGen':            
                dic = self.SRSDelayGen
                ttimes = [('ExperimentStart',0)]
                config = []
                #Converts channel timesteps into channel name of the step
                for i,(n,time) in enumerate(dic['AllChannels']):
                    name = dic['Num2Chan'][i+2][0]
                    ref = dic['Num2Chan'][n][0]
                    config.append([name,ref,time])
                i = 0
                while len(config):
#                    print len(config), '********,i=',i,' **** \n',config, '\n', ttimes
                    t = self.FindNextEntry(config,ttimes[i][0])
                    # sort in ascending order the different timings
                    if t+1 : 
                        name,ref, dt0 = config.pop(t)
                        dt = dt0 + dic['Config'][ref][1]-dic['Config'][name][1]
                        ttimes.append(tuple([name,ttimes[i][1]+dt]))
                    else : 
                        i+=1
                ttimes.append(tuple(['ExperimentStop',0.1]))             
        
        times = sorted(ttimes,key=sort2)
        return times
                
    
    def FindNextEntry(self,config,n):
        for i,entry in enumerate(config) : 
            if entry[1] == n: 
                return i
        return -1
                                  
    def FindChannel(self,List,edge):
        for i,elem in enumerate(List):
            if elem in edge : 
                return i
    
    def BuildTimingTable(self, cpt = None):
        if cpt is None : 
            cpt = self.Times
        name,t0 = cpt[0]
        Times = []
        TTags = [[name]]
        for name,t1 in cpt : 
            if t1 == t0:
                TTags[-1].append(name)
            else : 
                TTags.append([name])
                Times.append(t1-t0)
                t0 = t1        
        TimeTable={'Times':Times,'TimeTags':TTags}               
        for Instr in self.TimingInstr : 
            obj = getattr(self,Instr)
            temp =map(Zero,range(len(Times)))            
            for Chan in obj['Channels']:
                temp = map(Zero,range(len(Times)))
                if Instr in ['SRSDelayGen','BNCDelayGen']:
                    on= [Chan+'Start',Chan+'Stop']                    
                    i = FindIndex(TTags,on[0])
                    j = FindIndex(TTags,on[1])                    
                    for k in range(i,j):
                        temp[k] = 1
                        
                TimeTable[Instr+Chan] = temp       
        return TimeTable
        
                     

                

    def BuildTimingCommands(self,TimeTable):
        def InstrSort(tup):
            chn,tab = tup
            j = self.FindChannel(self.TimingInstr,chn)
            return j       
        dic =TimeTable.copy()       
        Times = dic.pop('Times')
        Tags = dic.pop('TimeTags')
        prevstate = range(-1,len(Tags)-1)
        obj = {}
        [obj.__setitem__(name, getattr(self,name)) for name in self.TimingInstr]

        for i,name in enumerate(Tags[:-1]):     
            newref = i-1
            for chname,tab in sorted(dic.items(),key=InstrSort):
                j = self.FindChannel(self.TimingInstr,chname)
                Instr = self.TimingInstr[j]
                ch = chname.replace(Instr,'')
                if Instr =='BNCDelayGen' and not i:
                    RealChInd=obj[Instr]['Channels'].index(ch)
                    l=0
                    try:
                        while tab[l]==0:
                            l=l+1
                        Delay = sum(Times[:l])
                        l0=l
                        while tab[l]==1:
                            l=l+1
                        Width= sum(Times[l0:l])
                    except IndexError:
                        Delay=0.
                        Width=0.   
                    Delay=Delay+TimingsBNC[obj[Instr]['Channels'][RealChInd]+'Start'][1]+TimingsBNC['ExperimentStart'][1]
                    obj[Instr]['AllChannels'][RealChInd] = (RealChInd+1, Delay, Width)
                    
  
                elif Instr== 'SRSDelayGen':
                    if (not newref-i+1) and (not tab[i] is tab[prevstate[i]]) : 
                        newref = i
                        ref = i-1
                    else :
                        ref = i
                    if tab[i] > tab[prevstate[i]]:
                        suffix = 'Start'
                        change = 1
                    elif tab[i] < tab[prevstate[i]]:
                        suffix = 'Stop'
                        change = 1
                    else : 
                        change = 0
                        suffix = 'None'
                    #print i, name, chname, tab, ref, newref, suffix
                    if change : 
                        ch0 = obj[Instr]['Config'][ch+suffix][0]-2
                        try :      
                            refnum = obj[Instr]['Config'][Tags[ref]][0]
                            if ch0 +2 == refnum : 
                                # to avoid self refrences of channels, that would be perfectly stupid
                                ref = ref-1
                                refnum = obj[Instr]['Config'][Tags[ref]][0]
                            dlay = ((i-ref)*Times[i-1] +obj[Instr]['Config'][ch+suffix][1] 
                                                - obj[Instr]['Config'][Tags[ref]][1])
                        
                        except AttributeError :
                            refnum = obj[Instr]['Config'][Tags[0]]
                            dlay = sum(Times[:i+1]) +obj[Instr]['Config'][ch+suffix][1]
                        except IndexError : 
                            refnum = 'ExperimentStart'
                            dlay = (obj[Instr]['Config'][ch+suffix][1])
                        NewLog(None,'SRSEdge Number {} ------ ref = {}, dlay = {:.3g}µs'.format(ch0,refnum,dlay*1e6),2)
                        obj[Instr]['AllChannels'][ch0] = (refnum, dlay)               

import numpy as np

class Traces():
    def TestResults(self,avg,avgcurrent,pause):

        if avg>=avgcurrent : 
            self.NeedsAveraging  =1
        else :
            self.NeedsAveraging  =0
        return not pause
        
    def TestResults_evolved(self):
        from time import sleep
        self.densitytolerance = 0.1
        if not self.Calib : 
            re = self.ScopeModel['VoltScale0']
            self.Calib = self._Calib[str(re)+'V']
        if self.currentDensity : 
            Signal = self.AnalyseData()
            self.IntegratedSignal.append(Signal)
            self.IntegratedSignal.remove(0)
            test = 0
            ([test.__add__(1) for e in self.IntegratedSignal if 
                (e-self.currentDensity)/self.currentDensity < self.Densitytol])
            if test<6 : 
                estim = self.EstimateDensity()
                c = self._tp['Rydberg'][2]   
                self.Exp.action.append(('Timings','Rydberg','ExperimentStart',10*us,c*estim/self.currentDensity))
            if (Signal-self.currentDensity)/self.currentDensity < self.Densitytol:
                return True
            else:
                return False
        if self.avgcurrent<10 : 
            if not self.Tbaseline : 
                self.Tbaseline = self.BaseLineDetection()
            Signal = self.AnalyseData()
            self.IntegratedSignal.append(Signal)
            self.IntegratedSignal.remove(0)
            return True
        if self.avgcurrent==10 : 
            self.ExpResults.addResults({'Params':curparams[:],'Model':self.Exp.ExpModel,
                                                    'Trace':self.ScopeModel['Data0'][:],
                                                    'Frequency' : self.Lambdameter['Frequency']})
            estim = self.EstimateDensity()
            test = 0
            ([test.__add__(1) for e in self.IntegratedSignal if 
                (e-estim) < self.Densitytol*estim])
            if test == 10 : 
                return False
            else : 
                self.avgcurrent = test
                lasts = self.ExpResults.FindLatest(curparams)
                self.IntegratedSignal = []
                t = []
                for data in lasts : 
                    test = self.AnalyseData(data[1])
                    if (test-estim) < self.Densitytol*estim : 
                        t.append(data)
                        self.IntegratedSignal.append(test)
                lasts = t 
                return False
                
                        
    
    def EstimateDensity(self):
        a = self.IntegratedSignal[:]
        a.sort()
        res = max([(len([e-f for j,e in enumerate(a) 
            if (abs(e-f)<2*self.Densitytol*self.currentDensity and i>j)]),i)
                for i,f in enumerate(a) ])  #finds the index of the sorted list 
                # where the lasgest number of previous shots hitted the specified tolerance
                #and returns also the number of such items
        
        if max(res)[0] :
            a = a[max(res)[1]-max(res)[0]:max(res)[1]+1]
            inte = 0
            [inte.__add__(e) for e in a]
            return inte/len(a)
        else :
            print ('tolerance is probably to small, or signal fluctuates too much compared',
                   ' to noise, returning the median of the list')
            return a[5]


    def BaseLineDetection(self,data):
        x,y = data
        dt = (x[-1]-x[0])/50.
        t = x[0]+dt
        yprim = np.extract(x<t ,y)
        while not (self.IsDetectableSignal(yprim)):
            t = t+dt
            yprim = np.extract(x<t ,y)
        self.Tbaseline =  t-dt
            
    def IsDetectableSignal(self,y0):
        '''
            Set with small signal : shoud detect an excess of points out of gaussian distribution
            Set with large signal : Check if it works also (it should)
            nsigma parameters describes the senstibity of the algorithm
        '''
        
        nsigmas = 5
        yres = [y0.mean(),y0.std(),y0.ptp()]
        n = len(y0)
        p1 =  len(np.extract((y0-yres[0])>yres[1] ,y))*1./n
        pm1 = len(np.extract((y0-yres[0])<-yres[1] ,y))*1./n
        p2 = len(np.extract((y0-yres[0])>2*yres[1] ,y))*1./n
        pm2 = len(np.extract((y0-yres[0])<-2*yres[1] ,y))*1./n
        p3 = len(np.extract((y0-yres[0])>3*yres[1] ,y))*1./n
        pm3 = len(np.extract((y0-yres[0])<-3*yres[1] ,y))*1./n
        p4 = len(np.extract((y0-yres[0])>4*yres[1] ,y))*1./n
        pm4 = len(np.extract((y0-yres[0])<-4*yres[1] ,y))*1./n                
        currentValue = [p1,pm1,p2,pm2,p3,pm3,p4,pm4]
        titles = ['y>sigma','y<-sigma','y>2*sigma','y<-2sigma','y>3*sigma','y<-3*sigma','y>4*sigma','y<-4*sigma']    
        maxValues = [self.Calib[elem][0]+max(5*self.Calib[elem][1],self.Calib[elem][2]) for elem in titles]
        minValues = [self.Calib[elem][0]-max(5*self.Calib[elem][1],self.Calib[elem][2]) for elem in titles] 
        Test = 0 
        [Test.__add__(m<c).__add__(c>M) for m,M,c in zip(maxValues,minValues,currentValue)]
        return Test
            
                
        
    def CheckTrace(self,a) :        
        #Removes Nan from the scope         
        b=np.isnan(a)
        if np.sum(b):
            #print 'NaN detected in the data, will be replaced by 0.6 V.\n'
            a = np.invert(b)*np.nan_to_num(a)+ b*0.599999
        return a
    

    def AnalyseData(self,**kwargs):
        """
        Analyse MCP signals coming from data (just the y of the scope)
        From 0 to Baseline (in numpoints units) in calculates average and 
        the actual noise level
        Then substracts offsets, removes Nan in the traces, and integrates
        over the points where the signal is non zero        
        """
        try : 
            x,y = kwargs['data']    
        except : 
            x,y = self.ScopeModel['Data0']
        SampleRate = np.diff(x).mean()

        Base = np.extract(self.Tbaseline>x,y)
        MeanPoints = Base[:].mean()
        LocalInfiniteSigma = Base.min(axis = 0)-MeanPoints
        trace = np.extract(self.Tbaseline<x,y)-MeanPoints  #substrats slowy varying offsets.
        trace = self.CheckTrace(trace)   # treats eventuals NaN
        Signal = np.extract(np.abs(trace)>2*LocalInfiniteSigma , trace).sum()/SampleRate
        if not Signal : 
            self.NeedsAveraging =1
        return Signal

class ExperimentResults(Traces):

    def __init__(self,fileResults=None , PPP = 100):
        self.Average = PPP  #PointsPerSamples
        self.Resultats = {}  #Experimental Results
        self.Refs = {} #Experimental References
        self.Params = {}   #Vayring Parameters
        self.Rec ={}
        self.temp = []
        self.CommandWaitForTrig=['PiezoVoltage']
        self.ModifiesRydbergFreq = ['']
        self.fileResults = fileResults
        self.i=0
        
    def ClearCurrent(self):
        self.temp = []
    
    def addTemp(self,Datas):
        self.temp.append(Datas)
        
    def NewPoint(self,curparams):
#        print self.temp
        self.Resultats[str(curparams)] = self.temp[:]
        self.temp = []
    
            
        
if __name__=="__main__":
    a = ExperimentParams()
    b=ExperimentResults()
    if not gh() =='lac57-95':
        a.TimeTable = ({'Times':[10*us,0.5*us,1*us,36*us,1000*us],
                        'TimeTags':['ExperimentStart','RydbergStart','RydbergStop','IonStart','IonStop','ExperimementStop'],
                        'SRSDelayGens7':[0,1,1,0,0],
                        'SRSDelayGenRydberg':[0,1,0,0,0],
                        'SRSDelayGenMicrowave':[0,0,1,0,0],
                        'SRSDelayGenIon':[0,0,0,1,0]
                       })
        a.BuildTimingCommands(a.TimeTable)
        toto = [(4, -3e-07),
                (5, 5e-07),
                (0, 10e-06),
                (4, 5.1e-07),
                (5, -2.5e-08),
                (6, 2e-06), 
                (7, 2.5e-08),   
                (8, 3.61e-05)]
    else : 
        pass