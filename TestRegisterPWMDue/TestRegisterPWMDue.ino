#include <Arduino.h>
/*
Trying to control some bit registers of the sam3x chip of arduino due.
In order to make some PWM. I think there are several ways to do it.
I need to know how to read/write those registers first and test stuff.
I read the doc on the Sam3x8e : I understood some stuff.
First You have to read the block diagram of the chip to find the location of the function you're
looking at.
You have for instance the NVIC that is used to have some programmable intterupts. in the processor
You have the system clock( PMC) in the SYstem controller, as well as the Programmable I/O ports A -- E
PWM can be done with Time Counters (software control needed ?) or with the PWM module (no software control
All Hardware ?)
The signals are switches using the PIO stuff -> need to find one that is not mandatory for the system to work correctly
To find the correct pinout, you must start by looking at the Peripherical Signal multiplexing of the doc, and then at the 
SAMX38E Pinout.

The functions of the arduino library for due can be found in (Windows Computers)
/Document and Settings/Users/ApplicationData/Arduinoxx/packages/arduino/hardwarre/sam/x.x.x/system/cmsis/device/ATMEL/... and
subdirectories systems from there. Should help to understand some already programmed stuff, because I did not find
any documentation for Due elsewhere. Have to hack the source code !!!
In another computer, it was under
/Document and Settings/Users/Local Settings/ApplicationData/Arduinoxx/packages/arduino/hardwarre/sam/x.x.x/system/cmsis/device/ATMEL/...

For the moment, I found in .../Amtel/sam32xa/include the headers defining the IRQn peripherical adresses, 
                                                     the Handler defs for various components/peripherical of the Sam3x device
                                                     #include for API of different components of the Sam3xe8
                                                     #include for register access definitions
                                                     definition of IDs of differnts components/peripherical
                                                     definition of components/pêrif/memory adresses, ...

In /Document and Settings/Users/ApplicationData/Arduinoxx/packages/arduino/hardwarre/sam/x.x.x/variants, the variant.cpp
shows some of the funtionnalities that can be enables

                            
/**/


#include <Arduino.h>
 

uint32_t maxDutyCount = 256;
uint32_t clkAFreq = 42000000ul;
uint32_t pwmFreq = 42000000ul; 
uint32_t DutyCycle = 200;


uint32_t pwmPin =33; //35
Pio* pPort = g_APinDescription[pwmPin].pPort;
EPioType pPioType= PIO_PERIPH_B;
uint32_t pPin = PIO_PC2B_PWML0; // 35 : PIO_PC5B_PWMH1;
uint32_t pPWM = PWM_CH0 ; //PWM_CH1;
uint32_t pPinConf = g_APinDescription[pwmPin].ulPinConfiguration;
/**/

uint32_t pwmPin2 =8;
Pio* pPort2 =  g_APinDescription[pwmPin2].pPort;
EPioType pPioType2=  g_APinDescription[pwmPin2].ulPinType;
uint32_t pPin2 = g_APinDescription[pwmPin2].ulPin;
uint32_t pPinConf2 = g_APinDescription[pwmPin2].ulPinConfiguration;
uint32_t pPWM2 = g_APinDescription[pwmPin2].ulPWMChannel;
/**/
RwReg Registre;

void setup() {
  Serial.begin(57600);
  while (!Serial) {
    ; // wait for serial port to connect. Only needed for ATmega32u4-based boards (Leonardo, etc).
  }
  pmc_enable_periph_clk(PWM_INTERFACE_ID);
  PWMC_ConfigureClocks(clkAFreq, 0, VARIANT_MCK);
//  pinMode(pwmPin, OUTPUT);
  PIO_Configure(
     pPort,
     pPioType,
     pPin,
     pPinConf);
   PIO_Configure(
     pPort2,
     pPioType2,
     pPin2,
     pPinConf2);
  
  uint32_t channel = pPWM;
  PWMC_ConfigureChannel(PWM_INTERFACE, channel , pwmFreq, 0, 0);
  PWMC_SetPeriod(PWM_INTERFACE, channel, maxDutyCount);
  PWMC_EnableChannel(PWM_INTERFACE, channel);
  PWMC_SetDutyCycle(PWM_INTERFACE, channel, DutyCycle);

  uint32_t channel2 = pPWM2;
  PWMC_ConfigureChannel(PWM_INTERFACE, channel2 , pwmFreq, 0, 0);
  PWMC_SetPeriod(PWM_INTERFACE, channel2, maxDutyCount);
  PWMC_EnableChannel(PWM_INTERFACE, channel2);
  PWMC_SetDutyCycle(PWM_INTERFACE, channel2, DutyCycle);
  
 /*pmc_mck_set_prescaler(2);*/

}
 
void loop() 
{
  uint32_t channel = pPWM2;/*
  Serial.println("PWM Channel Configuration");
  Registre = PWM_INTERFACE-> PWM_CH_NUM[channel].PWM_CPRD;
  Serial.println(Registre);
  Registre = PWM_INTERFACE-> PWM_CH_NUM[channel].PWM_CDTY;
  Serial.println(Registre);
  Registre = PWM_INTERFACE-> PWM_CH_NUM[channel].PWM_DT;
  Serial.println(Registre);
  Registre = PWM_INTERFACE-> PWM_CH_NUM[channel].PWM_CCNT;
  Serial.println(Registre);
  Registre = PWM_INTERFACE-> PWM_CH_NUM[channel].PWM_CMR;
  Serial.println(Registre);
  
  Serial.println("PIOC Channel Configuration");
  Registre = PIOC->PIO_PSR;
  Serial.println(Registre,HEX);
  Registre = PIOC->PIO_OSR;
  Serial.println(Registre,HEX);
  Registre = PIOC->PIO_PDSR;
  Serial.println(Registre);
  Registre = PIOC->PIO_OWSR;
  Serial.println(Registre,HEX);*/
  for (int i = 0; i <255;i++){
    PWMC_SetDutyCycle(PWM_INTERFACE, channel, DutyCycle);
    Serial.println(i);
    delay(200);
  }
  
}/**/


