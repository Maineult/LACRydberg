# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 22:02:27 2016

@author: henri
"""

import matplotlib.pyplot as plt
import numpy as np
import cPickle
import ExperimentModel
import os
os.chdir("C:\Users\lac\Documents\Python Scripts\ExperimentControl\YbSpectroData")
import DataAnalysisYb as an
from UserDefFunctions import *
from matplotlib import rcParams
rcParams['font.family']='serif'



##### INPUTs ######


        
DataRep ="C:\\Users\\lac\\Documents\\RydbergYtterbiumData\\2017\\02 - 14 (Tuesday)"       

filename='MW_64-1S0_1P1_VNS=+3.15.ExpResults'
 
Analysis=0
FitForm='DoubleGaussian'
X1=79.9912
X2=79.9929
X3=67.62177


AnalysedDataRep="C:\Users\lac\Desktop"




##### FUNKtions


def AverageTraces(Data):
    TracesList=[]
    for i in range(len(Data)):
        print type(Data)
        TracesList.insert(i,np.array(Data[i]['Data0'][1]))
    AvTraces=np.average(np.array(TracesList))
    
    return AvTraces   
    
def Sortbyx(Data):
    Sortx=sorted(Data[0])
    for j in range(len(Data)) :
        if j>0:
            CorrespSig=[]
            for i,x in enumerate(Sortx):
                CorrespSig.insert(i,Data[j][Data[0].index(x)])
            Data[j]=CorrespSig
    Data[0]=Sortx    
    return Data     


    
def LandeFactor(J,L,S):
    J=float(J)
    L=float(L)
    S=float(S)
    return float(1+ (J*(J+1)-L*(L+1)+S*(S+1))/(2*J*(J+1)))
    
        

###### MAIN #######

os.chdir(DataRep)
name=filename.split('.')[0]
InitialState=name.split('_')[1]
ResultsFile=open(filename,'r')
Results=cPickle.load(ResultsFile)
Freq=[]
Signal=[]
StdDevSignal=[]
RydSignal=[]
for i,keyParam in enumerate(Results.Resultats.iterkeys()):  
    Data=Results.Resultats[keyParam]
    AverageNumber=len(Data)
    a=keyParam.split('{')[1].split('}')[0]
    Freq.insert(i,float(a.split(': ')[1]))
    S=[]
    Sryd=[]
    for j in range(AverageNumber):
        S.insert(j,(Data[j]['Custom2']/Data[j]['Custom1']))
        Sryd.insert(j,Data[j]['Custom1'])
    RydSignal.insert(i,1e9*np.mean(Sryd))    
    
    Signal.insert(i,np.mean(S))
    StdDevSignal.insert(j,np.std(S))    


Data=[Freq,Signal,StdDevSignal]
Data=an.SortByX(Data)
#plt.errorbar(Data[0],Data[1],yerr=Data[2],linestyle='None',marker='o')
plt.plot(Data[0],Data[1],'-o')
Legend=[]



if Analysis:
    if FitForm=='DoubleGaussian':
        
        fit=an.FitDoubleGaussian(Data[1],Data[0],A1=0.15,A2=0.25,X1=X1,X2=X2,sigma=1e-3, offset=0,plot=False)
        plt.cla()
        plt.plot(Data[0],Data[1],'-o',color='slateblue',alpha=0.8,label='Experimental')
        plt.plot()
        XX=np.linspace(np.min(Data[0]),np.max(Data[0]),1000)
        R=fit.best_values
        plt.plot(XX,an.DoubleGaussian(XX,R['A1'],R['A2'],R['X1'],R['X2'],R['sigma'],R['offset']),color='mediumblue',label='Fit',linewidth=2)  
        R['Model']=FitForm
        print 'Splitting =', R['X2']-R['X1'],'GHz'
        plt.legend()
        setattr(Results,'AnalysedResults',R)
        
    if FitForm=='TripleGaussian':
        fit=an.FitTripleGaussian(Data[1],Data[0],A1=0.25,A2=0.15,A3=0.15,X1=X1,X2=X2,X3=X3,sigma=5e-4, offset=0,plot=False)
        plt.cla()
        plt.plot(Data[0],Data[1],'-o',color='darkred',alpha=0.5,label='Experimental')
        plt.plot()
        XX=np.linspace(np.min(Data[0]),np.max(Data[0]),1000)
        R=fit.best_values
        plt.plot(XX,an.TripleGaussian(XX,R['A1'],R['A2'],R['A3'],R['X1'],R['X2'],R['X3'],R['sigma'],R['offset']),color='darkred',label='Fit',linewidth=2)  
        R['Model']=FitForm
        print 'Splitting =', R['X3']-R['X1'],'GHz'
        plt.legend()
        setattr(Results,'AnalysedResults',R)

locs,labels = plt.xticks()
plt.xticks(locs, map(lambda x: "%10g" % x, locs))
plt.xlabel('Frequency (GHz)')
plt.ylabel('Population Transfer')
ax = plt.gca()
ax.format_coord = lambda x,y: '%10f, %10f' % (x,y)

plt.grid()

def SaveData(Name=name):
    os.chdir(AnalysedDataRep)
    np.savetxt(Name+'.ExpData',Data)
  #  File.close()
    
   
def plotTof():
    y=np.zeros(1000)
    x=1e6*np.array(Results.Resultats["[[u'MicroWaveGen', u'Frequency', {u'Freq': 77.077}]]"][0]['Data0'][0])
    
    y=-np.array(Results.Resultats["[[u'MicroWaveGen', u'Frequency', {u'Freq': 77.077}]]"][0]['Data0'][1])
    mask=np.isfinite(y)
    y=y[mask]  
    x=x[mask]
    plt.xlabel(r'Temps de vol ($\mu s$)',fontsize=14)
    plt.ylabel(r'Signal M.C.P. (a.u.)',fontsize=14)
    plt.vlines([12.7,12.85],-1,5,colors='darkred',linestyles='dashed',lw=2)
    plt.vlines([12.4,13.2],-1,5,colors='darkgoldenrod',linestyles='dashed',lw=2)
    plt.plot(x,y,color='navy',lw=2)
    