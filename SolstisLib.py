# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 11:20:34 2016

@author: lac
"""
import socket
import json
import time 


class Solstis:
    def __init__(self):
    #    print 'init Solstis'
        self.TCP_IP = '129.175.56.27'
        self.TCP_PORT = 39933 
        self.BUFFER_SIZE = 1024
        self.message = '{"message":{"transmission_id":[0],"op":"start_link","parameters":{"ip_address":"129.175.57.96"}}}'
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.TCP_IP, self.TCP_PORT))
        self.socket.send(self.message)
        Noreturn=self.socket.recv(self.BUFFER_SIZE)
        
        self.delaycommand = 0.05  ### 50 ms between every command, can be shortened ?
        self.t0 = time.time()
    
    def WaitEnough(self):
        if time.time()-self.t0>self.delaycommand :
            self.t0 = time.time()
            return
        else:
            time.sleep(self.delaycommand)
            self.t0 = time.time()
            
    def Relaunch(self):
        time.sleep(0.1)
        self.socket.close()
        time.sleep(0.1)
        self.TCP_IP = '129.175.56.27'
        self.TCP_PORT = 39933 
        self.BUFFER_SIZE = 1024
        self.message = '{"message":{"transmission_id":[0],"op":"start_link","parameters":{"ip_address":"129.175.57.96"}}}'
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.TCP_IP, self.TCP_PORT))
        
        

    def __test__(self): #### Ping has to be done 2 times before working... Don't know why
        self.WaitEnough()    
        self.message = '{"message":{"transmission_id":[0],"parameters":{"text_in":"ABCDEFabcdef"},"op":"ping"}}'
        self.socket.send(self.message)
        a=self.socket.recv(self.BUFFER_SIZE)
        self.WaitEnough()    
        self.message = '{"message":{"transmission_id":[0],"parameters":{"text_in":"ABCDEFabcdef"},"op":"ping"}}'
        self.socket.send(self.message)
        a=self.socket.recv(self.BUFFER_SIZE)
        
        return '{"message":{"transmission_id":[0],"op":"ping_reply","parameters":{"text_out":"abcdefABCDEF"}}}'==a
     
    def TuneWavelength(self, wavelength):  # wavelength in nanometre in range 600-1200.'
        self.WaitEnough()    
        self.message = '{"message":{"transmission_id":[0],"parameters":{"wavelength":['+unicode(wavelength)+'],"report":"finished"},"op":"set_wave_m"}}'
        self.socket.send(self.message)
        print self.socket.recv(self.BUFFER_SIZE)
        
    def GetWavelength(self):
        self.message = '{"message":{"transmission_id":[0],"op":"poll_wave_m"}}'
        self.WaitEnough()    
        self.socket.send(self.message)
        print self.socket.recv(self.BUFFER_SIZE)

    def StopWavelengthTuning(self):
        self.message = '{"message":{"transmission_id":[0],"op":"stop_wave_m"}}'
        self.WaitEnough()    
        self.socket.send(self.message)
        print self.socket.recv(self.BUFFER_SIZE)
    
    
    def LockWavelength(self, valeur):         # 'on' or 'off'
        self.WaitEnough()    
        if valeur =='on':
            self.message = '{"message":{"transmission_id":[0],"op":"lock_wave_m","parameters":{"operation":"on"}}}'
            self.socket.send(self.message)
        elif valeur =='off':
            self.message = '{"message":{"transmission_id":[0],"op":"lock_wave_m","parameters":{"operation":"off"}}}'
            self.socket.send(self.message)
            
    def StartTableTuning(self, wavelength):
        self.message = '{"message":{"transmission_id":[0],"op":"move_wave_t","parameters":{"wavelength":['+unicode(wavelength)+'],"report":"finished"}}}'
        self.WaitEnough()    
        self.socket.send(self.message)
   
    def SystemStatus(self):   
        self.message = '{"message":{"transmission_id":[0],"op":"get_status"}}'
        self.WaitEnough()    
        self.socket.send(self.message)
        return self.socket.recv(self.BUFFER_SIZE)
        
    def ReadResoVoltage(self):
        try : 
            time.sleep(0.1)
            ss= json.loads(self.SystemStatus())
            self.SysStatus=ss
        except ValueError:
            pass
        Message=self.SysStatus.get('message')
        Parameters=Message.get('parameters')
        try : 
            return  Parameters.get('resonator_voltage')[0]
        except Exception :
            from time import sleep
            sleep(0.3)
            return self.ReadResoVoltage()
        
           
    def WriteResoVoltage(self, Voltage):
        Percentage=(Voltage/192.9)*100
        self.WaitEnough()    
        self.message = '{"message":{"transmission_id":[0],"op":"tune_resonator","parameters":{"setting":['+unicode(Percentage)+'],"report":"finished"}}}'
               
        self.socket.send(self.message)
        self.WaitEnough() 
        Noreturn=self.socket.recv(self.BUFFER_SIZE)
        
    def WriteResoPercent(self, Percentage):
        
        self.WaitEnough()    
        self.message = '{"message":{"transmission_id":[0],"op":"tune_resonator","parameters":{"setting":['+unicode(Percentage)+']}}}'
        self.socket.send(self.message)
        self.WaitEnough()
        ret=self.socket.recv(self.BUFFER_SIZE)
        return ret


    def WriteResonatorLock(self,target,mode,current):
        e=target-current
        Gp=2000
        if mode == 'fast':
            if hasattr(self,'ini'):
                delattr(self,'ini')
            if not hasattr(self,'ResoPerCent'):
                Vinit=self.ReadResoVoltage()
                Voltage = Vinit+Gp*e
                self.ResoPerCent=(Voltage/192.9)*100
                Ret=self.WriteResoPercent(self.ResoPerCent)
            else : 
                if  abs(e) < 10e-3 :
                    self.ResoPerCent = self.ResoPerCent + Gp*e/1.929
                    Ret=self.WriteResoPercent(self.ResoPerCent)
                    
                elif abs(e) > 100e-3 and abs(e) < 1e-2:
                    self.ResoPerCent = self.ResoPerCent + 0.1*Gp*e/1.929
                    Ret=self.WriteResoPercent(self.ResoPerCent)
                
                else:
                    print('Locking error: Laser too far from Target')
                    Ret=''
            if not Ret.find('protocol_error')== -1 :
                print('SOLSTIS COMM ERROR')
                
                
            #    self.Relaunch()
                    
        if mode == 'slow':
            if hasattr(self,'ini'):
                dt = time.time() -self.ini[0]
                newfreq = self.ini[1]+ (target - self.ini[1])*dt/self.tscan
                e = newfreq-current
                if not hasattr(self,'ResoPerCent'):
                    Vinit=self.ReadResoVoltage()
                    Voltage = Vinit+Gp*e
                    self.ResoPerCent=(Voltage/192.9)*100
                    self.WriteResoPercent(self.ResoPerCent)
                else : 
                    if abs(e) > 2e-6 and abs(e) < 5e-3 :
                        self.ResoPerCent = self.ResoPerCent + Gp*e/1.929
                        self.WriteResoPercent(self.ResoPerCent)
                    elif abs(e) > 1e-3:
                        print('Locking error: Laser too far from Target')         
            else :
                self.ini = (time.time(),current)
                MHzPerSec = 10e-6
                self.tscan = abs(target - current)/(MHzPerSec)
               
                
                
#     Tune Etalon
    
    def __del__(self):
        self.socket.close()
        
if __name__=='__main__':
    a=Solstis()
    i=379.000000
    while 1==1:
        time.sleep(0.15)
        print a.WriteResonatorLock(i+0.000001,'fast',i)
        i=i+0.000001
        
        