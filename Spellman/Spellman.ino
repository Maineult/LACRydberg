#include <arduino.h>
#include <Firmata.h>
#include "utility/serialUtils.h"



/*==============================================================================
 * GLOBAL VARIABLES
 *============================================================================*/

/* timer variables */
unsigned long currentMillis;        // store the current value from millis()
unsigned long previousMillis;       // for comparison with currentMillis
unsigned int samplingInterval = 19; // how often to run the main loop (in ms)



int serialBytesToRead[SERIAL_READ_ARR_LEN];
int serialIndex = 0;

boolean isResetting = false;


char reply[] =  "0000000000000";
//char message[]  = "003ID?x\n";
char message[]  = "023V1=20.0x\n";
int n = 11;
/*==============================================================================
 * FUNCTIONS
 *============================================================================*/

// Check serial ports that have READ_CONTINUOUS mode set and relay any data
// for each port to the device attached to that port.
void checkSerial()
{
  byte portId, serialData;
  int bytesToRead = 0;
  int numBytesToRead = 0;

  if (serialIndex){
    if (Serial1.available() > 0) {
        Firmata.write(START_SYSEX);
        Firmata.write(SERIAL_MESSAGE);
        Firmata.write(SERIAL_REPLY | portId);
        numBytesToRead = Serial1.available();
        while (numBytesToRead > 0) {
          serialData = Serial1.read();
          Firmata.write(serialData & 0x7F);
          Firmata.write((serialData >> 7) & 0x7F);
          numBytesToRead--;
        }
        Firmata.write(END_SYSEX);
    }
  }
}

/*========================================================
 * SYSEX FUNCTIONS
 *========================================================*/
void sysexCallback(byte command, byte argc, byte *argv)
{
  byte mode;
  byte data;
  int slaveRegister;
  unsigned int delayTime;
  Firmata.sendString("SySex Processing");
  switch (command) {
    case SERIAL_MESSAGE:
      mode = argv[0] & SERIAL_MODE_MASK;
      switch (mode) {
        case SERIAL_CONFIG:
          {
            serial_pins pins;
            pins = getSerialPinNumbers(1);
            /*
            if (pins.rx != 0 && pins.tx != 0) {
              pinMode(pins.rx, INPUT);
              Firmata.write(pins.rx);
              Firmata.write(pins.tx);
            }
            /*((HardwareSerial*)serialPort)->begin(baud);*/
            /*Serial1.begin(9600);
            Firmata.sendString("Initialisé");
            Serial1.write(2);*/
            break; // SERIAL_CONFIG
          }
        case SERIAL_WRITE:
          {
            Serial1.write(message);
            Firmata.sendString("Envoye");
            Firmata.sendString(message);
            break; // SERIAL_WRITE
          }
        case SERIAL_READ: 
        {
          if (argv[1] == SERIAL_READ_CONTINUOUSLY) {
            serialIndex=1;
          } else if (argv[1] == SERIAL_STOP_READING) {
            serialIndex = 0;
          }
          break; // SERIAL_READ
        }
        case SERIAL_CLOSE:
        {
          Serial1.end();
          break; // SERIAL_CLOSE
        }
        case SERIAL_FLUSH:
        {
          Serial1.flush();
          break; // SERIAL_FLUSH
        }
      }
      break;
  }
}

/* ==============================================================
 *  Reset Function
 *=================================================================*/
void systemResetCallback()
{
  isResetting = true;

  isResetting = false;
}



void setup() {
  
  Firmata.setFirmwareVersion(FIRMATA_FIRMWARE_MAJOR_VERSION, FIRMATA_FIRMWARE_MINOR_VERSION);
  Firmata.attach(START_SYSEX, sysexCallback);
  Firmata.attach(SYSTEM_RESET, systemResetCallback);
  
  Firmata.begin(56700);
  while (!Serial) {
    ; 
  }
  Serial1.begin(9600);
  while (!Serial1) {
    ; 
  }  
  
  message[0]=char(2);
  //message[n-1]=char(81);
  //message[n]=char(10);
  message[n-1]=char(87);
  message[n]=char(10);

  
}
/*
void loop() {
  int k = 0;
  // put your main code here, to run repeatedly:
  Serial.println("message");
  Serial.write(message);
  Serial1.write(message);
  Serial.println("Available");
  delay(100);
  Serial.println(Serial1.available());
  while (Serial1.available() > 0){
    reply[k] = Serial1.read();
    k++;
  }
  
  Serial.println("reply");
  Serial.println(reply);
  Serial.println("endreply");
  k=0;
  delay(5000);
}
*/
void loop()
{
  byte pin, analogPin;


  while (Firmata.available())
    Firmata.processInput();

  currentMillis = millis();
  if (currentMillis - previousMillis > samplingInterval) {
    ;
  }

  checkSerial();
}

